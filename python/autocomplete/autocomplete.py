from core.buffer import cut, cpy, ins
from core.search import Search
from core.cur    import Cur
from editor      import log

import os

from typing import Union, Sequence, Optional, List, TYPE_CHECKING

from server.server import add_global

from .cursormenu import open_cursor_menu

if TYPE_CHECKING:
    from core.buffer import Buffer

@add_global()
class AutoCompleter(object):
    def __init__(self, buf: "Buffer"):
        self.buf = buf
        self.menu = None
        self.zerolen = False

    def select_word(self, regex: str=r"[\w\-\./~]+|(?<=\")[\w\-\./~]?(?=\")") -> bytes:
        self.origcur = self.buf.get_cur()

        search = Search()
        search.add_needle(regex, attrs=["REGEX", "BEYONDOFF"])

        startpos = self.origcur.min().copy()
        startpos.col += 1

        search.start(self.buf, startpos, backward=True)

        res = search.get_result()
        if res == None:
            log.str("ALERT", "ERR", "Found no word to autocomplete")
            raise Exception("No word to autocomplete found")

        self.zerolen = res.zero_len()

        self.buf.set_cur(res.get_cur())

        if self.zerolen:
            return ""

        return cpy(self.buf)

    def replace_word(self, word: str):
        cur   = self.buf.get_cur()
        start = cur.min()

        if not self.zerolen:
            cut(self.buf)

        self.buf.set_cur(Cur.at(start))
        ins(self.buf, word)

    def reset(self):
        if self.origcur is not None:
            self.buf.set_cur(self.origcur)

    def open_menu(self):
        word = self.select_word()
        opts = self.get_replacements(word)

        if len(opts) == 0:
            log.str("ALERT", "ERR", f"Found no valid completions for {repr(word)}")
            self.reset()
        else:
            open_cursor_menu(self.buf.editor, opts, self.replace_word, self.reset)

    def get_replacements(self, word: str) -> List[str]:
        raise NotImplementedError

@add_global()
class PathAutoCompleter(AutoCompleter):
    @staticmethod
    def add_slashes(paths: List[str]) -> Sequence[str]:
        for p in paths:
            if os.path.isdir(p) and not p.endswith("/"):
                yield p + "/"
            else:
                yield p

    def get_replacements(self, path: str) -> List[str]:
        opts = set()
        dirname = os.path.dirname(path)
        basename = os.path.basename(path)

        if path == "":
            return list(self.add_slashes(os.listdir(".")))

        try:
            ls = os.listdir(os.path.expanduser(dirname) if dirname else ".")

            for n in ls:
                 if n.startswith(basename) and n != basename:
                    opts.add(os.path.join(dirname, n))
        except OSError:
            pass

        try:
            ls = os.listdir(os.path.expanduser(path))
            for n in ls:
                opts.add(os.path.join(path, n))
        except OSError:
            pass

        res = list(sorted(opts, key=lambda p: (
            os.path.basename(p).startswith("."),
            p
        )))

        if len(opts) >= 2:
            res.insert(0, path)

        return list(self.add_slashes(res))
