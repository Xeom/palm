#if !defined(EVENT_POLL_H)
# define EVENT_POLL_H
# include "types.h"
# include "container/sortedvec.h"
# include "event/event.h"

struct poll_ctx
{
    sortedvec_s finfoptrs;
    pthread_mutex_t mtx;
    event_ctx_s *eventctx;
};

int poll_ctx_init(poll_ctx_s *ctx, event_ctx_s *eventctx);

void poll_ctx_kill(poll_ctx_s *ctx);

int poll_ctx_handle(poll_ctx_s *ctx);

int poll_add(poll_ctx_s *ctx, io_file_info_s *finfo);

void poll_rem(poll_ctx_s *ctx, io_file_info_s *finfo);

int poll_wait(poll_ctx_s *ctx, int toutms);

static inline void poll_event_spec(io_file_info_s *finfo, event_spec_s *spec)
{
    memset(spec, 0, sizeof(event_spec_s));
    spec->typ = EVENT_POLL;
    spec->ptr = finfo;
}

void poll_cb(poll_ctx_s *ctx, io_file_info_s *finfo, event_cb_t cb, void *param);

#endif
