#include "snow.h"
#include "util/escape.h"
#include "container/vec.h"

static void check_vec(vec_s *v, char *exp)
{
    asserteq(vec_len(v), strlen(exp));
    asserteq_buf((char *)vec_get(v, 0), exp, strlen(exp));
}

describe(escape)
{
    vec_s vec;

    before_each()
    {
        vec_init(&vec, sizeof(char));
    }

    after_each()
    {
        vec_kill(&vec);
    }

    it ("Escapes newlines, tabs etc.")
    {
        vec_str(&vec, "1\\a 2\\b 3\\t 4\\n 5\\v 6\\f 7\\r 8\\e");
        escape_vec(&vec);
        check_vec(&vec, "1\a 2\b 3\t 4\n 5\v 6\f 7\r 8\x1b");
    }

    it ("Escapes hex sequences")
    {
        vec_str(&vec, "\\x12\\x34 \\x1234 \\x1");
        escape_vec(&vec);
        check_vec(&vec, "\x12\x34 \x12\x34 \x01");
    }

    it ("Escapes oct sequences")
    {
        vec_str(&vec, "\\1\\23\\156");
        escape_vec(&vec);
        check_vec(&vec, "\001\023\156");
    }

    it ("Escapes backslashes")
    {
        vec_str(&vec, "\\\\\\n \\\\\\\\ \\\\");
        escape_vec(&vec);
        check_vec(&vec, "\\\n \\\\ \\");
    }

    it ("Sanitizes")
    {
        vec_s dst;
        vec_init(&dst, sizeof(char));
        vec_str(&vec, "\\\x01\x02\x03Hello");
        escape_sanitize_vec(&vec, &dst);
        check_vec(&dst, "\\\\\\x01\\x02\\x03Hello");
        vec_kill(&dst);
    }

}
