#include "editor.h"
#include "buf/buf.h"

#include "buf/sel.h"

static void buf_selector_clear_listeners(buf_selector_s *sel);
static void buf_selector_add_listeners(buf_selector_s *sel);

static void buf_keypress_cb(editor_s *editor, event_s *event, void *param)
{
    buf_selector_s *sel;
    buf_s *buf;

    sel = (buf_selector_s *)param;
    buf = buf_get_selected(sel);

    if (buf)
    {
        bufbind_key(&(buf->bind), &(event->key), buf);
    }
}

int buf_selector_init(buf_selector_s *sel, editor_s *editor)
{
    pthread_mutex_init(&(sel->mtx), NULL);

    sel->buf = NULL;

    sel->eventctx  = &(editor->eventctx);
    sel->keyparser = &(editor->keyparser);

    return PALM_OK;
}

int buf_selector_kill(buf_selector_s *sel)
{
    pthread_mutex_destroy(&(sel->mtx));

    if (sel->buf)
        buf_selector_clear_listeners(sel);

    return PALM_OK;
}

static void buf_selector_clear_listeners(buf_selector_s *sel)
{
    event_spec_s spec;
    key_parser_event_spec(sel->keyparser, &spec);
    event_unlisten(
        sel->eventctx, &spec,
        &(event_action_s){
            .cb = buf_keypress_cb,
            .param = sel
        }
    );
}

static void buf_selector_add_listeners(buf_selector_s *sel)
{
    event_spec_s spec;
    key_parser_event_spec(sel->keyparser, &spec);
    event_listen(
        sel->eventctx, &spec,
        &(event_action_s){
            .cb = buf_keypress_cb,
            .param = sel
        }
    );
}

int buf_select(buf_selector_s *sel, buf_s *buf)
{
    WITH_MTX(&(sel->mtx))
    {
        if (buf != sel->buf)
        {
            if (sel->buf)
                buf_selector_clear_listeners(sel);

            sel->buf = buf;

            if (sel->buf)
                buf_selector_add_listeners(sel);
        }
    }

    return PALM_OK;
}

buf_s *buf_get_selected(buf_selector_s *sel)
{
    buf_s *rtn;

    WITH_MTX(&(sel->mtx))
    {
        rtn = sel->buf;
    }

    return rtn;
}

int buf_get_selected_win(buf_selector_s *sel)
{
    int rtn;
    WITH_MTX(&(sel->mtx))
    {
       if (sel->buf)
            rtn = sel->buf->winind;
        else
            rtn = 0;
    }

    return rtn;
}
