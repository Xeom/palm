#if !defined(EVENT_READER_H)
# define EVENT_READER_H
# include "match/set.h"
# include "event/event.h"
# include "io/io.h"

# include "types.h"

# include <stdbool.h>

/*
 * @file reader.h
 * @brief Async file reader.
 *
 * This is an implementation for an async event-based file
 * reader!
 *
 * An example usage:
 * @code
 * reader_s rdr;
 *
 * // A callback for when a line is received
 * void recvcb(editor_s *, event_s *event, void *)
 * {
 *     vec_s     line;
 *     vec_init(&line, sizeof(char)); // Make a vector to hold the line.
 *
 *     reader_get_line(&rdr, &line);  // Get line contents.
 *     vec_app(&line, NULL);          // Append NULL terminator.
 *
 *     // Print the line
 *     printf("LINE: %s\n", vec_get(&line, 0));
 *
 *     vec_kill(&line);
 * }
 *
 * // A callback for EOF
 * void eofcb(editor_s *, event_s *, void *)
 * {
 *     printf("EOF\n");
 *     reader_kill(&rdr);
 * }
 *
 * // Asynchronously print a file!
 * void print_file(editor_s *editor, int fd)
 * {
 *     event_spec spec;
 *
 *     // Initialize the reader. Newlines are '\n'.
 *     reader_init(&rdr, editor->pollctx, fd, 0, "\n");
 *
 *     // Listen for the recv event.
 *     reader_recv_spec(&rdr, &spec);
 *     event_listen(editor->eventctx, &spec, &(event_action_s){
 *         .cb = recvcb
 *     });
 *
 *     // Listen for the EOF event.
 *     reader_eof_spec(&rdr, &spec);
 *     event_listen(editor->eventctx, &spec, &(event_action_s){
 *         .cb = eofcb
 *     });
 *
 *     // Start reading the file!
 *     reader_start(&rdr);
 * }
 *
 * @endcode
 *
 */

/**
 * A reader structure.
 *
 * This structure asynchronously reads data from a single file descriptor,
 * and emits events of type READER_RECV when a 'line' of data is received,
 * and events of type READER_EOF when the end of the file descriptor is
 * reached.
 *
 * What constitutes a line of data is configurable, by a match specification,
 * so data can be split up in a flexible way.
 *
 */
struct reader
{
    io_file_info_s finfo; /**< The file info for polling and reading data. */
    poll_ctx_s *pollctx;  /**< A pointer to the polling context. */
    match_set_s linesep;  /**< A match set to search for the end of lines. */
    vec_s buf;            /**< A buffer where the current line is received. */
    vec_s lines;          /**< Complete lines, each a vector of chars. */
    int fd;               /**< The file being read. */
    bool alive;           /**< Whether the reader is running. */
};

int reader_init(
    reader_s *reader,
    poll_ctx_s *pollctx,
    int fd,
    match_attr_t linesepattrs,
    const char *linesep
);

int reader_kill(reader_s *reader);

int reader_start(reader_s *reader);

int reader_stop(reader_s *reader);

int reader_get_line(reader_s *reader, vec_s *res);

static inline void reader_recv_spec(reader_s *reader, event_spec_s *spec)
{
    memset(spec, 0, sizeof(event_spec_s));
    spec->typ = EVENT_READER_RECV;
    spec->ptr = reader;
}

static inline void reader_eof_spec(reader_s *reader, event_spec_s *spec)
{
    memset(spec, 0, sizeof(event_spec_s));
    spec->typ = EVENT_READER_EOF;
    spec->ptr = reader;
}

#endif
