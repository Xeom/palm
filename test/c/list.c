#include "snow.h"
#include "container/list.h"

static int cmp_int_cb(const void *aptr, const void *bptr)
{
    const int *a, *b;
    a = aptr;
    b = bptr;
    return ((long)*a > (long)*b) ? 1 : -1;
}

describe (list)
{
    list_s list;

    it ("Initializes lists")
    {
        list_init(&list, sizeof(char));
        asserteq(list_len(&list), 0);
        asserteq(list.width,      sizeof(char));

        asserteq(list_first(&list), NULL);
        asserteq(list_last(&list),  NULL);
        list_kill(&list);
    }

    it ("push_before() inserts at end")
    {
        int vals[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        size_t ind;
        list_init(&list, sizeof(int));

        for (ind = 0; ind < sizeof(vals)/sizeof(int); ++ind)
            assert(list_push_before(&list, NULL, &vals[ind]));

        asserteq(list_len(&list), sizeof(vals)/sizeof(int));
        ind = 0;
        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            asserteq(*ptr, vals[ind++]);
        }
        asserteq(ind, sizeof(vals)/sizeof(int));
        list_kill(&list);
    }

    it ("push_before() inserts at start")
    {
        int vals[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        size_t ind;
        list_init(&list, sizeof(int));

        for (ind = 0; ind < sizeof(vals)/sizeof(int); ++ind)
            assert(list_push_before(&list, list_first(&list), &vals[ind]));

        asserteq(list_len(&list), sizeof(vals)/sizeof(int));
        ind = sizeof(vals)/sizeof(int);
        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            asserteq(*ptr, vals[--ind]);
        }
        asserteq(ind, 0);
        list_kill(&list);
    }

    it ("push_after() inserts at end")
    {
        int vals[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        size_t ind;
        list_init(&list, sizeof(int));

        for (ind = 0; ind < sizeof(vals)/sizeof(int); ++ind)
            assert(list_push_after(&list, list_last(&list), &vals[ind]));

        asserteq(list_len(&list), sizeof(vals)/sizeof(int));
        ind = 0;
        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            asserteq(*ptr, vals[ind++]);
        }
        asserteq(ind, sizeof(vals)/sizeof(int));
        list_kill(&list);
    }

    it ("push_before() inserts at start")
    {
        int vals[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        size_t ind;
        list_init(&list, sizeof(int));

        for (ind = 0; ind < sizeof(vals)/sizeof(int); ++ind)
            assert(list_push_after(&list, NULL, &vals[ind]));

        asserteq(list_len(&list), sizeof(vals)/sizeof(int));
        ind = sizeof(vals)/sizeof(int);
        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            asserteq(*ptr, vals[--ind]);
        }
        asserteq(ind, 0);
        list_kill(&list);
    }

    it ("Pushes NULL data")
    {
        list_init(&list, sizeof(int));

        assert(list_push_before(&list, NULL, NULL));
        assert(list_push_after(&list, NULL, NULL));

        asserteq(list_len(&list), 2);

        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            asserteq(*ptr, 0);
        }

        list_kill(&list);
    }

    it ("Deletes at the start")
    {
        int vals[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        size_t ind;
        list_item_s *iter, *first; 
        list_range_s range;
        list_init(&list, sizeof(int));

        for (ind = 0; ind < sizeof(vals)/sizeof(int); ++ind)
            assert(list_push_before(&list, NULL, &vals[ind]));

        first = list_first(&list);
        iter = list_item_iter(first, 3);
        range = LIST_RANGE(first, iter);
        asserteq(list_del(&list, range), 0);

        asserteq(list_len(&list), sizeof(vals)/sizeof(int) - 4);
    
        ind = 4;
        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            asserteq(*ptr, vals[ind++]);
        }
        list_kill(&list);
    }
    it ("Deletes at the end")
    {
        int vals[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        size_t ind;
        list_item_s *iter, *last; 
        list_range_s range;
        list_init(&list, sizeof(int));

        for (ind = 0; ind < sizeof(vals)/sizeof(int); ++ind)
            assert(list_push_before(&list, NULL, &vals[ind]));

        last = list_last(&list);
        iter = list_item_iter(last, -3);
        range = LIST_RANGE(iter, last);
        asserteq(list_del(&list, range), 0);

        asserteq(list_len(&list), sizeof(vals)/sizeof(int) - 4);
    
        ind = 0;
        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            asserteq(*ptr, vals[ind++]);
        }
        list_kill(&list);
    }

    it ("Sorts random numbers")
    {
        size_t ind;
        list_init(&list, sizeof(int));

        for (ind = 0; ind < 10000; ++ind)
        {
            int val;
            val = rand();
            list_push_before(&list, NULL, &val);
        }

        list_qsort(&list, cmp_int_cb);

        int max = 0;
        LIST_FOREACH(&list, item)
        {
            int *ptr = list_item_data(item);
            assert(max <= *ptr);
            max = *ptr;
        }
        list_kill(&list);
    }

    it ("Gets correct item indices")
    {
        int vals[] = { 1, 2, 3, 4 };
        size_t ind;
        list_item_s *item;

        list_init(&list, sizeof(int));

        for (ind = 0; ind < sizeof(vals)/sizeof(int); ++ind)
            assert(list_push_before(&list, NULL, &vals[ind]));

        item = list_first(&list);

        asserteq(list_item_ind(item), 0);
        item = list_item_iter(item, 1);
        asserteq(list_item_ind(item), 1);
        item = list_item_iter(item, 1);
        asserteq(list_item_ind(item), 2);
        item = list_item_iter(item, 1);
        asserteq(list_item_ind(item), 3);

        list_kill(&list);
    }
}
