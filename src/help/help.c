#include "help/help.h"

int help_init(help_s *help)
{
    help->type = HELP_NONE;

    vec_init(&(help->name), sizeof(char));
    vec_init(&(help->help), sizeof(char));

    list_init(&(help->subs), sizeof(help_s));

    return PALM_OK;
}

void help_kill(help_s *help)
{
    vec_kill(&(help->name));
    vec_kill(&(help->help));

    LIST_FOREACH_DATA(&(help->subs), help_s *, sub)
    {
        help_kill(sub);
    }

    list_kill(&(help->subs));
}

int help_set_type(help_s *help, help_typ_t type)
{
    help->type = type;

    return 0;
}

int help_set_info(help_s *help, const char *name, const char *helpmsg)
{
    TRY(vec_clr(&(help->name)));
    TRY(vec_clr(&(help->help)));

    if (!vec_str(&(help->name), name))
        return PALM_ERR;

    if (!vec_str(&(help->help), helpmsg))
        return PALM_ERR;

    return PALM_OK;
}

help_s *help_add_sub(help_s *help)
{
    list_item_s *item;
    item = list_push_first(&(help->subs), NULL);

    if (item)
    {
        help_s *rtn;
        rtn = list_item_data(item);
        if (help_init(rtn) == -1)
        {
            list_del(&(help->subs), LIST_RANGE(item, item));
            return NULL;
        }

        return rtn;
    }
    else
    {
        return NULL;
    }
}
