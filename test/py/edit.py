import os

from tempfile import NamedTemporaryFile, mktemp, TemporaryDirectory

from editortest import EditorTest, make_tmp_dir_structure

class EditTest(EditorTest):
    def test_file_loading(self):
        with NamedTemporaryFile() as tmpfile:
            tmpfile.write(b"line1\nline2\nline3\n")
            tmpfile.flush()

            self.testbed.run(
                f"from buffers.edit import EditBuffer\n"
                f"buf = editor.new_buf(EditBuffer)\n"
                f"buf.edit({repr(tmpfile.name)})\n"
            )

            self.awaitReprEqual("len(buf)", 4)

            self.assertBufferContents("buf", [
                b"line1", b"line2", b"line3"
            ])

    def test_cannot_edit_directories(self):
        with TemporaryDirectory() as tmpdir:
            self.testbed.run(
                f"from util.keys import Keys\n"
                f"from buffers.edit import EditBuffer\n"
                f"from buffers.dir  import DirBuffer\n"
                f"editor.playbacker.send(Keys.esc('fe') + {repr(tmpdir.encode('utf-8'))} + Keys.enter)\n"
            )

            self.awaitReprEqual("isinstance(editor.selbuf, EditBuffer)", True)
            self.assertBufferContents("editor.selbuf", [
                (
                    f"The file '{tmpdir}' is already a directory. "
                    f"Open it in a DirBuffer instead? (?/y/n) ..."
                ).encode("utf-8")
            ])

            self.testbed.run(
                "editor.playbacker.send(b'y')\n"
            )

            self.awaitReprEqual("isinstance(editor.selbuf, DirBuffer)", True)
            self.assertReprEqual("isinstance(editor.selbuf, DirBuffer)", True)
            self.assertReprEqual("editor.selbuf.path.real", tmpdir)

    def test_file_creation(self):
        path = mktemp()
        self.testbed.run(
            f"from util.keys import Keys\n"
            f"from buffers.edit import EditBuffer\n"
            f"editor.playbacker.send(Keys.esc('fe') + {repr(path.encode('utf-8'))} + Keys.enter)\n"
        )

        self.awaitReprEqual("isinstance(editor.selbuf, EditBuffer)", True)
        self.assertBufferContents("editor.selbuf", [
            (
                f"The file '{path}' does not exist. "
                f"Create and open it anyway? (?/y/n) ..."
            ).encode("utf-8")
        ])

        self.testbed.run(
            f"editor.playbacker.send(b'y')\n"
        )

        self.awaitReprEqual("editor.selbuf.finfo['associated']\n", True)
        self.awaitReprEqual("editor.selbuf.finfo.file['done']\n",  True)

        self.assertBufferContents("buf", [])
        self.assertTrue(os.path.exists(path))

        self.testbed.run(
            "editor.selbuf.finfo.file['done'] = False\n"
            "editor.playbacker.send("
                "b'line1' + Keys.enter +\n"
                "b'line2' + Keys.enter +\n"
                "b'line3' + Keys.enter +\n"
                "Keys.esc(b'fw')\n"
            ")\n"
        )

        self.awaitReprEqual("editor.selbuf.finfo.file['done']\n", True)

        with open(path, "r") as f:
            self.assertEqual(f.read(), "line1\nline2\nline3\n")

        os.unlink(path)
