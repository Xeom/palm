#include "snow.h"
#include "util/macro.h"
#include "cmdlang/lexer.h"
#include "cmdlang/lib.h"
#include "cmdlang/cmd.h"

struct tok_spec
{
    cmdlang_tok_typ_t typ;
    long ival;
    cmd_s *cval;
    const char *sval;
};

static int token_cmp(cmdlang_tok_s *a, struct tok_spec *b)
{
 //   printf("%d '%s' %d '%s'\n", a->typ, a->sval, b->typ, b->sval);
    if (a->typ != b->typ) return a->typ - b->typ;

    switch (a->typ)
    {
    case CMDLANG_TOK_NONE:
        return 0;
    case CMDLANG_TOK_NUMBER:
    case CMDLANG_TOK_PARAM:
        return a->ival - b->ival;
    case CMDLANG_TOK_STR:
        return vec_strcmp(&(a->sval), b->sval);
    case CMDLANG_TOK_CMD:
    case CMDLANG_TOK_SHORT_CMD:
        return a->cval - b->cval;
    default:
        fail("Unexpected token type: %d", a->typ);
        return -1;
    }
}

static void expect_tokens(
    cmdlang_lexer_s *lexer,
    const char *str,
    struct tok_spec *specs,
    size_t n
)
{
    size_t ind;
    for (ind = 0; ind < n; ++ind)
    {
        int res;
        cmdlang_tok_s tok;
        cmdlang_tok_init(&tok);

        do {
            bool next;

            if (!*str)
                fail("Ran out of string to produce tokens after %lu", ind);

            asserteq(cmdlang_lexer_chr(lexer, *str, &next, NULL), 0);
            res = cmdlang_lexer_token(lexer, &tok, NULL);
            assertneq(res, -1);

            if (next) ++str;

        } while (res == 0);

        asserteq(token_cmp(&tok, &specs[ind]), 0);
        cmdlang_tok_kill(&tok);
    }
}

describe(cmdlang)
{
    subdesc(lib)
    {
        cmd_lib_s lib;

        before_each()
        {
            cmd_lib_init(&lib);
        }

        after_each()
        {
            cmd_lib_kill(&lib);
        }

        it ("Can recall commands by name")
        {
            cmd_s *a, *b;

            a = cmd_lib_add_cmd(&lib, "cake",  "tasty");
            assert(a);

            b = cmd_lib_add_cmd(&lib, "tarts", "tasty");
            assert(b);

            asserteq((void *)cmd_lib_find_name(&lib, "cake",  4),  (void *)a);
            asserteq((void *)cmd_lib_find_name(&lib, "tarts", 5), (void *)b);
            asserteq((void *)cmd_lib_find_name(&lib, "pies",  4),  NULL);
        }

        it ("Can recall commands by shortname")
        {
            cmd_s *a;

            a = cmd_lib_add_cmd(&lib, "cake", "tasty");
            assert(a);

            asserteq(cmd_lib_add_shortname(&lib, a, "pie", 3), PALM_OK);

            asserteq(cmd_lib_find_shortname(&lib, "pies", 4), NULL);
            asserteq(cmd_lib_find_shortname(&lib, "x",    1), NULL);
            asserteq(cmd_lib_find_shortname(&lib, "p",    1), NULL);
            asserteq(cmd_lib_find_shortname(&lib, "pie",  3), a);
        }
    }

    subdesc(lexer)
    {
        cmdlang_lexer_s lexer;
        cmd_lib_s lib;

        before_each()
        {
            cmd_lib_init(&lib);
            asserteq(cmdlang_lexer_init(&lexer, &lib, NULL), 0);
        }

        after_each()
        {
            cmdlang_lexer_kill(&lexer);
            cmd_lib_kill(&lib);
        }


        it ("Initializes correctly")
        {
            cmdlang_tok_s tok;
            cmdlang_tok_init(&tok);
            asserteq(cmdlang_lexer_token(&lexer, &tok, NULL), 0);
            cmdlang_tok_kill(&tok);
        }

        it ("Parses numbers")
        {
            struct tok_spec expect[] = {
                { .typ = CMDLANG_TOK_NUMBER, .ival = 1 },
                { .typ = CMDLANG_TOK_NUMBER, .ival = 20 },
                { .typ = CMDLANG_TOK_NUMBER, .ival = 300 },
                { .typ = CMDLANG_TOK_NUMBER, .ival = 4000 }
            };
            expect_tokens(&lexer, "1 20 300 4000\n", expect, DIM(expect));
        }

        it ("Parses strings")
        {
            struct tok_spec expect[] = {
                { .typ = CMDLANG_TOK_STR, .sval = "Hello World" },
                { .typ = CMDLANG_TOK_STR, .sval = "\nTest\n" },
                { .typ = CMDLANG_TOK_STR, .sval = "123" },
            };
            expect_tokens(&lexer, "\"Hello World\" \"\\nTest\\n\" \"\\x31\\x32\\x33\"\n", expect, DIM(expect));
        }

        it ("Parses words")
        {
//            struct tok_spec expect[] = {
//            };
//            expect_tokens(&lexer, "Hello World\n", expect, DIM(expect));
        }

        it ("Parses commands")
        {
            cmd_s *a, *b;

            a = cmd_lib_add_cmd(&lib, "Hello", "Greeting");
            b = cmd_lib_add_cmd(&lib, "World", "Ball");
            struct tok_spec expect[] = {
                { .typ = CMDLANG_TOK_CMD, .cval = a },
                { .typ = CMDLANG_TOK_CMD, .cval = b }
            };
            expect_tokens(&lexer, ":Hello:World\n", expect, DIM(expect));
        }

        it ("Parses params")
        {
            cmd_s *a;
            a = cmd_lib_add_cmd(&lib, "cmd", "Test");
            struct tok_spec expect[] = {
                { .typ = CMDLANG_TOK_PARAM, .ival = 1 },
                { .typ = CMDLANG_TOK_PARAM, .ival = 2 },
                { .typ = CMDLANG_TOK_CMD, .cval = a }
            };
            expect_tokens(&lexer, " #1#2:cmd\n", expect, DIM(expect));
        }
    }

    subdesc (cmd)
    {
        cmd_s cmd;
        it ("Initializes commands")
        {
            asserteq(cmd_init(&cmd, "test", "helpful"), 0);
            asserteq(cmd.typ, CMD_NONE);
            cmd_kill(&cmd);
        }
    }
}
