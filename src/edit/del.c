#include "edit/del.h"
#include "buf/buf.h"
#include "edit/editinfo.h"

void edit_cut_at(edit_info_s *einfo, cur_s *cur)
{
    vec_s line;
    row_t row;
    pos_s *min, *max;

    vec_init(&line, sizeof(chr_s));

    CUR_RFOREACH_LINE(cur, einfo->buf, linecur)
    {
        row = linecur.pri.row;

        min = cur_min(&linecur);
        max = cur_max(&linecur);

        buf_get_line(einfo->buf, row, &line);

        if (max->col >= (col_t)vec_len(&line) && row < buf_len(einfo->buf) - 1)
        {
            vec_s next;
            vec_init(&next, sizeof(chr_s));
            vec_del(&line, min->col, vec_len(&line) - min->col);
            buf_get_line(einfo->buf, row + 1, &next);
            vec_cpy(&line, &next);
            buf_del_line(einfo->buf, row + 1, 1);
            vec_kill(&next);
        }
        else
        {
            vec_del(&line, min->col, max->col - min->col + 1);
        }

        buf_set_line(einfo->buf, row, &line);
    }
    vec_kill(&line);
}

void edit_backspace(edit_info_s *einfo)
{
    cur_s cur;

    edit_get_cur(einfo, &cur);

    if (cur.pri.row == 0 && cur.pri.col == 0)
        return;

    cur_move(&cur, CUR_DIR_LEFT, 1, einfo->buf);

    buf_set_cur(einfo->buf, &cur);

    edit_del(einfo);
}

void edit_del(edit_info_s *einfo)
{
    cur_s cur;

    edit_get_cur(einfo, &cur);

    switch (cur.type)
    {
    case CUR_TYPE_LINE:
    case CUR_TYPE_CHR:
        edit_cut_at(einfo, &CUR_AT(cur.pri.row, cur.pri.col));
        break;

    case CUR_TYPE_BLOCK:
        edit_cut_at(einfo, &(cur_s){
            .pri = { .row = cur_min(&cur)->row, .col = cur.pri.col },
            .sec = { .row = cur_max(&cur)->row, .col = cur.pri.col },
            .type = CUR_TYPE_BLOCK,
            .sticky = false
        });
        break;
    }
}
