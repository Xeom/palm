import paths
import libtypes

from core.buffer   import Buffer
from server.server import add_global

@add_global()
class WelcomeBuffer(Buffer):
    def init(self, *args, **kwargs):
        Buffer.init(self, *args, **kwargs)
        self.finfo.attrs = ["UTF8", "FNT", "STRIPNL", "NL"]
        self.finfo.assoc(paths.data + "/welcome.txt")
        self.finfo.read()
