#if !defined(BUF_CURSET_H)
# define BUF_CURSET_H
# include "types.h"
# include "buf/cur.h"
# include "container/list.h"

struct curset
{
    list_s active;
    list_s inactive;
    pthread_mutex_t mtx;
    buf_s *buf;
    list_item_s *selected;
    bool sorted;
};

void curset_init(curset_s *curset, buf_s *buf);

void curset_kill(curset_s *curset);

void curset_shift(curset_s *curset, pos_s *from, pos_s *to);

size_t curset_num_active(curset_s *curset);
size_t curset_num_inactive(curset_s *curset);

void curset_expand_all(curset_s *curset);

/** Get the selected cursor */
int curset_get_selected(curset_s *curset, cur_s *cur);
/** Set the selected cursor */
int curset_set_selected(curset_s *curset, cur_s *cur);

int curset_select_prev(curset_s *curset);
int curset_select_next(curset_s *curset);

int curset_select_ind(curset_s *curset, long n);
long curset_get_selected_ind(curset_s *curset);

/** Push a new selected cursor */
int curset_push(curset_s *curset, cur_s *cur);

/** Pop the selected cursor */
int curset_pop(curset_s *curset, cur_s *cur);

/** Push a new selected cursor */
int curset_push_inactive(curset_s *curset, cur_s *cur);

/** Pop the selected cursor */
int curset_pop_inactive(curset_s *curset, cur_s *cur);

int buf_get_cur(buf_s *buf, cur_s *cur);

int buf_set_cur(buf_s *buf, cur_s *new);

void buf_shift(buf_s *buf, pos_s *from, pos_s *to);

cur_hl_t curset_get_pos_hl(curset_s *curset, pos_s *pos, buf_s *buf);

#endif
