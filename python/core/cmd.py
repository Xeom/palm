import ctypes

from core.newstruct import CStructure, SimpleCStructure
from lib import lib, defs

from util.encode import bytify, strify

from server.globals import add_global

from typing import Union
from core.cmdlang import CmdlangObj

@add_global()
class Cmd(CStructure):
    class_name = "cmd_s"

    init_funct = lib.cmd_init
    kill_funct = lib.cmd_kill

    def init_args(self, args, kwargs):
        def inner(name, helpinfo):
            return [bytify(name), bytify(helpinfo)]
        return inner(*args, **kwargs)

    def add_argument(self, name, helpinfo, spec):
        name     = bytfiy(name)
        helpinfo = bytify(helpinfo)
        res = lib.cmd_add_argument(self.ptr, name, helpinfo, spec.ptr)

        if res == -1:
            raise Exception("Could not add argument to command")

    def set_info(self, info):
        info.set_on_cmd(self)
        self.pyinfo = info

    def set_verb_cb(self, cb):
        info = CmdVerbInfo.from_cb(cb)
        self.set_info(info)

    def set_noun_cb(self, cb):
        info = CmdNounInfo.from_cb(cb)
        self.set_info(info)

@add_global()
class CmdCtx(CStructure):
    class_name = "cmd_ctx_s"

    init_funct = lib.cmd_ctx_init
    kill_funct = lib.cmd_ctx_kill

    def init_args(self, args, kwargs):
        def inner(buf):
            return [buf.ptr]

        return inner(*args, **kwargs)

@add_global()
class CmdArgSpec(SimpleCStructure):
    class_name = "cmd_arg_spec_s"

    @classmethod
    def opt_arg(cls, *types):
        return cls(optional=True, typemask=types)

    @classmethod
    def req_arg(self, *types):
        return cls(optional=False, typemask=types)

class CmdInfo(CStructure):
    set_fptr = None

    init_funct = None
    kill_funct = None

    @staticmethod
    def make_arglist(args, nargs):
        return [args[i].contents for i in range(nargs)]

    def set_on_cmd(self, cmd):
        self.set_fptr(cmd.ptr, self.ptr)

class CmdCbInfo(CmdInfo):
    cbtype = None

    @classmethod
    def from_cb(cls, cb):
        def wrapper(ctx, args, nargs):
            try:
                cb(ctx.contents, self.make_arglist(args, nargs))
            except:
                return -1

            return 0

        return cls(cb=cls.cbtype(wrapper))

@add_global()
class CmdVerbInfo(CmdCbInfo):
    class_name = "cmd_verb_info_s"
    set_fptr   = lib.cmd_set_verb
    cbtype     = ctypes.CFUNCTYPE(
        ctypes.c_int,
        ctypes.POINTER(CmdCtx), ctypes.POINTER(CmdlangObj), ctypes.c_size_t
    )


@add_global()
class CmdNounInfo(CmdCbInfo):
    class_name = "cmd_noun_info_s"
    set_fptr   = lib.cmd_set_noun
    cbtype     = ctypes.CFUNCTYPE(
        ctypes.c_int,
        ctypes.POINTER(CmdCtx), ctypes.POINTER(CmdlangObj), ctypes.c_size_t,
        ctypes.POINTER(CmdlangObj)
    )

@add_global()
class CmdAliasInfo(CmdInfo):
    class_name = "cmd_alias_info_s"
    set_fptr   = lib.cmd_set_alias
    init_funct = lib.cmd_alias_info_init
    kill_funct = lib.cmd_alias_info_kill

    def init_args(self, args, kwargs):
        def inner(lib):
            return [lib.ptr]

        return inner(*args, **kwargs)

    def from_str(self, string):
        errmsg = defs.errmsg_t()
        res = lib.cmd_alias_info_from_str(self.ptr, bytify(string), errmsg)

        if res != 0:
            raise Exception(f"Could not set alias from str: {errmsg.value}")

@add_global()
class CmdAdverbInfo(CmdInfo):
    class_name = "cmd_adverb_info_s"
    set_fptr = lib.cmd_set_adverb

    @classmethod
    def from_start_iter_end(cls, startcb, itercb, endcb):
        def startwrapper(ctx, args, nargs, userptr):
            try:
                startcb(ctx.contents, self.make_arglist(args, nargs))
            except:
                return -1

            return 0

        def itercb(ctx, args, nargs, user, cont):
            try:
                if itercb(ctx.contents, self.make_arglist(args, nargs)):
                    cont.contents.value = True
                else:
                    cont.contents.value = False
            except:
                return -1

            return 0

        def endcb(ctx, args, nargs, user):
            try:
                endcb(ctx.contents, self.make_arglist(args, nargs))
            except:
                return -1

            return 0

        return cls(
            start_cb=ctypes.CFUNCTYPE(
                ctypes.c_int,
                ctypes.POINTER(CmdCtx),
                ctypes.POINTER(CmdlangObj), ctypes.c_size_t,
                ctypes.POINTER(ctypes.c_void_p)
            )(startcb),
            iter_cb=ctypes.CFUNCTYPE(
                ctypes.c_int,
                ctypes.POINTER(CmdCtx),
                ctypes.POINTER(CmdlangObj), ctypes.c_size_t,
                ctypes.c_void_p,
                ctypes.POINTER(ctypes.c_size_t)
            )(itercb),
            end_cb=ctypes.CFUNCTYPE(
                ctypes.c_int,
                ctypes.POINTER(CmdCtx),
                ctypes.POINTER(CmdlangObj), ctypes.c_size_t,
                ctypes.c_void_p
            )(endcb)
        )




@add_global()
class CmdLib(CStructure):
    class_name = "cmd_lib_s"

    init_funct = lib.cmd_lib_init
    kill_funct = lib.cmd_lib_kill

    def add_cmd(
        self,
        name: Union[str, bytes],
        helpinfo: Union[str, bytes]
    ) -> Cmd:
        name = bytify(name)
        helpinfo = bytify(helpinfo)
        ptr = lib.cmd_lib_add_cmd(self.ptr, name, helpinfo)
        if not ptr:
            raise Exception("Could not add command to library.")

        return ptr.contents

    def add_alias(
        self,
        name: Union[str, bytes],
        helpinfo: Union[str, bytes],
        args,
        code: Union[str, bytes]
    ) -> Cmd:
        cmd = self.add_cmd(name, helpinfo)

        alias = CmdAliasInfo(self)
        alias.from_str(bytify(code) + b"\n")

        cmd.set_info(alias)

        return cmd

    def add_shortname(self, cmd: Cmd, name: Union[str, bytes]):
        name = bytify(name)
        res = lib.cmd_lib_add_shortname(self.ptr, cmd.ptr, name, len(name))
        if res == -1:
            raise Exception("Could not add shortname to library.")

    def find_name(self, name: Union[str, bytes]) -> Cmd:
        ptr = lib.cmd_lib_find_name(self.ptr, bytify(name), len(name))
        if not ptr:
            return None

        return ptr.contents

    def mount_all_c_cmds(self):
        if lib.cmd_mount_all(self.ptr) == -1:
            raise Exception("Could not mount all C commands.")

        for name, info, code, args, shorts in (
            ("up",    "Move the cursor up.",    "0:move", [], ["k"]),
            ("down",  "Move the cursor down.",  "1:move", [], ["j"]),
            ("left",  "Move the cursor left.",  "2:move", [], ["h"]),
            ("right", "Move the cursor right.", "3:move", [], ["l"]),
            ("next_word", "Move the cursor to the next word.", "4:move", [], ["."]),
            ("prev_word", "Move the cursor to the next word.", "5:move", [], [","]),

            ("next_buf", "Select the next buffer.",     "\"editor.switch_buf(1)\":py_exec",  [], ["bn"]),
            ("prev_buf", "Select the previous buffer.", "\"editor.switch_buf(-1)\":py_exec", [], ["bp"]),
            ("new_buf", "Create a new editable buffer.", "\"editor.new_buf(EditBuffer, switch=True)\":py_exec", [], ["bb"]),
            ("kill_buf", "Kill the current buffer.", "\"editor.kill_buf_soon(buf)\":py_exec", [], ["bx"]),
            ("list_dir", "List the current directory.", "\"editor.new_buf(DirBuffer, switch=True).show_dir('.')\":py_exec", [], ["bf"]),
            ("list_bufs", "List the current buffers.", "\"editor.get_buf_of_type(BuflistBuffer, switch=True)\":py_exec", [], ["bl"]),
            ("editor_exit", "Exit the editor.", "\"editor.soft_exit()\":py_exec", [], ["ex"]),
            ("new_pyprompt", "Open a python prompt.", "\"open_buffer_pyprompt()\":py_exec", [], ["x"]),

            ("next_win",   "Select the next window.",     "\"editor.switch_win(1)\":py_exec",   [], ["wn"]),
            ("prev_win",   "Select the previous window.", "\"editor.switch_win(-1):\":py_exec", [], ["wp"]),
            ("kill_win",   "Close the current window.",   "\"editor.win_close()\":py_exec",     [], ["wx"]),
            ("split_win",  "Split the current window.",  "\"editor.win_split()\":py_exec",     [], ["ws"]),
            ("hsplit_win", "Horizontally split the current window.",  "\"editor.win_split(split='H')\":py_exec",     [], ["ws"]),
            ("vsplit_win", "Vertically split the current window.",  "\"editor.win_split(split='V')\":py_exec",     [], ["ws"]),
            ("file_info", "Show current buffer file info.", "\"editor.popup_buf(PathInfoBuffer).show_path(finfo.path)\":py_exec", [], ["fi"]),
            ("file_read",  "Load from file to current buffer.", "\"finfo.read()\":py_exec", [], ["fr"]),
            ("file_write", "Save to file from current buffer.", "\"finfo.write()\":py_exec", [], ["fw"]),
            ("file_write_all", "Save all buffers.", "\"save_all()\":py_exec", [], ["fW"]),
            ("file_edit", "Edit a new file.", "\"single_pyprompt('edit_file(\\\"','\\\")')\":py_exec", [], ["fe"]),
            ("file_force_read", "Force-load the current file.", "\"finfo.read(force=True)\":py_exec", [], ["f!r"]),
            ("file_force_write", "Force-write the current buffer.", "\"finfo.write(force=True)\":py_exec", [], ["f!w"]),
            ("file_force_write_all", "Force-write all buffers.", "\"save_all(force=True)\":py_exec", [], ["f!W"]),
            ("cursor_block_mode", "Select block cursor mode.", "1:cur_type", [], ["ck"]),
            ("cursor_char_mode", "Select character cursor mode.", "0:cur_type", [], ["cc"]),
            ("cursor_line_mode", "Select line cursor mode.", "2:cur_type", [], ["cl"]),
            ("incr_indent", "Increment the indent of the current line.", "4:add_indent", [], ["c>"]),
            ("decr_indent", "Decrement the indent of the current line.", "-4:add_indent", [], ["c<"])
#            ("copy", "Copy selection to clipboard.", "
        ):
            cmd = self.add_alias(name, info, args, code)
            for s in shorts:
                self.add_shortname(cmd, s)

        for cmd, shorts in (
                ("pgup", ["K"]),
                ("pgdn", ["J"]),
                ("home", ["H"]),
                ("end",  ["L"]),
                ("swap", ["cw"]),
                ("undo", ["u"]),
                ("stick", ["ss"]),
                ("insert", ["i"])
            ):
            for s in shorts:
                self.add_shortname(self.find_name(cmd), s)
