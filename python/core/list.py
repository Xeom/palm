from .newstruct import CStructure, SimpleCStructure

from lib import lib

class List(CStructure):
    class_name = "list_s"

    init_funct = lib.list_init
    kill_funct = lib.list_kill

class ListItem(SimpleCStructure):
    class_name = "list_item_s"

class ListRange(SimpleCStructure):
    class_name = "list_range_s"
