#include "hl/action.h"
#include "text/font.h"

int hl_action_init(hl_action_s *act)
{
    act->token    = -1;
    act->state    = -1;
    act->font     = -1;
    act->priority = 0;
    act->type     = HL_ACTION_NONE;

    return 0;
}

void hl_action_use_defaults(hl_action_s *act, hl_action_s *defaults)
{
    if (defaults->token != -1)
        act->token = defaults->token;
    if (defaults->state != -1)
        act->state = defaults->state;
    if (defaults->font != -1)
        act->font = defaults->font;
    if (defaults->type != HL_ACTION_NONE)
        act->type = defaults->type;
    if (defaults->priority != 0)
        act->priority = defaults->priority;
}

int hl_action_load_info(hl_action_s *act, yaml_obj_s *obj, hl_ctx_s *ctx)
{
    const char *tokstr, *statestr, *fontstr, *typestr, *pristr;

    statestr = yaml_obj_get_val(obj, (const char *[]){ "state",    NULL }, NULL);
    tokstr   = yaml_obj_get_val(obj, (const char *[]){ "tok",      NULL }, NULL);
    fontstr  = yaml_obj_get_val(obj, (const char *[]){ "font",     NULL }, NULL);
    typestr  = yaml_obj_get_val(obj, (const char *[]){ "type",     NULL }, NULL);
    pristr   = yaml_obj_get_val(obj, (const char *[]){ "priority", NULL }, NULL);

    if (statestr)
    {
        act->state = hl_ctx_lookup_state(ctx, statestr);
        if (act->state == -1)
        {
            LOG(NOTICE, ERR, "Could not find state: %s", statestr);
            return -1;
        }
    }
    if (tokstr)
    {
        act->token = hl_ctx_lookup_token(ctx, tokstr);
        if (act->token == -1)
        {
            LOG(NOTICE, ERR, "Could not find token: %s", tokstr);
            return -1;
        }
    }
    if (fontstr)
    {
        act->font = font_by_name(fontstr);
    }
    if (pristr)
    {
        act->priority = atoi(pristr);
    }
    if (typestr)
    {
        if (strcmp(typestr, "swap") == 0)
        {
            act->type = HL_ACTION_SWAP;
        }
        else if (strcmp(typestr, "pop") == 0)
        {
            act->type = HL_ACTION_POP;
        }
        else if (strcmp(typestr, "push") == 0)
        {
            act->type = HL_ACTION_PUSH;
        }
        else
        {
            LOG(NOTICE, ERR, "Unknown action type: %s", typestr);
            return -1;
        }
    }

    return 0;
}

int hl_action_verify(hl_action_s *act)
{
    if (act->token == -1)
    {
        LOG(NOTICE, ERR, "Action is associated with no token");
        return -1;
    }

    return 0;
}

int hl_action_cmp(const void *a, const void *b)
{
    const hl_action_s *acta, *actb;

    acta = a;
    actb = b;

    return acta->priority - actb->priority;
}

void hl_action_apply(
    hl_action_s *act, hl_ctx_s *ctx, vec_s *str, vec_s *fonts, vec_s *states, size_t start, size_t end)
{
    if (act->font != -1)
    {
        size_t ind;
        for (ind = start; ind < end; ++ind)
        {
            unsigned short *fptr;
            fptr = vec_get(fonts, ind);
            if (fptr)
                *fptr = act->font;
        }
    }

    switch (act->type)
    {
    case HL_ACTION_PUSH:
        if (act->state != -1)
        {
            vec_app(states, &(act->state));
        }
        break;
    case HL_ACTION_POP:
        if (vec_len(states) > 0)
            vec_del(states, vec_len(states) - 1, 1);
        break;
    case HL_ACTION_SWAP:
        if (act->state != -1)
        {
            *((long *)vec_end(states)) = act->state;
        }
        break;
    }
}

