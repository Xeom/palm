from .newstruct import CStructure

from lib import lib

class PollCtx(CStructure):
    class_name = "event_ctx_s"

    init_funct = lib.poll_ctx_init
    kill_funct = lib.poll_ctx_kill

    def init_args(self, args, kwargs):
        return [args[0].ptr]

    def add(self, finfo):
        lib.poll_add(self.ptr, finfo.ptr)

    def rem(self, finfo):
        lib.poll_rem(self.ptr, finfo.ptr)




