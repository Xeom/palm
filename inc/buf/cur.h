#if !defined(BUF_CUR_H)
# define BUF_CUR_H
# include <stdbool.h>
# include <stddef.h>
# include "types.h"

typedef long row_t;
typedef long col_t;
typedef long indent_t;

typedef enum
{
    CUR_DIR_UP,
    CUR_DIR_DOWN,
    CUR_DIR_LEFT,
    CUR_DIR_RIGHT,
    CUR_DIR_NWORD,
    CUR_DIR_PWORD
} cur_dir_t;

typedef enum
{
    CUR_TYPE_LINE  = 1,
    CUR_TYPE_CHR   = 2,
    CUR_TYPE_BLOCK = 3
} cur_type_t;

typedef enum
{
    CUR_HL_NONE,
    CUR_HL_END,
    CUR_HL_PRI,
    CUR_HL_SEC,
    CUR_HL_SEL,
    CUR_HL_PRIFAKE,
    CUR_HL_SECFAKE,
    CUR_HL_ALT_PRI,
    CUR_HL_ALT_SEC,
    CUR_HL_ALT_SEL,
    CUR_HL_ALT_PRIFAKE,
    CUR_HL_ALT_SECFAKE,
    CUR_HL_DEAD_PRI,
    CUR_HL_DEAD_SEC,
    CUR_HL_DEAD_SEL,
    CUR_HL_DEAD_PRIFAKE,
    CUR_HL_DEAD_SECFAKE
} cur_hl_t;

struct pos
{
    row_t row;
    col_t col;
};

struct cur
{
    pos_s pri, sec;
    cur_type_t type;
    bool sticky;
};

#define CUR_AT(r, c) \
    (cur_s){                               \
        .pri = { .row = r, .col = c }, \
        .sec = { .row = r, .col = c }, \
        .type = CUR_TYPE_CHR,              \
        .sticky = false                    \
    }

bool pos_gt(const pos_s *a, const pos_s *b);
bool pos_lt(const pos_s *a, const pos_s *b);

static inline bool cur_gt(const cur_s *a, const cur_s *b)
{
    return pos_gt(&(a->pri), &(b->pri));
}

static inline bool cur_lt(const cur_s *a, const cur_s *b)
{
    return pos_lt(&(a->pri), &(b->pri));
}

int pos_cmp(const pos_s *a, const pos_s *b);
int cur_cmp(const cur_s *a, const cur_s *b);

pos_s *pos_min(pos_s *a, pos_s *b);
pos_s *pos_max(pos_s *a, pos_s *b);

static inline pos_s *cur_min(cur_s *cur)
{
    return pos_min(&(cur->pri), &(cur->sec));
}

static inline pos_s *cur_max(cur_s *cur)
{
    return pos_max(&(cur->pri), &(cur->sec));
}

void cur_init(cur_s *cur);

bool cur_get_line(cur_s *cur, cur_s *line, buf_s *buf, row_t row);

void pos_shift(pos_s *pos, pos_s *from, pos_s *to);
void cur_shift(cur_s *cur, pos_s *from, pos_s *to);

void pos_correct(pos_s *pos, buf_s *buf);
void pos_move(pos_s *pos, cur_dir_t dir, int n, buf_s *buf);

void cur_correct(cur_s *pos, buf_s *buf);
void cur_move(cur_s *cur, cur_dir_t dir, int n, buf_s *buf);

bool cur_get_selected(cur_s *cur, pos_s pos, buf_s *buf);

cur_hl_t cur_max_hl(cur_hl_t a, cur_hl_t b);
cur_hl_t cur_get_pos_hl(cur_s *cur, pos_s pos, buf_s *buf);

const char *cur_type_name(cur_type_t type);

void cur_split_by_line(cur_s *cur, vec_s *alts);

void buf_expand_cur(buf_s *buf, cur_s *cur);

#endif
