#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "container/vec.h"

static int vec_resize_longer(vec_s *v);
static int vec_resize_shorter(vec_s *v);

void vec_init(vec_s *v, size_t width)
{
    memset(v, 0, sizeof(vec_s));
    v->width = width;
}

void vec_kill(vec_s *v)
{
    if (v->mem)
        free(v->mem);

    memset(v, 0, sizeof(vec_s));
}

/* Fix vec sizing when increasing length */
static int vec_resize_longer(vec_s *v)
{
    if (v->used <= v->allocated)
        return 0;

    if (v->allocated == 0)
        v->allocated = 1;

    do
    {
        v->allocated <<= 1;
    } while (v->used > v->allocated);

    v->mem = realloc(v->mem, v->allocated);

    if (!(v->mem)) return -1;

    return 0;
}

/* Fix vec sizing when decreasing length */
static int vec_resize_shorter(vec_s *v)
{
    if (v->used >= v->allocated >> 2)
        return 0;

    do
    {
        v->allocated >>= 1;
    } while (v->used < v->allocated >> 2);

    v->mem = realloc(v->mem, v->allocated);

    if (!(v->mem)) return -1;

    return 0;
}

void *vec_fill(vec_s *v, size_t ind, size_t n, const void *mem)
{
    char *new, *iter;

    new = vec_ins(v, ind, n, NULL);

    if (!new)
        return NULL;

    iter = new;

    for (ind = 0; ind < n; ++ind)
    {
        memcpy(iter, mem, v->width);
        iter += v->width;
    }

    return new;
}

void *vec_ins(vec_s *v, size_t ind, size_t n, const void *mem)
{
    size_t offset, numbytes, bytesafter;

    if (n == 0) return NULL;

    numbytes   = n   * v->width;
    offset     = ind * v->width;
    bytesafter = v->used - offset;

    if (offset > v->used) return NULL;

    v->used += numbytes;
    vec_resize_longer(v);

    if (bytesafter != 0)
        memmove(v->mem + offset + numbytes, v->mem + offset, bytesafter);

    if (mem)
        memcpy(v->mem + offset, mem, numbytes);
    else
        memset(v->mem + offset, 0, numbytes);

    return v->mem + offset;
}

int vec_del(vec_s *v, size_t ind, size_t n)
{
    size_t offset, numbytes, bytesafter;

    if (n == 0) return 0;

    numbytes   = n   * v->width;
    offset     = ind * v->width;
    bytesafter = v->used - offset - numbytes;

    if (offset > v->used || numbytes > v->used - offset) return -1;

    if (bytesafter != 0)
        memmove(v->mem + offset, v->mem + offset + numbytes, bytesafter);

    v->used -= numbytes;
    vec_resize_shorter(v);

    return 0;
}

void *vec_get(const vec_s *v, size_t ind)
{
    size_t offset;

    offset = ind * v->width;

    if (offset >= v->used) return NULL;

    return v->mem + offset;
}

size_t vec_len(const vec_s *v)
{
    return v->used / v->width;
}

void *vec_fmt(vec_s *v, const char *fmt, ...)
{
    va_list args;
    void *rtn;
    int n;
    char c;
    size_t ind;


    /* We calculate the required length */
    ind = vec_len(v);
    va_start(args, fmt);
    n = vsnprintf(&c, 1, fmt, args);
    va_end(args);

    /* We add the necessary space to the vector *
     * Including space for the final \x00.      */
    vec_add(v, n + 1, NULL);

    /* We do the formatting for real */
    va_start(args, fmt);
    vsnprintf(vec_get(v, ind), n + 1, fmt, args);
    va_end(args);

    /* We delete the space for the extra \x00 */
    vec_del(v, vec_len(v) - 1, 1);

    /* We get a pointer to the formatted text */
    rtn = vec_get(v, ind);

    return rtn;
}

void vec_rev(vec_s *v)
{
    long ind1, ind2;

    for (ind1 = 0, ind2 = vec_len(v) - 1; ind2 > ind1; ++ind1, --ind2)
    {
        char tmp[v->width];
        memcpy(tmp,              vec_get(v, ind1), v->width);
        memcpy(vec_get(v, ind1), vec_get(v, ind2), v->width);
        memcpy(vec_get(v, ind2), tmp,              v->width);
    }
}

/* BISECTION */
size_t vec_bst(
    const vec_s *v,
    const void *item,
    int (*cmpfunc)(const void *a, const void *b)
)
{
    size_t ltind, gtind;
    int cmp;

    if (!v || !cmpfunc) return 0;
    /* An empty vector returns 0 always */
    if (v->used == 0)   return 0;

    /* ltind and gtind will always be less than and greater than item, *
     * respectively. They are set to the ends of the vec here          */
    ltind = 0;
    gtind = vec_len(v) - 1;

    /* Check that ltind and gtind are less than and greater than item, *
     * otherwise return a limit.                                       */
    if (cmpfunc(vec_get(v, ltind), item) > 0) return 0;
    if (cmpfunc(vec_get(v, gtind), item) < 0) return vec_len(v);

    /* We're done when we've narrowed ltind and gtind down to one apart. *
     * Our new bisection index is between them.                          */
    while (ltind + 1 < gtind)
    {
        size_t midind;

        midind = (gtind + ltind) / 2;
        cmp = cmpfunc(vec_get(v, midind), item);

        if (cmp  > 0) gtind = midind;
        if (cmp  < 0) ltind = midind;
        if (cmp == 0) return  midind;
    }

    cmp = cmpfunc(vec_get(v, ltind), item);

    if (cmp == 0) return ltind;
    else          return gtind;
}
