import os
import time
import tempfile

from lib import *

from util.keys       import Keys
from core.cur        import Cur
from buffers.dir     import DirBuffer
from buffers.pathinfo import PathInfoBuffer

from editortest import EditorTest, make_tmp_dir_structure

class TestDirBuffer(EditorTest):
    def get_lines(self, buf):
        rng = range(len(buf) - sum(len(i) for i in buf.items), len(buf))
        lines = [
            buf.get_str(ln) for ln in rng
        ]

        return [l.decode("utf-8").strip().split()[-1] for l in lines]

    def test_dirbuffer_shows_files(self):
        with make_tmp_dir_structure({
            "file1": "",
            "file2": "",
            "file3":""
        }) as tmpdir:
            self.testbed.run(
                f"buf = editor.new_buf(DirBuffer)\n"
                f"buf.show_dir({repr(tmpdir)})\n"
            )

            self.assertReprEqual("buf.get_str(len(buf) - 3)", b"> file1")
            self.assertReprEqual("buf.get_str(len(buf) - 2)", b"> file2")
            self.assertReprEqual("buf.get_str(len(buf) - 1)", b"> file3")

    def test_dirbuffer_cmd(self):
        self.testbed.run(
            "from util.keys import Keys\n"
            "editor.playbacker.send(Keys.esc(b'bf'))\n"
        )

        self.awaitReprEqual("editor.selbuf.get_name()", "DirBuffer")
        self.assertReprEqual("editor.selbuf.get_name()", "DirBuffer")

    def test_file_info_key(self):
        with make_tmp_dir_structure({
            "file": ""
        }) as tmpdir:
            self.testbed.run(
                f"from util.keys import Keys\n"
                f"buf = editor.new_buf(DirBuffer, switch=True)\n"
                f"buf.show_dir({repr(tmpdir)})\n"
                f"editor.playbacker.send(Keys.esc(b'gJ') + b'i')\n"
            )

            self.awaitReprEqual ("editor.selbuf.get_name()", "PathInfoBuffer")
            self.assertReprEqual("editor.selbuf.get_name()", "PathInfoBuffer")

    def test_dirbuffer_subdirectory(self):
        with make_tmp_dir_structure({
            "dir":
            {
                "subdir": {"file": ""}
            },
            "file": ""
        }) as tmpdir:
            self.testbed.run(
                f"from util.keys import Keys\n"
                f"buf = editor.new_buf(DirBuffer, switch=True)\n"
                f"buf.show_dir({repr(tmpdir)})\n"
            )

            self.assertReprEqual("buf.get_str(len(buf) - 2)", b"> file")
            self.assertReprEqual("buf.get_str(len(buf) - 1)", b"[+] dir/")

            self.testbed.run(
                "from core.cur import Cur\n"
                "buf.set_cur(Cur.at(len(buf) - 1))\n"
                "buf.enter()\n"
            )

            self.assertReprEqual("buf.get_str(len(buf) - 3)", b"> file")
            self.assertReprEqual("buf.get_str(len(buf) - 2)", b"[-] dir/")
            self.assertReprEqual("buf.get_str(len(buf) - 1)", b"'-[+] subdir/")

            self.testbed.run(
                "buf.set_cur(Cur.at(len(buf) - 1))\n"
                "buf.enter()\n"
            )

            self.assertReprEqual("buf.get_str(len(buf) - 4)", b"> file")
            self.assertReprEqual("buf.get_str(len(buf) - 3)", b"[-] dir/")
            self.assertReprEqual("buf.get_str(len(buf) - 2)", b"'-[-] subdir/")
            self.assertReprEqual("buf.get_str(len(buf) - 1)", b"  '-> file")
