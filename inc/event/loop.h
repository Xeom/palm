#if !defined(EVENT_LOOP_H)
# define EVENT_LOOP_H
# include "types.h"

void event_loop(event_ctx_s *eventctx, poll_ctx_s *pollctx, timer_ctx_s *timerctx);

#endif
