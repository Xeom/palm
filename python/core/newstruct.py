import ctypes
from typing import List
from lib.util import Enum

ctypesptrbase = ctypes.POINTER(ctypes.c_int).__bases__[0]
class CStructure(ctypes.Structure):
    class_name = None

    init_funct = None
    kill_funct = None

    shallow_copyable = False

    init_fields_directly = False

    @classmethod
    def iter_arg_spec(cls, args, kwargs):
        for (name, typ), param in zip(cls._fields_, args):
            yield (name, typ), param

        for (name, typ) in cls._fields_:
            if name in kwargs:
                param = kwargs[name]
                yield (name, typ), param

    @classmethod
    def convert_args(cls, args, kwargs):
        rtn = {}
        for (name, typ), param in cls.iter_arg_spec(args, kwargs):
            if issubclass(typ, Enum):
                param = typ(param)

            if typ == ctypes.c_void_p:
                param = ctypes.cast(param, ctypes.c_void_p)

            rtn[name] = param

        return rtn

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "_fields_"):
            raise Exception(
                "Structure has no _fields_ attribute. "
                "Did you add it to the bindings generation script?"
            )
        if self.init_fields_directly:
            super().__init__(**self.convert_args(args, kwargs))
        else:
            super().__init__()

        self.is_inited: bool  = False
        self.is_killed: bool  = False

        if self._b_needsfree_:
            self.init(*args, **kwargs)
        elif len(args) + len(kwargs) > 0:
            raise Exception("Did not expect to be initialized with arguments.")

    def __eq__(self, oth):
        if isinstance(oth, type(self)):
            for fname, ftyp in self._fields_:
                if getattr(self, fname) != getattr(oth, fname):
                    return False

            return True

        else:
            return False

    def __del__(self):
        if self._b_needsfree_:
            self.kill()

    @classmethod
    def get_class_name(self):
        if self.class_name is not None:
            return self.class_name
        else:
            return type(self).__name__

    def copy_from(self, oth: "CStructure"):
        if not self.shallow_copyable:
            raise Exception(f"{type(self)} is not shallow copyable.")

        ctypes.memmove(self.ptr, oth.ptr, ctypes.sizeof(type(self)))

    def copy(self) -> "CStructure":
        new = type(self)()
        new.copy_from(self)

        return new

    def kill(self):
        if hasattr(self, "is_killed") and self.is_killed:
            return

        self.kill_action()
        self.is_killed = True

    def kill_action(self):
        if self.kill_funct is not None:
            self.kill_funct(self.ptr)

    def init(self, *args, **kwargs):
        if hasattr(self, "is_inited") and self.is_inited:
            return

        self.init_action(*args, **kwargs)
        self.is_inited = True

    def init_action(self, *args, **kwargs):
        if self.init_funct is not None:
            self.init_funct(self.ptr, *self.init_args(args, kwargs))

    def init_args(self, args: List, kwargs: List) -> List:
        return []

    repr_fmt:       str  = "<{name}: {vals}>"
    """The format of this structure's repr."""

    repr_multiline: bool = True
    def get_repr_vals(self) -> str:
        """Get a representation of the values contained within this structure.

        This will be used by __repr__ to build a representation of this
        whole structure.
        """
        vals = []

        for name, typ in self._fields_:
            cont = getattr(self, name)

            if issubclass(typ, ctypesptrbase):
                if cont:
                    val = "0x{0:x}".format(ctypes.addressof(cont.contents))
                else:
                    val = "(NULL)"
            elif issubclass(typ, Enum):
                val = "|".join(str(f) for f in cont.to_flags())
            elif issubclass(typ, CStructure):
                if not typ.repr_multiline:
                    val = repr(cont)
                else:
                    val = f"<{cont.get_class_name()} ...>"
            elif issubclass(typ, ctypes.Structure):
                val = "<...>"
            else:
                val = cont

            vals.append("{name}={val}".format(name=name, val=val))

        if self.repr_multiline:
            return "\n  " + ",\n  ".join(vals) + "\n"
        else:
            return ", ".join(vals)

    def __repr__(self) -> str:
        """Pretty-print this structure."""

        return self.repr_fmt.format(
            name=self.get_class_name(), vals=self.get_repr_vals()
        )

    @property
    def ptr(self):
        return ctypes.pointer(self)

class SimpleCStructure(CStructure):
    init_funct = None
    kill_funct = None

    shallow_copyable = True
    init_fields_directly = True
