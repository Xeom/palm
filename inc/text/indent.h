#if !defined(TEXT_INDENT_H)
# define TEXT_INDENT_H
# include "buf/cur.h"
# include <stdbool.h>
# include <stdint.h>
# include <stddef.h>

indent_t indent_from_col(vec_s *chrs, chrconf_s *conf, col_t col);

col_t indent_to_col(vec_s *chrs, chrconf_s *conf, indent_t indent);

col_t byte_off_to_col(vec_s *chrs, chrconf_s *conf, size_t byteoff);

size_t byte_off_from_col(vec_s *chrs, chrconf_s *conf, col_t col);

#endif
