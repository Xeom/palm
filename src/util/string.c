#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include "util/string.h"
#include "util/macro.h"

/*** STRING COPYING ***/

char *safe_strncpy(char *dst, const char *src, size_t n)
{
    size_t len;

    if (n == 0) return NULL;

#if defined(_GNU_SOURCE)
    len = strnlen(src, n - 1);
#else
    len = MIN(n - 1, strlen(src));
#endif

    memcpy(dst, src, len);
    dst[len] = '\0';

    return dst;
}

char *zeroed_safe_strncpy(char *dst, const char *src, size_t n)
{
    dst[n - 1] = '\0';
    strncpy(dst, src, n);

    if (dst[n - 1])
    {
        dst[n - 1] = '\0';
        return NULL;
    }
    else
    {
        return dst;
    }
}

/*** STRING CONVERSION ***/

int str_to_uint(const char *str, unsigned long *dst, size_t *nchrs)
{
    char *end;
    long res;

    errno = 0;
    res = strtoul(str, &end, 0);
    if (errno != 0)
    {
        return -1;
    }

    *dst = res;

    if (nchrs) *nchrs = end - str;

    return 0;
}

int str_to_int(const char *str, long *dst, size_t *nchrs)
{
    char *end;
    long res;

    errno = 0;
    res = strtol(str, &end, 0);
    if (errno != 0)
    {
        return -1;
    }

    *dst = res;

    if (nchrs) *nchrs = end - str;

    return 0;
}

/*** STRING COMPARISON ***/

bool str_startswith(const char *str, const char *start)
{
    return strncmp(str, start, strlen(start)) == 0;
}

bool str_endswith(const char *str, const char *end)
{
    size_t slen, endlen;
    slen   = strlen(str);
    endlen = strlen(end);

    if (slen < endlen) return false;

    return strncmp(&str[slen - endlen], end, endlen) == 0;
}
