#if !defined(CMDLANG_LIB_H)
# define CMDLANG_LIB_H
# include "container/vec.h"
# include "container/list.h"
# include "container/sortedvec.h"
# include "container/table.h"

# include "cmdlang/lexer.h"

# define CMDLANG_MAX_SHORTNAME_LEN 32

struct cmd_shortname
{
    size_t len;
    char   name[CMDLANG_MAX_SHORTNAME_LEN];
    cmd_s *cmd;
};

struct cmd_lib
{
    list_s  cmds;
    table_s byname;
    table_s shortnames;
};

int cmd_lib_init(cmd_lib_s *lib);

void cmd_lib_kill(cmd_lib_s *lib);

cmd_s *cmd_lib_add_cmd(cmd_lib_s *lib, const char *name, const char *help);

int cmd_lib_add_shortname(
    cmd_lib_s *lib,
    cmd_s *cmd,
    const char *name,
    size_t len
);

cmd_s *cmd_lib_find_name(cmd_lib_s *lib, const char *name, size_t len);

cmd_s *cmd_lib_find_shortname(cmd_lib_s *lib, const char *name, size_t len);

#endif
