#if !defined(CMDLANG_PARSER_H)
# define CMDLANG_PARSER_H
# include "types.h"

# include "util/err.h"
# include "container/vec.h"

# include "cmdlang/lexer.h"

typedef enum
{
    CMDLANG_PARSER_HAS_RUN = 0x01,
    CMDLANG_PARSER_GREEDY = 0x02
} cmdlang_parser_attr_t;

struct cmdlang_parser
{
    cmdlang_lexer_s lexer;
    cmd_ctx_s   *ctx;
    cmd_lib_s  *lib;
    cmdlang_tok_s   tok;
    cmdlang_parser_attr_t attrs;
};

int cmdlang_parser_init(
    cmdlang_parser_s *parser, cmd_ctx_s *ctx, cmd_lib_s *lib, errmsg_t errmsg
) DONT_IGNORE;

void cmdlang_parser_kill(cmdlang_parser_s *parser);

int cmdlang_parser_type(cmdlang_parser_s *parser, char c, errmsg_t errmsg) DONT_IGNORE;

int cmdlang_parser_cancel(cmdlang_parser_s *parser, errmsg_t errmsg) DONT_IGNORE;

int cmdlang_parser_run(cmdlang_parser_s *parser, errmsg_t errmsg) DONT_IGNORE;

#endif
