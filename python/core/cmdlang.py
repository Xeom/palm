import ctypes

from typing import Union

from lib import lib, defs

from core.pos       import Pos
from core.cur       import Cur
from core.vec       import Vec
from core.newstruct import CStructure
from server.globals import add_global

from util.encode import strify, bytify

@add_global()
class CmdlangError(Exception):
    pass

@add_global()
class CmdlangParser(CStructure):
    class_name = "cmdlang_parser_s"

    init_funct = lib.cmdlang_parser_init
    kill_funct = lib.cmdlang_parser_kill

    def init_args(self, args, kwargs):
        def inner(ctx: "CmdCtx", lib: "CmdLib"):
            return [ctx.ptr, lib.ptr]
        return inner(*args, **kwargs)

    def type_char(self, char: bytes):
        errmsg = defs.errmsg_t()
        if lib.cmdlang_parser_type(self.ptr, char, errmsg) == -1:
            raise CmdlangError(errmsg.value)

    def type_str(self, s: Union[str, bytes]):
        s = bytify(s)
        for c in s:
            self.type_char(c)

    def has_verb(self) -> bool:
        rtn = ctypes.c_bool()
        if lib.cmdlang_parser_has_verb(self.ptr, ctypes.byref(rtn)) == -1:
            raise CmdlangError("Could not check if parser has a verb")

        return rtn.value

    def run(self):
        errmsg = defs.errmsg_t()
        if lib.cmdlang_parser_run(self.ptr, errmsg) == -1:
            raise CmdlangError(errmsg.value)

@add_global()
class CmdlangObj(CStructure):
    class_name = "cmdlang_obj_s"

    init_funct = lib.cmdlang_obj_init
    kill_funct = lib.cmdlang_obj_kill

    @classmethod
    def from_pyobject(cls, obj):
        rtn = cls()
        if isinstance(obj, (bytes, str)):
            string = bytify(obj)
            lib.cmdlang_obj_init_str(rtn.ptr, string, len(string))
        elif isinstance(obj, int):
            lib.cmdlang_obj_init_number(rtn.ptr, obj)
        elif isinstance(obj, Pos):
            lib.cmdlang_obj_init_pos(rtn.ptr, obj.ptr)
        elif isinstance(obj, Cur):
            lib.cmdlang_obj_init_cur(rtn.ptr, obj.ptr)
        else:
            raise Exception(f"Unknown cmdlang object type: {type(obj)}")

        return rtn

    def to_pyobject(self):
        if self.typ == enums.cmdlang_obj_typ_t.STR:
            return strify(bytes(self.val.str))
        elif self.typ == enums.cmdlang_obj_typ_t.NUMBER:
            return self.val.number.value
        elif self.typ == enums.cmdlang_obj_typ_t.POS:
            return self.val.pos
        elif self.typ == enums.cmdlang_obj_typ_t.CUR:
            return self.val.cur

    def to_string(self) -> str:
        rtn = Vec(ctypes.c_char)
        lib.cmdlang_obj_to_str(self.ptr, rtn.ptr)
        return strify(bytes(rtn))

    def __repr__(self) -> str:
        return f"<CmdlangObj: {self.to_string()}>"

    def __str__(self) -> str:
        return self.to_string()

@add_global()
class CmdlangTok(CStructure):
    pass
 
@add_global()
class CmdlangLexer(CStructure):
    pass
