#include "cmd/regs.h"
#include "container/vec.h"
#include "buf/buf.h"
#include "text/reg.h"
#include "log/log.h"

/* --- COMMANDS FOR USING TEXT IN REGISTERS --- *
 *                                              *
 * In this file, are commands for using         *
 * registers to store text. They can be used    *
 * as a clipboard, or a place to record         *
 * keystrokes.                                  *
 *                                              *
 * -------------------------------------------- */

/* --- Commands to Insert into Registers --- */

/** Cut to Register Command.
 *
 * Copy the current selection to a register, and delete it from the
 * current buffer.
 *
 * This command takes one optional parameter, the index of the
 * register to save the text to. Otherwise, a new register is
 * pushed to index 0.
 *
 * @param arg The index of the register to save the text to by default.
 *            A new register is pushed to index 0 if NULL.
 */
void cmd_cut_to_reg(vec_s *keys, buf_s *buf, void *arg, size_t len)
{
    int  res;
    long ind;
    vec_s str;
    cur_s cur;
    vec_init(&str, sizeof(char));

    edit_cpy(buf, &(buf->einfo), &str);

    if (buf_get_int_param(buf, 0, &ind))
    {
        res = reg_set(&str, ind);
    }
    else if (arg && len == sizeof(int))
    {
        ind = *((int *)arg);
        res = reg_set(&str, ind);
    }
    else
    {
        ind = 0;
        res = reg_push(&str, ind);
    }

    if (res == -1)
    {
        LOG(ALERT, ERR, "Could not cut to reg %ld", ind);
    }
    else
    {
        LOG(ALERT, SUCC, "Cut %lu chars to reg %ld", vec_len(&str), ind);
        edit_cut(buf, &(buf->einfo));
        buf_get_cur(buf, &cur);
        cur.sticky = false;
        buf_set_cur(buf, &cur);
    }

    vec_kill(&str);
}

/** Copy to Register Command.
 *
 * Copy the current selection to a register.
 *
 * This command takes one optional parameter, the index of the
 * register to save the text to. Otherwise, a new register is
 * pushed to index 0.
 *
 * @param arg A pointer to an integer, specifying the index
 *            of the register to save the text to by default.
 *            A new register is pushed to index 0 if NULL.
 */
void cmd_copy_to_reg(vec_s *keys, buf_s *buf, void *arg, size_t len)
{
    bool push;
    long ind;
    vec_s str;
    vec_init(&str, sizeof(char));

    if (buf_get_int_param(buf, 0, &ind))
    {
        push = false;
    }
    else if (arg && len == sizeof(int))
    {
        ind = *((int *)arg);
        push = false;
    }
    else
    {
        ind = 0;
        push = true;
    }

    edit_cpy(buf, &(buf->einfo), &str);

    if (push)
    {
        if ((push ? reg_push:reg_set)(&str, ind) == -1)
            LOG(ALERT, ERR, "Could not copy to reg %ld", ind);
        else
            LOG(ALERT, SUCC, "Copied %lu chars to reg %ld", vec_len(&str), ind);
    }

    vec_kill(&str);
}

/** Append to Register Command.
 *
 * This command will append the keys that it is called with to a
 * register.
 *
 * @param arg  A pointer to an integer, specifying which register the
 *             keys should be pushed to.
 * @param keys A vector of the keys to be pushed.
 */
void cmd_append_to_reg(vec_s *keys, buf_s *buf, void *arg, size_t len)
{
    long ind;

    if (arg && len)
        ind = *((int *)arg);
    else
        ind = 0;

    VEC_FOREACH(keys, char *, c)
    {
        if (reg_chr(*c, ind) == -1)
        {
            LOG(ALERT, ERR, "Could not append character to reg %ld", ind);
            break;
        }
    }
}

/* --- Commands to Insert into Buffers --- */

/** PopPaste Command.
 *
 * This command will pop the contents of a register, and insert them
 * into the current buffer. It's can take one parameter, telling it
 * which register to insert.
 *
 * @param arg A pointer to an integer, describing the default register
 *            to pop.
 */
void cmd_poppaste_reg(vec_s *keys, buf_s *buf, void *arg, size_t len)
{
    long ind;
    vec_s str;
    cur_s cur;
    vec_init(&str, sizeof(char));

    buf_get_cur(buf, &cur);

    if (arg && len)
        ind = *((int *)arg);
    else
        ind = 0;

    buf_get_int_param(buf, 0, &ind);

    if (reg_pop(&str, ind) == -1)
    {
        LOG(ALERT, ERR, "Could not pop from reg %ld", ind);
    }
    else
    {
        edit_ins(buf, &(buf->einfo), &str);
        LOG(ALERT, SUCC, "Popped %lu chars from reg %ld", vec_len(&str), ind);
    }
    vec_kill(&str);

    memcpy(&(cur.sec), &(cur.pri), sizeof(pos_s));
    cur.sticky = false;
    buf_set_cur(buf, &cur);
}

/** Paste Command.
 *
 * This command will insert the contents of a register
 * into the current buffer. It's can take one parameter, telling it
 * which register to insert.
 *
 * @param arg A pointer to an integer, describing the default register
 *            to pop.
 */
void cmd_paste_reg(vec_s *keys, buf_s *buf, void *arg, size_t len)
{
    long ind;
    vec_s str;
    cur_s cur;
    vec_init(&str, sizeof(char));

    buf_get_cur(buf, &cur);

    if (arg && len)
        ind = *((int *)arg);
    else
        ind = 0;

    buf_get_int_param(buf, 0, &ind);

    if (reg_get(&str, ind))
    {
        LOG(ALERT, ERR, "Could not get contents of reg %ld", ind);
    }
    else
    {
        edit_ins(buf, &(buf->einfo), &str);
        LOG(ALERT, SUCC, "Inserted %lu chars from reg %ld", vec_len(&str), ind);
    }
    vec_kill(&str);

    memcpy(&(cur.sec), &(cur.pri), sizeof(pos_s));
    cur.sticky = false;
    buf_set_cur(buf, &cur);
}


