#include <time.h>
#include <sys/time.h>
#include "log/log.h"
#include "editor.h"
#include "util/pool.h"

static void *pool_thread(void *arg);
static int pool_handout(pool_s *pool);
static void pool_thread_init(pool_thread_s *thread, pool_s *pool);
static void *pool_thread_thread(void *arg);
static void pool_thread_kill(pool_thread_s *thread);
static void pool_job_run(pool_job_s *job);

int pool_init(pool_s *pool, int nthreads)
{
    LOG(INFO, INFO, "Initializing threadpool at %p", (void *)pool);

    vec_init(&(pool->threads), sizeof(pool_thread_s));
    vec_init(&(pool->pending), sizeof(pool_job_s *));

    if (!vec_ins(&(pool->threads), 0, nthreads, NULL))
        return -1;

    VEC_FOREACH(&(pool->threads), pool_thread_s *, thread)
    {
        pool_thread_init(thread, pool);
    }

    pthread_mutex_init(&(pool->mtx), NULL);
    pthread_cond_init(&(pool->wait), NULL);
    pool->alive = true;

    if (pthread_create(&(pool->master), NULL, pool_thread, pool) != 0)
        return -1;

    return 0;
}

void pool_kill(pool_s *pool)
{
    LOG(INFO, INFO, "Killing thread pool at %p", (void *)pool);

    pool->alive = false;
    pthread_join(pool->master, NULL);

    pthread_mutex_destroy(&(pool->mtx));
    pthread_cond_destroy(&(pool->wait));

    VEC_FOREACH(&(pool->threads), pool_thread_s *, thread)
    {
        pool_thread_kill(thread);
    }

    vec_kill(&(pool->pending));
    vec_kill(&(pool->threads));

    LOG(INFO, SUCC, "Thread pool at %p killed", (void *)pool);
}

static void *pool_thread(void *arg)
{
    pool_s *pool;
    pool = arg;

    LOG(INFO, SUCC, "Thread pool at %p manager thread started", (void *)pool);

    editor_mask_signals();

    pthread_mutex_lock(&(pool->mtx));
    while (pool->alive)
    {
        struct timeval  now;
        struct timespec wakeup;
        long usec;

        while (1)
        {
            int rtn;
            rtn = pool_handout(pool);
            if (rtn != 1)
                break;
        }

        gettimeofday(&now, NULL);
        usec = now.tv_sec * 1000000 + now.tv_usec;
        usec += 10000;
        wakeup.tv_sec  = (usec / 1000000);
        wakeup.tv_nsec = (usec % 1000000) * 1000;

        pthread_cond_timedwait(&(pool->wait), &(pool->mtx), &wakeup);
    }
    pthread_mutex_unlock(&(pool->mtx));

    LOG(INFO, SUCC, "Thread pool at %p manager thread stopped", (void *)pool);

    return NULL;
}

static int pool_handout(pool_s *pool)
{
    pool_job_s **job;

    /* If there are no jobs to hand out, return 0 */
    if (vec_len(&(pool->pending)) == 0)
        return 0;

    job = vec_get(&(pool->pending), 0);

    /* If we have a job, go through each thread until we find one that is ready */
    VEC_FOREACH(&(pool->threads), pool_thread_s *, thread)
    {
        if (thread->done)
        {
            pthread_mutex_lock(&(thread->mtx));
            thread->done = false;
            thread->job  = *job;
            pthread_cond_broadcast(&(thread->wait));
            pthread_mutex_unlock(&(thread->mtx));
            vec_del(&(pool->pending), 0, 1);
            return 1;
        }
    }

    /* There are jobs but no threads */
    return 0;
}

void pool_add_job(pool_s *pool, pool_job_s *job)
{
    pthread_mutex_lock(&(pool->mtx));
    job->done = false;
    vec_app(&(pool->pending), &job);
    pthread_cond_broadcast(&(pool->wait));
    pthread_mutex_unlock(&(pool->mtx));
}

static void pool_thread_init(pool_thread_s *thread, pool_s *pool)
{
    pthread_mutex_init(&(thread->mtx), NULL);
    pthread_cond_init(&(thread->wait), NULL);

    thread->alive = true;
    thread->done  = true;
    thread->pool  = pool;
    thread->job   = NULL;

    pthread_create(&(thread->thread), NULL, pool_thread_thread, thread);
}

static void pool_thread_kill(pool_thread_s *thread)
{
    thread->alive = false;
    pthread_join(thread->thread, NULL);

    pthread_mutex_destroy(&(thread->mtx));
    pthread_cond_destroy(&(thread->wait));
}

static void *pool_thread_thread(void *arg)
{
    pool_thread_s *thread;
    thread = arg;

    editor_mask_signals();

    pthread_mutex_lock(&(thread->mtx));
    while (thread->alive)
    {
        long usec;
        struct timeval  now;
        struct timespec wakeup;

        if (!(thread->done))
        {
            pool_s *pool;
            pthread_mutex_unlock(&(thread->mtx));
            pool_job_run(thread->job);
            thread->done = true;

            pool = thread->pool;
            pthread_mutex_lock(&(pool->mtx));
            pthread_cond_broadcast(&(pool->wait));
            pthread_mutex_unlock(&(pool->mtx));
            pthread_mutex_lock(&(thread->mtx));
        }

        if (thread->done)
        {
            gettimeofday(&now, NULL);
            usec = now.tv_sec * 1000000 + now.tv_usec;
            usec += 10000;
            wakeup.tv_sec  = (usec / 1000000);
            wakeup.tv_nsec = (usec % 1000000) * 1000;

            pthread_cond_timedwait(&(thread->wait), &(thread->mtx), &wakeup);
        }
    }
    pthread_mutex_unlock(&(thread->mtx));

    return NULL;
}

void pool_job_init(pool_job_s *job, void (*funct)(void *), void *param)
{
    job->param = param;
    job->funct = funct;
    job->done  = true;

    pthread_mutex_init(&(job->mtx), NULL);
    pthread_cond_init(&(job->wait), NULL);
}

static void pool_job_run(pool_job_s *job)
{
    (job->funct)(job->param);
    pthread_mutex_lock(&(job->mtx));
    job->done = true;
    pthread_cond_broadcast(&(job->wait));
    pthread_mutex_unlock(&(job->mtx));
}

void pool_job_kill(pool_job_s *job)
{
    pthread_mutex_destroy(&(job->mtx));
    pthread_cond_destroy(&(job->wait));
}

bool pool_job_await_tout(pool_job_s *job, long ns)
{
    struct timespec tout;
    struct timeval  now;

    bool rtn;

    gettimeofday(&now, NULL);

    tout.tv_sec   = now.tv_sec;
    tout.tv_nsec  = now.tv_usec * 1000;
    tout.tv_nsec += ns;

    tout.tv_sec  += now.tv_usec / 1e9;
    tout.tv_nsec %= (long)1e9;

    pthread_mutex_lock(&(job->mtx));
    while (job->done == false)
    {
        if (pthread_cond_timedwait(&(job->wait), &(job->mtx), &tout) == -1)
            break;
    }

    rtn = job->done;
    pthread_mutex_unlock(&(job->mtx));

    return rtn;
}

void pool_job_await(pool_job_s *job)
{
    pthread_mutex_lock(&(job->mtx));
    while (job->done == false)
    {
        pthread_cond_wait(&(job->wait), &(job->mtx));
    }
    pthread_mutex_unlock(&(job->mtx));
}

