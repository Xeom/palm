#if !defined(DISPLAY_FRAMEBUF_H)
# define DISPLAY_FRAMEBUF_H
# include "container/vec.h"
# include "text/chr.h"
# include "types.h"

struct framebuf
{
    unsigned int w, h;
    long tmpy, tmpoff;
    vec_s chrs;
};

struct rect
{
    int x, y;
    unsigned int w, h;
};


void rect_overlap(rect_s *rect, rect_s *other);

void framebuf_init(framebuf_s *fb);

void framebuf_kill(framebuf_s *fb);

void framebuf_dims_rect(framebuf_s *fb, rect_s *rect);

void framebuf_trim_rect(framebuf_s *fb, rect_s *rect);

void framebuf_cpy(framebuf_s *fb, framebuf_s *oth);

chr_s *framebuf_get(framebuf_s *fb, unsigned int x, unsigned int y);

void framebuf_resize(framebuf_s *fb, unsigned int w, unsigned int h);

void framebuf_stamp(framebuf_s *dst, rect_s *dstrect, framebuf_s *src, rect_s *srcrect);

void framebuf_set(framebuf_s *f, rect_s *rect, chr_s *mem);

void framebuf_fill(framebuf_s *f, rect_s *rect, chr_s *c);

void framebuf_print_diff(framebuf_s *fb, framebuf_s *prevfb, tty_s *tty);

#endif
