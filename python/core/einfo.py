import libtypes
import ctypes
from lib import lib, enums
from .newstruct import CStructure
from .pos       import Pos

class EditInfo(CStructure):
    class_name = "edit_info_s"

    init_funct = lib.edit_info_init
    kill_funct = lib.edit_info_kill

    def init_args(self, args, kwargs):
        def inner(buf):
            return [buf.ptr]

        return inner(*args, **kwargs)

    def set_decoder_attrs(self, attrs):
        lib.edit_set_decoder_attrs(self.ptr, enums.decoder_attr_t(attrs))

