#include "util/macro.h"
#include "buf/cur.h"
#include "buf/buf.h"
#include "log/log.h"
#include "wm/wm.h"

bool pos_gt(const pos_s *a, const pos_s *b)
{
    return (a->row > b->row) || (a->row == b->row && a->col > b->col);
}

bool pos_lt(const pos_s *a, const pos_s *b)
{
    return (a->row < b->row) || (a->row == b->row && a->col < b->col);
}

int pos_cmp(const pos_s *a, const pos_s *b)
{
    if (a->row != b->row)
        return a->row - b->row;
    return a->col - b->col;
}

int cur_cmp(const cur_s *a, const cur_s *b)
{
    int res;
    res = pos_cmp(&(a->pri), &(b->pri));
    if (res != 0) return res;

    res = pos_cmp(&(a->sec), &(b->sec));
    if (res != 0) return res;

    return a->type - b->type;
}

pos_s *pos_min(pos_s *a, pos_s *b)
{
    return pos_gt(a, b) ? b : a;
}

pos_s *pos_max(pos_s *a, pos_s *b)
{
    return pos_gt(a, b) ? a : b;
}

void pos_shift(pos_s *pos, pos_s *from, pos_s *to)
{
    if (pos_gt(from, pos) && pos_gt(to, pos))
    {
        return;
    }

    if (pos->row == from->row && pos->col >= from->col)
    {
        pos->row  = to->row;
        pos->col += to->col - from->col;
        return;
    }

    if (pos_gt(from, pos) && pos_gt(pos, to))
    {
        pos->row = to->row;
        pos->col = to->col;
        return;
    }

    if (pos->row > from->row)
    {
        pos->row += to->row - from->row;
        return;
    }

    pos->col = MAX(0, pos->col);
    pos->row = MAX(0, pos->row);
}

void cur_init(cur_s *cur)
{
    cur->type   = CUR_TYPE_CHR;
    cur->sticky = false;

    memcpy(&(cur->pri), &(pos_s){0, 0}, sizeof(pos_s));
    memcpy(&(cur->sec), &(pos_s){0, 0}, sizeof(pos_s));
}

void cur_shift(cur_s *cur, pos_s *from, pos_s *to)
{
    pos_shift(&(cur->pri), from, to);
    pos_shift(&(cur->sec), from, to);
}

void pos_correct(pos_s *pos, buf_s *buf)
{
    /* This is here so we can use a dummy NULL buffer during testing. */
    if (!buf) return;

    pos->row = MAX(0, MIN(pos->row, (long)buf_len(buf) - 1));
    pos->col = MAX(0, MIN(pos->col, (long)buf_line_len(buf, pos->row)));
}

static inline bool end_of_word(vec_s *line, size_t ind)
{
    chr_s *c, *prev;

    c = vec_get(line, ind);
    if (ind > 0)
        prev = vec_get(line, ind - 1);
    else
        prev = NULL;

    return (!c || chr_is_whitespace(c)) && (prev && !chr_is_whitespace(prev));
}

static inline bool start_of_word(vec_s *line, size_t ind)
{
    chr_s *c, *prev;

    c = vec_get(line, ind);
    if (ind > 0)
        prev = vec_get(line, ind - 1);
    else
        prev = NULL;

    return (c && !chr_is_whitespace(c)) && (!prev || chr_is_whitespace(prev));
}

void pos_move(pos_s *pos, cur_dir_t dir, int n, buf_s *buf)
{
    vec_s line;
    long buflen, linelen;
    buflen = MAX(1, buf_len(buf));

    switch (dir)
    {
    case CUR_DIR_UP:
        pos->row -= n;
        break;

    case CUR_DIR_DOWN:
        pos->row += n;
        break;

    case CUR_DIR_LEFT:
        linelen = buf_line_len(buf, pos->row);
        while (n > pos->col && pos->row > 0)
        {
            n        -= 1 + pos->col;
            pos->row -= 1;

            linelen = buf_line_len(buf, pos->row);
            pos->col = linelen;
        }

        pos->col = MIN(linelen, MAX(pos->col - n, 0));
        break;

    case CUR_DIR_RIGHT:
        linelen = buf_line_len(buf, pos->row);
        while (n + pos->col > linelen && pos->row < buflen)
        {
            n        -= MAX(linelen - pos->col + 1, 1);
            pos->row += 1;

            linelen = buf_line_len(buf, pos->row);
            pos-> col = 0;
        }

        pos->col = MAX(0, MIN(pos->col + n, linelen));
        break;

    case CUR_DIR_NWORD:
        vec_init(&line, sizeof(chr_s));
        buf_get_line(buf, pos->row, &line);

        for (; n > 0; --n)
        {
            pos_s prev;

            do
            {
                prev = *pos;
                pos_move(pos, CUR_DIR_RIGHT, 1, buf);

                if (pos->row != prev.row)
                    buf_get_line(buf, pos->row, &line);

                if (prev.row == pos->row && prev.col == pos->col)
                {
                    n = 0;
                    break;
                }
            } while (!end_of_word(&line, pos->col));
        }

        vec_kill(&line);
        break;

    case CUR_DIR_PWORD:
        vec_init(&line, sizeof(chr_s));
        buf_get_line(buf, pos->row, &line);

        for (; n > 0; --n)
        {
            pos_s prev;

            do
            {
                prev = *pos;
                pos_move(pos, CUR_DIR_LEFT, 1, buf);

                if (pos->row != prev.row)
                    buf_get_line(buf, pos->row, &line);

                if (prev.row == pos->row && prev.col == pos->col)
                {
                    n = 0;
                    break;
                }
            } while (!start_of_word(&line, pos->col));
        }

        vec_kill(&line);
        break;
    }

    pos_correct(pos, buf);
}

void cur_correct(cur_s *cur, buf_s *buf)
{
    pos_correct(&(cur->pri), buf);
    pos_correct(&(cur->sec), buf);
}

void cur_move(cur_s *cur, cur_dir_t dir, int n, buf_s *buf)
{
    if (cur->sticky)
    {
        pos_move(&(cur->pri), dir, n, buf);
    }
    else
    {
        pos_move(&(cur->pri), dir, n, buf);
        pos_move(&(cur->sec), dir, n, buf);
    }
}

bool cur_get_selected(cur_s *cur, pos_s pos, buf_s *buf)
{
    cur_s line;

    if (cur_get_line(cur, &line, buf, pos.row))
    {
        if (pos.col >= cur_min(&line)->col && pos.col <= cur_max(&line)->col)
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

bool cur_get_line(cur_s *cur, cur_s *line, buf_s *buf, row_t row)
{
    pos_s *max, *min;

    min = cur_min(cur);
    max = cur_max(cur);

    line->pri  = (pos_s){ .row = row, .col = 0 };
    line->sec  = (pos_s){ .row = row, .col = buf_line_len(buf, row) };
    line->type = CUR_TYPE_CHR;

    if (row < min->row || row > max->row)
        return false;

    if (cur->type == CUR_TYPE_BLOCK)
    {
        line->pri.col = MIN(min->col, max->col);
        line->sec.col = MAX(min->col, max->col);
    }

    if (cur->type == CUR_TYPE_CHR && row == max->row)
        line->sec.col = max->col;

    if (cur->type == CUR_TYPE_CHR && row == min->row)
        line->pri.col = min->col;

    cur_correct(line, buf);

    return true;
}

cur_hl_t cur_max_hl(cur_hl_t a, cur_hl_t b)
{
    cur_hl_t order[] = {
        CUR_HL_PRI,
        CUR_HL_ALT_PRI,
        CUR_HL_DEAD_PRI,
        CUR_HL_SEC,
        CUR_HL_ALT_SEC,
        CUR_HL_DEAD_SEC,
        CUR_HL_SEL,
        CUR_HL_ALT_SEL,
        CUR_HL_DEAD_SEL,
        CUR_HL_PRIFAKE,
        CUR_HL_ALT_PRIFAKE,
        CUR_HL_DEAD_PRIFAKE,
        CUR_HL_SECFAKE,
        CUR_HL_ALT_SECFAKE,
        CUR_HL_DEAD_SECFAKE,
        CUR_HL_NONE,
        CUR_HL_END
    };

    size_t ind;
    for (ind = 0; ind < DIM(order); ++ind)
    {
        if (order[ind] == a)
            return a;
        if (order[ind] == b)
            return b;
    }

    return CUR_HL_END;
}

cur_hl_t cur_get_pos_hl(cur_s *cur, pos_s pos, buf_s *buf)
{
    long len;
    len = buf_line_len(buf, pos.row);
    pos_s *pri, *sec, *pos1, *pos2;

    pri  = &cur->pri;
    sec  = &cur->sec;
    pos1 = cur_min(cur);
    pos2 = cur_max(cur);

    if (pos.row == pri->row)
    {
        if (pos.col == MIN(pri->col, len))
            return CUR_HL_PRI;
        else if (pos.col == pri->col)
            return CUR_HL_PRIFAKE;
    }

    if (pos.row == sec->row)
    {
        if (pos.col == MIN(sec->col, len))
            return CUR_HL_SEC;
        else if (pos.col == sec->col)
            return CUR_HL_SECFAKE;
    }

    if (cur_get_selected(cur, pos, buf))
        return CUR_HL_SEL;

    if (pos.row == pos1->row && pos1->col > pos.col)
        return CUR_HL_NONE;

    if (pos.row == pos2->row && pos2->col > pos.col)
        return CUR_HL_NONE;

    return CUR_HL_END;
}

const char *cur_type_name(cur_type_t type)
{
    switch (type)
    {
    case CUR_TYPE_LINE: return "LINE";
    case CUR_TYPE_BLOCK: return "BLOCK";
    case CUR_TYPE_CHR:   return "CHR";
    }

    return "UNKNOWN";
}

void cur_split_by_line(cur_s *cur, vec_s *alts)
{
    long ln;
    pos_s *max, *min;

    max = cur_max(cur);
    min = cur_min(cur);

    if (cur->sec.col == cur->pri.col)
        cur->sticky = false;

    for (ln = 1 + min->row; ln <= max->row; ++ln)
    {
        cur_s alt;
        memcpy(&alt, cur, sizeof(cur_s));
        alt.pri  = (pos_s){ .row = ln, .col = cur->pri.col };
        alt.sec  = (pos_s){ .row = ln, .col = cur->sec.col };
        vec_app(alts, &alt);
    }

    max->row = min->row;
}

void buf_expand_cur(buf_s *buf, cur_s *cur)
{
    size_t min, max;

    /* This is here so we can use NULL as a dummy buffer *
     * during testing.                                   */
    if (!buf) return;

    min = MIN(cur->pri.row, cur->sec.row);
    max = MAX(cur->pri.row, cur->sec.row);

    size_t ln;
    for (ln = min; ln <= max; ++ln)
    {
        line_s *line;
        line = vec_get(&(buf->lines), ln);
        if (line)
            line_needs_expansion(line);
    }

    wm_handle_buf_update(buf_wm(buf), buf->winind, buf, min, max);
}
