#if !defined(SEARCH_SEARCH_H)
# define SEARCH_SEARCH_H
# define PCRE2_CODE_UNIT_WIDTH 8
# include <pcre2.h>

# include "types.h"
# include "buf/cur.h"
# include "container/vec.h"
# include "util/pool.h"
# include "buf/buf.h"
# include "match/set.h"

typedef enum
{
    REPLACE_REGEX    = 0x01,
    REPLACE_ESCAPES  = 0x02,
    REPLACE_KEEPCASE = 0x04
} replace_attr_t;

/**
 * A structure used to contain the result of a search.
 *
 */
struct search_result
{
    pos_s start; /**< The start of the match.                   */
    pos_s end;   /**< The character after the end of the match. */
    char  flag;  /**< A user-defined flag.                      */
};

struct search
{
    match_set_s matchset;

    search_result_s result;

    buf_s *buf;
    pos_s  off;

    pool_job_s job;

    bool done;
    bool found;
    bool backward;
};

struct replace
{
    search_s       search;
    replace_attr_t attrs;
};

int search_init(search_s *search);

int search_add_needle(search_s *search, match_attr_t attr, const char *str, char flag);

void search_kill(search_s *search);

void search_start(search_s *search, buf_s *buf, pos_s off, pool_s *pool, bool backward);

bool search_get_result(search_s *search, search_result_s *result);

bool search_is_done(search_s *search);

void search_continue(search_s *search, pool_s *pool);

#endif
