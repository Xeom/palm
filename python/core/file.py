import libtypes
import ctypes
from lib import lib, defs, enums
from .newstruct import CStructure

class File(CStructure):
    class_name = "file_s"

    init_funct = lib.file_init
    kill_funct = lib.file_kill

    def init_args(self, args, kwargs):
        def inner(pollctx):
            return [pollctx.ptr]

        return inner(*args, **kwargs)

    def pause(self):
        lib.file_pause(self.ptr)

    def set_file(self, fd, name=None):
        if hasattr(fd, "fileno"):
            self.filehandle = fd
            fd = fd.fileno()
        if name == None:
            name = "<fd-{}>".format(fd)
        if isinstance(name, str):
            name = name.encode("utf-8")

        lib.file_set_fd(self.ptr, fd, name)

    def set_attrs(self, attrs):
        lib.file_set_attrs(self.ptr, enums.decoder_attr_t(attrs))

    @property
    def line_range(self):
        return (
            self.startln,
            self.endln
        )

    @line_range.setter
    def line_range(self, val):
        start = val[0]
        end   = val[1]

        if start == None:
            start = 0
        if end == None:
            end = 2 ** (ctypes.sizeof(ctypes.c_long) * 8 - 1) - 1

        start, end = min((start, end)), max((start, end))

        lib.file_set_line_range(self.ptr, start, end)

    def read(self, param, rcb=lib.file_read_to_buf_cb, rescb=None):
        rcb = ctypes.cast(rcb, defs.file_rcb_t)
        rescb = ctypes.cast(rescb, defs.file_cb_t)

        lib.file_read(self.ptr, rcb, rescb, param)

    def write(self, param, wcb=lib.file_write_from_buf_cb, rescb=None):
        wcb = ctypes.cast(rcb, defs.file_wcb_t)
        rescb = ctypes.cast(rescb, defs.file_cb_t)

        lib.file_write(self.ptr, wcb, rescb, param)

    def read_to_buf(self, buf):
        self.read(buf.ptr)

    def write_from_buf(self, buf):
        self.write(buf.ptr)

