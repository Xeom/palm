import libtypes
import ctypes
from lib import lib, enums
from .structure import Structure
from .chrvec    import ChrVec

class Encoder(Structure):
    typ        = libtypes.encoder_s
    init_funct = lib.encoder_init
    kill_funct = None

    def init_args(self, attrs):
        return [enums.decoder_attr_t(attrs)]

    def chr(self, c, out):
        return lib.encoder_chr(self.ptr, c.ptr, out.ptr)

    def nl(self, out):
        return lib.encoder_nl(self.ptr, out.ptr)
