from server.globals import add_global

from lib.enums import decoder_attr_t

from .chr import Chr
from .vec import Vec

import ctypes

@add_global()
class ChrVec(Vec):
    class_name = "vec_s<chr_s>"

    def init_args(self, args, kwargs):
        return super().init_args([Chr], {})

    def set_font(self, font, ind, n):
        for i in range(ind, ind + n):
            self[i].contents.set_font(font)

    @classmethod
    def from_str(cls, string, attrs=decoder_attr_t.UTF8) -> "ChrVec":
        return cls.from_fmt(string, attrs)

    @classmethod
    def from_fmt(cls, string, attrs=None) -> "ChrVec":
        if attrs == None:
            attrs = decoder_attr_t.UTF8 | decoder_attr_t.FNT

        decoder = Decoder(attrs)

        return decoder.str(string)

    def ins_str(self, ind, s):
        if isinstance(s, bytes):
            s = s.decode("utf-8")

        for c in s:
            struct = Chr.new(c)
            self.ins(ind, 1, struct.ptr)
            ind += 1

    def to_str(self, attrs=decoder_attr_t.UTF8):
        return self.to_fmt(attrs)

    def to_fmt(self, attrs=None):
        if attrs == None:
            attrs = libtypes.decoder_attr_t.UTF8 | libtypes.decoder_attr_t.FNT

        res = Vec(ctypes.c_char)
        encoder = Encoder(attrs)
        for p in self:
            encoder.chr(p.contents, res)

        return bytes(res)

    def __str__(self):
        return self.to_str().decode("utf-8")

    def __bytes__(self):
        return self.to_str()

    def get_repr_vals(self):
        vals = []
        for p in self:
            c = p.contents
            if c.font == 0:
                vals.append("{text}".format(text=c.get_text())[1:])
            else:
                vals.append("{text}:{font}".format(text=c.get_text(), font=c.font)[1:])

        return ",".join(vals)

from .decoder import Decoder
from .encoder import Encoder
