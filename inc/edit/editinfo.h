#if !defined(EDIT_INFO_H)
# define EDIT_INFO_H
# include "types.h"
# include "buf/cur.h"
# include "text/decoder.h"
# include "text/encoder.h"

typedef enum
{
    EDIT_MODE_NORMAL,
    EDIT_MODE_PROMPT,
    EDIT_MODE_MENU,
    EDIT_MODE_QUESTION,
} edit_mode_t;

typedef enum
{
    EDIT_RO         = 0x01,
    EDIT_OW         = 0x02,
    EDIT_ALTS       = 0x04,
    EDIT_AUTOINDENT = 0x08,
    EDIT_AUTORSTRIP = 0x10,
    EDIT_USE_TABS   = 0x20,
} edit_attr_t;

struct edit_info
{
    buf_s *buf;
    edit_mode_t mode;
    edit_attr_t attrs;
    unsigned shiftwidth;
    row_t base;
    chrconf_s *chrconf;
    decoder_s insdecoder;
    encoder_s cpyencoder;
    decoder_attr_t encoding;
};

void edit_info_init(edit_info_s *einfo, buf_s *buf);

void edit_info_kill(edit_info_s *einfo);

void edit_set_decoder_attrs(edit_info_s *einfo, decoder_attr_t attrs);

#endif
