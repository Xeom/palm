#if !defined(MATCH_SET_H)
# define MATCH_SET_H
# include "types.h"
# include "container/vec.h"
# include "match/match.h"

/**
 * @file set.h
 * @brief Functions implementing string matching with sets of patterns.
 *
 */

/**
 * A structure containing a set of matching patterns.
 *
 * This can be used to search a series of bytes for the next
 * pattern that matches.
 *
 */
struct match_set
{
    vec_s matches; /**< A vector of matches. */
};

/**
 * Initailze an empty set of matches.
 *
 * @param set A pointer to the @c match_set_s structure to initialize.
 *
 * @return 0 on success, -1 on error.
 *
 */
int match_set_init(match_set_s *set);

/**
 * Release resources allocated by a @c match_set_s structure.
 *
 * This must be called on any @c match_set_s structure that has had
 * match_set_init() called on it.
 *
 * @param set A pointer to the @c match_set_s structure to kill.
 *
 */
void match_set_kill(match_set_s *set);

/**
 * Add a pattern to search for to a match set.
 *
 * These arguments have the same meaning as those to match_init().
 *
 * @param set  A pointer to the match set to add a pattern to.
 * @param attr The pattern attributes.
 * @param str  The pattern itself, as a NULL-terminated string.
 *
 * @return A pointer to the new @c match_s structure, on success,
 *         otherwise, NULL.
 *
 * NOTE: The pointer returned by this function is invalid after another
 *       call to this function.
 *
 */
match_s *match_set_add(match_set_s *set, match_attr_t attr, const char *str);

/**
 * Check whether a match exists for any of the patterns in a set.
 *
 * This function is similar to match_check().
 *
 * @param set  A pointer to the match set containing functions to check for.
 * @param text A vector of bytes containing the text to check.
 * @param off  The offset that the hypothetical match would start at.
 * @param res  If a match exists, information about it will be returned here.
 *
 * @return -1 on error, 0 on no match, 1 on match found.
 *
 */
int match_set_check(
    match_set_s *set,
    vec_s *text,
    size_t off,
    match_result_s *res
);

/**
 * Search a string matches with a set of patterns.
 *
 * @param set  A pointer to the match set containing functions to look for.
 * @param text A vector of bytes containing the text to check.
 * @param off  The offset to start searching from.
 * @param res  If a match is found, information about it will be returned here.
 * @param backward If true, search backwards from off, rather than forwards.
 *
 * @return -1 on error, 0 on no match, 1 on match found.
 *
 */
int match_set_search(
    match_set_s *set,
    vec_s *text,
    size_t off,
    match_result_s *res,
    bool backward
);

#endif
