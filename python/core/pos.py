from lib import lib, enums
from .newstruct import SimpleCStructure

from typing import TYPE_CHECKING

from .cur import Cur

if TYPE_CHECKING:
    from .buffer import Buffer

class Pos(SimpleCStructure):
    class_name = "pos_s"
    repr_multiline = False

    @classmethod
    def at(cls, pos, secarg="None"):
        if secarg != "None":
            pos = (pos, secarg)

        rtn = cls()
        rtn.goto(pos)

        return rtn

    def goto(self, pos):
        if isinstance(pos, (int, float)):
            if pos < 0:
                raise Exception("Cannot goto a negative row")
            self.row = pos
        elif isinstance(pos, (list, tuple)) and len(pos) == 2:
            if pos[0] != None:
                if pos[0] < 0:
                    raise Exception("Cannot goto a negative row")
                self.row = pos[0]
            
            if pos[1] != None:
                if pos[1] < 0:
                    raise Exception("Cannot goto a negative column")
                self.col = pos[1]
        elif isinstance(pos, Pos):
            self.copy_from(pos)
        elif isinstance(pos, Cur):
            self.copy_from(pos.pri)
        elif pos == None:
            pass
        else:
            raise Exception("Unknown type for position to goto: {}".format(pos))

    def __lt__(self, oth):
        if oth.row == self.row:
            return self.col < oth.col
        else:
            return self.row < oth.row

    def __gt__(self, oth):
        if oth.row == self.row:
            return self.col > oth.col
        else:
            return self.row > oth.row

    def __eq__(self, oth):
        if isinstance(oth, Pos):
            return self.col == oth.col and \
                   self.row == oth.row
        else:
            return False

    def move(self, direction, n: int, buf: "Buffer"):
        direction = cur_dir_t(direction)
        lib.pos_move(self.ptr, direction, n, buf.ptr)
