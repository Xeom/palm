#if !defined(TABLE_H)
# define TABLE_H
# include "vec.h"
# include "types.h"
# include <stdint.h>

struct table_chain
{
    table_chain_s *next;
    size_t keylen;
    size_t vallen;
    char data[];
};

struct table
{
    size_t nitems;
    size_t nchains;
    vec_s chains;
};

#define TABLE_MIN_SIZE 8

#define TABLE_FOREACH(table, chain) \
    table_chain_s *chain = NULL;    \
    for (size_t _ind = 0;           \
         (chain = table_next(table, &_ind, chain)); )

uint64_t table_hash(const char *mem, size_t n);

void table_init(table_s *t);

void table_kill(table_s *t);

size_t table_len(table_s *t);

void *table_chain_key(table_chain_s *chain, size_t *len);

void *table_chain_val(table_chain_s *chain, size_t *len);

void *table_get(table_s *t, const void *key, size_t keylen, size_t *vallen);

void table_set(table_s *t, const void *key, size_t keylen, const void *val, size_t vallen);

int  table_del(table_s *t, const void *key, size_t keylen);

table_chain_s *table_next(table_s *t, size_t *ind, table_chain_s *chain);

#endif
