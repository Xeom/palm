import libtypes
import ctypes
from lib import lib, enums

from .structure import Structure
from .pos       import Pos
from .cur       import Cur

from typing import List, Union

from util.encode import bytify

class Search(Structure):
    typ        = libtypes.search_s
    init_funct = lib.search_init
    kill_funct = lib.search_kill

    def add_needle(
        self,
        needle: Union[str, bytes],
        attrs=0,
        flag:   Union[str, bytes]="0",
    ):
        attrs = self.get_attrs(attrs)

        needle = bytify(needle)
        flag   = bytify(flag)

        if lib.search_add_needle(self.ptr, attrs, needle, flag) == -1:
            raise Exception("Could not add search needle")

    def get_attrs(self, attrs=0):
        flagnames = {
            "r": "REGEX",
            "v": "VERBOSE",
            "e": "ESCAPES",
            "i": "ANYCASE"
        }

        if isinstance(attrs, str) and all(c in flagnames for c in attrs):
            attrs = [flagnames[c] for c in attrs]

        return enums.match_attr_t(attrs)

    def start(self, buf, off=None, backward=False):
        if off == None:
            off = Pos.at(0, 0)

        self.buf = buf
        lib.search_start(
            self.ptr, buf.ptr, off.ptr.contents,
            buf.editor.pool.ptr, backward
        )

    def cont(self):
        lib.search_continue(self.ptr, self.buf.editor.pool.ptr)

    def is_done(self) -> bool:
        return lib.search_is_done(self.ptr)

    def get_result(self):
        rtn = SearchResult()
        if lib.search_get_result(self.ptr, rtn.ptr):
            return rtn
        return None

    @property
    def attrs(self) -> List[str]:
        return self["attrs"].to_flags()

    @property
    def backward(self) -> bool:
        return self["backward"]

class SearchResult(Structure):
    typ        = libtypes.search_result_s
    init_funct = None
    kill_funct = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start = self["start"]
        self.end   = self["end"]

    def zero_len(self):
        return self.start == self.end

    def get_cur(self):
        rtn = Cur()
        rtn.type.set("CHR")
        rtn.pri.copy_from(self.start)
        rtn.sec.copy_from(self.end)

        if not self.zero_len():
            rtn.sec.col -= 1

        if rtn.pri != rtn.sec:
            rtn.sticky = True

        return rtn

    @property
    def flag(self):
        return self["flag"]
