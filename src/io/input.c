#include <unistd.h>
#include <errno.h>
#include "bind/bufbind.h"
#include "container/vec.h"
#include "io/input.h"
#include "io/io.h"
#include "log/log.h"
#include "buf/buf.h"
#include "event/poll.h"
#include "event/event.h"
#include "editor.h"

static void input_key(input_s *inp, char c);
static void input_cb(editor_s *editor, event_s *event, void *inpptr);

static void input_cb(editor_s *editor, event_s *event, void *inpptr)
{
    input_s *inp;
    io_file_info_s *finfo;

    inp = inpptr;
    finfo = event->spec.ptr;

    if (finfo->status & (IO_FILE_EOF | IO_FILE_CLOSED))
    {
        LOG(INFO, INFO, "input file %d has closed", finfo->fd);
    }

    VEC_FOREACH(&(inp->keys), char *, c)
    {
        input_key(inp, *c);
    }

    vec_clr(&(inp->keys));
}

void input_init(input_s *inp, editor_s *editor)
{
    inp->npolls = 0;
    inp->editor = editor;

    vec_init(&(inp->keys), sizeof(char));

    inp->alive = false;
}

void input_pause(input_s *inp)
{
    size_t ind;
    if (!(inp->alive))
        return;

    for (ind = 0; ind < inp->npolls; ++ind)
        poll_rem(&(inp->editor->pollctx), &(inp->polls[ind]));

    LOG(INFO, INFO, "Removing input polls");

    inp->alive = false;
}

void input_resume(input_s *inp)
{
    size_t ind;
    if (inp->alive)
        return;

    for (ind = 0; ind < inp->npolls; ++ind)
        poll_cb(&(inp->editor->pollctx), &(inp->polls[ind]), input_cb, inp);

    inp->alive = true;

    LOG(INFO, SUCC, "Input resumed");
}

int input_add_fd(input_s *inp, int fd)
{
    io_file_info_s *conf;

    if (inp->alive)
    {
        LOG(ALERT, ERR, "Cannot add fds to running input_s");
        return -1;
    }

    if (inp->npolls == INPUT_MAX_POLLS)
    {
        LOG(ALERT, ERR, "Tried to add too many input fds (more than %d)", INPUT_MAX_POLLS);
        return -1;
    }

    conf = &(inp->polls[inp->npolls]);
    inp->npolls += 1;

    memset(conf, 0, sizeof(io_file_info_s));
    conf->fd = fd;
    conf->readvec = &(inp->keys);

    LOG(INFO, SUCC, "Added input file %d", fd);

    return 0;
}

int input_rem_fd(input_s *inp, int fd)
{
    size_t ind;

    if (inp->alive)
    {
        LOG(ALERT, ERR, "Cannot remove fds from running input_s");
        return -1;
    }

    for (ind = 0; ind < inp->npolls; ++ind)
    {
        io_file_info_s *finfo;
        finfo = &(inp->polls[ind]);

        if (finfo->fd == fd)
        {
            inp->npolls -= 1;
            memmove(finfo, finfo + 1, INPUT_MAX_POLLS - ind - 1);
            return 0;
        }
    }

    LOG(ALERT, ERR, "Could not find fd %d in input_s to remove", fd);
    return -1;
}

void input_kill(input_s *inp)
{
    input_pause(inp);

    vec_kill(&(inp->keys));

    LOG(INFO, SUCC, "Input %p killed", (void *)inp);
}

static void input_key(input_s *inp, char c)
{
    event_s event;
    memset(&event, 0, sizeof(event));
    input_event_spec(inp, &(event.spec));
    event.chr = c;

    event_push(
        &(inp->editor->eventctx),
        &event
    );
}

