#if !defined(IO_KEY_H)
# define IO_KEY_H
# include <stdint.h>
# include <stdbool.h>

# include "types.h"

# include "container/sortedvec.h"
# include "container/vec.h"

/* event.h depends on the definition of key_s */
# include "event/timer.h"
# include "event/event.h"

typedef enum
{
    KEY_PARSER_IMPATIENT     = 0x01,
    KEY_PARSER_TIMER_RUNNING = 0x02
} key_parser_attr_t;

struct key_parser
{
    editor_s   *editor;
    timer_s     timer;
    vec_s       symbol;

    key_parser_attr_t attrs;
};

void key_start(void);

void key_end(void);

int key_parser_init(key_parser_s *parser, editor_s *editor);

void key_parser_kill(key_parser_s *parser);

void key_parser_event_spec(key_parser_s *parser, event_spec_s *spec);

int key_add_mapping(key_s *key, const char *symbol, size_t len);

void key_add_duplicate(key_s *alias, key_s *canonical);

void key_canonicalise(key_s *key);

int key_get_name(key_s *key, char *buf, size_t buflen);

void key_to_string(key_s *key, vec_s *string);

#endif
