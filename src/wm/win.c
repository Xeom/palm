#include "wm/win.h"
#include "util/macro.h"
#include "text/font.h"
#include "log/log.h"
#include <stdlib.h>

static void win_replace_child(win_s *w, win_s *prev, win_s *new)
{
    if (new)
        new->parent = w;
    if (w->child1 == prev)
        w->child1 = new;
    else if (w->child2 == prev)
        w->child2 = new;
}

static win_s *win_sister(win_s *w)
{
    win_s *parent;
    parent = w->parent;

    if (parent->child1 == w)
        return parent->child2;
    else
        return parent->child1;
}

void win_init(win_s *w, win_split_t type)
{
    memset(w, 0, sizeof(win_s));
    w->adj = 0.5;

    if (type == WIN_SPLIT_NONE)
    {
        vec_init(&(w->status), sizeof(chr_s));
        framebuf_init(&(w->fbuf));
    }

    LOG(INFO, SUCC, "Initialized window at %p, type %d", (void *)w, type);
}

int win_rotate(win_s *w)
{
    if (w->type == WIN_SPLIT_H)
    {
        win_s *tmp;
        tmp = w->child1;

        w->child1 = w->child2;
        w->child2 = tmp;
        w->type = WIN_SPLIT_V;

        return 0;
    }
    else if (w->type == WIN_SPLIT_V)
    {
        w->type = WIN_SPLIT_H;

        return 0;
    }
    else
    {
        return -1;
    }
}

win_s *win_split(win_s *w, win_split_t dir)
{
    win_s *parent;
    win_s *splitter;
    win_s *new;

    if (dir != WIN_SPLIT_H && dir != WIN_SPLIT_V)
        return NULL;

    parent = w->parent;

    splitter = malloc(sizeof(win_s));
    new = malloc(sizeof(win_s));

    LOG(INFO, INFO, "Splitting window at %p into two ...", (void *)w);

    win_init(new, WIN_SPLIT_NONE);
    win_init(splitter, dir);

    splitter->type   = dir;
    splitter->child1 = w;
    splitter->child2 = new;
    splitter->parent = parent;

    new->parent = splitter;
    w->parent   = splitter;

    if (parent)
        win_replace_child(parent, w, splitter);

    return new;
}

void win_kill(win_s *w)
{
    if (w->type == WIN_SPLIT_H || w->type == WIN_SPLIT_V)
    {
        win_kill(w->child1);
        win_kill(w->child2);
    }
    else
    {
        framebuf_kill(&(w->fbuf));
    }

    vec_kill(&(w->status));
    free(w);
}

int win_remove(win_s *w)
{
    /*
     * +------------------+
     * |+---------+       |
     * ||+---+---+|       |
     * ||| w |sis||       |
     * ||+---+---+| . . . |
     * ||splitter |       |
     * |+---------+       |
     * | parent           |
     * +------------------+
     */

    win_s *parent;
    win_s *splitter;
    win_s *sister;

    splitter = w->parent;

    if (!splitter)
        return -1;

    parent = splitter->parent;
    sister = win_sister(w);

    if (parent)
        win_replace_child(parent, splitter, sister);

    free(splitter);
    win_kill(w);

    sister->parent = parent;

    return 0;
}

win_s *win_get_root(win_s *w)
{
    if (!(w->parent))
        return w;
    else
        return win_get_root(w->parent);
}

void win_get_dims(win_s *w, rect_s *rootdims, rect_s *dims)
{
    win_s *parent;
    parent = w->parent;
    if (parent)
    {
        int adj;
        win_get_dims(parent, rootdims, dims);
        if (parent->type == WIN_SPLIT_H)
        {
            adj = w->parent->adj * dims->w;
            if (w == parent->child1)
            {
                dims->w = adj;
            }
            else
            {
                dims->x += adj;
                dims->w -= adj;
            }
        }
        else if (parent->type == WIN_SPLIT_V)
        {
            adj = w->parent->adj * dims->h;
            if (w == parent->child1)
            {
                dims->h = adj;
            }
            else
            {
                dims->y += adj;
                dims->h -= adj;
            }
        }
    }
    else
    {
        memcpy(dims, rootdims, sizeof(rect_s));
    }
}

void win_resize(win_s *w, rect_s *dims)
{
    if (w->type == WIN_SPLIT_H || w->type == WIN_SPLIT_V)
    {
        win_resize(w->child1, dims);
        win_resize(w->child2, dims);
    }
    else
    {
        int winw, winh;
        rect_s windims;

        win_get_dims(w, dims, &windims);

        winw = MAX(0, (int)windims.w - 1);
        winh = MAX(0, (int)windims.h - 1);

        framebuf_resize(&(w->fbuf), winw, winh);
        LOG(INFO, SUCC, "Resized window at %p to %u x %u @%d,%d",
            (void *)w, dims->w, dims->h, dims->x, dims->y);
    }

}

void win_stamp(win_s *w, display_s *disp, rect_s *rect)
{
    if (w->type == WIN_SPLIT_H || w->type == WIN_SPLIT_V)
    {
        win_stamp(w->child1, disp, NULL);
        win_stamp(w->child2, disp, NULL);
    }
    else
    {
        rect_s dstrect, srcrect, disprect;
        display_dims(disp, &disprect);
        win_get_dims(w, &disprect, &dstrect);

        if (rect)
        {
            memcpy(&srcrect, rect, sizeof(rect_s));
            dstrect.x += rect->x;
            dstrect.y += rect->y;
            dstrect.w  = rect->w;
            dstrect.h  = rect->h;
        }
        else
        {
            framebuf_dims_rect(&(w->fbuf), &srcrect);
        }

        display_stamp(disp, &dstrect, &(w->fbuf), &srcrect);
    }
}

