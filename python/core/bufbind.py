import libtypes
import ctypes

from lib import lib
from .structure import Structure
from .bind      import Bind

class BufBind(Structure):
    typ        = libtypes.bufbind_s
    init_funct = lib.bufbind_init
    kill_funct = lib.bufbind_kill

    def init_args(self, cb, buf):
        self.buf = buf
        return [cb, ctypes.byref(buf.editor["pool"])]

    def kill(self):
        del self.buf
        Structure.kill(self)

    def bind(self, bind: Bind):
        lib.bufbind_bind(self.ptr, bind.ptr)

    def unbind(self, key):
        lib.bufbind_unbind(self.ptr, key)

    def key(self, key):
        if isinstance(key, str):
            key = ord(key)

        lib.bufbind_key(self.ptr, key, self.buf.ptr)

    def set_mode(self, mode):
        if isinstance(mode, str):
            mode = mode.encode("utf-8")

        lib.bufbind_set_mode(self.ptr, mode)

    def prev_mode(self):
        lib.bufbind_prev_mode(self.ptr)

    def reset(self):
        lib.bufbind_reset(self.ptr)
