#if !defined(EDIT_EDIT_H)
# define EDIT_EDIT_H
# include "types.h"
# include <stddef.h>
# include "buf/cur.h"
# include "container/vec.h"
# include "edit/ins.h"
# include "edit/enter.h"
# include "edit/indent.h"
# include "edit/del.h"

#define CUR_RFOREACH_LINE(_cur, _buf, _var) \
    for (cur_s _var = { .pri.row = cur_max(_cur)->row }; \
         cur_get_line((_cur), &_var, _buf, _var.pri.row); \
         --(_var.pri.row))

#define CUR_FOREACH_LINE(_cur, _buf, _var) \
    for (cur_s _var = { .pri.row = cur_min(_cur)->row }; \
         cur_get_line((_cur), &_var, _buf, _var.pri.row); \
         ++(_var.pri.row))


// OPERATIONS:
// ins, cpy, cut, del, ent, mov
void edit_get_cur(edit_info_s *einfo, cur_s *rtn);

void edit_copy_at(edit_info_s *einfo, cur_s *cur, vec_s *chrs);

void edit_copy(edit_info_s *einfo, vec_s *chrs);

#endif
