#if !defined(LOG_NOTIFY_H)
# define LOG_NOTIFY_H
# include "log/log.h"
# include "types.h"

struct notification
{
    log_lvl_t lvl;
    log_type_t typ;
    long long tstamp;
    long long tout;
    long long id;
    vec_s chrs;
};

void log_notify_update(log_s *log, vec_s *chrs, size_t len);

void log_notify(log_s *log, log_lvl_t lvl, log_type_t typ, const char *str);

bool log_has_printable(log_s *log);

#endif
