"""A generic wrapper for ctype structs."""

import ctypes
import libtypes
from lib import lib
from lib.util import Enum
from typing import Optional, Type, Any, List, NoReturn, Callable

ctypesptrbase = ctypes.POINTER(ctypes.c_int).__bases__[0]

class Structure(object):
    """A class to represent ctypes structures in Python.

    This class wraps constuctor and destructor functions, so
    that garbage collection will release resources allocated
    in C.

    The class also provides a way of pretty-printing structures,
    and of accessing their attributes using obj["attr"] syntax.
    """

    # The ctypes structure being wrapped
    typ: Type[ctypes.Structure] = ctypes.Structure

    # C functions with signatures init_funct(struct *s, ...) -> int or void,
    #                         and kill_funct(struct *s)      -> void
    # which construct and destruct instances of this structure.
    # If no such function exists, they can be None.
    #
    # If init_funct returns an integer, it should be negative on
    # error.
    init_funct = None
    kill_funct = None

    @classmethod
    def cpy_from(cls, ptr):
        """Make a copy of a structure from a pointer."""
        rtn = cls(toinit=False, tokill=True)
        ctypes.memmove(rtn.ptr, ptr, ctypes.sizeof(cls.typ))
        return rtn

    @classmethod
    def cpy_to(cls, ptr):
        """Copy the contents of this structure to a pointer."""
        ctypes.memmove(ptr, self.ptr, ctypes.sizeof(cls.typ))

    def __init__(
        self,
        *args:  Any,
        ptr                   =None,
        toinit: Optional[bool]=None,
        tokill: Optional[bool]=None,
        **kwargs
    ):
        """Initialize an instance of this class.

        Arguments:
            args   - The set of arguments, which are passed to cls.init_args(),
                     so they can be translated into arguments to call init_funct
                     with.
            ptr    - If not None, the pointer where the structure is located. 
                     If set to None, the memory for the structure is allocated and
                     stored as cls.struct. If toinit and tokill are not specified,
                     then ptr == None implies toinit=True, and tokill=True,
                     while ptr != None implies toinit=False, and tokill=False.
            toinit - If True, run init_funct on the structure on initialization.
            tokill - If True, run kill_funct on the structure on deletion.
        """
        # .struct will be set to None when the structure is
        # provided by a pointer. It should not be relied upon!
        self.struct: Optional[ctypes.Structure]

        if ptr is None:
            # Construct and allocate a structure. It will
            # be freed on deletion automatically.
            self.struct = self.typ()
            self.ptr    = ctypes.pointer(self.struct)

            # Make implicit assumptions about tokill and toinit
            toinit = True if toinit is None else toinit
            tokill = True if tokill is None else tokill

        else:
            # Store only the pointer.
            self.struct = None
            self.ptr    = ptr

            # Make implicit assumptions about tokill and toinit.
            toinit = False if toinit is None else toinit
            tokill = False if tokill is None else tokill

        self.toinit: bool = toinit
        self.tokill: bool = tokill
        self.killed: bool = False

        # Try an initialize the structure.
        if self.toinit:
            try:
                self.init(*self.init_args(*args, **kwargs))
            # If we fail to init properly, do not kill()!
            except Exception as e:
                self.tokill = False
                raise e

    def init_args(self, *args: Any, **kwargs: Any) -> List: # type: ignore
        """Get initialization arguments.

        This function is called on structure initialization, with
        the arguments given to __init__, to produce a series of
        arguments to be handed to the C function init_funct.

        It should be overriden by subclasses, e.g. for a vector,

        /* In C ... */
        int vec_init(struct vec *v, size_t width) { ... }

        # In Python ...
        class Vector(Structure):
            typ = vec_s
            init_funct = vec_init

            def init_args(self, width):
                return [width]

        An example of the usage of the Vector class would then
        be v = Vector(3) to produce a vector of width three.
        """
        return []

    def init(self, *args: Any) -> None:
        """Initialize this structure."""
        if self.init_funct is not None:
            res = self.init_funct(self.ptr, *args)
            if self.init_funct.restype == ctypes.c_int and res < 0:
                raise Exception("Error while initializing {}".format(type(self).__name__))

    def kill(self):
        """Kill this structure."""
        if self.kill_funct is not None:
            self.kill_funct(self.ptr)

        self.tokill = False
        self.killed = True

    def __del__(self):
        """Handle cleanup of this structure."""
        # If we need to, kill this structure.
        #
        # This happens when we have init'd and not yet
        # killed it, or when we were specifically told to kill
        # this structure.
        if self.tokill:
            self.kill()

    def __getitem__(self, key: str) -> Any:
        """Get an attribute of the structure.

        This is shorthand for struct.ptr.contents.item,
        and does not perform any special processing.
        """
        return getattr(self.ptr.contents, key)

    def __setitem__(self, key: str, val: Any):
        """Set an attribute of the structure.

        This is shorthand for struct.ptr.contents.item,
        and does not perform any special processing.
        """
        setattr(self.ptr.contents, key, val)

    # repr_fmt - The format to represent this structure.
    repr_fmt:       str  = "<{name}: {vals}>"
    #repr_fmt: str = "<{name} @ 0x{ptr:x}: {vals}>"

    # repr_multiline - Whether to print the structure on multiple lines
    repr_multiline: bool = True

    def get_repr_vals(self) -> str:
        """Get a representation of the values contained within this structure.

        This will be used by __repr__ to build a representation of this
        whole structure.
        """
        vals = []

        for name, typ in self.ptr.contents._fields_:
            cont = getattr(self.ptr.contents, name)

            if issubclass(typ, ctypesptrbase):
                if cont:
                    val = "0x{0:x}".format(ctypes.addressof(cont.contents))
                else:
                    val = "(NULL)"
            elif issubclass(typ, Enum):
                val = "|".join(str(f) for f in cont.to_flags())
            elif issubclass(typ, ctypes.Structure):
                val = "<...>"
            else:
                val = cont

            vals.append("{name}={val}".format(name=name, val=val))

        if self.repr_multiline:
            return "\n  " + ",\n  ".join(vals) + "\n"
        else:
            return ", ".join(vals)

    def __repr__(self) -> str:
        """Pretty-print this structure."""
        clsname = type(self).__name__
        return self.repr_fmt.format(
            name=clsname, vals=self.get_repr_vals()
        )

def only_for_alive(f):
    """Generate a decorator to mark functions as invalid on killed structures.

    It is an error to call a function marked with @only_for_alive, after
    .kill() has been called on a structure.
    """
    # Return the modified function
    def inner(self, *args, **kwargs):
        if self.killed:
            raise Exception("Cannot perform method on killed structure")

        return f(self, *args, **kwargs)

    return inner
