#include "snow.h"
#include "event/event.h"

const char *actionparam;
const char *eventparam;

static void event_cb(editor_s *editor, event_s *event, void *param)
{
    actionparam = param;
    eventparam  = event->ptr;
}

static event_ctx_s ctx;

describe(event)
{
    before_each()
    {
        event_ctx_init(&ctx, NULL);
    }

    after_each()
    {
        event_ctx_kill(&ctx);
    }

    it ("Handles events")
    {
        event_s *next;

        event_action_s act = {
            .cb = event_cb,
            .param = "ACTION"
        };

        event_s event = {
            .spec = { .typ = EVENT_TEST, .ptr = NULL },
            .ptr  = "EVENT"
        };

        event_spec_s spec = {
            .typ = EVENT_TEST
        };

        actionparam = "NO";
        eventparam  = "NO";

        asserteq(event_listen(&ctx, &spec, &act), 0);
        asserteq(event_push(&ctx, &event), 0);

        next = event_get_next(&ctx);
        assert(next);

        event_handle(&ctx, next);

        assert(actionparam);
        assert(eventparam);

        asserteq(actionparam, "ACTION");
        asserteq(eventparam,  "EVENT");

        assert(!event_get_next(&ctx));
    }

    it ("Has priority")
    {
        event_s *next;

        event_action_s act0 = {
            .cb = event_cb,
            .param = "PRI0",
            .pri   = 0
        };
        event_action_s act1 = {
            .cb = event_cb,
            .param = "PRI1",
            .pri   = 1
        };
        event_action_s act2 = {
            .cb = event_cb,
            .param = "PRI2",
            .pri   = 2
        };

        event_s event = {
            .spec = { .typ = EVENT_TEST, .ptr = NULL },
            .ptr  = "EVENT"
        };

        event_spec_s spec = {
            .typ = EVENT_TEST
        };

        actionparam = "NO";
        eventparam  = "NO";

        asserteq(event_listen(&ctx, &spec, &act1), 0);
        asserteq(event_push(&ctx, &event), 0);

        next = event_get_next(&ctx);
        assert(next);

        event_handle(&ctx, next);

        assert(actionparam);
        assert(eventparam);
        asserteq(actionparam, "PRI1");
        asserteq(eventparam,  "EVENT");

        actionparam = "NO";
        eventparam  = "NO";

        asserteq(event_listen(&ctx, &spec, &act0), 0);
        asserteq(event_push(&ctx, &event), 0);

        next = event_get_next(&ctx);
        assert(next);

        event_handle(&ctx, next);

        assert(actionparam);
        assert(eventparam);
        asserteq(actionparam, "PRI1");
        asserteq(eventparam,  "EVENT");

        actionparam = "NO";
        eventparam  = "NO";

        asserteq(event_listen(&ctx, &spec, &act2), 0);
        asserteq(event_push(&ctx, &event), 0);

        next = event_get_next(&ctx);
        assert(next);

        event_handle(&ctx, next);

        assert(actionparam);
        assert(eventparam);
        asserteq(actionparam, "PRI2");
        asserteq(eventparam,  "EVENT");

        next = event_get_next(&ctx);
        assert(!next);
    }
}
