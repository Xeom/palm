#include "wm/update.h"
#include "wm/wm.h"
#include "util/macro.h"
#include "editor.h"
#include "log/log.h"

static void *wm_updater_thread(void *arg);
static int wm_updater_do(wm_updater_s *updater, wm_update_s *update);

void wm_updater_init(wm_updater_s *updater, wm_s *wm)
{
    updater->wm = wm;

    updater->alive = false;
    vec_init(&(updater->queue), sizeof(wm_update_s));

    pthread_mutex_init(&(updater->mtx), NULL);
    pthread_cond_init(&(updater->update), NULL);

    wm_updater_resume(updater);
}

void wm_updater_kill(wm_updater_s *updater)
{
    wm_updater_pause(updater);
    vec_kill(&(updater->queue));

    pthread_mutex_destroy(&(updater->mtx));
    pthread_cond_destroy(&(updater->update));
}

void wm_updater_resume(wm_updater_s *updater)
{
    if (updater->alive)
        return;

    updater->alive = true;
    pthread_create(&(updater->thread), NULL, wm_updater_thread, updater);

    LOG(INFO, SUCC, "Started wm updater thread");
}

void wm_updater_pause(wm_updater_s *updater)
{
    if (!updater->alive)
        return;

    LOG(INFO, INFO,  "Stopping wm updater thread");
    pthread_mutex_lock(&(updater->mtx));
    updater->alive = false;
    pthread_cond_signal(&(updater->update));
    pthread_mutex_unlock(&(updater->mtx));

    pthread_join(updater->thread, NULL);
    LOG(INFO, SUCC, "Stopped wm updater thread");
}

void wm_updater_purge_buf(wm_updater_s *updater, buf_s *buf)
{
    wm_updater_pause(updater);

    VEC_RFOREACH(&(updater->queue), wm_update_s *, update)
    {
        if (update->buf == buf)
        {
            vec_del(&(updater->queue), _ind, 1);
        }
    }

    wm_updater_resume(updater);
}

void wm_updater_queue(wm_updater_s *updater, wm_update_s *update)
{
    bool duplicate;
    pthread_mutex_lock(&(updater->mtx));

    duplicate = false;
    VEC_FOREACH(&(updater->queue), wm_update_s *, cmp)
    {
        if (memcmp(cmp, update, sizeof(wm_update_s)) == 0)
            duplicate = true;
    }

    if (!duplicate)
    {
        vec_app(&(updater->queue), update);
        pthread_cond_signal(&(updater->update));
    }

    pthread_mutex_unlock(&(updater->mtx));
}

static void *wm_updater_thread(void *arg)
{
    wm_updater_s *updater;
    updater = arg;

    editor_mask_signals();

    while (updater->alive)
    {
        bool doupdate;
        wm_update_s update;

        pthread_mutex_lock(&(updater->mtx));

        if (vec_len(&(updater->queue)) == 0)
            pthread_cond_wait(&(updater->update), &(updater->mtx));

        if (vec_len(&(updater->queue)) == 0)
        {
            doupdate = false;
        }
        else
        {
            doupdate = true;
            memcpy(&update, vec_get(&(updater->queue), 0), sizeof(wm_update_s));
            vec_del(&(updater->queue), 0, 1);
        }

        pthread_mutex_unlock(&(updater->mtx));

        if (doupdate)
            wm_updater_do(updater, &update);
    }

    return NULL;
}

static int wm_updater_do(wm_updater_s *updater, wm_update_s *update)
{
    wm_s   *wm;
    buf_s  *buf;
    rect_s  dims;
    long    ln;
    int     ind;

    wm  = updater->wm;
    buf = update->buf;
    ln  = update->ln;
    ind = update->ind;

    if (wm_get_dims(updater->wm, ind, &dims) == -1)
        return -1;

    rect_s  dst;
    vec_s   exp;
    chr_s  *mem;

    if (ln < buf->scry || ln > buf->scry + dims.h)
        return -1;

    vec_init(&exp, sizeof(chr_s));
    buf_get_expanded(buf, ln, &exp);

    dst.x = 0;
    dst.y = ln - buf->scry;
    dst.w = MIN(dims.w, vec_len(&(exp)) - buf->scrx);
    dst.h = 1;
    mem   = vec_get(&(exp), buf->scrx);

    if (dst.w > 0 && mem)
    {
        wm_set_win(wm, ind, &dst, mem);
        wm_fill_win(wm, ind, &(rect_s){
            .x = dst.w, .y = dst.y,
            .w = dims.w - dst.w, .h = 1
        }, &chr_blank);
    }
    else
    {
        wm_fill_win(wm, ind, &(rect_s){
            .x = 0, .y = dst.y,
            .w = dims.w, .h = 1
        }, &chr_blank);
    }

    vec_kill(&(exp));
    return 0;
}

