#if !defined(BUF_LINE_H)
# define BUF_LINE_H
# include "container/vec.h"
# include "text/chr.h"
# include "text/decoder.h"

struct line
{
    buf_s *buf;

    vec_s chrs;
    vec_s expanded;
    vec_s hlstate;
    bool  needsexpand;
};

# include "buf/buf.h"

void line_init(line_s *line, buf_s *buf);

void line_kill(line_s *line);

size_t line_get_ln(line_s *line);

void line_get_hlstate(line_s *line, vec_s *state);

void line_set_hlstate(line_s *line, vec_s *state);

void line_set_str(line_s *line, char *str, decoder_attr_t attrs);

void line_set_chrs(line_s *line, chr_s *mem, size_t len);

void line_cpy_font(line_s *line, vec_s *oth);

void line_set_font(line_s *line, unsigned short font, size_t ind, size_t n);

size_t line_pos_to_ind(line_s *line, size_t pos);
size_t line_ind_to_pos(line_s *line, size_t ind);

size_t line_off_to_ind(line_s *line, size_t off);
size_t line_ind_to_off(line_s *line, size_t ind);

void line_get_expanded(line_s *line, vec_s *expanded);

void line_needs_expansion(line_s *line);

int line_cmp(line_s *line, vec_s *chrs);

#endif
