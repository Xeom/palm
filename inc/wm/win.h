#if !defined(WM_WIN_H)
# define WM_WIN_H
# include "display/framebuf.h"
# include "display/display.h"
# include "types.h"

typedef enum
{
    WIN_SPLIT_NONE = 0,
    WIN_SPLIT_H,
    WIN_SPLIT_V
} win_split_t;

struct win
{
    framebuf_s fbuf;
    vec_s      status;
    win_s *parent;
    win_s *child1, *child2;
    win_split_t type;
    float adj;
};


void win_init(win_s *w, win_split_t type);

int win_rotate(win_s *w);

win_s *win_split(win_s *w, win_split_t dir);

void win_kill(win_s *w);

int win_remove(win_s *w);

void win_resize(win_s *w, rect_s *dims);

win_s *win_get_root(win_s *w);

void win_get_dims(win_s *w, rect_s *rootdims, rect_s *dims);

void win_stamp(win_s *w, display_s *disp, rect_s *rect);

#endif
