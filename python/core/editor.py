import threading
import ctypes
import select
import sys
import shlex
import os

import libtypes
from lib import lib

from hooks         import hooks
from server.server import ShellServer, add_global

from .hl           import HlCtx
from .buffer       import Buffer
from .structure    import Structure
from .input        import Input
from .playbacker   import Playbacker
from .pool         import Pool
from .statusformat import StatusFormat

from .display      import Display
from .event        import EventAction, EventSpec
from .timer        import Timer

from editor import log

from core.io import realpath
from typing import List

from util.encode import strify

@add_global()
class Editor(Structure):
    typ        = libtypes.editor_s
    init_funct = lib.editor_init
    kill_funct = lib.editor_kill

    editbuf = None
    notebuf = None

    def __init__(self, *args, **kwargs):
        self.bufs     = []
        self.killsoon = []
        self.wins     = [0]
        self.wait     = threading.Condition()
        self.lastpty  = "wc"

        self.selbuf = None
        Structure.__init__(self, *args, **kwargs)

        self.wm         = Wm(ptr=ctypes.pointer(self["wm"]))
        self.input      = Input(ptr=ctypes.pointer(self["input"]))
        self.hlctx      = HlCtx(ptr=ctypes.pointer(self["hlctx"]))
        self.playbacker = Playbacker(ptr=ctypes.pointer(self["playbacker"]))
        self.pool       = Pool(ptr=ctypes.pointer(self["pool"]))
        self.statusfmt  = self["statusfmt"]
        self.display    = Display(ptr=ctypes.pointer(self["display"]))
        self.timerctx   = self["timerctx"]
        self.eventctx   = self["eventctx"]
        self.pollctx    = self["pollctx"]
        self.bufselector = self["bufselector"]
        self.cmdlib      = self["cmdlib"]

        self.hlctx.load_defaults()

        self.attach_all()


    def init_args(self, stdin=0, stdout=1):
        return [stdin, stdout]

    def start_server(self):
        self.server = ShellServer(self, globaldef={
                "editor": self,
                "wm":     self.wm
            })

    def attach_all(self):
        self.periodic_kill_timer   = Timer(delay=0.2, attrs=["REPEAT"])
        self.periodic_status_timer = Timer(delay=0.1, attrs=["REPEAT"])

        @self.eventctx.on(self.periodic_kill_timer.eventspec())
        def periodic_kill_action():
            for buf in self.killsoon:
                self.kill_buf(buf)
            self.killsoon = []

        @self.eventctx.on(self.periodic_status_timer.eventspec())
        def periodic_status_action():
            for buf in self.bufs:
                self.wm.update_status(self, buf)

        self.periodic_kill_action   = periodic_kill_action
        self.periodic_status_action = periodic_status_action

        self.periodic_kill_timer.start(self.timerctx, periodic_kill_action)
        self.periodic_status_timer.start(self.timerctx, periodic_status_action)

        @self.eventctx.on(EventSpec(typ="RESIZE"))
        def on_resize():
            self.refresh_display()

        self.on_resize = on_resize

    def kill(self):
        """Kill the editor"""
        self.periodic_kill_timer.remove()
        self.periodic_status_timer.remove()
        self.periodic_kill_action.remove()
        self.periodic_status_action.remove()

        self.on_resize.remove()

        self.sel_buf(None)

        for b in self.bufs:
            b.kill()

        Structure.kill(self)

    def next_buf(self, n, buf=None):
        """Get the 'next' buffer in the editor, relative to another."""
        if buf == None:
            buf = self.selbuf

        if buf in self.bufs:
            ind = self.bufs.index(buf)
        else:
            ind = self.bufs[0]

        ind += n
        ind %= len(self.bufs)

        return self.bufs[ind]

    def next_win(self, n, winid=None):
        """Get the 'next' window in the editor, relative to another."""
        if winid == None:
            winid = self.selbuf.get_win()

        if winid in self.wins:
            ind = self.wins.index(winid)
        else:
            ind = 0

        ind += n
        ind %= len(self.wins)

        return self.wins[ind]

    def sel_buf(self, buf):
        """Select and send input towards a buffer.

        Note  that this will have confusing effects if the buffer in
        question is not associated with a window, since you won't be
        able to see what is going on."""
        self.selbuf = buf
        self.bufselector.select(buf)
        hooks.call("buf-switch", self, buf)

    def sel_win(self, winid):
        """Select a particular window."""
        for buf in self.bufs:
            if buf.get_win() == winid:
                self.sel_buf(buf)
                break

    def switch_buf(self, n):
        """Switch the buffer selected in the current window."""
        switchto = self.selbuf
        while True:
            switchto = self.next_buf(n, switchto)

            if switchto.get_win() == -1:
                self.win_assoc(switchto)
                self.sel_buf(switchto)
                return

            if switchto == self.selbuf:
                return

    def switch_win(self, n):
        """Switch the currently selected window."""
        switchto = self.next_win(n, self.selbuf.get_win())
        self.sel_win(switchto)

    def get_free_buf(self):
        """Get a buffer that is not used in any other window. If no such buffer
        exists, it is created."""
        for buf in self.bufs:
            if buf.get_win() == -1:
                return buf

        return self.new_buf(self.notebuf)

    def win_assoc(self, buf, winid=None):
        """Associate a buffer with a particular window."""
        if winid == None:
            winid = self.selbuf.get_win()

        prev = buf.get_win()

        if prev == winid:
            return

        if winid != -1:
            for other in self.bufs:
                if other.get_win() == winid:
                    if other.ptr == self.selbuf.ptr:
                        self.sel_buf(buf)
                    other.win_assoc(-1)

        buf.win_assoc(winid)

        if prev != -1:
            self.get_free_buf().win_assoc(prev)

        hooks.call("buf-win-assoc", buf)
        self.refresh_all()

    def win_split(self, winid=None, split=None, buf=None):
        """Split a window into two.

        Arguments:
            winid - The id of the window to be split.
            split - The direction to split, i.e. one of
                    'V', 'H'. If None, a direction will
                    be automatically chosen.
            buf   - The buffer to place in the new window.
        """
        if winid == None:
            winid = self.selbuf.get_win()
        if split == None:
            dims = self.wm.get_dims(winid)

            if dims.w > dims.h * 2.5:
                split = "H"
            else:
                split = "V"

        new = self.wm.split(winid, split)

        if buf == None or buf.get_win() != -1:
            self.get_free_buf().win_assoc(new)
        else:
            buf.win_assoc(new)

        self.wins.append(new)

        hooks.call("win-new", self, winid)

        return new

    def win_adj(self, winid=None, adj=0.5):
        """Adjust the size of a window.

        Arguments:
            winid - The window to adjust.
            adj   - The window proportion to set.
        """
        if winid == None:
            winid = self.selbuf.get_win()

        if winid not in self.wins:
            return

        self.wm.adj(winid, adj)
        self.refresh_all()

        hooks.call("win-adj", self, winid)

    def win_close(self, winid=None):
        """Close a window. The buffer inside is unaffected."""
        hooks.call("win-close", self, winid)

        if winid == None:
            winid = self.selbuf.get_win()

        if winid not in self.wins or len(self.wins) == 1:
            return

        for buf in self.bufs:
            if buf.get_win() == winid:
                 buf.win_assoc(-1)

        self.wm.close(winid)
        self.wins.remove(winid)
        self.sel_win(self.wins[0])

        self.refresh_all()

    def refresh_display(self):
        """Refresh the display."""
        self.refresh_all()
        self.wm.refresh_display()

    def refresh_all(self):
        """Refresh all buffers.

        This will refresh the contents of all buffers.
        """
        for buf in self.bufs:
            buf.refresh_all()

    def new_buf(self, typ=Buffer, switch=False):
        """Create a new buffer.

        Arguments:
            typ    - The type of the new buffer.
            switch - If True, show the new buffer in the current
                     window, and select it.
        """
        buf = typ(self)
        self.bufs.append(buf)

        if switch:
            self.win_assoc(buf)

        hooks.call("buf-new", self, buf)

        return buf

    def popup_buf(self, typ=Buffer, winid=None):
        """Create a new buffer and split the current window to show it."""
        buf = self.new_buf(typ)
        self.win_split(winid, buf=buf)
        self.sel_buf(buf)

        return buf

    def get_buf_of_type(self, typ, switch=False):
        """Create or if one already exists, get, a buffer of a type."""
        for buf in self.bufs:
            if type(buf) == typ:
                rtn = buf
                break
        else:
            rtn = self.new_buf(typ)

        if switch:
            self.win_assoc(rtn)

        return rtn

    def get_buf_editing_path(self, path, switch=False):
        """Create, or if one already exists, get, an EditBuffer of a file."""
        path = realpath(path)

        for buf in self.bufs:
            if isinstance(buf, self.editbuf):
                if strify(buf.finfo.paths.real) == strify(path):
                    rtn = buf
                    break
        else:
            rtn = self.new_buf(self.editbuf)
            rtn.edit(path)

        if switch:
            self.win_assoc(rtn)

        return rtn

    def kill_buf(self, buf=None):
        """Kill a buffer."""
        if buf == None:
            buf = self.selbuf

        if buf not in self.bufs:
            return

        hooks.call("buf-kill", self, buf)

        winid = buf.get_win()
        self.bufs.remove(buf)
        buf.kill()

        if winid != -1:
            self.get_free_buf().win_assoc(winid)

        if self.selbuf == buf:
            if winid != -1:
                self.sel_win(winid)
            else:
                self.sel_win(self.wins[0])

        hooks.call("buf-post-kill")
        self.refresh_display()

    def main(self):
        lib.editor_main(self.ptr)
        hooks.call("exit", self)

    def kill_buf_soon(self, buf=None):
        """Kill a buffer 'soon'.

        This function is used when a buffer needs to kill itself,
        but this will cause issues, so it tells the editor to kill
        it soon.
        """
        if buf not in self.bufs:
            return

        self.killsoon.append(buf)

    def soft_exit(self):
        """Kill the editor gently."""
        self["alive"] = False

    @staticmethod
    def pty_args(cmd):
        """Change command args into an array of c strings."""
        res = (ctypes.c_char_p * (len(cmd) + 1))()

        for i, arg in enumerate(cmd):
            if isinstance(arg, str):
                arg = arg.encode("utf-8")

            res[i] = ctypes.c_char_p(arg)

        return res

    def run_cmd(self, cmd: List[str], inp: int, out: int):
        """Run a command in the background.

        Arguments:
            cmd - The command to run, as a list of arguments
            inp - A pipe to substitute for stdin.
            out - A pipe to substitute for stdout.
        """
        cmd = self.pty_args(cmd)
        lib.editor_run(self.ptr, inp, out, cmd)

    def run(self, cmd: str, inp: int, out: int):
        """Run a command in a shell in the background."""
        cmd = self.pty_args(["/bin/sh", "-c", cmd]);
        lib.editor_run(self.ptr, inp, out, cmd)

    def stdout_pty(self, cmd: str, inp: int, out: int):
        """Run a command and print its output to stdout."""
        cmd = self.pty_args(["/bin/sh", "-c", cmd])
        lib.editor_stdout_pty(self.ptr, inp, out, cmd)

    def pty(self, cmd="/usr/bin/python3", src=-1, dst=-1):
        """A nicer wrapper around stdout_pty and run.

        This command will run a command in a shell, either in the
        background, or using stdin/out if necessary.
        """
        toclose = []

        if len(cmd) == 0:
            cmd = self.lastpty

        self.lastpty = cmd

        if isinstance(src, Buffer):
            src = src.pipe_from_buf()
        elif hasattr(src, "fileno"):
            src = src.fileno()
        elif src == None:
            src = -1

        if isinstance(dst, Buffer):
            dst = dst.pipe_to_cur()
        elif hasattr(dst, "fileno"):
            dst = dst.fileno()
        elif dst == None:
            dst = -1

        if dst != -1 and src != -1:
            self.run(cmd, src, dst)
        else:
            self.stdout_pty(cmd, src, dst)

from .wm import Wm
