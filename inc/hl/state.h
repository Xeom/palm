#if !defined(HL_STATE_H)
# define HL_STATE_H
# include "types.h"
# include "container/vec.h"
# include "hl/action.h"
struct hl_state
{
    vec_s name;
    short font;
    vec_s start, end;
    vec_s actions;
    hl_action_s startattrs, endattrs;
};

void hl_state_init(hl_state_s *state, char *name);

void hl_state_kill(hl_state_s *state);

int hl_state_verify(hl_state_s *state);

int hl_state_add_action(hl_state_s *state, hl_action_s *action);

hl_action_s *hl_state_match(
    hl_state_s *state,
    hl_ctx_s *ctx,
    vec_s *str,
    size_t *off,
    size_t *start,
    size_t *end
);

void hl_state_apply(hl_state_s *state, hl_ctx_s *ctx, vec_s *str, vec_s *fonts, vec_s *states, size_t *off);

int hl_state_load_info(hl_state_s *state, yaml_obj_s *obj, hl_ctx_s *ctx);

int hl_state_load_actions(hl_state_s *state, yaml_obj_s *obj, hl_ctx_s *ctx);

#endif
