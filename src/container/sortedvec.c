#include "container/sortedvec.h"

void sortedvec_init(sortedvec_s *svec, size_t width, sortedvec_cmpcb_t cb)
{
    vec_init(&(svec->vec), width);
    svec->cmpcb = cb;
}

void sortedvec_kill(sortedvec_s *svec)
{
    vec_kill(&(svec->vec));
}

int sortedvec_ins(sortedvec_s *svec, void *item)
{
    size_t ind;

    if (!item) return PALM_ERR;

    ind = vec_bst(&(svec->vec), item, svec->cmpcb);

    if (!vec_ins(&(svec->vec), ind, 1, item))
        return PALM_ERR;

    return PALM_OK;
}

void *sortedvec_find(sortedvec_s *svec, void *item, size_t *count)
{
    size_t ind;
    void *found;
    ind = vec_bst(&(svec->vec), item, svec->cmpcb);

    if (!item) return NULL;

    found = vec_get(&(svec->vec), ind);
    if (!found) return NULL;

    if (svec->cmpcb(item, found) != 0) return NULL;

    if (count)
    {
        size_t curr;
        void *cmpitem;

        *count = 1;
        curr   = ind;
        while ((cmpitem = vec_get(&(svec->vec), ++curr)) != NULL)
        {
            if (svec->cmpcb(item, cmpitem) != 0)
                break;

            *count += 1;
        }

        curr = ind;
        while ((cmpitem = vec_get(&(svec->vec), --curr)) != NULL)
        {
            if (svec->cmpcb(item, cmpitem) != 0)
                break;

            *count += 1;
        }
    }

    return found;
}

int sortedvec_rem(sortedvec_s *svec, void *item)
{
    size_t ind;
    void *found;

    if (!item) return PALM_ERR;

    ind = vec_bst(&(svec->vec), item, svec->cmpcb);

    found = vec_get(&(svec->vec), ind);
    if (!found) return PALM_ERR;

    if (svec->cmpcb(item, found) != 0) return PALM_OK;

    TRY(vec_del(&(svec->vec), ind, 1));

    return PALM_OK;
}

int sortedvec_contains(sortedvec_s *svec, void *item, sortedvec_startcb_t cb)
{
    size_t ind;
    void *found;

    if (!item) return PALM_ERR;

    ind = vec_bst(&(svec->vec), item, svec->cmpcb);

    found = vec_get(&(svec->vec), ind);
    if (!found) return PALM_OK;

    if (svec->cmpcb(item, found) == 0) return PALM_COMPLETE;
    if (cb && cb(found, item))  return PALM_INCOMPLETE;

    return PALM_OK;
}
