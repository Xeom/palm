from typing import Union
from socket import socket

from util.encode import bytify
from hooks       import SockListener
from core.log    import log

class PyClient(object):
    def __init__(self, sock: socket):
        super().__init__()
        self.sock  = sock
        self.listener = SockListener(sock, linesep=b"\x00")

        self.on_recv = self.listener.on_line_recv
        self.on_end  = self.listener.on_eof

    def __del__(self):
        self.listener.stop()
        self.sock.close()

    def send(self, data: Union[str, bytes]):
        self.listener.send(bytify(data))
        self.listener.send(b"\x00")

