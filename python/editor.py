import ctypes
import core.key
import paths
import inspect
from core.font import load_fonts_from_csv
from server.globals import add_global

class log:
    default = None

    @classmethod
    def get_default(cls):
        from lib import lib
        from core.log import Log
        return ctypes.cast(lib.default_log, ctypes.POINTER(Log)).contents

    @classmethod
    def start(cls, logfile):
        log = cls.get_default()
        cls.default = log
        log.init()
        log.to_file(logfile, ts="%Y-%m-%d %H:%M:%S ")
        cls.str("INFO", "INFO", "Started python log!")

    @classmethod
    def str(cls, *args, **kwargs):
        caller = inspect.getframeinfo(inspect.stack()[1][0])
        if cls.default is not None:
            cls.default.str(*args, caller=caller, **kwargs)

    @classmethod
    def fmt(cls, *args, **kwargs):
        caller = inspect.getframeinfo(inspect.stack()[1][0])
        if cls.default is not None:
            cls.default.fmt(*args, caller=caller, **kwargs)

def start_editor(logfile):
    from buffers.notes import NotesBuffer
    from buffers.edit  import EditBuffer
    from buffers.log   import LogBuffer
    from core.editor import Editor

    log.start(logfile)

    # Create editor ...
    log.str("INFO", "INFO", "Creating editor ...")

    for f in paths.fontfiles:
        load_fonts_from_csv(open(f))

    core.key.start()

    add_global("defaultlog")(log)

    editor = Editor()

    editor.cmdlib.mount_all_c_cmds()

    editor.editbuf = EditBuffer
    editor.notebuf = NotesBuffer

    # Make a buffer to capture defaultlog messages ...
    log.str("INFO", "INFO", "Creating log buffer ...")
    editor.new_buf(LogBuffer).from_log(log)

    return editor

