from typing import *

import ctypes
import os
import sys

import libtypes

from lib            import lib, enumspec, enums
from util.encode    import bytify
from server.globals import add_global
from bind import modes

from .structure import Structure, only_for_alive
from .vec       import Vec
from .chr       import Chr
from .chrvec    import ChrVec
from .bufbind   import BufBind
from .decoder   import Decoder
from .finfo     import Finfo
from .hl        import HlWorker
from .file      import File

@add_global()
class Buffer(Structure):
    typ        = libtypes.buf_s
    init_funct = lib.buf_init
    kill_funct = lib.buf_kill

    # Aliases for types
    lnspec    = Optional[Union["Pos", "Cur", int]]
    linerangespec = Optional[Tuple[int, Union[None, int]]]

    def init(self, *args):
        super().init(*args)

        self.curset = self["curset"]
        finfoptr = ctypes.pointer(self["finfo"])

        self.cmdparser = self["cmdparser"]

        self.finfo = Finfo(self, ptr=finfoptr)
        self.einfo = self["einfo"]

        self.filehandles = []

        self.hlworker = HlWorker(ptr=ctypes.pointer(self["hlworker"]))

        self.start_server()
        bindptr = ctypes.pointer(self["bind"])
        self.bind = BufBind(
            self.server.add_session().gen_c_cb(),
            self, ptr=bindptr, toinit=True, tokill=True
        )
        self.search = None
        modes.setup_all(self.bind)
        self.bind_all()

    def bind_all(self):
        pass

    def kill(self):
        for f in self.filehandles:
            f.kill()

        Structure.kill(self)

    @only_for_alive
    def start_server(self):
        self.server = ShellServer(self.editor,
            globaldef={
                "buf":    self,
                "editor": self.editor,
                "wm":     self.editor.wm,
                "finfo":  self.finfo,
                "einfo":  self.einfo,
                "pollctx": self.editor["pollctx"],
                "eventctx": self.editor["eventctx"],
                "timerctx": self.editor["timerctx"],
                "curset":   self.curset
            })

    def init_args(self, editor: "Editor") -> List: # type: ignore
        self.editor = editor
        return [editor.ptr]

    def get_name(self) -> str:
        """Get a name that can be used to describe this buffer."""
        typ = type(self).__name__
        if self.finfo["associated"]:
            return f"{typ}:{self.finfo.paths.real}"
        else:
            return f"{typ}"

    # --- BUFFER DIMENSIONS ---
    @only_for_alive
    def __len__(self) -> int:
        return lib.buf_len(self.ptr)

    @only_for_alive
    def line_len(self, ln: int) -> int:
        return lib.buf_line_len(self.ptr, ln)

    def last_pos(self) -> "Pos":
        """Get the last cursor position in the buffer.

        i.e. this is the position of the cursor that you'll
        get to if you hold down the right arrow forever.

        For nonzero length buffers, it is the same as
        (len(buf) - 1, buf.line_len(len(buf) - 1))
        """
        ln = len(self) - 1

        if ln < 0:
            return Pos.at(0, 0)

        cn = self.line_len(ln)

        return Pos.at(ln, cn)

    def get_ln(self, val: lnspec) -> int:
        if isinstance(val, Pos):
            return val["row"]
        elif isinstance(val, Cur):
            return val.pri["row"]
        elif val == None:
            return self.get_cur().pri["row"]
        elif isinstance(val, int):
            return val
        else:
            raise Exception(f"Cannot interpret '{val}' as a line number")

    def is_empty(self) -> bool:
        return len(self) == 0 or (
            len(self) == 1 and self.line_len(0) == 0
        )

    # --- DISPLAYING THE BUFFER IN WINDOWS ---
    @only_for_alive
    def win_assoc(self, win: int):
        lib.buf_win_assoc(self.ptr, win)

    @only_for_alive
    def get_win(self) -> int:
        return lib.buf_get_win(self.ptr)

    # --- GETTING/SETING CURSOR POSITION ---

    @only_for_alive
    def get_cur(self) -> "Cur":
        rtn = Cur()
        lib.buf_get_cur(self.ptr, rtn.ptr)

        return rtn

    @only_for_alive
    def set_cur(self, cur: "Cur"):
        lib.buf_set_cur(self.ptr, cur.ptr)

    @only_for_alive
    def shift(self, frm: "Pos", to: "Pos"):
        lib.buf_shift(self.ptr, frm.ptr, to.ptr)

    @only_for_alive
    def move_cur(self, direction, n: int):
        lib.buf_move_cur(self.ptr, enums.cur_dir_t(direction), n)

    # --- GETTING BUFFER CONTENTS ---

    @only_for_alive
    def get_line(self, ln: int) -> ChrVec:
        rtn = ChrVec()
        lib.buf_get_line(self.ptr, ln, rtn.ptr)

        return rtn

    @only_for_alive
    def get_str(self, ln: int) -> bytes:
        vec = Vec(ctypes.c_char)
        lib.buf_get_str(self.ptr, ln, vec.ptr)
        if len(vec) == 0:
            return b""

        string = ctypes.cast(vec[0], ctypes.POINTER(ctypes.c_char * len(vec)))

        return string.contents.value

    # --- CHANGING BUFFER CONTENTS ---

    @only_for_alive
    def set(
        self,
        ln:    lnspec,
        lines,
        attrs: enumspec=["UTF8"]
    ):
        attrs = enums.decoder_attr_t(attrs)

        ln = self.get_ln(ln)

        numlines = len(self)
        if ln >= numlines:
            self.ins(numlines, ln - numlines + 1)

        if isinstance(lines, list):
            for line in lines:
                self.set(ln, line, attrs)
                ln += 1
        elif isinstance(lines, ChrVec):
            lib.buf_set_line(self.ptr, ln, lines.ptr)
        elif isinstance(lines, (str, bytes)):
            lines = ChrVec.from_fmt(bytify(lines), attrs)
            lib.buf_set_line(self.ptr, ln, lines.ptr)

    @only_for_alive
    def ins(
        self,
        ln:    lnspec            =None,
        lines                    =1,
        attrs: enumspec          =["UTF8"]
    ):
        attrs = enums.decoder_attr_t(attrs)

        ln = self.get_ln(ln)
        if isinstance(lines, int):
            lib.buf_ins_line(self.ptr, ln, lines)
        elif isinstance(lines, list):
            lib.buf_ins_line(self.ptr, ln, len(lines))
            self.set(ln, lines, attrs)
        else:
            lib.buf_ins_line(self.ptr, ln, 1)
            self.set(ln, lines, attrs)

    @only_for_alive
    def rem(self, ln: lnspec=None, n: int=1):
        ln = self.get_ln(ln)
        lib.buf_del_line(self.ptr, ln, n)

    # --- REDRAWING BUFFER ---

    @only_for_alive
    def refresh(self, ln: lnspec, n: int):
        ln = self.get_ln(ln)
        lib.buf_refresh(self.ptr, ln, n)

    @only_for_alive
    def refresh_all(self):
        self.refresh(self["scry"], 1024)

    # --- SYNTAX HIGHLIGHTING ---
    @only_for_alive
    def set_defhl(self, statenames: Sequence[Union[str, bytes]]):
        """Set the default highlight state stack.

        Arguments:
            statenames - A list of the names of the states that
                         make up the initial stack of states.
                         e.g. ["c"], ["py", "py-single-string"]

        This function sets an initial state for the buffer to
        start highlighting itself. The 'stack' refers to the states
        in the push-down FSM that is used to highlight syntax in
        the editor.
        """
        vec = Vec(ctypes.c_long)

        for name in statenames:
            statenum = self.editor.hlctx.lookup_state(name)

            if statenum == -1:
                raise Exception("Unknown highlight state: {}".format(name))

            vec.add(1, ctypes.byref(ctypes.c_long(statenum)))

        lib.buf_set_defhl(self.ptr, vec.ptr)

    # --- SETTING/GETTING INDENTS ---
    @only_for_alive
    def indent_get(self, ln: lnspec) -> int:
        """Get the indentation of a line.

        Arguments:
            ln - The line to get the indentation of

        The indentation of a line is the depth to which it appears
        to be indented by whitespace characters. This may not necessarily
        correspond to the number of whitespace characters at the start
        of the line, if, for example, tabs are involved.
        """
        ln = self.get_ln(ln)
        return lib.indent_get_depth(self.ptr, ln)

    @only_for_alive
    def indent_set(self, ln: lnspec, depth: int):
        """Set the indentation of a line.

        Arguments:
            ln    - The line to set the indentation of.
            depth - The depth to set the indentation to.

        This function will indent a line to a specific depth, using
        either spaces or tabs, as configured in the chrconf_s
        structure of the buffer.
        """
        ln = self.get_ln(ln)
        if lib.indent_set_depth(self.ptr, ln, depth) == -1:
            raise Exception("Error calling indent_set_depth")

    # --- OPENING PIPES TO/FROM THE BUFFER ---

    def pipe_to_cur(self, attrs: enumspec=None) -> int:
        """Open a pipe to the buffer cursor.

        Arguments:
            attrs - The attributes to open the pipe with, as
                    decoder_attr_ts, e.g. "FNT", "UTF8", etc.
                    or None for defaults.

        This returns a file descriptor, which, when written to,
        will insert the characters into the buffer as if they
        were typed by the cursor.

        os.close() MUST be called on the file descriptor, so
        it is properly cleaned up!
        """
        return self.pipe_to_lines(attrs=attrs, cb=lib.file_read_to_cur_cb)

    def pipe_to_lines_repl(self, attrs: enumspec=None) -> int:
        """Open a pipe to replace the lines covered by the buffer cursor.

        Arguments:
            attrs - The attributes to open the pipe with, as
                    decoder_attr_ts, e.g. "FNT", "UTF8", etc.
                    or None for defaults.

        This returns a file descriptor, which, when written to,
        will replace lines in the current cursor. The read end
        will be closed once all lines in the cursor are replaced.

        Lines in the cursor are lines that the cursor wholly or
        partially selects.

        os.close() MUST be called on the file descriptor, so
        it is properly cleaned up!
        """
        cur = self.get_cur()
        lines = (cur.pri["row"], cur.sec["row"])
        return self.pipe_to_lines(lines=lines, attrs=attrs, cb=lib.file_read_to_buf_repl_cb)

    def pipe_to_buf(self, attrs: enumspec=None) -> int:
        """Open a pipe to insert lines at the start of this buffer.

        Arguments:
            attrs - The attributes to open the pipe with, as
                    decoder_attr_ts, e.g. "FNT", "UTF8", etc.
                    or None for defaults.

        This returns a file descriptor, which, when written to,
        inserts lines at the start of this buffer.

        os.close() MUST be called on the file descriptor, so
        it is properly cleaned up!
        """
        return self.pipe_to_lines(lines=(0, None), attrs=attrs)

    @only_for_alive
    def pipe_to_lines(
        self,
        lines: linerangespec     =None,
        attrs: enumspec          =None,
        cb                       =lib.file_read_to_buf_cb
    ) -> int:
        """Open a pipe to insert lines into this buffer.

        Arguments:
            lines - The range of lines to insert. By default,
                    the range is from the start of the cursor,
                    to infinity. Unbounded ranges are specified
                    with (start, None). e.g. to insert lines
                    between lines 10 and 20, lines=(10, 20).
            attrs - The attributes to open the pipe with, as
                    decoder_attr_ts, e.g. "FNT", "UTF8", etc.
                    or None for defaults.
            cb    - The C callback to insert a line.

        This returns a file decriptor, which, when written to,
        inserts lines at some point in this buffer.

        os.close() MUST be called on the file descriptor, so
        it is properly cleaned up!
        """
        if lines == None:
            cur = self.get_cur()
            lines = (min(cur.pri["row"], cur.sec["row"]), None)

        outfd, infd = os.pipe()

        handle = self.get_filehandle()
        handle.set_file(outfd)
        handle.line_range = lines
        if attrs != None:
            handle.set_attrs(attrs)

        handle.read(self.ptr, cb)

        return infd

    def pipe_from_buf(self, attrs: enumspec=None) -> int:
        """Open a pipe from which the buffer contents can be read.

        Arguments:
            attrs - The attributes to open the pipe with, as
                    decoder_attr_ts, e.g. "FNT", "UTF8", etc.
                    or None for defaults.

        This returns a file descriptor, from which can be read the
        contents of this buffer.

        os.close() should be called on the file descriptor after it
        is used.
        """
        return self.pipe_from_lines(lines=(0, None), attrs=attrs)

    @only_for_alive
    def pipe_from_lines(
        self,
        lines: linerangespec     =None,
        attrs: enumspec          =None,
        cb                       =lib.file_write_from_buf_cb
    ) -> int:
        """Open a pipe from which some lines of this buffer can be read.

        Arguments:
            lines - The range of lines to read from. By default,
                    this range is from the start to the end of the
                    cursor. Unbounded ranger are specified by
                    (start, None).
            attrs - The attributes to open the pipe with, as
                    decoder_attr_ts, e.g. "FNT", "UTF8", etc.
                    or None for defaults.
            cb    - The C callback to get lines.

        This returns a file descriptor, from which can be read some of
        the lines of contents in this buffer.

        os.close() should be called on the file descriptor after it
        is used.
        """
        if lines == None:
            cur = self.get_cur()
            lines = (cur.pri["row"], cur.sec["row"])

        outfd, infd = os.pipe()

        handle = self.get_filehandle()
        handle.set_file(infd)
        handle.line_range = lines
        if attrs != None:
            handle.attrs = attrs

        handle.write(self.ptr, cb)

        return outfd

    def get_filehandle(self) -> File:
        """Get a File() associated with this buffer.

        It will be killed when this buffer is purged,
        and it has finished.
        """
        handle = File(self.editor["pollctx"])
        self.add_filehandle(handle)
        return handle

    def purge_filehandles(self):
        """Kill all done File()s associated with this buffer."""
        new = []
        for f in self.filehandles:
            if f.done:
                f.kill()
            else:
                new.append(f)

        self.filehandles = new

    def add_filehandle(self, f: File):
        """Associate a filehandle with this buffer."""
        self.filehandles.append(f)
        self.purge_filehandles()

    # --- UN/REDO ---
    @only_for_alive
    def undo(self):
        """Undo one action."""
        lib.buf_undo(self.ptr)

    @only_for_alive
    def redo(self, branch: int=0):
        """Redo one action.

        Arguments:
            branch - If multiple paths can be taken for the redo, choose which.
        """
        lib.buf_redo(self.ptr, branch)

    @only_for_alive
    def undo_set_enable(self, val: bool):
        """Enable or disable un/redo for this buffer."""
        lib.buf_undo_set_enable(self.ptr, val)

    @only_for_alive
    def clear_undo(self):
        lib.buf_undo_clear(self.ptr)

from .editor import Editor

"""Cut the cursor in a buffer and return the contents.

Note: This function does NOT cut to a register.
"""
@add_global(args=["buf"])
def cut(b) -> str:
    res = cpy(b)
    lib.edit_cut(b.ptr, ctypes.pointer(b["einfo"]))

    return res

"""Copy the cursor in a buffer and return the contents."""
@add_global(args=["buf"])
def cpy(b) -> str:
    res = Vec(ctypes.c_char)
    lib.edit_cpy(b.ptr, ctypes.pointer(b["einfo"]), res.ptr)

    return bytes(res).decode("utf-8")

"""Insert characters into a buffer at the cursor position."""
@add_global(args=["buf"])
def ins(b, chrs: Union[str, bytes]):
    chrs = bytify(chrs)

    keys = Vec(ctypes.c_char)
    keys.ins(0, len(chrs), chrs)

    lib.edit_ins(b.ptr, ctypes.pointer(b["einfo"]), keys.ptr)

from .cur       import Cur
from .pos       import Pos
from .einfo     import EditInfo
from server.server import ShellServer
