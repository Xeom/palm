#include "util/time.h"
#include "util/macro.h"

#include "log/log.h"

#include "editor.h"
#include "event/timer.h"

static void timer_handle_tout(timer_ctx_s *ctx, timer_s *timer);
static timer_s *timer_pop(timer_ctx_s *ctx);
static void timer_push(timer_ctx_s *ctx, timer_s *timer);
static void timer_remove_from_event_ctx(timer_s *timer, event_ctx_s *ctx);

static void timer_handle_tout(timer_ctx_s *ctx, timer_s *timer)
{
    event_s event = {
        .spec = { .typ = EVENT_TIMER, .ptr = timer }
    };

    event_push(ctx->eventctx, &event);

    if (timer->attrs & TIMER_ATTR_REPEAT)
    {
        double now;
        now = util_time_fsec();

        timer->expiry += timer->delay;

        if (timer->expiry < now)
            timer->expiry = now + timer->delay;

        timer_push(ctx, timer);
    }
    else
    {
        SET_BITS(timer->attrs, TIMER_ATTR_RUNNING, false);
    }
}

static timer_s *timer_pop(timer_ctx_s *ctx)
{
    timer_s *timer;
    timer = ctx->first;

    if (!timer)
        return NULL;

    if (timer->next)
        timer->next->prev = NULL;
    else
        ctx->last = NULL;

    ctx->first = timer->next;

    timer->next = NULL;
    timer->prev = NULL;

    return timer;
}

/**
 * A callback that is run after the expiry of a timer.
 *
 * This is the final action to be taken in response to the expiry event,
 * and cleans up the timer.
 *
 */
static void timer_release_cb(editor_s *editor, event_s *event, void *param)
{
    timer_s *timer;
    timer = param;

    if (timer->attrs & TIMER_ATTR_REPEAT)
        return;

    timer_remove_from_event_ctx(timer, &(editor->eventctx));

    if (timer->attrs & TIMER_ATTR_FREE)
        free(timer);

    event->attrs |= EVENT_ATTR_CANCELLED;
}

static void timer_add_release_cb(timer_ctx_s *ctx, timer_s *timer)
{
    event_spec_s spec;
    timer_event_spec(timer, &spec);
    event_listen(ctx->eventctx, &spec, &(event_action_s){
        .cb = timer_release_cb,
        .param = timer,
        .pri   = 1000
    });
}

static void timer_push(timer_ctx_s *ctx, timer_s *timer)
{
    timer_s *prev;
    prev = ctx->last;

    timer_add_release_cb(ctx, timer);

    while (prev && prev->expiry > timer->expiry)
        prev = prev->prev;

    if (prev)
    {
        timer->prev = prev;
        timer->next = prev->next;

        if (prev->next)
            prev->next->prev = timer;
        else
            ctx->last = timer;

        prev->next = timer;
    }
    else
    {
        timer->next = ctx->first;
        timer->prev = NULL;

        if (ctx->first)
            ctx->first->prev = timer;
        else
            ctx->last = timer;

        ctx->first = timer;
    }
}

int timer_ctx_init(timer_ctx_s *ctx, event_ctx_s *eventctx)
{
    pthread_mutexattr_t attr;

    ctx->eventctx = eventctx;
    ctx->first    = NULL;
    ctx->last     = NULL;

    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP);

    pthread_mutex_init(&(ctx->mtx), &attr);

    pthread_mutexattr_destroy(&attr);

    return 0;
}

void timer_ctx_kill(timer_ctx_s *ctx)
{
    pthread_mutex_destroy(&(ctx->mtx));
}

int timer_ctx_handle(timer_ctx_s *ctx)
{
    double now;
    int res;
    now = util_time_fsec();

    res = 0;
    WITH_MTX(&(ctx->mtx))
    {
        while (ctx->first && ctx->first->expiry <= now)
        {
            timer_s *timer;
            timer = timer_pop(ctx);
            timer_handle_tout(ctx, timer);

            res = 1;
        }
    }

    return res;
}

double timer_ctx_get_expiry(timer_ctx_s *ctx)
{
    if (ctx->first)
        return ctx->first->expiry;
    else
#if defined(NAN)
        return NAN;
#else
        return 0.0 / 0.0;
#endif
}

void timer_add(timer_ctx_s *ctx, timer_s *timer)
{
    double now;
    now = util_time_fsec();

    if (timer->attrs & TIMER_ATTR_USEEXPIRY)
    {
        if (timer->expiry > now + 3600.0 || timer->expiry < now - 60.0)
            LOG(ALERT, INFO, "Suspicious timer: set %f seconds in the future", timer->expiry - now);

        timer->delay = timer->expiry - util_time_fsec();
    }
    else
    {
        if (timer->delay <= 0.00001 || timer->delay > 3600.0)
            LOG(ALERT, INFO, "Suspicious timer: set %f second duration", timer->delay);

        timer->expiry = util_time_fsec() + timer->delay;
    }

    SET_BITS(timer->attrs, TIMER_ATTR_RUNNING, true);

    WITH_MTX(&(ctx->mtx))
    {
        timer_push(ctx, timer);
    }
}

void timer_rem(timer_ctx_s *ctx, timer_s *timer)
{
    WITH_MTX(&(ctx->mtx))
    {
        if (timer->attrs & TIMER_ATTR_RUNNING)
        {
            if (timer->next)
                timer->next->prev = timer->prev;
            else
                ctx->last = timer->prev;

            if (timer->prev)
                timer->prev->next = timer->next;
            else
                ctx->first = timer->next;

            timer->next = NULL;
            timer->prev = NULL;

            SET_BITS(timer->attrs, TIMER_ATTR_RUNNING, false);
        }
    }


    timer_remove_from_event_ctx(timer, ctx->eventctx);

    /* This function should not be called on ATTR_FREE timers!  *
     *                                                          *
     * The caller of timer_add() should discard pointers to the *
     * timer_s structure immediately after calling timer_add()! */
    if (timer->attrs & TIMER_ATTR_FREE)
    {
        LOG(ALERT, ERR, "timer_rem() should NOT be called on ATTR_FREE timers!");
        free(timer);
    }
}

static void timer_remove_from_event_ctx(timer_s *timer, event_ctx_s *ctx)
{
    event_spec_s spec;

    timer_event_spec(timer, &spec);
    event_unlisten(ctx, &spec, NULL);
}

static void timer_with_cb(timer_ctx_s *ctx, double delay, timer_attr_t attrs, event_cb_t cb, void *param)
{
    timer_s *timer;
    event_spec_s spec;
    event_action_s act = {
        .cb    = cb,
        .param = param
    };

    timer = malloc(sizeof(timer_s));
    *timer = (timer_s){
        .delay = delay,
        .attrs  = attrs
    };

    timer_event_spec(timer, &spec);
    event_listen(ctx->eventctx, &spec, &act);

    timer_add(ctx, timer);
}

void timer_after(timer_ctx_s *ctx, double delay, event_cb_t cb, void *param)
{
    timer_with_cb(ctx, delay, TIMER_ATTR_FREE, cb, param);
}

void timer_every(timer_ctx_s *ctx, double delay, event_cb_t cb, void *param)
{
    timer_with_cb(ctx, delay, TIMER_ATTR_FREE | TIMER_ATTR_REPEAT, cb, param);
}
