#include "editor.h"

#include "cmdlang/ctx.h"

#include "cmdlang/parser.h"

int cmdlang_parser_init(
    cmdlang_parser_s *parser, cmd_ctx_s *ctx, cmd_lib_s *lib, errmsg_t errmsg
)
{
    parser->ctx = ctx;
    parser->lib = lib;

    parser->attrs = CMDLANG_PARSER_GREEDY;

    TRY(cmdlang_lexer_init(&(parser->lexer), lib, errmsg));
    cmdlang_tok_init(&(parser->tok));

    return PALM_OK;
}

void cmdlang_parser_kill(cmdlang_parser_s *parser)
{
    cmdlang_lexer_kill(&(parser->lexer));
    cmdlang_tok_kill(&(parser->tok));
}

static int cmdlang_parser_push_token(cmdlang_parser_s *parser, errmsg_t errmsg)
{
    bool verb;

    if (cmdlang_tok_is_verb(&(parser->tok), parser->lib, &verb) == PALM_ERR)
    {
        ERRMSG(errmsg, "Unknown command.");
        return PALM_ERR;
    }

    TRY(cmd_ctx_push_token(parser->ctx, &(parser->tok), errmsg));

    memset(&(parser->tok), 0, sizeof(cmdlang_tok_s));
    cmdlang_tok_init(&(parser->tok));

    if (verb && (parser->attrs & CMDLANG_PARSER_GREEDY))
    {
        TRY(cmdlang_parser_run(parser, errmsg));
    }

    return PALM_OK;
}

int cmdlang_parser_type(cmdlang_parser_s *parser, char c, errmsg_t errmsg)
{
    bool next;

    SET_BITS(parser->attrs, CMDLANG_PARSER_HAS_RUN, false);

    do
    {
        TRY(cmdlang_lexer_chr(&(parser->lexer), c, &next, errmsg));

        switch (cmdlang_lexer_token(&(parser->lexer), &(parser->tok), errmsg))
        {
        case PALM_OK:
            break;
        case PALM_ERR:
            return PALM_ERR;
        case PALM_COMPLETE:
            TRY(cmdlang_parser_push_token(parser, errmsg));
            break;
        }
    } while (!next);

    return PALM_OK;
}

int cmdlang_parser_run(cmdlang_parser_s *parser, errmsg_t errmsg)
{
    TRY(cmdlang_parser_type(parser, '\n', errmsg));

    SET_BITS(parser->attrs, CMDLANG_PARSER_HAS_RUN, true);

    if (parser->lexer.state != CMDLANG_LEXER_IDLE)
    {
        ERRMSG(errmsg, "Unexpected end of command.");
        return PALM_ERR;
    }

    cmdlang_lexer_kill(&(parser->lexer));
    TRY(cmdlang_lexer_init(&(parser->lexer), parser->lib, errmsg));

    TRY(cmd_ctx_run(parser->ctx, errmsg));
    TRY(cmd_ctx_print_tokens(parser->ctx));
    TRY(cmd_ctx_cancel(parser->ctx));

    return PALM_OK;
}

int cmdlang_parser_cancel(cmdlang_parser_s *parser, errmsg_t errmsg)
{
    cmdlang_lexer_kill(&(parser->lexer));
    TRY(cmdlang_lexer_init(&(parser->lexer), parser->lib, errmsg));

    TRY(cmd_ctx_cancel(parser->ctx));

    SET_BITS(parser->attrs, CMDLANG_PARSER_HAS_RUN, false);

    return 0;
}
