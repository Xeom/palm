#include <unistd.h>
#include "io/pyclient.h"
#include "log/log.h"
#include "io/io.h"

int pyclient_send(int pyfd, vec_s *send, vec_s *recv)
{
    char errbuf[IO_ERRBUF_LEN];

    /* Ensure we send a message ending in \x00 */
    if (vec_len(send) == 0 || *(char *)vec_end(send) != '\0')
    {
        vec_app(send, NULL);
    }

    LOG(DEBUG, INFO, "Pyclient (fd %d) sending '%s'", pyfd, (char *)vec_get(send, 0));

    while (vec_len(send) > 0)
    {
        io_file_info_s wfile = {
            .fd = pyfd,
            .writevec = send
        };

        switch (io_poll_file(&wfile, 5000, errbuf))
        {
        case 0:
            LOG(NOTICE, ERR, "Pyclient (fd %d) write timed out", pyfd);
            return -1;
        case -1:
            LOG(NOTICE, ERR, "Pyclient (fd %d) error writing: %s", pyfd, errbuf);
            return -1;
        }

        if (wfile.status & IO_FILE_EOF)
        {
            LOG(NOTICE, ERR, "Pyclient (fd %d) got EOF while writing", pyfd);
            return -1;
        }
    }

    vec_clr(recv);
    while (1)
    {
        io_file_info_s rfile = {
            .fd = pyfd,
            .ntoread = 1,
            .readvec = recv
        };

        switch (io_poll_file(&rfile, -1, errbuf))
        {
        case 0:
            LOG(NOTICE, ERR, "Pyclient (fd %d) read timed out", pyfd);
            return -1;
        case -1:
            LOG(NOTICE, ERR, "Pyclient (fd %d) error reading: %s", pyfd, errbuf);
            return -1;
        }

        if (vec_len(recv) && *(char *)vec_end(recv) == '\0')
            break;

        if (rfile.status & IO_FILE_EOF)
        {
            LOG(NOTICE, ERR, "Pyclient (fd %d) got EOF while reading", pyfd);
            return -1;
        }
    }

    return 0;
}
