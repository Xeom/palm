import ctypes

from .event     import EventSpec, EventAction
from .newstruct import CStructure

from lib       import lib
from lib.enums import match_attr_t
from core.vec  import Vec

from util.encode import bytify

from server.globals import add_global

@add_global()
class Reader(CStructure):
    init_funct = lib.reader_init
    kill_funct = lib.reader_kill

    class_name = "reader_s"

    def init_args(self, args, kwargs):
        def f(poll, fd, linesepattrs=0, linesep=b"\n"):
            return [poll.ptr, fd, match_attr_t(linesepattrs), bytify(linesep)]

        return f(*args, **kwargs)

    @property
    def eventctx(self):
        return self.pollctx.contents.eventctx.contents


    def start(self):
        if lib.reader_start(self.ptr) == -1:
            raise Exception("Could not start reader")

    def stop(self):
        if lib.reader_stop(self.ptr) == -1:
            raise Exception("Could not stop reader")

    def on_recv(self, args=[]):
        spec = EventSpec(
            _ptr = self.ptr,
            typ  = "READER_RECV"
        )

        def inner(f):
            @self.eventctx.on(spec, [self])
            def cb(reader):
                f(reader.get_line(), *args)

            return cb

        return inner

    def on_eof(self, args=[]):
        spec = EventSpec(
            _ptr = self.ptr,
            typ  = "READER_EOF"
        )

        def inner(f):
            @self.eventctx.on(spec, [self])
            def cb(reader):
                f(*args)

            return cb

        return inner

    def get_line(self) -> bytes:
        vec = Vec(ctypes.c_char)
        res = lib.reader_get_line(self, vec)

        if res == 0:
            return None
        elif res == 1:
            return bytes(vec)
        elif res == -1:
            raise Exception("Could not get line from reader")
