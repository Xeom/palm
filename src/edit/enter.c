#include "buf/buf.h"
#include "text/indent.h"
#include "edit/enter.h"

void edit_break_line_at(edit_info_s *einfo, pos_s *pos)
{
    pos_s newpos;
    col_t nmove, indent;
    vec_s line1, line2;

    if (buf_len(einfo->buf) == 0)
        buf_ins_line(einfo->buf, 0, 1);

    if (pos->row >= buf_len(einfo->buf))
        return;

    vec_init(&line1, sizeof(chr_s));
    vec_init(&line2, sizeof(chr_s));

    buf_get_line(einfo->buf, pos->row, &line1);
    buf_ins_line(einfo->buf, pos->row + 1, 1);

    pos->col = MIN((col_t)vec_len(&line1), pos->col);
    nmove    = MAX(0, (col_t)vec_len(&line1) - pos->col);

    if (nmove > 0)
    {
        vec_ins(&line2, 0, nmove, vec_get(&line1, pos->col));
        vec_del(&line1, pos->col, nmove);
    }

    if (einfo->attrs & EDIT_AUTOINDENT)
    {
        indent = edit_get_indent(einfo, &line1);
        edit_set_indent(einfo, &line2, indent);
    }
    else
    {
        indent = 0;
    }

    if (einfo->attrs & EDIT_AUTORSTRIP)
    {
        edit_rstrip(einfo, &line1);
        edit_rstrip(einfo, &line2);
    }

    buf_set_line(einfo->buf, pos->row,     &line1);
    buf_set_line(einfo->buf, pos->row + 1, &line2);

    newpos = (pos_s){ .row = pos->row + 1, .col = indent };

    buf_shift(einfo->buf, pos, &newpos);
    *pos = newpos;

    vec_kill(&line1);
    vec_kill(&line2);
}

void edit_break_column_at(edit_info_s *einfo, pos_s *pos)
{
    pos_s newpos;
    indent_t indent;

    vec_s line1, line2;

    if (buf_len(einfo->buf) == 0)
        buf_ins_line(einfo->buf, 0, 1);

    if (pos->row >= buf_len(einfo->buf))
        return;

    vec_init(&line1, sizeof(chr_s));
    vec_init(&line2, sizeof(chr_s));

    buf_get_line(einfo->buf, pos->row, &line1);
    buf_ins_line(einfo->buf, pos->row + 1, 1);

    pos->col = MIN((col_t)vec_len(&line1), pos->col);

    indent = indent_from_col(&line1, einfo->chrconf, pos->col);

    newpos = (pos_s){
        .row = pos->row + 1,
        .col = edit_pad_to_indent(einfo, &line2, indent)
    };

    *pos = newpos;

    buf_set_line(einfo->buf, pos->row, &line1);
    buf_set_line(einfo->buf, pos->row + 1, &line2);


    vec_kill(&line1);
    vec_kill(&line2);
}

void edit_enter(edit_info_s *einfo)
{
    cur_s cur;

    edit_get_cur(einfo, &cur);

    switch (cur.type)
    {
    case CUR_TYPE_LINE:
    case CUR_TYPE_CHR:
        edit_break_line_at(einfo, &(cur.pri));
        break;

    case CUR_TYPE_BLOCK:
        cur.pri.col = MIN(cur.pri.col, cur.sec.col);
        edit_break_column_at(einfo, &(cur.pri));
        cur.sec = cur.pri;
        buf_set_cur(einfo->buf, &cur);
        break;
    }
}

void edit_preline_enter(edit_info_s *einfo)
{
    cur_s cur;

    edit_get_cur(einfo, &cur);
    edit_break_line_at(einfo, &(pos_s){ .row = cur.pri.row, .col = 0 });
}
