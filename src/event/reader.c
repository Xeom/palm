#include "event/reader.h"

#include "event/poll.h"
#include "container/vec.h"
#include "log/log.h"

static void reader_poll_cb(editor_s *editor, event_s *event, void *param);

int reader_init(
    reader_s *reader,
    poll_ctx_s *pollctx,
    int fd,
    match_attr_t linesepattrs,
    const char *linesep
)
{
    reader->pollctx = pollctx;
    reader->fd      = fd;
    reader->alive   = false;

    vec_init(&(reader->buf), sizeof(char));
    vec_init(&(reader->lines), sizeof(vec_s));
    reader->finfo = (io_file_info_s){
        .fd = fd,
        .readvec = &(reader->buf)
    };

    match_set_init(&(reader->linesep));
    match_set_add(&(reader->linesep), linesepattrs, linesep);

    return 0;
}

int reader_kill(reader_s *reader)
{
    poll_ctx_s *pollctx;
    event_ctx_s *eventctx;
    event_spec_s spec;

    pollctx  = reader->pollctx;
    eventctx = pollctx->eventctx;

    reader_stop(reader);
    vec_kill(&(reader->buf));
    vec_kill(&(reader->lines));

    reader_recv_spec(reader, &spec);
    event_unlisten(eventctx, &spec, &(event_action_s){
        .cb = reader_poll_cb,
        .param = reader
    });

    reader_eof_spec(reader, &spec);
    event_unlisten(eventctx, &spec, &(event_action_s){
        .cb = reader_poll_cb,
        .param = reader
    });

    return 0;
}

int reader_start(reader_s *reader)
{
    poll_ctx_s *pollctx;
    event_ctx_s *eventctx;
    event_spec_s spec;

    if (reader->alive)
        return 0;

    pollctx  = reader->pollctx;
    eventctx = pollctx->eventctx;

    poll_event_spec(&(reader->finfo), &spec);
    if (event_listen(eventctx, &spec, &(event_action_s){
           .cb = reader_poll_cb,
           .param = reader
        }) == -1)
    {
        return -1;
    }

    reader->alive = true;

    poll_add(pollctx, &(reader->finfo));

    return 0;
}

int reader_stop(reader_s *reader)
{
    if (!reader->alive)
        return 0;

    poll_rem(reader->pollctx, &(reader->finfo));

    reader->alive = false;

    return 0;
}

static void reader_poll_cb(editor_s *editor, event_s *eventprm, void *param)
{
    reader_s *reader;
    reader = param;

    if (reader->finfo.status & IO_FILE_READABLE)
    {
        bool noline;

        noline = false;
        while (!noline)
        {
            /* TODO: optimize for faster matching! */
            match_result_s res;
            event_s event;
            vec_s *line;
            switch (match_set_search(
                &(reader->linesep), &(reader->buf), 0, &res, false
            ))
            {
            case 0:
                noline = true;
                break;

            case 1:
                line = vec_app(&(reader->lines), NULL);

                if (!line)
                {
                    LOG(ALERT, ERR, "Could not allocate space for line vector");
                    return;
                }

                vec_init(line, sizeof(char));
                if (!vec_add(line, res.end, vec_get(&(reader->buf), 0)))
                {
                    LOG(ALERT, ERR, "Could not append line to vector");
                    return;
                }
                vec_del(&(reader->buf), 0, res.end);

                memset(&event, 0, sizeof(event_s));
                reader_recv_spec(reader, &(event.spec));
                event_push(reader->pollctx->eventctx, &event);
                break;
            case -1:
                LOG(ALERT, ERR, "Could not search for linesep");

                return;
            }
        }
    }

    if (reader->finfo.status & (IO_FILE_EOF | IO_FILE_CLOSED))
    {
        event_s event;
        memset(&event, 0, sizeof(event_s));
        reader_eof_spec(reader, &(event.spec));
        event_push(reader->pollctx->eventctx, &event);

        reader_stop(reader);
    }
}

int reader_get_line(reader_s *reader, vec_s *res)
{
    vec_s *line;

    line = vec_get(&(reader->lines), 0);
    if (!line)
        return 0;

    vec_cpy(res, line);
    vec_del(&(reader->lines), 0, 1);

    return 1;
}

