#if !defined(BUF_BUF_H)
# define BUF_BUF_H
# include <pthread.h>
# include "container/vec.h"
# include "types.h"
# include "buf/cur.h"
# include "buf/line.h"
# include "buf/finfo.h"
# include "bind/bufbind.h"
# include "edit/editinfo.h"
# include "hl/worker.h"
# include "buf/undo.h"
# include "buf/curset.h"
# include "cmdlang/ctx.h"
# include "cmdlang/parser.h"

struct buf
{
    pthread_mutex_t  mtx;

    vec_s            lines;

    int              winind;
    long             scrx, scry;

    vec_s            cmdparams;

    line_s           blankline;
    chrconf_s        chrconf;

    finfo_s          finfo;

    bufbind_s        bind;
    edit_info_s      einfo;
    undo_list_s      undolist;

    vec_s            defhl;
    hl_worker_s      hlworker;

    curset_s         curset;

    cmd_ctx_s        cmdctx;
    cmdlang_parser_s cmdparser;

    editor_s        *editor;
};

void buf_kill(buf_s *buf);

void buf_init(buf_s *buf, editor_s *editor);

row_t buf_len(buf_s *buf);

int buf_win_assoc(buf_s *buf, int winind);

int buf_get_win(buf_s *buf);

void buf_set_defhl(buf_s *buf, vec_s *defhl);

int buf_hlstate(buf_s *buf, row_t ln, vec_s *get, vec_s *set);

int buf_get_line(buf_s *buf, row_t ln, vec_s *chrs);

int buf_get_str(buf_s *buf, row_t ln, vec_s *chrs);

int buf_get_expanded(buf_s *buf, row_t ln, vec_s *chrs);

int buf_set_line(buf_s *buf, row_t ln, vec_s *chrs);

int buf_cpy_font(buf_s *buf, row_t ln, vec_s *chrs);

int buf_set_font(buf_s *buf, row_t ln, unsigned short font, size_t ind, size_t n);

int buf_ins_line(buf_s *buf, row_t ln, size_t n);

int buf_del_line(buf_s *buf, row_t ln, size_t n);

size_t buf_line_len(buf_s *buf, row_t ln);

void buf_refresh(buf_s *buf, row_t ln, size_t n);

void buf_move_cur(buf_s *buf, cur_dir_t dir, int n);

void buf_get_scr(buf_s *buf, long *x, long *y);

void buf_set_scr(buf_s *buf, long x, long y);

wm_s *buf_wm(buf_s *buf);

void buf_pos_to_ind(buf_s *buf, pos_s *pos);
void buf_ind_to_pos(buf_s *buf, pos_s *pos);

void buf_off_to_ind(buf_s *buf, pos_s *pos);
void buf_ind_to_off(buf_s *buf, pos_s *pos);

void buf_chrconf(buf_s *buf, chrconf_s *get, chrconf_s *set);
#endif
