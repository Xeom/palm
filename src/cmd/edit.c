#include "cmd/edit.h"
#include "cmdlang/helpers.h"
#include "container/vec.h"
#include "buf/buf.h"
#include "edit/edit.h"
#include "util/macro.h"

/* --- COMMANDS FOR EDITING TEXT --- *
 *                                   *
 * In this file, are commands for    *
 * editing text in files.            *
 *                                   *
 * --------------------------------- */

/* --- Commands Bound to edit_* Functions --- */

/* If you need to call these programatically, just call the *
 * edit_* function!                                         */

/** Insert Command.
 *
 * Inserts into the current buffer, the keys that the command
 * was called with.
 */
void insert_binding(key_s *key, buf_s *buf, void *arg, size_t len)
{
    vec_s str;

    vec_init(&str, sizeof(char));
    key_to_string(key, &str);
    edit_ins_vec(&(buf->einfo), &str);
    vec_kill(&str);
}

VERB(insert,
    "Insert a string into the current buffer."
    " The exact behaviour of this command depends on"
    " the current editing modes.",
    ADD_REQ_ARG(CMDLANG_OBJ_STR, "string", "The string to insert.")
)
{
    edit_ins_vec(&(ctx->buf->einfo), &(args[0].val.str));

    return PALM_OK;
}

VERB(backsp,
    "Delete backwards."
    " The exact behaviour of this command depends on"
    " the current editing modes.",
    NO_ARGS)
{
    edit_backspace(&(ctx->buf->einfo));

    return PALM_OK;
}

VERB(del,
    "Delete the current character."
    " The exact behaviour of this command depends on"
    " the current editing modes.",
    NO_ARGS)
{
    edit_del(&(ctx->buf->einfo));

    return PALM_OK;
}

VERB(enter,
    "Enter to get a new line."
    " The exact behaviour of this command depends on"
    " the current editing modes.",
    NO_ARGS)
{
    edit_enter(&(ctx->buf->einfo));

    return PALM_OK;
}

VERB(preline_enter,
    "Insert a new line before the current one."
    " The exact behaviour of this command depends on"
    " the current editing modes.",
    NO_ARGS)
{
    edit_preline_enter(&(ctx->buf->einfo));

    return PALM_OK;
}

VERB(undo, "Undo the last action.", NO_ARGS)
{
    buf_undo(ctx->buf);

    return PALM_OK;
}

VERB(redo, "Redo the last action.",
    ADD_OPT_ARG(
        CMDLANG_OBJ_NUMBER, "branch",
        "The branch of history to select, starting from 0."
        " This is used where there are multiple possible redos to select "
        " from, for example if you undo an action, do something else, "
        " and undo again."
    )
)
{
    long branch;

    if (nargs == 0)
        branch = 0;
    else
        branch = args[0].val.number;

    buf_redo(ctx->buf, branch);

    return PALM_OK;
}

VERB(add_indent,
    "Add an indent to the current line(s), rounding to a multiple of the"
    " requested depth. If un-indenting, we round up, otherwise we round down.",
    ADD_OPT_ARG(
        CMDLANG_OBJ_NUMBER, "depth",
        "The number of spaces change the indentation by."
    )
)
{
    long delta;

    if (nargs == 1)
        delta = args[0].val.number;
    else
        delta = 1;

    edit_shift_indent(&(ctx->buf->einfo), delta);

    return PALM_OK;
}

VERB(toggle_edit_attrs,
    "This command will toggle a mask of editing attributes. "
    "e.g. Overwrite mode, or noalts mode.",
    ADD_REQ_ARG(
        CMDLANG_OBJ_NUMBER, "mask",
        "The bits to toggle, as edit_attr_t"
    )
)
{
    edit_attr_t mask;

    if (nargs == 1)
        mask = args[0].val.number;
    else
        mask = EDIT_OW;

    ctx->buf->einfo.attrs ^= mask;

    /* Necessary hack to update any buffer colours */
    buf_shift(ctx->buf, &(pos_s){ 0, 0 }, &(pos_s){ 0, 0 });

    return PALM_OK;
}

int cmd_edit_mount(cmd_lib_s *lib)
{
    MOUNT(insert);

    MOUNT(backsp); MOUNT(del);

    MOUNT(enter); MOUNT(preline_enter);

    MOUNT(undo); MOUNT(redo);

    MOUNT(add_indent);

    MOUNT(toggle_edit_attrs);

    return PALM_OK;
}
