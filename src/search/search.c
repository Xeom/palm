#include "search/search.h"
#include "util/escape.h"
#include "log/log.h"

static void search_job(void *arg);

int search_init(search_s *search)
{
    memset(search, 0, sizeof(search_s));
    match_set_init(&(search->matchset));

    search->done  = true;
    search->found = false;

    pool_job_init(&(search->job), search_job, search);

    return 0;
}

int search_add_needle(search_s *search, match_attr_t attrs, const char *str, char flag)
{
    match_s *match;

    match = match_set_add(&(search->matchset), attrs, str);

    if (!match)
    {
        LOG(ALERT, ERR, "Could not add search needle '%s', flag %d", str, flag);
        return -1;
    }

    match->usrchr = flag;

    return 0;
}

void search_kill(search_s *search)
{
    pool_job_await(&(search->job));
    pool_job_kill(&(search->job));

    match_set_kill(&(search->matchset));
}

void search_start(search_s *search, buf_s *buf, pos_s off, pool_s *pool, bool backward)
{
    pool_job_await(&(search->job));
    search->off   = off;
    buf_ind_to_off(buf, &(search->off));

    search->done     = false;
    search->buf      = buf;
    search->backward = backward;

    pool_add_job(pool, &(search->job));
}

void search_continue(search_s *search, pool_s *pool)
{
    pool_job_await(&(search->job));
    pool_add_job(pool, &(search->job));
}

bool search_get_result(search_s *search, search_result_s *result)
{
    pool_job_await(&(search->job));
    if (search->done && search->found)
    {
        memcpy(result, &(search->result), sizeof(search_result_s));

        return true;
    }

    return false;
}

bool search_is_done(search_s *search)
{
    return search->done;
}

static void search_line(search_s *search, vec_s *line, bool backward)
{
    match_result_s result;

    if (search->found)
        return;

    switch (match_set_search(&(search->matchset), line, search->off.col, &result, backward))
    {
    case 0:
        break;
    case 1:
        search->found = true;
        search->result = (search_result_s) {
            .flag = result.match->usrchr,
            .start = { .row = search->off.row, .col = result.start },
            .end   = { .row = search->off.row, .col = result.end   }
        };
        buf_off_to_ind(search->buf, &(search->result.start));
        buf_off_to_ind(search->buf, &(search->result.end));
        break;
    case -1:
        LOG(ALERT, ERR, "Error searching line");
        break;
    }

    if (search->found)
    {
        if (backward)
        {
            search->off.col = result.start;
            if (memcmp(&(search->result.start), &(search->result.end), sizeof(pos_s)) == 0)
                search->off.col -= 1;
        }
        else
        {
            search->off.col = result.end;
            if (memcmp(&(search->result.start), &(search->result.end), sizeof(pos_s)) == 0)
                search->off.col += 1;
        }
    }
}

static void search_job(void *arg)
{
    vec_s line;
    vec_init(&line, sizeof(char));
    search_s *search;
    search = arg;

    search->found = false;

    if (search->backward)
    {
        while (search->off.row >= 0 && !search->found)
        {
            buf_get_str(search->buf, search->off.row, &line);
            search_line(search, &line, true);

            if (!search->found)
            {
                search->off.row -= 1;
                search->off.col  = buf_line_len(search->buf, search->off.row);
            }
        }
    }
    else
    {
        while (search->off.row < (long)buf_len(search->buf) && !search->found)
        {
            buf_get_str(search->buf, search->off.row, &line);
            search_line(search, &line, false);

            if (!search->found)
            {
                search->off.row += 1;
                search->off.col  = 0;
            }
        }
    }

    search->done = true;
    vec_kill(&line);
}
