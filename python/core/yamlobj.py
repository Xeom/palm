import os
import libtypes
import ctypes
from lib import lib
from .structure import Structure

class YAMLObj(Structure):
    typ = libtypes.yaml_obj_s
    init_funct = lib.yaml_obj_init
    kill_funct = lib.yaml_obj_kill

    def init_args(self):
        return [None, 0]

    def parse(self, fname):
        import cfiles
        fn = os.open(fname, os.O_RDONLY)
        fptr = lib.fdopen(fn, b"r")

        if lib.yaml_obj_parse(self.ptr, fptr) == -1:
            raise Exception("Could not parse YAML")
