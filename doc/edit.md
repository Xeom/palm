Guide to Editing text with Palm
===============================

Cursors
-------

Each buffer in Palm has a cursor associated with it. The cursor
is represented by a struct which looks something like this:

```c
struct cur
{
    bool sticky;
    cur_type_t type;
    struct {
        long row, col;
    } pri;
    struct {
        long row, col;
    } sec;
}
```

The cursor stores two positions, a primary position and a secondary
posion. It also stores the 'type' of the cursor, which corresponds
to one of `CHR`, `LINE` or `BLOCK`. Whether the cursor is `sticky`.

A buffer's cursor can be manipulated by using the functions
`buf.get_cur` and `buf.set_cur`.

### Basic commands

The `h`, `j`, `k`, and `l` commands move cursors around by one
place. They have arrow keys as their shortcuts.

The `H`, `J`, `K`, and `L` commands meanwhile, move cursors to the
end of the screen, and the end of the line. They are bound to
`PgUp`, `PgDown`, `End` and `Home`.

The `q` command can be used to print the cursor's current attributes.

In `edit` mode, typing any character that isn't part of a binding
will insert that character, `Delete`, `Backspace` and `Enter`,
have their usual meanings. The `>` and `<` commands, bound to `Tab`
and `Shift+Tab`, will indent or dedent the current line.

### Selecting Text

If you run the `s` command, by pressing `Esc`, `s`, or it's shortcut, 
`Ctrl+s`, your cursor will enter sticky mode. 
When the cursor is 'sticky' then the primary part of the
cursor moves independently from the secondary part, and the primary
part can be moved away from the secondary part to select a number of
characters between the two parts. Once you are done with your 
selection, the `s` command can be run again to move the secondary
cursor to the primary cursor, and to un-sticky the cursor.

The `w` command (Shortcut `Ctrl+w`),
can be used to swap the primary and secondary positions of the cursor.
This can be useful to adjust the starting point of your selection, for
example.

### Cursor Shapes

There are three shapes of cursors supported by Palm, all of which
behave differently.

#### CHR Cursors

Character cursors work the normal way that you would expect.
They are used to insert characters, and are the default cursor in
Palm. The `cc` command will set the cursor to a character cursor.

#### LINE Cursors

Line cursors select a whole line at once. Even without using the
`s` command, the entire current line is highlighted, and can be
cut, copied, or pasted. They also copy and paste whole lines,
without splitting them, making them excellent cursors for
manipulating whole lines. Entering a new-line with a line cursor
will not split the current line. The `cl` (Shortcut `Ctrl+l`)
command will toggle on and off a line cursor. In order to cut
the current line, for example, you could run the commands `cl`,
`Y`.

#### BLK Cursors

Block cursors select a square of characters between the primary
and secondary cursor. If you select a block sticky cursor,
type something, and press enter, you the cursor will return to
the starting column on the next line, padding the next line with
spaces if needed. Using this feature, a block cursor is ideal for
typing aligned columns of text, and an overwriting block cursor
is ideal for filling in a table.

If multiple lines of text are selected with the cursor, any
text inserted or deleted is duplicated to each line.

The `ck` (Shortcut `Ctrl+k`)
command will toggle on and off a sticky block cursor.

### Multiple Cursors

A buffer in Palm can contain more than one cursor. Each buffer
has a vector of alternate cursors, accessible as `buf.altcurs`.

These alternate cursors are all controlled at once by commands.
Some commands are marked as non-multicursor, such as opening
a new window, and some such as copying are run in reverse order.
The `:` command can be used to disable or enable all multicursor
commands.

The `'` command can be used to place an alternate cursor at the current
cursor position. This also disables multicursor commands, so you can
move the cursor from on top of the new alternate cursor.

The `@` command can be used to remove the current cursor, and the
`cn` and `cp` commands can be used to select a particular alternate
cursor.

If there are already multiple cursors, the `;` command will clear
them all, otherwise, it will place a cursor on each line of the
current selection.

Copying and Pasting
-------------------

Palm uses a series of 'registers' to keep text for copying and pasting.
They each contain a series of bytes. This is a utf-8 encoded
representation of text, split into lines by the `\n` character.

These registers are used for copying, pasting, and for recording
keypresses.

You can access any of these registers in the prompt with the `reg`
module, exposing the functions `reg.get`, `reg.set`, `reg.push`,
`reg.pop`, `reg.chr` (append character to reg), and `reg.num` (total
number of used registers)

The copy command, `y`, will push text selected in the current buffer
into one of these registers, and the paste command, `p`,
will insert the text in the first register into
the current buffer, at the position of the cursor.
The cut and poppaste commands, `Y` and `P`, are similar, but destroy the
text at its source. The cut command will delete the selected text from
the buffer, and the poppaste command will pop the first register rather
than just looking at it.

Keypresses in `record` mode are also pushed into a register, and can be
examined by pasting them, before being modified and copied again, and
then played back by the `P` command.

