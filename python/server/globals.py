from typing import Optional, List

defaultglobals = []

class GlobalVar(object):
    def __init__(self, obj, name: Optional[str]=None, args: Optional[List[str]]=None):
        self.obj = obj
        if name == None:
            self.name = obj.__name__
        else:
            self.name = name
        self.args = args

    def add_to_namespace(self, ns):
        if self.args != None and all(arg in ns for arg in self.args):
            defaultargs = tuple(ns[n] for n in self.args)
            def subst(*args, **kwargs):
                return self.obj(*(defaultargs + args), **kwargs)

            obj = subst

        else:
            obj = self.obj

        ns[self.name] = obj

def add_global(name: Optional[str]=None, args: Optional[List[str]]=None):
    def inner(obj):
        defaultglobals.append(GlobalVar(obj, name=name, args=args))
        return obj

    return inner

def add_default_globals_to_namespace(ns):
    for glob in defaultglobals:
        glob.add_to_namespace(ns)
