#if !defined(PLAT_VT100_H)
# define PLAT_VT100_H
# include "types.h"
# include <stdbool.h>

typedef enum
{
    VT100_MODE_LINE_FEED     = 0x0001,
    VT100_MODE_CURSOR_KEY    = 0x0002,
    VT100_MODE_COLUMN        = 0x0004,
    VT100_MODE_SCROLLING     = 0x0008,
    VT100_MODE_SCREEN        = 0x0010,
    VT100_MODE_ORIGIN        = 0x0020,
    VT100_MODE_AUTO_WRAP     = 0x0040,
    VT100_MODE_AUTO_REP      = 0x0080,
    VT100_MODE_INTERLACE     = 0x0100,
    VT100_MODE_SHOW_CUR      = 0x0200, // Technically not VT100 but who cares
    VT100_MODE_ALT_SCREEN    = 0x0400,
    VT100_MODE_PASTE_BRACKET = 0x0800,
    VT100_MODE_KEYPAD        = 0x1000,

    VT100_MODE_DEFAULT_SET = VT100_MODE_SHOW_CUR,

    VT100_MODE_DEFAULT_RESET = VT100_MODE_KEYPAD     |
                               VT100_MODE_CURSOR_KEY |
                               VT100_MODE_PASTE_BRACKET
} vt100_mode_t;

struct vt100
{
    vt100_mode_t modes;
    tty_s *tty;
};

/**
 * Initialize a VT100 terminal structure.
 *
 * @param vt  A pointer to the structure to initialize.
 * @param tty A pointer to a tty connected to the VT100.
 *
 * @return 0 on success, -1 on error.
 *
 */
int vt100_init(vt100_s *vt, tty_s *tty);

/**
 * Reset the font of a terminal to the default.
 *
 * @param vt A pointer to the VT100 structure.
 *
 * @param 0 on success, -1 on error.
 *
 */
int vt100_reset_font(vt100_s *vt);

/**
 * Set the font of a VT100 terminal.
 *
 * @param vt   A pointer to the VT100 structure.
 * @param font A pointer to the font structure to set on the terminal.
 *
 * @return 0 on success, -1 on error.
 *
 */
int vt100_set_font(vt100_s *vt, font_s *font);

/**
 * Enable or disable a set of modes on a VT100 terminal.
 *
 * @param vt    A pointer to a VT100 structure on which to change modes.
 * @param modes A set of VT100 modes OR'd together.
 *
 * @return 0 on success, -1 on error.
 *
 */
int vt100_set_modes(vt100_s *vt, vt100_mode_t modes, bool val);

/**
 * Set the location of a cursor on a VT100 terminal.
 *
 * @param vt A pointer to the VT100 structure.
 * @param ln The new line number.
 * @param cn The new column number.
 *
 * @param 0 on success, -1 on error.
 *
 */
int vt100_set_cursor(vt100_s *vt, int ln, int cn);

/**
 * Clear all characters on the current line of a VT100 terminal.
 *
 * @param vt A pointer to the VT100 structure.
 *
 * @param 0 on success, -1 on error.
 *
 */
int vt100_clear_line(vt100_s *vt);

/**
 * Clear all characters on the screen of a VT100 terminal.
 *
 * @param vt A pointer to the VT100 structure.
 *
 * @param 0 on success, -1 on error.
 *
 */
int vt100_clear_display(vt100_s *vt);

#endif
