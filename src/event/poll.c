#include "io/io.h"
#include "event/poll.h"
#include "util/macro.h"
#include "log/log.h"

static int poll_ptr_cmp(const void *aptr, const void *bptr);

int poll_ctx_init(poll_ctx_s *ctx, event_ctx_s *eventctx)
{
    pthread_mutexattr_t attr;
    sortedvec_init(&(ctx->finfoptrs), sizeof(io_file_info_s *), poll_ptr_cmp);

    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP);

    pthread_mutex_init(&(ctx->mtx), &attr);

    pthread_mutexattr_destroy(&attr);

    ctx->eventctx = eventctx;

    return 0;
}

void poll_ctx_kill(poll_ctx_s *ctx)
{
    sortedvec_kill(&(ctx->finfoptrs));
    pthread_mutex_destroy(&(ctx->mtx));
}

static int poll_ptr_cmp(const void *aptr, const void *bptr)
{
    io_file_info_s *a, *b;

    a = *(io_file_info_s **)aptr;
    b = *(io_file_info_s **)bptr;

    return a - b;
}

int poll_ctx_handle(poll_ctx_s *ctx)
{
    int res;

    res = 0;

    WITH_MTX(&(ctx->mtx))
    {
        vec_s *vec;
        vec = sortedvec_vec(&(ctx->finfoptrs));
        VEC_RFOREACH(vec, io_file_info_s **, finfo)
        {
            io_file_status_t status;
            status = (*finfo)->status;

            if (status)
            {
                event_s event;
                memset(&event, 0, sizeof(event_s));
                poll_event_spec(*finfo, &(event.spec));

                event_push(ctx->eventctx, &event);
                res += 1;
            }

            if (status & (IO_FILE_EOF | IO_FILE_CLOSED))
            {
                vec_del(vec, _ind, 1);
            }
        }
    }

    return res;
}

int poll_add(poll_ctx_s *ctx, io_file_info_s *finfo)
{
    int rtn;

    WITH_MTX(&(ctx->mtx))
    {
        rtn = sortedvec_ins_if_uniq(&(ctx->finfoptrs), &(finfo));
    }

    return rtn;
}

void poll_rem(poll_ctx_s *ctx, io_file_info_s *finfo)
{
    event_spec_s spec;

    WITH_MTX(&(ctx->mtx))
    {
        sortedvec_rem(&(ctx->finfoptrs), &(finfo));

        poll_event_spec(finfo, &spec);
        event_unlisten(ctx->eventctx, &spec, NULL);
    }
}

int poll_wait(poll_ctx_s *ctx, int toutms)
{
    char errbuf[IO_ERRBUF_LEN];
    int rtn;

    WITH_MTX(&(ctx->mtx))
    {
        vec_s *vec;
        vec = sortedvec_vec(&(ctx->finfoptrs));

        rtn = io_poll_files_by_ptr(
            vec_get(vec, 0),
            vec_len(vec),
            toutms,
            errbuf
        );
    }

    if (rtn == -1)
        LOG(ALERT, ERR, "Error polling in poll_ctx: %s", errbuf);

    return rtn;
}

void poll_cb(poll_ctx_s *ctx, io_file_info_s *finfo, event_cb_t cb, void *param)
{
    event_spec_s spec = {
        .typ = EVENT_POLL,
        .ptr = finfo
    };
    event_action_s act = {
        .cb = cb,
        .pri = 0,
        .param = param
    };

    poll_add(ctx, finfo);
    event_listen(ctx->eventctx, &spec, &act);
}
