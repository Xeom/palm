from typing import Sequence, List, Union

import ctypes
import libtypes
from lib import lib, enumspec, enums

from .structure import Structure, only_for_alive
from .rect      import Rect
from .chrvec    import Chr, ChrVec
from .display   import Display

from server.server import add_global

@add_global()
class Wm(Structure):
    """A Python wrapper for the window manager structure."""

    typ           = libtypes.wm_s
    init_function = lib.wm_init
    kill_function = lib.wm_kill

    def init_args(self, display):
        return [display.ptr]

    @property
    def display(self):
        return Display(ptr=self["disp"])

    def split(self, winid: int, split: enumspec):
        return lib.wm_split(self.ptr, winid, enums.win_split_t(split))

    def close(self, winid: int):
        return lib.wm_close(self.ptr, winid)

    def get_dims(self, winid: int) -> Rect:
        rtn = Rect()
        lib.wm_get_dims(self.ptr, winid, rtn.ptr)
        return rtn

    def refresh_display(self):
        lib.display_refresh_all(self["disp"])

    def adj(self, winid: int, val: float):
        lib.wm_adj(self.ptr, winid, val)

    def update_status(self, editor: "Editor", buf: "Buffer"):
        win = buf.get_win()
        if win == -1:
            return -1
        lib.wm_update_status(self.ptr, win, editor.ptr, buf.ptr)

    def add_floatwin(self) -> int:
        rtn = lib.wm_add_floatwin(self.ptr)
        if rtn == -1:
            raise Exception("Could not add floating window to window manager")

        return rtn

    def rem_floatwin(self, ind: int):
        rtn = lib.wm_rem_floatwin(self.ptr, ind)
        if rtn == -1:
            raise Exception("Could not remove floating window from window manager")

    def resize_floatwin(self, ind: int, dims: Rect):
        rtn = lib.wm_resize_floatwin(self.ptr, ind, dims.ptr)
        if rtn == -1:
            raise Exception("Could not resize floating window")

    def set_floatwin(self, ind: int, x: int, y: int, c: Chr):
        self.set_floatwin_rect(ind, Rect.at(x=x, y=y), [c])

    def set_floatwin_rect(self, ind: int, rect: Rect, chrs: Sequence[Chr]):
        buflen = rect.w * rect.h
        buf = (libtypes.chr_s * buflen)()

        for i, c in enumerate(sequence):
            c.cpy_to(ctypes.pointer(buf[i]))

        if i < buflen:
            raise ValueError("Not enough characters to fill out buffer")

        self.set_floatwin_rect_ptr(ind, rect, ctypes.byref(buf))

    def set_floatwin_rect_ptr(self, ind: int, rect: Rect, ptr):
        rtn = lib.wm_set_floatwin(self.ptr, ind, rect.ptr, ptr)

        if rtn == -1:
            raise Exception("Could not set contents of floatwin")

    def fill_floatwin(self, ind: int, rect: Rect, c: Chr):
        rtn = lib.wm_fill_floatwin(self.ptr, ind, rect.ptr, c.ptr)
        if rtn == -1:
            raise Exception("Could not fill floatwin")


@add_global()
class FloatingWindow(object):
    """A class for managing floating windows in a window manager."""

    def __init__(self, wm):
        self.wm = wm
        self.ind = wm.add_floatwin()

    def rem(self):
        if self.ind is not None and not self.wm.killed:
            self.wm.rem_floatwin(self.ind)
            self.ind = None

    def resize(self, rect: Rect):
        self.wm.resize_floatwin(self.ind, rect)

    def set_rect(self, rect: Rect, chrs: Sequence[Chr]):
        self.wm.set_floatwin_rect(self.ind, rect, chrs)

    def set_rect_ptr(self, rect: Rect, ptr):
        self.wm.set_floatwin_rect_ptr(self.ind, rect, ptr)

    def set(self, x: int, y: int, c: Chr):
        self.wm.set_floatwin(self.ind, x, y, c)

    def fill(self, rect: Rect, c: Chr):
        self.wm.fill_floatwin(self.ind, rect, c)

    def display_lines(self, lines: List[Union[str, bytes]]):
        for y, line in enumerate(lines):
            vec = ChrVec.from_fmt(line)
            rect = Rect.at(x=0, y=y, w=len(vec), h=1)
            self.wm.set_floatwin_rect_ptr(self.ind, rect, vec[0])

@add_global()
class AnnotationFloatingWindow(FloatingWindow):
    def resize(self, rect: Rect):
        newrect = rect.copy()

        dims = self.wm.display.dims
        if rect.y > dims.h / 2:
            newrect.y = rect.y - rect.h - 1
        else:
            newrect.y = rect.y + 1

        newrect.x = max(0, min(rect.x, dims.w - rect.w))

        super().resize(newrect)

from .editor    import Editor
