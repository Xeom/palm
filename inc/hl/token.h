#if !defined(HL_TOKEN_H)
# define HL_TOKEN_H
# if !defined(NO_HL)
#  include "types.h"
#  include "container/vec.h"
#  include <stdbool.h>
#  define PCRE2_CODE_UNIT_WIDTH 8
#  include <pcre2.h>
#  include "hl/action.h"
#  include "match/match.h"

typedef enum
{
    HL_TOK_NOTYPE,
    HL_TOK_REGEX
} hl_tok_type_t;

struct hl_tok
{
    hl_tok_type_t type;
    int group;
    match_s match;
    vec_s name;
    hl_action_s defaults;
};

void hl_tok_init(hl_tok_s *tok, char *name);

void hl_tok_kill(hl_tok_s *tok);

int hl_tok_set_regex(hl_tok_s *tok, const char *regex, bool ext);

int hl_tok_verify(hl_tok_s *tok);

bool hl_tok_match(hl_tok_s *tok, vec_s *chrs, size_t *off, size_t *start, size_t *end);

int hl_tok_load_attrs(hl_tok_s *tok, yaml_obj_s *obj, hl_ctx_s *ctx);

# endif
#endif
 
