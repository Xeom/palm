#if !defined(UTIL_POOL_H)
# define UTIL_POOL_H
# include <pthread.h>
# include <stdbool.h>
# include "container/vec.h"

struct pool
{
    vec_s threads;
    vec_s pending;

    pthread_t       master;
    pthread_cond_t  wait;
    pthread_mutex_t mtx;

    bool alive;
};

struct pool_job
{
    pthread_cond_t  wait;
    pthread_mutex_t mtx;

    void (*funct)(void *);
    void  *param;

    bool    done;
};

struct pool_thread
{
    pthread_t       thread;
    pthread_cond_t  wait;
    pthread_mutex_t mtx;

    pool_s     *pool;
    pool_job_s *job;
    bool done;
    bool alive;
};

int pool_init(pool_s *pool, int nthreads);

void pool_kill(pool_s *pool);

void pool_add_job(pool_s *pool, pool_job_s *job);

void pool_job_init(pool_job_s *job, void (*funct)(void *), void *param);

void pool_job_kill(pool_job_s *job);

void pool_job_await(pool_job_s *job);

bool pool_job_await_tout(pool_job_s *job, long ns);

#endif
