Using the Python Prompt
=======================

Activating the prompt
---------------------

The 'x' command will open an interactive python prompt in a new 
window. You can press `Esc`, `x` in order to run this command from
a normal mode. Within the prompt, you can press `Esc`, `x` again, in
order to close it.

This opening of the pyprompt corresponds to calling the function
`open_buffer_pyprompt()`. The function `single_pyprompt()` is also
sometimes called by shortcut commands. This opens a single-use prompt,
which closes after one command is executed, and has text already
typed both before and after the cursor. As an example, you could bind
a key to `single_pyprompt("goto(", ")")`, in order to make goto 
commands quicker.

Using the Prompt
----------------

The prompt behaves mostly like a normal python prompt, with the
exception that the text that has already been typed is entirely
editable, and that, in order to run a command, you must press `Enter`
twice. This allows for multiple commands to be run in one go. You
can also use the `Ctrl-V` and `Ctrl-N` keys to view command history.

Session Files
-------------

For each session of Python console, certain default variables are defined.
These include - `buf`, the buffer that the session is associated with,
as well as `editor`, `wm`, and `finfo`, which are all associated with the
buffer. `session` is also added. This variable refers to the ShellSession
instance that the console is running.

After these variables are added to the global scope, some code can be
executed in this scope, to define useful helper functions. This code is
stored in 'session files', which are stored in the `python/session` 
directory, or in the home directory. The files should be short, as they
will be executed whenever a python prompt is started, and should simply
define helper functions.

Helper functions
----------------

Here are some examples of helper functions defined in Python prompt sessions.

### Editing Helpers - `session/edit.py`

 * `edit_file()` - Open a file for editing.

 * `getl()`, `setl()` - Set or get the current line's contents as a string.

 * `insl()` - Insert some lines.

 * `ins()` - Insert some characters as if they were pasted or typed.

 * `cut()`, `cpy()` - Cut or copy the selection to a string.

 * `backspace()`, `delete()`, `enter()` - Run the actions corresponding to these
                                          keys.

### Iterating Helpers - `session/iter.py`

 * `iter_sel()`, `iter_buf()` - Iterate across the lines in the buffer or
                                selection, selecting each one with the cursor
                                before yielding the line number.

 * `sort_sel()`, `sort_buf()` - Sort the lines in the buffer or selection by some
                                key.

 * `apply_sel()`, `apply_buf()` - Apply a function to the contents of all the
                                  lines in the buffer or selection.

### Navigation Helpers - `session/nav.py`

 * `cur()`, `pri()`, `sec()` - Get the cursor and it's components,

 * `row()`, `col()` - Get or set the cursor row and column.

 * `goto()` - Move the cursor to a location.

 * `move()` - Move the cursor a number of places in a direction.

 * `home()`, `end()` - Go to the start or end of the line.

 * `swap()` - Swap the primary and secondary cursors.

 * `snap()` - Snap the secondary cursor to the primary.

 * `stick()`, `cur_type()` - Change the cursor type and stickiness.


### Session Helpers - `session/session.py`

 * `print()` `print_expr()` - Print a value to the prompt.

 * `sys()` - Run a command and display the result in a popup.

 * `print_version()` - Print the Python version.

Examples
--------

### Capitalize Selection

```Python
ins(cut().upper())
```

### Indent selection

```Python
apply_sel(lambda x:"    "+x)
```

or, a faster approach, using the functions for doing this
specific job, implemented in C. As a bonus, this method
will respect your indentation settings - using tabs or
spaces as appropriate.

```Python
for ln in iter_sel():
    buf.indent_set(ln, buf.indent_get(ln) + 4)
```

or, the fastest is to simply use the C function bound to the tab key!
This will do everything in C and save a lot of time!
To invoke the C action associated with the tab key manually from the
python console, you can do the following, though it is clearly a hacky
method.

```Python
lib.default_add_indent(
    Vec(ctypes.c_char).ptr, 
    buf.ptr, 
    ctypes.byref(ctypes.c_int(4)), 
    ctypes.sizeof(ctypes.c_int)
)
```

### Strip trailing whitespace

```Python
apply_buf(str.rstrip)
```

### Sort by length

```Python
sort_sel(len)
```

### Select current line.

```Python
home()
stick(True)
end()
```

or, using `goto()` to move the cursor,

```Python
stick(True)
goto([row(), 0], [row(), buf.line_len(row())])
a```
