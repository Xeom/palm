#include <stdbool.h>
#include "log/log.h"
#include "match/set.h"

int match_set_init(match_set_s *set)
{
    vec_init(&(set->matches), sizeof(match_s));

    return 0;
}

void match_set_kill(match_set_s *set)
{
    VEC_FOREACH(&(set->matches), match_s *, match)
    {
        match_kill(match);
    }

    vec_kill(&(set->matches));
}

match_s *match_set_add(match_set_s *set, match_attr_t attrs, const char *str)
{
    match_s *res;

    res = vec_app(&(set->matches), NULL);

    if (!res)
        return NULL;

    if (match_init(res, attrs, str) == -1)
    {
        vec_del(&(set->matches), vec_len(&(set->matches)) - 1, 1);
        return NULL;
    }

    return res;
}

int match_set_check(match_set_s *set, vec_s *text, size_t off, match_result_s *res)
{
    VEC_FOREACH(&(set->matches), match_s *, match)
    {
        switch (match_check(match, text, off, res))
        {
        case  0: break;
        case  1: return 1;
        case -1: return -1;
        }
    }

    return 0;
}

int match_set_search(match_set_s *set, vec_s *text, size_t off, match_result_s *res, bool backward)
{
    match_result_s tmpres;
    long startoff, endoff, iter;
    bool first, found;

    if (backward)
    {
        startoff = 0;
        endoff = off - 1;
        first = false;
    }
    else
    {
        startoff = off;
        endoff   = vec_len(text) - 1;
        first    = true;
    }

    found = false;
    for (iter = startoff; iter <= endoff; ++iter)
    {
        switch (match_set_check(set, text, iter, &tmpres))
        {
        case  0: break;
        case  1:
            if (!(tmpres.match->attrs & MATCH_BEYONDOFF) && backward)
            {
                if (tmpres.end > off)
                    break;

                if (tmpres.start >= off)
                    break;
            }

            if (found && tmpres.end <= res->end)
                break;

            memcpy(res, &tmpres, sizeof(match_result_s));

            found = true;

            break;
        case -1: return -1;
        }

        if (first && found)
            break;
    }


    if (found)
        return 1;
    else
        return 0;
}
