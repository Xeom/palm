#include "help/help.h"

#include "cmdlang/lexer.h"
#include "cmdlang/cmd.h"

int cmd_init(cmd_s *cmd, const char *name, const char *help)
{
    cmd->typ = CMD_NONE;

    TRY(help_init(&(cmd->help)));
    TRY(help_set_type(&(cmd->help), HELP_CMD));
    TRY(help_set_info(&(cmd->help), name, help));

    vec_init(&(cmd->argspecs), sizeof(cmd_arg_spec_s));

    return PALM_OK;
}

void cmd_kill(cmd_s *cmd)
{
    help_kill(&(cmd->help));
    vec_kill(&(cmd->argspecs));

    switch (cmd->typ)
    {
    case CMD_ALIAS:
        {
            VEC_FOREACH(&(cmd->info.alias.toks), cmdlang_tok_s *, tok)
            {
                cmdlang_tok_kill(tok);
            }

            vec_kill(&(cmd->info.alias.toks));

            break;
        }
    }
}

int cmd_add_argument(cmd_s *cmd, const char *name, const char *help, cmd_arg_spec_s *spec)
{
    cmd_arg_spec_s *last;
    help_s *arghelp;

    if (cmd->typ == CMD_NONE)
        return PALM_ERR;

    arghelp = help_add_sub(&(cmd->help));
    if (!arghelp) return PALM_ERR;

    TRY(help_set_type(arghelp, HELP_CMDARG));
    TRY(help_set_info(arghelp, name, help));

    // TODO: Add info about types

    last = vec_end(&(cmd->argspecs));

    if (!(spec->optional) && last && last->optional)
    {
        /* Optional arguments must come after non-optional ones. */
        return PALM_ERR;
    }

    if (vec_app(&(cmd->argspecs), spec) == NULL)
    {
        return PALM_ERR;
    }

    return PALM_OK;
}

int cmd_set_verb(cmd_s *cmd, cmd_verb_info_s *info)
{
    if (cmd->typ != CMD_NONE) return PALM_ERR;

    cmd->typ = CMD_VERB;
    memcpy(&(cmd->info.verb), info, sizeof(cmd_verb_info_s));

    return PALM_OK;
}

int cmd_set_adverb(cmd_s *cmd, cmd_adverb_info_s *info)
{
    if (cmd->typ != CMD_NONE) return PALM_ERR;

    cmd->typ = CMD_ADVERB;
    memcpy(&(cmd->info.adverb), info, sizeof(cmd_adverb_info_s));

    return PALM_OK;
}

int cmd_set_noun(cmd_s *cmd, cmd_noun_info_s *info)
{
    if (cmd->typ != CMD_NONE) return PALM_ERR;

    cmd->typ = CMD_NOUN;
    memcpy(&(cmd->info.noun), info, sizeof(cmd_noun_info_s));

    return PALM_OK;
}

int cmd_set_alias(cmd_s *cmd, cmd_alias_info_s *info)
{
    bool verb;
    if (cmd->typ != CMD_NONE) return PALM_ERR;

    verb = false;
    VEC_FOREACH(&(info->toks), cmdlang_tok_s *, tok)
    {
        if (verb)
            return PALM_ERR;

        TRY(cmdlang_tok_is_verb(tok, info->lib, &verb));
    }

    cmd->typ = CMD_ALIAS;

    vec_init(&(cmd->info.alias.toks), sizeof(cmdlang_tok_s));
    vec_cpy(&(cmd->info.alias.toks), &(info->toks));
    vec_clr(&(info->toks)); // Clear to ensure we don't reuse

    return PALM_OK;
}

int cmd_alias_info_init(
    cmd_alias_info_s *info, cmd_lib_s *lib
)
{
    vec_init(&(info->toks), sizeof(cmdlang_tok_s));
    info->lib = lib;

    return PALM_OK;
}

int cmd_alias_info_from_str(
    cmd_alias_info_s *info, const char *str, errmsg_t errmsg
)
{
    cmdlang_lexer_s lexer;

    TRY(cmdlang_lexer_init(&lexer, info->lib, errmsg));

    if (cmdlang_lexer_str_to_toks(&lexer, str, &(info->toks), errmsg) != PALM_OK)
    {
        cmdlang_lexer_kill(&lexer);
        return PALM_ERR;
    }

    cmdlang_lexer_kill(&lexer);
    return PALM_OK;
}

void cmd_alias_info_kill(cmd_alias_info_s *info)
{
    vec_kill(&(info->toks));
}

int cmd_is_verb(cmd_s *cmd, cmd_lib_s *lib, bool *verb)
{
    cmdlang_tok_s *tok;
    switch (cmd->typ)
    {
    case CMD_VERB:
        *verb = true;
        return PALM_OK;

    case CMD_ALIAS:
        tok = vec_end(&(cmd->info.alias.toks));
        if (!tok) return PALM_ERR;

        return cmdlang_tok_is_verb(tok, lib, verb);

    default:
        *verb = false;
        return PALM_OK;
    }
}
