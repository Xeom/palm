#include <stdlib.h>
#include <string.h>
#include "container/list.h"

void list_init(list_s *list, size_t width)
{
    list->width = width;
    list->len   = 0;
    list->range.first = NULL;
    list->range.last  = NULL;
}

void list_kill(list_s *list)
{
    list_del(list, list_range(list));
    list->width = 0;
}

size_t list_len(list_s *list)
{
    return list->len;
}

list_item_s *list_first(list_s *list)
{
    return list->range.first;
}

list_item_s *list_last(list_s *list)
{
    return list->range.last;
}

list_range_s list_range(list_s *list)
{
    return list->range;
}

long list_range_len(list_range_s range)
{
    list_item_s *iter;
    long rtn;
    rtn = 1;

    if (!(range.first) || !(range.last))
        return -1;

    for (iter = range.first; iter != range.last; iter = iter->next)
    {
        rtn += 1;

        if (!iter)
            return -1;
    }

    return rtn;
}

list_item_s *list_range_mid(list_range_s range)
{
    list_item_s *fast, *slow;

    fast = range.first;
    slow = range.first;

    while (fast != range.last)
    {
        fast = fast->next;
        if (fast == range.last) break;

        slow = slow->next;
        fast = fast->next;
    }

    return slow;
}

list_item_s *list_item_iter(list_item_s *item, long n)
{
    while (item && n < 0)
    {
        ++n;
        item = item->prev;
    }

    while (item && n > 0)
    {
        --n;
        item = item->next;
    }

    return item;
}

size_t list_item_ind(list_item_s *item)
{
    size_t rtn;
    rtn = 0;

    while (item->prev)
    {
        ++rtn;
        item = item->prev;
    }

    return rtn;
}

static list_item_s *list_item_create(list_s *list, const char *data)
{
    list_item_s *new;

    new = malloc(sizeof(list_item_s) + list->width);
    if (!new) return NULL;

    if (data)
        memcpy(new->data, data, list->width);
    else
        memset(new->data, 0, list->width);

    return new;
}

static int list_cut(list_s *list, list_range_s range)
{
    list_item_s *before, *after;
    long len;

    len = list_range_len(range);
    if (len == -1) return -1;

    before = range.first->prev;
    after  = range.last->next;

    range.first->prev = NULL;
    range.last->next  = NULL;

    if (before)
        before->next = after;
    else
        list->range.first = after;

    if (after)
        after->prev = before;
    else
        list->range.last = before;

    list->len -= len;

    return 0;
}

static int list_ins_after(list_s *list, list_item_s *item, list_range_s range)
{
    list_item_s *next;
    long len;

    len = list_range_len(range);
    if (len == -1) return -1;

    if (item)
    {
        next = item->next;
        item->next = range.first;
    }
    else
    {
        next = list->range.first;
        list->range.first = range.first;
    }

    if (next)
        next->prev = range.last;
    else
        list->range.last = range.last;

    range.first->prev = item;
    range.last->next  = next;

    list->len += len;

    return 0;
}

list_item_s *list_push_before(list_s *list, list_item_s *item, const void *data)
{
    if (item == NULL)
        item = list->range.last;
    else
        item = item->prev;

    return list_push_after(list, item, data);
}

list_item_s *list_push_after(list_s *list, list_item_s *item, const void *data)
{
    list_item_s *new;

    new = list_item_create(list, data);
    if (!new) return NULL;

    if (list_ins_after(list, item, LIST_RANGE(new, new)) == -1)
        return NULL;

    return new;
}

int list_del(list_s *list, list_range_s range)
{
    list_item_s *iter, *next, *end;

    if (list_cut(list, range) == -1)
        return -1;

    end = range.last->next;
    for (iter = range.first; iter != end; iter = next)
    {
        next = iter->next;
        free(iter);
    }

    return 0;
}

int list_graft_before(list_s *list, list_item_s *item, list_s *src, list_range_s range)
{
    if (item == NULL)
        item = list->range.last;
    else
        item = item->prev;

    return list_graft_after(list, item, src, range);
}

int list_graft_after(list_s *list, list_item_s *item, list_s *src, list_range_s range)
{
    if (list->width != src->width)
        return -1;

    if (list_cut(src, range) == -1)
        return -1;

    return list_ins_after(list, item, range);
}

int list_mitosis(list_s *list, list_s *new, list_range_s range)
{
    list_init(new, list->width);

    return list_graft_after(new, NULL, list, range);
}

void list_msort(list_s *list, int (*f)(const void *, const void *))
{
    list_s left, right;
    list_range_s lrange, rrange;
    list_item_s *mid;

    if (list_len(list) < 2)
        return;

    mid = list_range_mid(list_range(list));

    lrange = LIST_RANGE(list_first(list), mid);
    rrange = LIST_RANGE(mid->next, list_last(list));

    list_mitosis(list, &left,  lrange);
    list_mitosis(list, &right, rrange);

    list_msort(&left,  f);
    list_msort(&right, f);

    while (list_len(&left) > 0 && list_len(&right) > 0)
    {
        int cmp;
        list_item_s *litem, *ritem;

        litem = list_first(&left);
        ritem = list_first(&right);

        cmp = f(list_item_data(litem), list_item_data(ritem));

        if (cmp <= 0)
            list_graft_before(list, NULL, &left,  LIST_RANGE(litem, litem));
        else
            list_graft_before(list, NULL, &right, LIST_RANGE(ritem, ritem));
    }

    if (list_len(&left) > 0)
        list_graft_before(list, NULL, &left, list_range(&left));

    if (list_len(&right) > 0)
        list_graft_before(list, NULL, &right, list_range(&right));

    list_kill(&left);
    list_kill(&right);
}

static void list_range_qsort(
    list_s *list,
    list_range_s range,
    int (*f)(const void *, const void *)
)
{
    list_s unsorted;
    list_item_s *mid, *prev;

    if (range.first == range.last)
        return;

    prev = range.first->prev;
    list_mitosis(list, &unsorted, range);

    mid = list_range_mid(list_range(&unsorted));
    list_graft_after(list, prev, &unsorted, LIST_RANGE(mid, mid));

    range.first = mid;
    range.last  = mid;

    while (list_len(&unsorted) > 0)
    {
        list_item_s *cmp;
        cmp = list_first(&unsorted);

        if (f(list_item_data(cmp), list_item_data(mid)) >= 0)
        {
            if (range.last == mid) range.last = cmp;
            list_graft_after(list, mid, &unsorted, LIST_RANGE(cmp, cmp));
        }
        else
        {
            if (range.first == mid) range.first = cmp;
            list_graft_before(list, mid, &unsorted, LIST_RANGE(cmp, cmp));
        }
    }

    list_kill(&unsorted);

    if (range.first != mid)
        list_range_qsort(list, LIST_RANGE(range.first, mid->prev), f);

    if (range.last != mid)
        list_range_qsort(list, LIST_RANGE(mid->next, range.last), f);

}

void list_qsort(list_s *list, int (*f)(const void *, const void *))
{
    if (list_len(list) > 0)
        list_range_qsort(list, list_range(list), f);
}
