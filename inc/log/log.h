#if !defined(LOG_LOG_H)
# define LOG_LOG_H
# include <stdbool.h>
# include <pthread.h>
# include <stdarg.h>
# include "util/macro.h"
# include "container/vec.h"
# include "types.h"

/**
 * @file log.h
 * @brief Implementation of the logging system.
 *
 * The logging system is used to record events and present them to the user,
 * or developer, as notifications etc.
 *
 */

/**
 * An enum containing the types of logging message.
 *
 */
typedef enum
{
    LOG_TYP_SUCC, /**< A success (good) message. */
    LOG_TYP_ERR,  /**< An error (bad) message. */
    LOG_TYP_INFO, /**< An information (neutral) message. */
    LOG_TYP_CNT
} log_type_t;

/**
 * An enum containing the levels of importance of logged messages.
 *
 */
typedef enum
{
    LOG_LVL_DEBUG,  /**< Debugging messages - hidden by default. */
    LOG_LVL_INFO,   /**< Information - logged quietly by default. */
    LOG_LVL_NOTICE, /**< Notification - an icon is shown by default. */
    LOG_LVL_ALERT,  /**< Alert - the user is shown the message by default. */
    LOG_LVL_CNT
} log_lvl_t;

/**
 * Configuration for a type of logging message.
 *
 */
struct log_typ_conf
{
    char *name;     /**< The name of the type, as shown in the log. */
    char *prifont;  /**< The name of the primary font for the message. */
    char *secfont;  /**< The name of the secondary font for the message. */
    char *iconfont; /**< The font for the notification icon. */

    bool logdefault; /**< By default, log these messages. */
    char icon;       /**< The character to show in the icon. */
    bool print;      /**< By default, show the user this message. */
};

/**
 * Structure containing a file to log messages to.
 *
 */
struct log_file
{
    int  fd;    /**< The file descriptor of the file. */
    bool fonts; /**< If true, encode and send fonts to the file. */
    char tsfmt[64]; /**< Log timestamps in this format. */

    /**
     * An array containing bools for every possible log message type.
    v* Messages for every enabled type are sent to the file.
     *
     */
    bool enabled[LOG_LVL_CNT][LOG_TYP_CNT];
};

/**
 * A logging system.
 *
 */
struct log
{
    /**
     * A vector of recent notificaions to notify the user of.
     *
     */
    vec_s notifications;
    vec_s files;         /**< A vector of @c log_file_s to log messages to. */
    pthread_mutex_t mtx; /**< A mutex for logging messages. */

    /**
     * A configuration for each level of logging.
     *
     */
    log_typ_conf_s conf[LOG_LVL_CNT][LOG_TYP_CNT];

    /**
     * An incrementing notification ID.
     *
     */
    long long notifyid;
};

/**
 * The default logging system.
 *
 */
extern log_s default_log;

/**
 * Initialize a logging system.
 *
 * @param log A pointer to the logging system.
 *
 */
void log_init(log_s *log);

/**
 * De-allocate resources allocated to a logging system.
 *
 * @param log A pointer to the logging system.
 *
 */
void log_kill(log_s *log);

/**
 * Log to a file.
 *
 * After this call, messages will be logged to the specified file descriptor.
 *
 * @param log   A pointer to the logging system.
 * @param fd    The file descriptor to send logging information to.
 * @param fonts Whether to send font formatting information.
 * @param ts    A strftime-compatible string for timestamps, or NULL if
 *              timestamps should be omitted.
 *
 */
void log_to_file(log_s *log, int fd, bool fonts, const char *ts);

/**
 * Stop logging to a file.
 *
 * This undoes a call to log_to_file().
 *
 * @param log A pointer to the logging system.
 * @param fd  The file descriptor to stop logging to.
 *
 */
int log_forget_file(log_s *log, int fd);

/**
 * Log a message.
 *
 * @param log   A pointer to the logging system.
 * @param lvl   The priority of this message.
 * @param typ   The type of this message.
 * @param fname The name of the file that produced this message.
 * @param ln    The line number that produced this message.
 * @param str   A null-terminated string containing the message to log.
 *
 */
void log_str(
    log_s *log,
    log_lvl_t lvl,
    log_type_t typ,
    const char *fname,
    int ln,
    const char *str
);

/**
 * Log a formatted message.
 *
 * This function works the same as log_str(), except the string
 * can contain printf-style formatting sequences.
 *
 * @param log   A pointer to the logging system.
 * @param lvl   The priority of this message.
 * @param typ   The type of this message.
 * @param fname The name of the file that produced this message.
 * @param ln    The line number that produced this message.
 * @param str   A null-terminated printf-style formatted message.
 * @param ...   Printf-style formatting arguments.
 *
 */
__attribute__(( format (printf, 6, 7) ))
void log_fmt(
    log_s *log,
    log_lvl_t lvl,
    log_type_t typ,
    const char *fname,
    int ln,
    const char *fmt,
    ...
);

/**
 * A macro used to make logging a lot more compact.
 *
 * @param lvl The importance of the logging message - one of @c DEBUG, 
 *            @c NOTICE, @c INFO, @c ALERT.
 * @param typ The type of logging message, one of @c SUCC, @c ERR, @c INFO.
 * @param ... A printf-style formatted logging message and its parameters.
 *
 */
#define LOG(lvl, typ, ...) \
    log_fmt(               \
        &default_log,      \
        LOG_LVL_ ## lvl,   \
        LOG_TYP_ ## typ,   \
        __FILE__,          \
        __LINE__,          \
        __VA_ARGS__        \
    )

#endif
