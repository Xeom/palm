#include "snow.h"
#include "hl/token.h"
#include "hl/ctx.h"
#include "text/font.h"
#include "text/decoder.h"
#include "text/encoder.h"

static void load_yaml(hl_ctx_s *ctx, char *yaml)
{
    yaml_obj_s obj;
    FILE *tmp;
    tmp = tmpfile();
    fputs(yaml, tmp);
    fseek(tmp, 0, SEEK_SET);
    yaml_obj_init(&obj, NULL, 0);
    asserteq(yaml_obj_parse(&obj, tmp), 0);
    fclose(tmp);

    asserteq(hl_ctx_load_yaml(ctx, &obj), 0);
    yaml_obj_kill(&obj);
}

static void decode_str(vec_s *chrs, char *str)
{
    decoder_s decoder;
    decoder_init(&decoder, DECODER_UTF8);
    decoder_str(&decoder,  str, chrs);
    decoder_kill(&decoder);
}

static void highlight_chrs(vec_s *chrs, hl_ctx_s *ctx, char *statename)
{
    long state;
    vec_s states;
    vec_init(&states, sizeof(long));
    state = hl_ctx_lookup_state(ctx, statename);
    vec_app(&states, &state);

    hl_ctx_apply(ctx, chrs, &states);

    vec_kill(&states);
}


describe(hl_ctx)
{
    hl_ctx_s ctx;

    before_each()
    {
        hl_ctx_init(&ctx);
    }

    after_each()
    {
        hl_ctx_kill(&ctx);
    }

    subdesc(Loading)
    {
        it ("Parses tokens")
        {
            hl_tok_s *tok;

            load_yaml(&ctx,
                "tokens:\n"
                "  digits:\n"
                "    regex: \"[0-9]+\"\n"
                "  letters:\n"
                "    regex: \"[a-zA-Z]+\"\n");

            asserteq(vec_len(&(ctx.tokens)), 2);


            tok = vec_get(&(ctx.tokens), 0);
            asserteq((char *)vec_get(&tok->name, 0), "digits");
            asserteq(vec_strcmp(&tok->match.text, "[0-9]+"), 0);

            tok = vec_get(&(ctx.tokens), 1);
            asserteq((char *)vec_get(&tok->name, 0), "letters");
            asserteq(vec_strcmp(&tok->match.text, "[a-zA-Z]+"), 0);
        }

        it ("Parses states")
        {
            hl_state_s *state;
            hl_action_s *action;

            load_yaml(&ctx,
                "tokens:\n"
                "  letters:\n"
                "    regex: \"[a-zA-Z]+\"\n"
                "  digits:\n"
                "    regex: \"[0-9]+\"\n"
                "states:\n"
                "  hl-letters:\n"
                "    font: boring\n"
                "    actions:\n"
                "      - tok: letters\n"
                "        font: beautiful\n"
                "      - tok: digits\n"
                "        type: pop\n"
            );

            asserteq(vec_len(&(ctx.states)), 1);

            state = hl_ctx_get_state(&ctx, hl_ctx_lookup_state(&ctx, "hl-letters"));
            assert(state);

            asserteq(state->font, font_by_name("boring"));

            size_t off, start, end;
            off = 0;
            vec_s chrs;
            vec_init(&chrs, sizeof(char));
            vec_str(&chrs, "abc123");

            action = hl_state_match(state, &ctx, &chrs, &off, &start, &end);
            assert(action);
            asserteq(off, 3);
            asserteq(start, 0);
            asserteq(end,   3);
            asserteq(action->token, hl_ctx_lookup_token(&ctx, "letters"));
            asserteq(action->type,  HL_ACTION_NONE);
            asserteq(action->font,  font_by_name("beautiful"));

            action = hl_state_match(state, &ctx, &chrs, &off, &start, &end);
            asserteq(off, 6);
            asserteq(start, 3);
            asserteq(end,   6);
            asserteq(action->token, hl_ctx_lookup_token(&ctx, "digits"));
            asserteq(action->type,  HL_ACTION_POP);
            asserteq(action->font,  -1);

            vec_kill(&chrs);
        }
    }

    subdesc(Highlighting)
    {
        it ("Highlights contained tokens")
        {
            short big, small;
            big   = font_by_name("big");
            small = font_by_name("small");
            chr_s expect[] = {
                { "1", small }, { "2", small }, { "3", small },
                { "H", big },   { "e", big },   { "l", big },
                { "l", big },   { "o", big },   { "4", small },
                { "5", small }, { "6", small }
            };
            load_yaml(&ctx,
                "tokens:\n"
                "  letter:\n"
                "    font: big\n"
                "    regex: \"[a-zA-Z]+\"\n"
                "states:\n"
                "  hl-letters:\n"
                "    font: small\n"
                "    contains-tokens:\n"
                "      - letter\n"
            );

            vec_s chrs;
            vec_init(&chrs, sizeof(chr_s));
            decode_str(&chrs, "123Hello456");
            highlight_chrs(&chrs, &ctx, "hl-letters");

            asserteq(vec_len(&chrs), sizeof(expect)/sizeof(expect[0]));
            asserteq_buf(vec_get(&chrs, 0), expect, sizeof(expect));

            vec_kill(&chrs);
        }

        it ("Uses simple states")
        {
            short f;
            f = font_by_name("shiny");
            chr_s expect[] = {
                { "*", 0 }, { "(", 0 }, { "*", f }, { ")", 0 }, 
                { "*", 0 }, { "(", 0 }, { "(", 0 }, { "*", f },
                { ")", 0 }, { "*", f }, { ")", 0 }, { "*", 0 }
            };
            load_yaml(&ctx,
                "tokens:\n"
                "  open-bra:\n"
                "    regex: '\\('\n"
                "  close-bra:\n"
                "    regex: '\\)'\n"
                "  star:\n"
                "    font: shiny\n"
                "    regex: '\\*'\n"
                "states:\n"
                "  main:\n"
                "    contains-states:\n"
                "      - sub\n"
                "  sub:\n"
                "    start:\n"
                "      - open-bra\n"
                "    end:\n"
                "      - close-bra\n"
                "    contains-tokens:\n"
                "      - star\n"
                "    contains-states:\n"
                "      - sub\n"
            );

            vec_s chrs;
            vec_init(&chrs, sizeof(chr_s));
            // We should see every star within a pair of brackets highlighted
            decode_str(&chrs, "*(*)*((*)*)*");
            highlight_chrs(&chrs, &ctx, "main");

            asserteq(vec_len(&chrs), sizeof(expect)/sizeof(expect[0]));
            asserteq_buf(vec_get(&chrs, 0), expect, sizeof(expect));

            vec_kill(&chrs);
        }
    }
}

describe(hl_tok)
{
    hl_tok_s tok;

    it ("Does not verify without being set up")
    {
        hl_tok_init(&tok, "TOK-1");
        asserteq(hl_tok_verify(&tok), -1);
        hl_tok_kill(&tok);
    }

    it ("Initializes and verifies with valid regex")
    {
        hl_tok_init(&tok, "TOK-1");
        asserteq(hl_tok_set_regex(&tok, "abc", false), 0);
        asserteq(hl_tok_verify(&tok), 0);
        hl_tok_kill(&tok);
    }

    it ("Fails to initialize with invalid regex")
    {
        hl_tok_init(&tok, "TOK-1");
        asserteq(hl_tok_set_regex(&tok, "abc)", false), -1);
        asserteq(hl_tok_verify(&tok), -1);
        hl_tok_kill(&tok);
    }

    it ("Matches strings at zero offset")
    {
        hl_tok_init(&tok, "TOK-1");
        asserteq(hl_tok_set_regex(&tok, "abcd", false), 0);
        asserteq(hl_tok_verify(&tok), 0);

        vec_s chrs;
        size_t off, start, end;
        vec_init(&chrs, sizeof(char));
        vec_str(&chrs, "abcdefg");

        off = 0;
        asserteq(hl_tok_match(&tok, &chrs, &off, &start, &end), true);
        asserteq(start, 0);
        asserteq(end,   4);
        asserteq(off,   4);

        off = 0;
        *(char *)vec_get(&chrs, 2) = 'x';
        asserteq(hl_tok_match(&tok, &chrs, &off, &start, &end), false);
        asserteq(off, 0);

        vec_kill(&chrs);
        hl_tok_kill(&tok);
    }

    it ("Adds one to zero length strings")
    {
        hl_tok_init(&tok, "TOK-1");
        asserteq(hl_tok_set_regex(&tok, "a*(?=b)", false), 0);
        asserteq(hl_tok_verify(&tok), 0);

        vec_s chrs;
        size_t off, start, end;
        vec_init(&chrs, sizeof(char));
        vec_str(&chrs, "xbcdefg");

        off = 1;
        asserteq(hl_tok_match(&tok, &chrs, &off, &start, &end), true);
        asserteq(start, 1);
        asserteq(end,   1);
        asserteq(off,   2);

        vec_kill(&chrs);
        hl_tok_kill(&tok);
    }

    it ("Matches strings at offsets")
    {
        vec_s chrs;
        size_t off, start, end;

        hl_tok_init(&tok, "TOK-1");
        asserteq(hl_tok_set_regex(&tok, "bcde", false), 0);
        asserteq(hl_tok_verify(&tok), 0);

        vec_init(&chrs, sizeof(char));
        vec_str(&chrs, "abcdefg");

        off = 1;
        asserteq(hl_tok_match(&tok, &chrs, &off, &start, &end), true);
        asserteq(off, 5);

        off = 2;
        asserteq(hl_tok_match(&tok, &chrs, &off, &start, &end), false);
        asserteq(off, 2);

        vec_kill(&chrs);
        hl_tok_kill(&tok);
    }

    it ("Matches strings with lookbehinds")
    {
        vec_s chrs;
        size_t off, start, end;

        hl_tok_init(&tok, "TOK-1");
        asserteq(hl_tok_set_regex(&tok, "(?<=b)cd", false), 0);
        asserteq(hl_tok_verify(&tok), 0);

        vec_init(&chrs, sizeof(char));
        vec_str(&chrs, "abcdefg");

        off = 2;
        asserteq(hl_tok_match(&tok, &chrs, &off, &start, &end), true);
        asserteq(off, 4);

        off = 2;
        *(char *)vec_get(&chrs, 1) = 'c';
        asserteq(hl_tok_match(&tok, &chrs, &off, &start, &end), false);
        asserteq(off, 2);

        vec_kill(&chrs);
        hl_tok_kill(&tok);
    }

}
