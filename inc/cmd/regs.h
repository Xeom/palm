#if !defined(CMD_REGS_H)
# define CMD_REGS_H
# include <stddef.h>
# include "types.h"

/* These are functions that can be bound to keys, using *
 * a bufbind_s structure.                               *
 *                                                      *
 * They are called with the following parameters:       *
 *  - keys Is a vector of chars, containing the bytes   *
 *         that caused the binding to be activated.     *
 *  - buf  Is the current buffer.                       *
 *  - arg  Is a pointer, chosen at binding time,        *
 *         containing some data for the command.        *
 *  - len  Is the length of the data pointed to by arg. */

void cmd_cut_to_reg(vec_s *keys, buf_s *buf, void *arg, size_t len);
void cmd_copy_to_reg(vec_s *keys, buf_s *buf, void *arg, size_t len);

void cmd_append_to_reg(vec_s *keys, buf_s *buf, void *arg, size_t len);

void cmd_poppaste_reg(vec_s *keys, buf_s *buf, void *arg, size_t len);
void cmd_paste_reg(vec_s *keys, buf_s *buf, void *arg, size_t len);

#endif
