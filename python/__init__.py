#!/usr/bin/python3
#
#    * * * * * * * * * * *
#   * ___  ____ __   _  _  *
#  * / . // . // /_ / `' \  *
# * /_,-'/,-.//___//_/`'\_\  *
# *                         *
#  * * * * * * * * * * * * *
#
# This is the main executable file
# to start palm!
"""Palm! My Editor."""

import os
import paths
import sys
import argparse
import importlib.util
import signal

def run_vg_cmd(args, unknown):
    """Replace the current process with a palm wrapped in valgrind.

    Arguments:
        args    - The arguments to add to the valgrind command.
        unknown - Arguments to pass to the palm command.
    """
    vgpath = preargs.vgexe
    vgargs = [
        vgpath,
        "--log-file=log",
        "--suppressions={}/valgrind-python.supp".format(paths.etcdir)
    ]
    vgargs += args
    vgargs.append(sys.argv[0])
    vgargs += unknown

    print("Running valgrind command: " + " ".join(vgargs))

    try:
        os.execv(vgpath, vgargs)
    except OSError as e:
        print((
            "Could not run command, got error: {}\n\n"
            "Is the valgrind command installed?\n\n"
            "If it is, maybe try using the --vgexe [path]\n"
            " option to set the path of the executable.\n"
        ).format(str(e)), file=sys.stderr)
        exit(1)

def import_rc_file(path, name=None):
    """Import a .py file as an rc file, i.e. make it into a module.

    Arguments:
        path - The path of the file.
        name - The name of the module to make, or None to name
               the modules rc, rc1, rc2, ... etc.
    """
    if name == None:
        name = "rc"
        cnt  = 0
        while name in sys.modules:
            cnt += 1
            name = "rc" + str(cnt)

    spec   = importlib.util.spec_from_file_location(name, path)
    module = importlib.util.module_from_spec(spec)

    sys.modules[name] = module

    spec.loader.exec_module(module)

# -*- PRE ARGUMENTS -*-
# These arguments are parsed before
# the others, so that they can wrap
# the rest of the command in another
# program if needed.
# -*- --- --------- -*-
preparser = argparse.ArgumentParser(
    add_help=False
)
preparser.add_argument(
    "--valgrind", "--vg", action="store_true",
    help="Open palm in valgrind."
)
preparser.add_argument(
    "--valgrindplus", "--vg+", action="store_true",
    help="Open palm in valgrind with fuller checking."
)
preparser.add_argument(
    "--callgrind", "--cg", action="store_true",
    help="Open palm in callgrind."
)
preparser.add_argument(
    "--helgrind", "--hg", action="store_true",
    help="Open palm in helgrind."
)
preparser.add_argument(
    "--vgexe", default="/usr/bin/valgrind",
    help="Provide an alternative path to the valgrind executable."
)

# -*- MAIN ARGUMENTS -*-
# These are the normal arguments
# for starting Palm!
# --- ---- --------- ---
argparser = argparse.ArgumentParser(
    description="My text editor!",
    parents = [preparser]
)
argparser.add_argument(
    "--log", default="/tmp/palm.log", type=argparse.FileType("w"),
    help="Log file location"
)
argparser.add_argument(
    "--version", "-v", action="store_true",
    help="Display the current version and exit."
)
argparser.add_argument(
    "files", metavar="file", nargs="*", type=str,
    help="Open files on startup."
)
argparser.add_argument(
    "--rc", metavar="pyfile", nargs="+",
    help="Execute file as palmrc instead of default."
)
argparser.add_argument(
    "--cprof", action="store_true",
    help="Enable cprofiler."
)
argparser.add_argument(
    "--sock", type=str,
    help="A filename for a ShellServer socket."
)

# Parse preargs ...
#
# Only parse the recognized ones, so the remainder
#  can be passed either to the main argument
#  parser, or to another command.
#
preargs, unknown = preparser.parse_known_args()
if preargs.valgrindplus:
    run_vg_cmd(["--leak-check=full", "--track-origins=yes"], unknown)
if preargs.valgrind:
    run_vg_cmd([], unknown)
if preargs.callgrind:
    run_vg_cmd(["--tool=callgrind"], unknown)
if preargs.helgrind:
    run_vg_cmd(["--tool=helgrind"], unknown)

# Parse main arguments ...
args = argparser.parse_args()

# TODO: Implement printing binary and python version!
if args.version:
    print("PALM")
    print("----")
    print("I have no version")
    quit()

# Start the profiler if we need to!
if args.cprof:
    import cProfile
    import pstats
    profiler = cProfile.Profile()
    profiler.enable()

# -*-------------------------*-
# We begin starting the editor!
# -*-------------------------*-
# Import everything!
from lib import lib
from lib import *
from core import *
import bind

from editor import log, start_editor

editor = start_editor(args.log)

import buffers
import autocomplete
import user

from buffers.log     import LogBuffer
from core.buffer     import Buffer
from buffers.welcome import WelcomeBuffer
from buffers.edit    import EditBuffer
from hooks           import hooks


# Load palmrc.py files ...
if args.rc == None:
    for rc in paths.rcfiles:
        log.fmt("INFO", "INFO", "Loading default rc file '%s' ...", rc)
        import_rc_file(rc)
else:
    for rc in args.rc:
        log.fmt("INFO", "INFO", "Loading rc file '%s' from arguments ...", rc)
        import_rc_file(rc)


# Create the default buffers ...
if len(args.files) == 0:
    log.str("INFO", "INFO", "Creating Welcome Buffer ...")
    buf = editor.new_buf(WelcomeBuffer)

    buf.win_assoc(0)
    editor.sel_buf(buf)

else:
    for fname in args.files:
        log.fmt("INFO", "INFO", "Opening file '%s' from arguments ...", fname)
        buf = editor.new_buf(EditBuffer)
        buf.edit(fname)

    buf.win_assoc(0)
    editor.sel_buf(buf)

log.str("INFO", "INFO", "Starting editor main()")

if args.sock is not None:
    editor.start_server()
    editor.server.bind_and_wait_for_connection(args.sock)

# Startup the editor
editor.main()

# Kill the editor
editor.kill()

# Shutdown the font system
lib.font_end()

# Shutdown the log
args.log.close()

# If we're profiling, we need to display
# the results!
if args.cprof:
    # Create a stats summary ...
    stats = pstats.Stats(profiler)
    stats.sort_stats("cumtime")

    # Open a pipe ...
    rend, wend = os.pipe()

    # To display this, we need to fork
    # the process and create the following
    # situation:
    #   palm | less > stdout
    # And then have palm print out all of
    # the stats to stdout.

    # Fork the process ...
    if os.fork() == 0:
        # For the child, replace stdout with
        # the write end of the pipe.
        fileno = sys.stdout.fileno()
        os.close(fileno)
        os.dup2(wend, fileno)

        # Print the stats to stdout (note that
        # this will now print them to the pipe)
        stats.print_stats()

    else:
        # For the parent, replace stdin with
        # the read end of the pipe.
        fileno = sys.stdin.fileno()
        os.close(fileno)
        os.dup2(rend, fileno)

        # Replace the palm process with less. Note
        # that less is now reading the pipe as stdin.
        os.execv("/bin/sh", ["/bin/sh", "-c", "less"])

if args.sock is not None:
    os.unlink(args.sock)
