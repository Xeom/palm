#if !defined(UTIL_MACRO_H)
# define UTIL_MACRO_H

/**
 * @file macro.h
 * @brief A file just for macro utils
 *
 */

# define DIM(arr)  (sizeof(arr)/sizeof(arr[0]))
# define DIM2(arr) DIM(arr[0])
# define DIM3(arr) DIM(arr[0][0])

# define SET_BITS(n, bits, val) \
    do { n = (val) ? ((n) | (bits)) : ((n) & ~(bits)); } while (0);

/** Return the maximum of two values */
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
/** Return the minimum of two values */
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define ABS(a) (((a) < 0) ? (-(a)) : (a))

/** Swap two variables */
#define SWAP(a, b) do { typeof(a) tmp; tmp=a; a=b; b=tmp; } while (0);

#define _STRIFY(a) a
#define STRIFY(a) _STRIFY(#a)

#define BEFORE_AFTER(before, after) \
    for (int _done = (before, 0); !_done; _done = (after, 1))

#define WITH_MTX(mtx) \
    BEFORE_AFTER(pthread_mutex_lock(mtx), pthread_mutex_unlock(mtx))


#endif
