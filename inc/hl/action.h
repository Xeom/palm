#if !defined(HL_ACTION_H)
# define HL_ACTION_H
# include <stdbool.h>
# include <stddef.h>
# include "types.h"

typedef enum
{
    HL_ACTION_NONE,
    HL_ACTION_PUSH,
    HL_ACTION_POP,
    HL_ACTION_SWAP
} hl_action_type_t;

struct hl_action
{
    long   token;
    long   state;
    int    priority;
    hl_action_type_t type;
    short  font;
};

# include "hl/ctx.h"

int hl_action_init(hl_action_s *act);

int hl_action_verify(hl_action_s *act);

int hl_action_cmp(const void *a, const void *b);

void hl_action_use_defaults(hl_action_s *act, hl_action_s *defaults);

void hl_action_apply(
    hl_action_s *act, hl_ctx_s *ctx, vec_s *str, vec_s *fonts, vec_s *states, size_t start, size_t end);

int hl_action_load_info(hl_action_s *act, yaml_obj_s *obj, hl_ctx_s *ctx);

#endif
