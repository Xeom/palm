import libtypes

from lib           import lib
from server.server import add_global

from core.buffer  import Buffer
from core.chrvec  import ChrVec
from core.cur     import Cur
from core.pos     import Pos
from core.decoder import fmt_sanitize, Decoder

@add_global()
class PromptBuffer(Buffer):
    prompt1 = ChrVec.from_fmt("{prompt-1}>>> ")
    prompt2 = ChrVec.from_fmt("{prompt-2}  > ")
    prompt3 = ChrVec.from_fmt("{prompt-3}<<< ")

    multiline = True

    def init(self, *args):
        super().init(*args)
        self.decoder = Decoder("UTF8")
        self.prompts    = []
        self.running    = False

        self.push_prompt()

    def push_prompt(self, chrs=None):
        ln = len(self)
        prompt = self.get_prompt()

        if chrs == None:
            line = prompt
        else:
            line = ChrVec.from_other(prompt)
            line.cpy_vec(chrs)

        self.ins(ln, line)

        self.prompts.append(prompt)

        self.set_cur(Cur.at(self.last_pos()))

    def pop_prompt(self):
        if len(self.prompts) == 0:
            return None

        ln = len(self) - 1
        line = self.get_line(ln)

        line.rem(0, len(self.prompts.pop()))
        self.rem(len(self) - 1, 1)
        self.set_cur(Cur.at(self.last_pos()))

        return line.to_str()

    def clear_prompt(self):
        while self.pop_prompt() != None:
            pass

    def get_all_typed(self):
        for ln, prompt in zip(range(len(self)-len(self.prompts), len(self)), self.prompts):
            line = self.get_line(ln)
            line.rem(0, len(prompt))
            yield line.to_str()

    def typed_len(self):
        return self.line_len(len(self) - 1) - len(self.prompts[-1])

    def get_prompt(self):
        if len(self.prompts) == 0:
            return self.prompt3 if self.running else self.prompt1

        else:
            return self.prompt2

    def log(self, string):
        ln = len(self) - len(self.prompts)
        self.ins(ln, string)
        self.shift(Pos.at(ln), Pos.at(ln + 1))

    def run_code(self, lines):
        pass

    def run(self):
        code = list(self.get_all_typed())
        self.prompts = []
        self.running = True
        self.push_prompt()
        self.run_code(code)

    def done(self):
        self.running = False
        self.pop_prompt()
        self.push_prompt()

    def enter(self):
        # When multiline=False, pressing Enter once will
        # run the command!
        if self.typed_len() == 0 or not self.multiline:
            self.run()
        else:
            self.push_prompt()

    def backspace(self):
        if self.typed_len() == 0 and len(self.prompts) > 1 and self.get_cur().pri.row == len(self) - 1:
            self.pop_prompt()
        else:
            lib.edit_backspace(self.ptr, self.einfo.ptr)

    def next(self):
        pass

    def prev(self):
        pass

    def bind_all(self):
        self.bind.set_mode("prompt")

