#include "text/indent.h"
#include "text/chr.h"
#include "buf/buf.h"

#include "edit/indent.h"

void edit_rstrip(edit_info_s *einfo, vec_s *chrs)
{
    col_t n;

    n = 0;

    while (vec_len(chrs) > 0 && chr_is_whitespace(vec_end(chrs)))
    {
        n += 1;
        vec_del(chrs, vec_len(chrs) - 1, 1);
    }
}

col_t edit_skip_whitespace(edit_info_s *einfo, vec_s *chrs)
{
    col_t rtn;

    rtn = 0;

    VEC_FOREACH(chrs, chr_s *, chr)
    {
        if (chr_is_whitespace(chr))
            rtn += 1;
        else
            break;
    }

    return rtn;
}

indent_t edit_get_indent(edit_info_s *einfo, vec_s *chrs)
{
    col_t col;

    col = edit_skip_whitespace(einfo, chrs);

    return indent_from_col(chrs, einfo->chrconf, col);
}

col_t edit_set_indent(edit_info_s *einfo, vec_s *chrs, indent_t indent)
{
    size_t ntabs, nspaces;
    col_t col;
    col = edit_skip_whitespace(einfo, chrs);

    vec_del(chrs, 0, col);

    if (einfo->attrs & EDIT_USE_TABS)
    {
        ntabs   = indent / einfo->chrconf->tabwidth;
        nspaces = indent % einfo->chrconf->tabwidth;
    }
    else
    {
        ntabs   = 0;
        nspaces = indent;
    }

    vec_fill(chrs, 0, nspaces, &(einfo->chrconf->space));
    vec_fill(chrs, 0, ntabs,   &(einfo->chrconf->tab));

    return ntabs + nspaces;
}

col_t edit_pad_to_indent(edit_info_s *einfo, vec_s *chrs, indent_t indent)
{
    indent_t lineindent;

    lineindent = indent_from_col(chrs, einfo->chrconf, vec_len(chrs));

    if (lineindent >= indent)
        return indent_to_col(chrs, einfo->chrconf, indent);

    vec_fill(chrs, vec_len(chrs), indent - lineindent, &(einfo->chrconf->space));

    return indent_to_col(chrs, einfo->chrconf, indent);
}

void edit_shift_indent(edit_info_s *einfo, int lvl)
{
    cur_s cur;
    vec_s line;

    vec_init(&line, sizeof(chr_s));

    edit_get_cur(einfo, &cur);

    CUR_FOREACH_LINE(&cur, einfo->buf, linecur)
    {
        indent_t depth, off;
        row_t row = linecur.pri.row;

        buf_get_line(einfo->buf, row, &line);

        depth = edit_get_indent(einfo, &line);
        depth = MAX(0, depth + lvl * einfo->shiftwidth);
        off   = depth % einfo->shiftwidth;

        if (off != 0)
        {
            if (lvl > 0) depth -= off;
            else         depth += einfo->shiftwidth - off;
        }

        edit_set_indent(einfo, &line, depth);

        buf_set_line(einfo->buf, row, &line);
    }

    vec_kill(&line);
}
