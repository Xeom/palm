import weakref
from typing import ClassVar, Dict, Type, List, Any

class InstanceIter(object):
    instances: ClassVar[Dict[Type[Any], List[Any]]] = {}

    @classmethod
    def instanceiter_new(cls, inst):
        l = cls.instances.get(cls, [])
        cls.instances[cls] = l

        l.append(weakref.ref(inst))

    @classmethod
    def instanceiter(cls):
        l = cls.instances.get(cls, [])
        
        for ref in l:
            inst = ref()
            if inst != None:
                yield inst

    def __init__(self, *args, **kwargs):
        self.instanceiter_new(self)
