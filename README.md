README
======

Palm is a text editor, designed to be simple and hackable.

The core of the editor is written in C, allowing the editor to be fast and
efficient, while a layer of Python on top glues everything together and allows
for very simple scripting, customization, and rapid development of new
high-level features.

Key Features
------------

 * In-editor Python console for simple and easy scripting, or direct access
   to C functions and internals for powerful functionality. Keys are bound
   directly to lines of python to run in an internal virtual prompt.
 * Regex push-down FSM based syntax highlighting, with language information
   stored in .yaml files.
 * Run commands on the contents of buffers in a pty, and even redirect their
   output to another buffer.
 * Support for multiple buffers and windows.
 * Editing with multiple cursors. Each command can be marked as multicursor,
   making it extremely simple to add multicursor functionality to commands
   without breaking other ones.
 * Line cursors for operating on whole lines or paragraphs at a time, and
   block cursors for editing columns or blocks of text.
 * A compromise between Vim and Emacs style editing, with both a command mode
   (Press `Ctrl+Space`), and command prefix (`Esc`).
 * All rendering and basic cursor actions run solely in C, for performant
   editing.
 * Keypress recording and playback as clipboard bytes - You can record
   keystrokes, paste them into a buffer, edit them, and copy them again
   before replaying them.

The Python Prompt
-----------------

You can run the `x` command by pressing `Esc`, `x`, to open a Python console
and run code on your current buffer. For example, you might run:

```python
insl([" %s. "%n for n in range(1,10)])
```

in order to make a numbered list. Or, in order to trim the trailing whitespace
from the lines in your selection, you could run the following code:

```python
apply_sel(str.rstrip)
```

Some more examples of using the in-editor Python prompt can be found in the
file `doc/prompt.md`.

Multiple Cursors
----------------

You can use the `'` command to place cursors, the `;` command to split
your current cursor into individual lines, and the `:` command to toggle
on and off multi-cursor commands.

For example, you could make every argument of a function `const` by
navigating to the start of each argument, running the `'` command, to
place cursors at each, then the `:` command to enable them, and typing
the text 'const '.

