#if !defined(IO_TTY_H)
# define IO_TTY_H
# include <stdio.h>
# include <termios.h>
# include <stdbool.h>
# include "types.h"
# include "container/vec.h"
# include "event/writer.h"
# include "plat/vt100.h"

/* Contains a description of how to configure *
 * the terminal.                              */
struct tty_conf
{
    struct termios attrs;
    vt100_mode_t set;
    vt100_mode_t reset;
};

struct tty
{
    /* Terminal file descriptors */
    int in, out;

    /* Configurations for the terminal */
    tty_conf_s origconf;
    tty_conf_s appconf;
    tty_conf_s rawconf;

    vt100_s vt;

    /* It would be good to add the writer in the future, it's just that *
     * events code doesn't work very well multi-threaded.               */
    //writer_s writer;

    /* Terminal dimensions */
    unsigned int w, h;
};

/**
 * Initialize a tty_s with an input and output fd.
 *
 * @param tty     A pointer to the tty_s structure.
 * @param infd    The file descriptor for the read end of
 *                the terminal.
 * @param outfd   The file descriptor for the write end
 *                of the terminal.
 * @param pollctx A polling context for IO operations.
 *
 * @return 0 on success, -1 on error.
 */
int tty_init(tty_s *tty, int infd, int outfd, poll_ctx_s *pollctx);

/**
 * Kill a tty_s.
 *
 * @param tty   A pointer to the tty_s structure.
 */
void tty_kill(tty_s *tty);

/**
 * Set the terminal attributes back to their original values.
 *
 * @param tty   A pointer to the tty_s structure.
 */
void tty_set_orig(tty_s *tty);

/**
 * Set the terminal attributes to their app settings.
 *
 * @param tty   A pointer to the tty_s structure.
 */
void tty_set_app(tty_s *tty);

/**
 * Set the terminal attributes to their raw settings.
 *
 * @param tty   A pointer to the tty_s structure.
 */
void tty_set_raw(tty_s *tty);

/**
 * Clear the terminals screen.
 *
 * @param tty   A pointer to the tty_s structure.
 */
int tty_clear_screen(tty_s *tty);

/**
 * Update tty dimensions.
 *
 * @param tty   A pointer to the tty_s structure.
 */
void tty_update_dims(tty_s *tty);

int tty_write(tty_s *tty, const void *data, size_t len);

#endif
