#if !defined(EDIT_DEL_H)
# define EDIT_DEL_H
# include "edit/edit.h"

void edit_cut_at(edit_info_s *einfo, cur_s *cur);

void edit_backspace(edit_info_s *einfo);

void edit_del(edit_info_s *einfo);

#endif
