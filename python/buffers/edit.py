from buffers.question import QuestionBuffer, Question
from lib              import lib, enums
from hooks            import hooks
from core.io          import create_file, is_dir, is_file, IoPath
from server.server    import add_global

default_edit_attrs = [
    "AUTOINDENT",
    "AUTORSTRIP"
]

@add_global()
class EditBuffer(QuestionBuffer):
    def init(self, *args):
        super().init(*args)

        self.bind.set_mode("edit")
        self.undo_set_enable(True)
        self.einfo.attrs = enums.edit_attr_t(default_edit_attrs)

    def confirm_create(self, path):
        self.rem(0, len(self))
        self.edit(path, force=True)

    def replace_with_dirbuf(self, path):
        buf = self.editor.new_buf(DirBuffer)
        buf.show_dir(path)

        win = self.get_win()
        if win != -1:
            self.editor.win_assoc(buf, win)

        self.editor.kill_buf_soon(self)

    def edit(self, path, force=False):
        pathinfo = IoPath.decompose(path)

        if is_dir(pathinfo.real):
            question  = Question(
                f"The file '{pathinfo.real}' is already a directory."
                f" Open it in a DirBuffer instead?"
            )

            question.add_answer("y",
                self.replace_with_dirbuf, args=[path],
                helptext="Open this directory in a DirBuffer"
            )
            question.add_answer("n",
                self.editor.kill_buf_soon, args=[self],
                helptext="Give up and quit this buffer"
            )
            self.ask(question, inbuf=True)
            return

        elif not is_file(pathinfo.real) and not force:
            if is_dir(pathinfo.dir):
                question = Question(
                    f"The file '{pathinfo.real}' does not exist."
                    f" Create and open it anyway?"
                )
            else:
                question = Question(
                    f"The directory '{pathinfo.real}' does not exist."
                    f" Create and open the file anyway?"
                )

            question.add_answer("y",
                self.confirm_create, args=[path],
                helptext="Create the file"
            )
            question.add_answer("n",
                self.editor.kill_buf_soon, args=[self],
                helptext="Give up and quit this buffer"
            )

            self.ask(question, inbuf=True)
            return

        self.finfo.assoc(path)
        self.finfo.read(force=True)

from buffers.dir import DirBuffer
