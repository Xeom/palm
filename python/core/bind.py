import ctypes

from typing import List, Union, Any, Optional

from lib import lib
from util.encode import bytify

from .newstruct import SimpleCStructure, CStructure
from .vec       import Vec
from .key       import Key

class CmdBindingInfo(SimpleCStructure):
    class_name = "cmd_binding_info_s"

class Bind(CStructure):
    class_name = "bind_s"
    init_funct = lib.bind_init
    kill_funct = lib.bind_kill

    def __init__(
            self,
            key:         Optional[Key],
            pybind:      Optional[str]              =None,
            cbind                                   =None,
            carg                                    =None,
            multicursor: Optional[int]              =None,
            noparamclr:  Optional[bool]             =None,
            modeswitch:  Optional[Union[str, bytes]]=None,
        ):

        super().__init__(key)
        if pybind is not None:
            self.set_pybind(pybind)

        if cbind is not None:
            self.set_cbind(cbind, carg)

        if multicursor is not None:
            self.set_multicursor(multicursor)

        if noparamclr is not None:
            self.set_noparamclr(noparamclr)

        if modeswitch is not None:
            self.set_modeswitch(modeswitch)

    def init_args(self, args, kwargs):
        def inner(key):
            return [key.ptr if key is not None else None]

        return inner(*args, **kwargs)

    def set_cbind(self, cbind, arg=None):
        vec = Vec(ctypes.c_char)

        if arg is None:
            size = 0
            ptr  = None

        else:
            size = ctypes.sizeof(arg)
            ptr  = ctypes.byref(arg)

        vec.add(size, ptr)

        cbind = ctypes.cast(cbind, lib.bind_cbind.argtypes[1])
        lib.bind_cbind(self.ptr, cbind, vec.ptr)

    def set_pybind(self, py: Union[str, bytes]):
        py = bytify(py)
        lib.bind_pybind(self.ptr, py)

    def set_multicursor(self, multicursor: int):
        lib.bind_set_multicursor(self.ptr, multicursor)

    def set_noparamclr(self, noparamclr: bool):
        lib.bind_set_noparamclr(self.ptr, noparamclr)

    def set_modeswitch(self, mode: Union[str, bytes]):
        mode = bytify(mode)
        lib.bind_set_modeswitch(self.ptr, mode)



