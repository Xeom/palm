"""Timers!

Example usage:
t = Timer()
act = EventAction.with_cb(buf.ins, [0, "Hello world"])
t.listen(editor.eventctx, act)
editor.timerctx.add(t)

or, equivalently

t = Timer()
act = EventAction.with_cb(buf.ins, [0, "Hello world"])
t.start(editor.timerctx, act)
"""

import ctypes

from lib import lib, defs
from lib.enums import event_attr_t, event_typ_t, timer_attr_t

from .event import Event, EventSpec, EventAction
from .newstruct import CStructure, SimpleCStructure

from server.globals import add_global

@add_global()
class Timer(SimpleCStructure):
    class_name = "timer_s"

    def start(self, timerctx, *acts):
        for a in acts:
            timerctx.eventctx.contents.listen(self.eventspec(), a)

        timerctx.add(self)

    def remove(self):
        if hasattr(self, "ctx"):
            self.ctx.rem(self)
            del self.ctx

    def kill_action(self):
        self.remove()

    def eventspec(self) -> EventSpec:
        return EventSpec(
            _ptr=ctypes.cast(self.ptr, ctypes.c_void_p),
            typ=event_typ_t("TIMER")
        )

@add_global()
class TimerCtx(CStructure):
    class_name = "timer_ctx_s"

    init_funct = lib.timer_ctx_init
    kill_funct = lib.timer_ctx_kill

    aftercbs = {}
    everycbs = {}

    def init_args(self, args, kwargs):
        return [args[0].ptr]

    def add(self, timer: Timer):
        if hasattr(timer, "ctx") and timer.ctx is not None:
            raise Exception("Timer can only be added to one context")

        lib.timer_add(self.ptr, timer.ptr)
        timer.ctx = self

    def rem(self, timer: Timer):
        if hasattr(timer, "ctx") and timer.ctx is not self:
            raise Exception("Timer is not added in this context")

        lib.timer_rem(self.ptr, timer.ptr)
        timer.ctx = None

    def after(self, delay, cb, args=[]):
        def inner(editorptr, eventptr, param):
            cb(*args)
            del self.aftercbs[inner]

        cf = defs.event_cb_t(inner)
        lib.timer_after(self.ptr, delay, cf, None)
        self.aftercbs[inner] = cf

    def every(self, delay, cb, args=[]):
        def inner(editorptr, eventptr, param):
            cb(*args)

        cf = defs.event_cb_t(inner)
        lib.timer_every(self.ptr, delay, cf, None)
        self.everycbs[inner] = cf
