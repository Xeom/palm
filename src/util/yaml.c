#include "util/yaml.h"
#include <yaml.h>
#include <stdbool.h>
#include "log/log.h"

typedef enum
{
    YAML_OBJ_EXP_KEY,
    YAML_OBJ_EXP_VALUE,
    YAML_OBJ_EXP_ALIAS
} yaml_obj_state_t;

typedef struct yaml_obj_parser yaml_obj_parser_s;
struct yaml_obj_parser
{
    char errbuf[256];
    table_s anchors;
    yaml_obj_s *curr;
    yaml_obj_state_t state;
};

static const char *yaml_event_name(yaml_event_t *event);

static const char *yaml_event_name(yaml_event_t *event)
{
    switch (event->type)
    {
    case YAML_STREAM_START_EVENT:   return "STREAM_START";
    case YAML_STREAM_END_EVENT:     return "STREAM_END";
    case YAML_DOCUMENT_START_EVENT: return "DOCUMENT_START";
    case YAML_DOCUMENT_END_EVENT:   return "DOCUMENT_END";
    case YAML_SEQUENCE_START_EVENT: return "SEQUENCE_START";
    case YAML_SEQUENCE_END_EVENT:   return "SEQUENCE_END";
    case YAML_MAPPING_START_EVENT:  return "MAPPING_START";
    case YAML_MAPPING_END_EVENT:    return "MAPPING_END";
    case YAML_ALIAS_EVENT:          return "ALIAS";
    case YAML_SCALAR_EVENT:         return "SCALAR";
    default:                        return "UNKNOWN";
    }
}

void yaml_obj_init(yaml_obj_s *obj, const char *name, size_t len)
{
    obj->parent = NULL;
    obj->type   = YAML_OBJ_NONE;

    vec_init(&(obj->key), sizeof(char));
    if (name)
        vec_ins(&(obj->key), 0, len, name);

    vec_app(&(obj->key), NULL);
    vec_init(&(obj->val), sizeof(char));
    vec_init(&(obj->seq), sizeof(yaml_obj_s));
    table_init(&(obj->subs));
}

int yaml_obj_kill(yaml_obj_s *obj)
{
    vec_kill(&(obj->key));
    vec_kill(&(obj->val));

    VEC_FOREACH(&(obj->seq), yaml_obj_s *, seq)
    {
        yaml_obj_kill(seq);
    }

    vec_kill(&(obj->seq));

    TABLE_FOREACH(&(obj->subs), sub)
    {
        yaml_obj_kill(table_chain_val(sub, NULL));
    }

    table_kill(&(obj->subs));

    return 0;
}

yaml_obj_s *yaml_obj_add_sub(yaml_obj_s *obj, const char *key, bool *exists)
{
    size_t keylen;
    yaml_obj_s *res;

    keylen = strlen(key);

    if (obj->type != YAML_OBJ_MAPPING)
        return NULL;

    res = table_get(&(obj->subs), key, keylen + 1, NULL);
    if (res)
    {
        if (exists)
            *exists = true;

        return res;
    }
    else
    {
        if (exists)
            *exists = false;

        table_set(&(obj->subs), key, keylen + 1, NULL, sizeof(yaml_obj_s));
        res = table_get(&(obj->subs), key, keylen + 1, NULL);
        yaml_obj_init(res, key, keylen);
        res->parent = obj;

        return res;
    }
}

yaml_obj_s *yaml_obj_add_seq(yaml_obj_s *obj)
{
    yaml_obj_s *new;

    if (obj->type != YAML_OBJ_SEQUENCE)
        return NULL;

    new = vec_app(&(obj->seq), NULL);
    yaml_obj_init(new, NULL, 0);
    new->parent = obj;

    return new;
}

#define PARSER_ERR(parser, ...) snprintf((parser)->errbuf, sizeof((parser)->errbuf), __VA_ARGS__)

static int yaml_obj_consume(yaml_obj_parser_s *parser, yaml_event_t *event)
{
    char *val;
    size_t len;

    val = (char *)event->data.scalar.value;
    len = event->data.scalar.length;

    if (parser->state == YAML_OBJ_EXP_KEY && parser->curr->type == YAML_OBJ_SEQUENCE)
    {
        if (event->type == YAML_SEQUENCE_END_EVENT)
        {
            parser->curr  = parser->curr->parent;
            parser->state = YAML_OBJ_EXP_KEY;

            return 0;
        }
        else
        {
            yaml_obj_s *new;
            new = yaml_obj_add_seq(parser->curr);

            if (new)
            {
                parser->state = YAML_OBJ_EXP_VALUE;
                parser->curr  = new;
            }
            else
            {
                PARSER_ERR(parser, "Could not add item to sequence");
                return -1;
            }
        }
    }

    switch (parser->state)
    {
    case YAML_OBJ_EXP_VALUE:
        if (event->type == YAML_MAPPING_START_EVENT)
        {
            const char *anchorname;

            if (parser->curr->type != YAML_OBJ_MAPPING && parser->curr->type != YAML_OBJ_NONE)
            {
                PARSER_ERR(parser, "Non-mapping object overriden by mapping");
                return -1;
            }

            parser->curr->type = YAML_OBJ_MAPPING;
            parser->state      = YAML_OBJ_EXP_KEY;

            anchorname = (const char *)event->data.mapping_start.anchor;
            if (anchorname)
            {
                LOG(INFO, INFO, "Setting anchor '%s'", anchorname);
                table_set(
                    &(parser->anchors),
                    anchorname, strlen(anchorname) + 1,
                    &(parser->curr), sizeof(yaml_obj_parser_s *)
                );
            }

            return 0;
        }
        else if (event->type == YAML_SCALAR_EVENT)
        {
            if (parser->curr->type != YAML_OBJ_VALUE && parser->curr->type != YAML_OBJ_NONE)
            {
                PARSER_ERR(parser, "Non-value object overriden by value");
                return -1;
            }

            vec_clr(&(parser->curr->val));
            vec_ins(&(parser->curr->val), 0, len + 1, val);

            parser->curr->type = YAML_OBJ_VALUE;
            parser->state     = YAML_OBJ_EXP_KEY;
            parser->curr      = parser->curr->parent;

            return 0;
        }
        else if (event->type == YAML_SEQUENCE_START_EVENT)
        {
            if (parser->curr->type != YAML_OBJ_SEQUENCE && parser->curr->type != YAML_OBJ_NONE)
            {
                PARSER_ERR(parser, "Non-sequence object overriden by sequence");
                return -1;
            }

            parser->curr->type = YAML_OBJ_SEQUENCE;
            parser->state      = YAML_OBJ_EXP_KEY;

            return 0;
        }
        else
        {
            PARSER_ERR(parser,
                "Unhandled %s event in YAML."
                " Expected MAPPING_START, SEQUENCE_START, or SCALAR.",
                yaml_event_name(event)
            );

            return -1;
        }
        break;

    case YAML_OBJ_EXP_ALIAS:
        if (event->type == YAML_ALIAS_EVENT)
        {
            const char *anchorname;
            yaml_obj_s **aliasptr;
            yaml_obj_s *alias;

            anchorname = (const char *)event->data.alias.anchor;
            aliasptr = table_get(
                &(parser->anchors),
                anchorname, strlen(anchorname) + 1,
                NULL
            );

            if (aliasptr == NULL)
            {
                PARSER_ERR(parser, "Could not find alias '%s'.", event->data.alias.anchor);
                return -1;
            }

            alias = *aliasptr;

            /* We need both objects to be mappings to perform an alias. */
            if (alias->type != YAML_OBJ_MAPPING)
            {
                PARSER_ERR(parser, "Alias '%s' is not a mapping.", event->data.alias.anchor);
                return -1;
            }

            yaml_obj_copy_from(parser->curr, alias);
            parser->state = YAML_OBJ_EXP_KEY;

            return 0;
        }
        else
        {
            PARSER_ERR(parser,
                "Unhandled %s event in YAML. Expected ALIAS.",
                yaml_event_name(event)
            );

            return -1;
        }
        break;

    case YAML_OBJ_EXP_KEY:
        if (event->type == YAML_SCALAR_EVENT)
        {
            if (len == 2 && memcmp(val, "<<", 2) == 0)
            {
                parser->state = YAML_OBJ_EXP_ALIAS;
            }
            else
            {
                yaml_obj_s *new;
                new = yaml_obj_add_sub(parser->curr, val, NULL);

                if (!new)
                {
                    PARSER_ERR(parser, "Could not add subitem '%s'", val);
                    return -1;
                }

                parser->state = YAML_OBJ_EXP_VALUE;
                parser->curr  = new;
            }
            return 0;
        }
        else if (event->type == YAML_MAPPING_END_EVENT)
        {
            parser->curr  = parser->curr->parent;
            parser->state = YAML_OBJ_EXP_KEY;

            return 0;
        }
        else
        {
            snprintf(parser->errbuf, sizeof(parser->errbuf),
                "Unhandled %s event in YAML. Expected MAPPING_END or SCALAR.", yaml_event_name(event));
            return -1;
        }
        break;
    default:
        snprintf(parser->errbuf, sizeof(parser->errbuf), "YAML parser got into invalid state.");
        return -1;
    }
}

static void yaml_obj_parser_init(yaml_obj_parser_s *parser, yaml_obj_s *obj)
{
    memset(parser, 0, sizeof(yaml_obj_parser_s));
    parser->curr = obj;
    parser->state = YAML_OBJ_EXP_VALUE;
    table_init(&(parser->anchors));
}

static void yaml_obj_parser_kill(yaml_obj_parser_s *parser)
{
    table_kill(&(parser->anchors));
}


int yaml_obj_parse(yaml_obj_s *obj, FILE *f)
{
    bool document;
    yaml_parser_t parser;
    yaml_obj_parser_s objparser;

    if (!yaml_parser_initialize(&parser))
    {
        LOG(NOTICE, ERR, "YAML parser failed to initialize");
        return -1;
    }

    yaml_obj_parser_init(&objparser, obj);

    yaml_parser_set_input_file(&parser, f);

    document = false;
    while (objparser.curr)
    {
        yaml_event_t event;
        if (!yaml_parser_parse(&parser, &event))
        {
            // TODO: Make this more explicit
            LOG(NOTICE, ERR, "LibYAML parser error");
            yaml_event_delete(&event);
            yaml_parser_delete(&parser);
            yaml_obj_parser_kill(&objparser);
            return -1;
        }

        if (event.type == YAML_DOCUMENT_END_EVENT)
            document = false;

        if (event.type == YAML_STREAM_END_EVENT || event.type == YAML_NO_EVENT)
        {
            yaml_event_delete(&event);
            break;
        }

        if (document)
        {
            if (yaml_obj_consume(&objparser, &event) == -1)
            {
                LOG(NOTICE, ERR, "YAML Parser encountered issue: %s", objparser.errbuf);
                yaml_event_delete(&event);
                yaml_parser_delete(&parser);
                yaml_obj_parser_kill(&objparser);
                return -1;
            }
        }

        if (event.type == YAML_DOCUMENT_START_EVENT)
            document = true;

        yaml_event_delete(&event);
    }

    yaml_parser_delete(&parser);
    yaml_obj_parser_kill(&objparser);

    return 0;
}

int yaml_obj_copy_from(yaml_obj_s *obj, yaml_obj_s *oth)
{
    obj->type = oth->type;

    if (oth->type == YAML_OBJ_MAPPING)
    {
        YAML_MAPPING_FOREACH(oth, key, val)
        {
            bool exists;
            yaml_obj_s *new;
            new = yaml_obj_add_sub(obj, key, &exists);

            if (!exists)
            {
                yaml_obj_copy_from(new, val);
            }
        }
    }
    else if (oth->type == YAML_OBJ_SEQUENCE)
    {
        YAML_SEQUENCE_FOREACH(oth, val)
        {
            yaml_obj_s *new;
            new = yaml_obj_add_seq(obj);
            yaml_obj_copy_from(new, val);
        }
    }
    else if (oth->type == YAML_OBJ_VALUE)
    {
        vec_clr(&(obj->val));
        vec_cpy(&(obj->val), &(oth->val));
    }

    return 0;
}

yaml_obj_s *yaml_obj_get(yaml_obj_s *obj, const char *addr[])
{
    size_t ind;

    for (ind = 0; addr[ind]; ++ind)
    {
        if (obj->type != YAML_OBJ_MAPPING)
            return NULL;
        obj = table_get(&(obj->subs), addr[ind], strlen(addr[ind]) + 1, NULL);
        if (!obj)
            return NULL;
    }

    return obj;
}

const char *yaml_obj_get_val(yaml_obj_s *obj, const char *addr[], size_t *len)
{
    obj = yaml_obj_get(obj, addr);

    if (!obj)
        return NULL;

    if (obj->type != YAML_OBJ_VALUE)
        return NULL;

    if (len)
        *len = vec_len(&(obj->val)) - 1;
    return vec_get(&(obj->val), 0);
}
