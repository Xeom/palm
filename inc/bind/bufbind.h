#if !defined(BIND_BUFBIND_H)
# define BIND_BUFBIND_H
# include "bind/bind.h"
# include "bind/mode.h"

# include "io/pyclient.h"

struct bufbind
{
    pthread_mutex_t mtx;
    mode_s *mode;
    vec_s keys;

    bind_pycb_t pycb;
};

void bufbind_init(bufbind_s *bbind, bind_pycb_t pycb);

void bufbind_kill(bufbind_s *bbind);

void bufbind_key(bufbind_s *bbind, key_s *key, buf_s *buf);

void bufbind_bind(bufbind_s *bbind, bind_s *bind);

void bufbind_unbind(bufbind_s *bbind, key_s *key);

void bufbind_set_mode(bufbind_s *bbind, const char *mode);

void bufbind_get_mode(bufbind_s *bbind, char *name);

void bufbind_prev_mode(bufbind_s *bbind);

#endif
