#if !defined(BIND_BIND_H)
# define BIND_BIND_H
# include "container/vec.h"
# include "io/key.h"
# include <stdbool.h>

# define MODE_NAME_LEN 64

typedef void (*bind_pycb_t)(vec_s *, vec_s *);
typedef void (*bind_c_t)(key_s *, buf_s *, void *, size_t);

struct bind
{
    bool  anykey;
    key_s key;
    vec_s arg;
    vec_s modeswitch;

    bind_c_t cbind;
    int multicursor;
};

void bind_kill(bind_s *bind);

void bind_init(bind_s *bind, const key_s *key);

void bind_init_cpy(bind_s *bind, bind_s *oth);

void bind_cbind(bind_s *bind, bind_c_t cbind, vec_s *arg);

void bind_pybind(bind_s *bind, const char *py);

void bind_set_multicursor(bind_s *bind, int multicursor);

void bind_set_modeswitch(bind_s *bind, const char *mode);

void bind_pycb(key_s *key, buf_s *buf, void *arg, size_t len);

void bind_trigger(bind_s *bind, key_s *key, buf_s *buf);

int bind_cmp(const void *a, const void *b);

#endif
