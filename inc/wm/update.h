#if !defined(WM_UPDATE_H)
# define WM_UPDATE_H
# include "types.h"
# include "container/vec.h"
# include <pthread.h>
# include <stdbool.h>

struct wm_update
{
    int    ind;
    buf_s *buf;
    long   ln;
};

struct wm_updater
{
    wm_s *wm;

    pthread_mutex_t mtx;
    pthread_cond_t  update;
    pthread_t       thread;

    vec_s queue;

    bool alive;
};

void wm_updater_init(wm_updater_s *updater, wm_s *wm);

void wm_updater_kill(wm_updater_s *updater);

void wm_updater_purge_buf(wm_updater_s *updater, buf_s *buf);

void wm_updater_resume(wm_updater_s *updater);

void wm_updater_pause(wm_updater_s *updater);

void wm_updater_queue(wm_updater_s *updater, wm_update_s *update);

#endif
