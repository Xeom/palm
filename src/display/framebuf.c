#include <stdbool.h>
#include "text/chr.h"
#include "container/vec.h"
#include "text/font.h"
#include "io/tty.h"
#include "display/framebuf.h"

void framebuf_init(framebuf_s *fb)
{
    fb->w = 0;
    fb->h = 0;
    vec_init(&(fb->chrs), sizeof(chr_s));
    framebuf_resize(fb, 30, 30);
}

void framebuf_kill(framebuf_s *fb)
{
    vec_kill(&(fb->chrs));
}

void framebuf_cpy(framebuf_s *fb, framebuf_s *oth)
{
    fb->w = oth->w;
    fb->h = oth->h;

    vec_del(&(fb->chrs), 0, vec_len(&(fb->chrs)));
    vec_cpy(&(fb->chrs), &(oth->chrs));
}

static inline void onedim_overlap(int *a1, int *a2, int b1, int b2)
{
    if (*a2 < b1)
    {
        *a1 = b1;
        *a2 = b1;
        return;
    }

    if (*a1 > b2)
    {
        *a1 = b2;
        *a2 = b2;
        return;
    }

    if (*a2 > b2)
        *a2 = b2;

    if (*a1 < b1)
        *a1 = b1;
}

void rect_overlap(rect_s *rect, rect_s *other)
{
    int a1, a2, b1, b2;

    a1 = rect->x;
    a2 = rect->x + rect->w;
    b1 = other->x;
    b2 = other->x + other->w;

    onedim_overlap(&a1, &a2, b1, b2);

    rect->x = a1;
    rect->w = a2 - a1;

    a1 = rect->y;
    a2 = rect->y + rect->h;
    b1 = other->y;
    b2 = other->y + other->h;

    onedim_overlap(&a1, &a2, b1, b2);

    rect->y = a1;
    rect->h = a2 - a1;
}

void framebuf_dims_rect(framebuf_s *fb, rect_s *rect)
{
    rect->x = 0;
    rect->y = 0;
    rect->w = fb->w;
    rect->h = fb->h;
}

void framebuf_trim_rect(framebuf_s *fb, rect_s *rect)
{
    rect_s fbrect;
    framebuf_dims_rect(fb, rect);
    rect_overlap(rect, &fbrect);
}

chr_s *framebuf_get(framebuf_s *fb, unsigned int x, unsigned int y)
{
    if (x >= fb->w || y >= fb->h)
        return NULL;

    return vec_get(&(fb->chrs), x + y * fb->w);
}

void framebuf_resize(framebuf_s *fb, unsigned int w, unsigned int h)
{
    vec_s new;

    vec_init(&new, sizeof(chr_s));

    unsigned int x, y;
    for (y = 0; y < h; ++y)
    {
        for (x = 0; x < w; ++x)
        {
            chr_s *c;
            c = framebuf_get(fb, x, y);

            if (c)
                vec_app(&new, c);
            else
                vec_app(&new, &chr_blank);
        }
    }

    vec_del(&(fb->chrs), 0, vec_len(&(fb->chrs)));
    vec_cpy(&(fb->chrs), &new);

    fb->w = w;
    fb->h = h;

    vec_kill(&new);
}

void framebuf_stamp(framebuf_s *dst, rect_s *dstrect, framebuf_s *src, rect_s *srcrect)
{
    unsigned int x, y;
    for (x = 0; x < srcrect->w; ++x)
    {
        for (y = 0; y < srcrect->h; ++y)
        {
            chr_s *cdst, *csrc;

            cdst = framebuf_get(dst, dstrect->x + x, dstrect->y + y);
            if (!cdst) continue;

            csrc = framebuf_get(src, srcrect->x + x, srcrect->y + y);
            if (!csrc) continue;

            memcpy(cdst, csrc, sizeof(chr_s));
        }
    }
}

void framebuf_set(framebuf_s *f, rect_s *rect, chr_s *mem)
{
    unsigned int x, y;
    for (y = rect->y; y < rect->h + rect->y; ++y)
    {
        for (x = rect->x; x < rect->w + rect->x; ++x)
        {
            chr_s *dst, *src;
            src = mem++;
            dst = framebuf_get(f, x, y);
            if (!dst) continue;

            memcpy(dst, src, sizeof(chr_s));
        }
    }
}

void framebuf_fill(framebuf_s *f, rect_s *rect, chr_s *c)
{
    unsigned int x, y;
    for (y = rect->y; y < rect->h + rect->y; ++y)
    {
        for (x = rect->x; x < rect->w + rect->x; ++x)
        {
            chr_s *dst;
            dst = framebuf_get(f, x, y);
            if (!dst) continue;

            memcpy(dst, c, sizeof(chr_s));
        }
    }
}

void framebuf_print_diff(framebuf_s *fb, framebuf_s *prevfb, tty_s *tty)
{
    bool continuous;
    unsigned short lastfont = font_by_name("default");

    if (prevfb && (fb->w != prevfb->w || fb->h != prevfb->h))
    {
        framebuf_print_diff(fb, NULL, tty);
        return;
    }

    font_print(lastfont, tty);

//    if (!prevfb)
//    {
//        fprintf(stream, GOTO(0, 0) CLR_SCREEN);
//    }

    unsigned int x, y;
    for (y = 0; y < fb->h; ++y)
    {
        continuous = false;
        for (x = 0; x < fb->w; ++x)
        {
            chr_s *prev, *c;

            c = framebuf_get(fb, x, y);

            if (prevfb)
                prev = framebuf_get(prevfb, x, y);
            else
                prev = NULL;

            if (prev && memcmp(prev, c, sizeof(chr_s)) == 0)
            {
                continuous = false;
                continue;
            }

            if (!continuous)
                vt100_set_cursor(&(tty->vt), y + 1, x + 1);

            if (lastfont != c->font)
            {
                font_print(c->font, tty);
                lastfont = c->font;
            }

            chr_print(c, tty);
            continuous = true;
        }
    }
}
