#include "snow.h"

#include "io/io.h"

describe(io)
{
    subdesc(relpath)
    {
        char rel[PATH_MAX];
        it ("Calculates subpaths")
        {
            asserteq(io_relpath(rel, "/a/b/c/d", "/a/b", NULL), 0);
            asserteq(rel, "c/d");
            asserteq(io_relpath(rel, "/tmp/test", "/tmp", NULL), 0);
            asserteq(rel, "test");
        }

        it ("Uses absolute paths when there is no subpath")
        {
            // This test fails when in '/' ...
            asserteq(io_relpath(rel, "/tmp/test", ".", NULL), 0);
            asserteq(rel, "/tmp/test");
            asserteq(io_relpath(rel, "/tmp/test", "/tmp/notexist", NULL), 0);
            asserteq(rel, "/tmp/test");
            asserteq(io_relpath(rel, "/tmp/test", "/var", NULL), 0);
            asserteq(rel, "/tmp/test");
        }

        it ("Uses the cwd")
        {
            // This test assumes the existance of /tmp ...
            char prevwd[PATH_MAX];
            assert(getcwd(prevwd, sizeof(prevwd)));
            asserteq(chdir("/tmp"), 0);

            asserteq(io_relpath(rel, "/tmp/test", ".", NULL), 0);
            asserteq(rel, "test");
            asserteq(io_relpath(rel, "/tmp/test", "..", NULL), 0);
            asserteq(rel, "tmp/test");

            chdir(prevwd);
        }
    }

    subdesc(path_join)
    {
        char dst[PATH_MAX];
        it ("Appends to root directory")
        {
            strcpy(dst, "/");
            asserteq(io_path_join(dst, "tmp/file", NULL), 0);
            asserteq(dst, "/tmp/file");

            strcpy(dst, "/");
            asserteq(io_path_join(dst, "/tmp/file", NULL), 0);
            asserteq(dst, "/tmp/file");

            strcpy(dst, "///");
            asserteq(io_path_join(dst, "/tmp/file", NULL), 0);
            asserteq(dst, "/tmp/file");

            strcpy(dst, "/");
            asserteq(io_path_join(dst, "/tmp///dir/a/", NULL), 0);
            asserteq(dst, "/tmp/dir/a");
        }

        it ("Appends to blank directory")
        {
            char dst[PATH_MAX];

            strcpy(dst, "");
            asserteq(io_path_join(dst, "tmp/file", NULL), 0);
            asserteq(dst, "tmp/file");

            strcpy(dst, "");
            asserteq(io_path_join(dst, "/tmp/file", NULL), 0);
            asserteq(dst, "tmp/file");

            strcpy(dst, "");
            asserteq(io_path_join(dst, "//tmp//dir/a", NULL), 0);
            asserteq(dst, "tmp/dir/a");
        }

        it ("Ignores appending blank path")
        {
            strcpy(dst, "");
            asserteq(io_path_join(dst, "", NULL), 0);
            asserteq(dst, "");

            strcpy(dst, "/");
            asserteq(io_path_join(dst, "", NULL), 0);
            asserteq(dst, "/");

            strcpy(dst, "file");
            asserteq(io_path_join(dst, "", NULL), 0);
            asserteq(dst, "file");

            strcpy(dst, "/tmp/file/");
            asserteq(io_path_join(dst, "", NULL), 0);
            asserteq(dst, "/tmp/file");
        }

        it ("Ignores appending path with just slashes")
        {
            strcpy(dst, "");
            asserteq(io_path_join(dst, "///", NULL), 0);
            asserteq(dst, "");

            strcpy(dst, "/");
            asserteq(io_path_join(dst, "///", NULL), 0);
            asserteq(dst, "/");

            strcpy(dst, "file");
            asserteq(io_path_join(dst, "///", NULL), 0);
            asserteq(dst, "file");

            strcpy(dst, "/tmp/file/");
            asserteq(io_path_join(dst, "///", NULL), 0);
            asserteq(dst, "/tmp/file");
        }
        it ("Appends paths")
        {
            strcpy(dst, "a/b/");
            asserteq(io_path_join(dst, "c", NULL), 0);
            asserteq(io_path_join(dst, "d/", NULL), 0);
            asserteq(dst, "a/b/c/d");

            strcpy(dst, "/part1");
            asserteq(io_path_join(dst, "part2/part3", NULL), 0);
            asserteq(dst, "/part1/part2/part3");


        }
    }

}
