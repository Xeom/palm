#if !defined(TEXT_FILE_H)
# define TEXT_FILE_H
# define FILE_CHNK_LEN 64
# include "container/vec.h"
# include "buf/cur.h"
# include "text/decoder.h"
# include "text/encoder.h"
# include "io/io.h"
# include "types.h"
# include <pthread.h>
# include <stdio.h>
# include <limits.h>

typedef void (*file_rcb_t)(file_s *, vec_s *, void *, row_t ln);
typedef void (*file_wcb_t)(file_s *, vec_s *, void *, row_t ln, bool *lastline);
typedef void (*file_cb_t)(file_s *, void *);

extern int file_chnk_len;

#define FILE_NAME_LEN 256

struct file
{
    char name[FILE_NAME_LEN + 1];
    bool writing;
    bool reading;
    bool done;
    bool err;

    io_file_info_s finfo;
    int fd;

    poll_ctx_s *pollctx;

    vec_s bytes;
    vec_s line;

    decoder_attr_t attrs;
    decoder_s decoder;
    encoder_s encoder;

    row_t startln, endln;
    row_t currln;

    file_wcb_t wcb;
    file_rcb_t rcb;
    file_cb_t  cb;
    void  *param;

    long ccount, lcount;
};

int file_init(file_s *file, poll_ctx_s *pollctx);

void file_pause(file_s *file);

void file_kill(file_s *file);

int file_set_fd(file_s *file, int fd, const char *name);
void file_set_attrs(file_s *file, decoder_attr_t attrs);
void file_set_line_range(file_s *file, long start, long end);

int file_read (file_s *file, file_rcb_t rcb, file_cb_t cb, void *cbparam);
int file_write(file_s *file, file_wcb_t wcb, file_cb_t cb, void *cbparam);

void file_enable_undo_cb(file_s *file, void *arg);
void file_read_to_buf_cb(file_s *file, vec_s *line, void *arg, row_t ln);
void file_write_from_buf_cb(file_s *file, vec_s *line, void *arg, row_t ln, bool *ended);
void file_read_to_cur_cb(file_s *file, vec_s *line, void *arg, row_t ln);
void file_read_to_buf_repl_cb(file_s *file, vec_s *line, void *arg, row_t ln);

#endif
