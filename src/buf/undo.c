#include "buf/undo.h"
#include "buf/buf.h"
#include "util/time.h"
#include "log/log.h"

#define UNDO_LIST_DEFAULT_MAXLEN 1024

double undo_max_mergable_time = 2.0;

static undo_action_s *undo_list_get(undo_list_s *list, long off);

bool buf_undo_get_enable(buf_s *buf)
{
    bool res;

    WITH_MTX(&(buf->mtx))
    {
        res = buf->undolist.enabled;
    }

    return res;
}

void buf_undo_set_enable(buf_s *buf, bool enable)
{
    WITH_MTX(&(buf->mtx))
    {
        buf->undolist.enabled = enable;
    }
}

void buf_undo_clear(buf_s *buf)
{
    WITH_MTX(&(buf->mtx))
    {
        undo_list_kill(&(buf->undolist));
        undo_list_init(&(buf->undolist));
    }
}

int buf_undo(buf_s *buf)
{
    int res;
    WITH_MTX(&(buf->mtx))
    {
        res = undo_list_undo(&(buf->undolist), buf);
    }

    return res;
}

int buf_redo(buf_s *buf, long branch)
{
    int res;
    WITH_MTX(&(buf->mtx))
    {
        res = undo_list_redo(&(buf->undolist), buf, branch);
    }

    return res;
}

void undo_list_init(undo_list_s *list)
{
    list->maxlen  = UNDO_LIST_DEFAULT_MAXLEN;
    list->off     = 0;
    list->enabled = true;
    vec_init(&(list->actions), sizeof(undo_action_s));
    undo_list_add(list);
    list->enabled = false;
}

void undo_list_kill(undo_list_s *list)
{
    VEC_FOREACH(&(list->actions), undo_action_s *, action)
    {
        undo_action_kill(action);
    }

    vec_kill(&(list->actions));
}

static undo_action_s *undo_list_get(undo_list_s *list, long off)
{
    long ind;
    ind = off + list->off;
    if (ind < 0 || ind >= (long)vec_len(&(list->actions)))
        return NULL;

    return vec_get(&(list->actions), ind);
}

int undo_list_compact(undo_list_s *list)
{
    undo_action_s *curr, *prev;

    curr = undo_list_get(list, 0);
    if (!curr)
        return -1;

    if (list->off != (long)vec_len(&(list->actions)) - 1)
        return -1;

    prev = undo_list_get(list, curr->parentoff);
    if (!prev)
        return 0;

    if (curr->ts - prev->ts > undo_max_mergable_time)
        return 0;

    if (prev->typ == UNDO_TYPE_SET && curr->typ == UNDO_TYPE_DEL)
    {
        if (prev->ln != curr->ln)
            return 0;

        vec_kill(prev->new);
        free(prev->new);
        prev->new = NULL;

        prev->typ = UNDO_TYPE_DEL;
    }
    else if (prev->typ == UNDO_TYPE_SET && curr->typ == UNDO_TYPE_SET)
    {
        if (prev->ln != curr->ln)
            return 0;

        vec_kill(prev->new);
        free(prev->new);

        prev->new = curr->new;
        curr->new = NULL;
    }
    else
    {
        return 0;
    }

    list->off += curr->parentoff;
    vec_clr(&(prev->children));
    undo_action_kill(curr);

    vec_del(&(list->actions), vec_len(&(list->actions)) - 1, 1);

    return 0;
}

undo_action_s *undo_list_add(undo_list_s *list)
{
    undo_action_s *action, *new;
    long childoff;
    size_t len;

    if (!list->enabled)
        return NULL;

    len    = vec_len(&(list->actions));
    action = vec_get(&(list->actions), list->off);

    if (action)
    {
        childoff = len - ((long)list->off);
        vec_app(&(action->children), &childoff);
    }

    new = vec_app(&(list->actions), NULL);
    undo_action_init(new);

    if (action)
        new->parentoff = ((long)list->off) - len;
    else
        new->parentoff = -((long)vec_len(&(list->actions)));

    list->off = len;

    if ((long)vec_len(&(list->actions)) > list->maxlen)
    {
        vec_del(&(list->actions), 0, len / 2);
        list->off -= len / 2;
    }

    return new;
}

int undo_list_undo(undo_list_s *list, buf_s *buf)
{
    undo_action_s *curr, *prev;

    curr = undo_list_get(list, 0);
    if (!curr)
        return -1;

    prev = undo_list_get(list, curr->parentoff);
    if (!prev)
        return -1;

    if (vec_len(&(prev->children)) == 1)
    {
        LOG(ALERT, SUCC, "Travelled %.1fs back in time.", curr->ts - prev->ts);
    }
    else
    {
        LOG(
            ALERT, SUCC,
            "Travelled %.1fs backwards. There are %lu branches to go forward.",
            curr->ts - prev->ts,
            vec_len(&(prev->children))
        );
    }

    list->enabled = false;
    undo_action_undo(curr, buf);
    list->enabled = true;

    list->off += curr->parentoff;

    return 0;
}

int undo_list_redo(undo_list_s *list, buf_s *buf, size_t branch)
{
    undo_action_s *curr, *next;
    long off, *offptr;

    curr = undo_list_get(list, 0);
    if (!curr)
        return -1;

    offptr = vec_get(&(curr->children), branch);
    if (!offptr)
        return -1;

    off = *offptr;

    next = undo_list_get(list, off);
    if (!next)
        return -1;

    if (vec_len(&(next->children)) == 1)
    {
        LOG(ALERT, SUCC, "Travelled %.1fs forward in time.", next->ts - curr->ts);
    }
    else
    {
        LOG(
            ALERT, SUCC,
            "Travelled %.1fs forwards. There are %lu branches to go forward.",
            next->ts - curr->ts,
            vec_len(&(next->children))
        );
    }

    LOG(ALERT, SUCC, "Travelled %.1f seconds forward in time.", next->ts - curr->ts);

    list->enabled = false;
    undo_action_redo(next, buf);
    list->enabled = true;

    list->off += off;

    return 0;
}

size_t undo_list_num_branches(undo_list_s *list)
{
    undo_action_s *curr;

    curr = undo_list_get(list, 0);
    if (!curr)
        return 0;

    return vec_len(&(curr->children));
}

void undo_action_init(undo_action_s *act)
{
    memset(act, 0, sizeof(undo_action_s));
    act->ts = util_time_fsec();
    vec_init(&(act->children), sizeof(long));
    act->typ = UNDO_TYPE_NONE;
}

void undo_from_ins(undo_action_s *act, size_t ln, size_t n)
{
    if (!act) return;

    act->typ = UNDO_TYPE_INS;
    act->ln  = ln;
    act->n   = n;
}

void undo_from_set(undo_action_s *act, size_t ln, vec_s *prev, vec_s *new)
{
    if (!act) return;

    act->typ = UNDO_TYPE_SET;
    act->ln  = ln;

    if (!(act->prev))
    {
        act->prev = malloc(sizeof(vec_s));
        vec_init(act->prev, sizeof(chr_s));
    }

    if (!(act->new))
    {
        act->new = malloc(sizeof(vec_s));
        vec_init(act->new, sizeof(chr_s));
    }

    vec_cpy(act->prev, prev);
    vec_cpy(act->new,  new);
}

void undo_from_del(undo_action_s *act, size_t ln, vec_s *prev)
{
    if (!act || !prev) return;

    act->typ = UNDO_TYPE_DEL;
    act->ln  = ln;

    if (!(act->prev))
    {
        act->prev = malloc(sizeof(vec_s));
        vec_init(act->prev, sizeof(chr_s));
    }

    vec_cpy(act->prev, prev);
}

void undo_action_kill(undo_action_s *act)
{
    if (act->prev)
    {
        vec_kill(act->prev);
        free(act->prev);
        act->prev = NULL;
    }

    if (act->new)
    {
        vec_kill(act->new);
        free(act->new);
        act->new = NULL;
    }

    vec_kill(&(act->children));
}

void undo_action_redo(undo_action_s *act, buf_s *buf)
{
    switch (act->typ)
    {
    case UNDO_TYPE_NONE:
        break;
    case UNDO_TYPE_INS:
        buf_ins_line(buf, act->ln, act->n);
        break;
    case UNDO_TYPE_SET:
        buf_set_line(buf, act->ln, act->new);
        break;
    case UNDO_TYPE_DEL:
        buf_del_line(buf, act->ln, 1);
        break;
    }
}

void undo_action_undo(undo_action_s *act, buf_s *buf)
{
    switch (act->typ)
    {
    case UNDO_TYPE_NONE:
        break;
    case UNDO_TYPE_INS:
        buf_del_line(buf, act->ln, act->n);
        break;
    case UNDO_TYPE_SET:
        buf_set_line(buf, act->ln, act->prev);
        break;
    case UNDO_TYPE_DEL:
        buf_ins_line(buf, act->ln, 1);
        buf_set_line(buf, act->ln, act->prev);
        break;
    }
}
