from lib       import lib, enums
from util.keys import Keys
from bind.mode import Mode, PyCommand, CCommand

from core.chrvec import ChrVec

from server.server import add_global

Mode.add_default_cmds([
    PyCommand("Select default mode", "md", "buf.bind.set_mode('default')"),

    PyCommand("Next window",                       "wn", "editor.switch_win(1)"),
    PyCommand("Previous window",                   "wp", "editor.switch_win(-1)"),
    PyCommand("Close current window",              "wx", "editor.win_close()"),
    PyCommand("Split current window",              "ws", "editor.win_split()"),
    PyCommand("Horizontally split current window", "whs", "editor.win_split(split='H')"),
    PyCommand("Vertically split current window",   "wvs", "editor.win_split(split='V')"),

    PyCommand("New editable buffer", "bb", "editor.new_buf(EditBuffer, switch=True)"),
    PyCommand("Next buffer",         "bn", "editor.switch_buf(1)",  shortcut=Keys.ctrl("n")),
    PyCommand("Previous buffer",     "bp", "editor.switch_buf(-1)", shortcut=Keys.ctrl("v")),
    PyCommand("Kill current buffer", "bx", "editor.kill_buf_soon(buf)"),
    PyCommand("New directory list",  "bf", "editor.new_buf(DirBuffer, switch=True).show_dir('.')"),
    PyCommand("New buffer list",     "bl", "editor.get_buf_of_type(BuflistBuffer, switch=True)"),

    PyCommand("Exit editor", "ex", "editor.soft_exit()"),

    PyCommand("New Python prompt", "x",  "open_buffer_pyprompt()"),

    PyCommand("Show current buffer file info", "fi",
        "editor.popup_buf(PathInfoBuffer).show_path(finfo.path)"),

    PyCommand("Load from file to current buffer", "fr", "finfo.read()"),
    PyCommand("Save from current buffer to file", "fw", "finfo.write()"),
    PyCommand("Save from current buffer to file", "fW", "save_all()"),
    PyCommand("Associate current buffer with a file", "fa",
        "single_pyprompt_param('finfo.assoc(\"','\")')"),

    PyCommand("Force-load from file to current buffer", "f!r", "finfo.read(force=True)"),
    PyCommand("Force-save from current buffer to file", "f!w", "finfo.write(force=True)"),
    PyCommand("Save from current buffer to file",       "f!W", "save_all(force=True)"),
    PyCommand("Force-associate current buffer with a file", "f!a",
        "single_pyprompt_param('finfo.assoc(\"','\", force=True)')"),

    PyCommand("Open a new file", "fe",
        "single_pyprompt_param('edit_file(\"','\")')", shortcut=Keys.ctrl("e")),

    # Vim-style keybindings!
    #
    #       ^
    # < H J K L >
    #     v

    CCommand("Move cursor left", "h",
        lib.cmd_move, enums.cur_dir_t("LEFT"),  shortcut=Keys.left, multicursor=True),
    CCommand("Move cursor down", "j",
        lib.cmd_move, enums.cur_dir_t("DOWN"),  shortcut=Keys.down, multicursor=True),
    CCommand("Move cursor up", "k",
        lib.cmd_move, enums.cur_dir_t("UP"),    shortcut=Keys.up,   multicursor=True),
    CCommand("Move cursor right", "l",
        lib.cmd_move, enums.cur_dir_t("RIGHT"), shortcut=Keys.right,multicursor=True),

    CCommand("Move cursor to next word", ".",
        lib.cmd_move, enums.cur_dir_t("NWORD"), shortcut=Keys.esc(Keys.right), multicursor=True),
    CCommand("Move cursor to previous word", ",",
        lib.cmd_move, enums.cur_dir_t("PWORD"), shortcut=Keys.esc(Keys.left), multicursor=True),

    CCommand("Go to start of line", "H",
        lib.cmd_home, None, shortcut=Keys.home, multicursor=True),
    CCommand("Go to end of screen or scroll down", "J",
        lib.cmd_pgdn, None, shortcut=Keys.pgdn),
    CCommand("Go to top of screen or scroll up", "K",
        lib.cmd_pgup, None, shortcut=Keys.pgup),
    CCommand("Go to end of line", "L",
        lib.cmd_end,  None, shortcut=Keys.end, multicursor=True),

    CCommand("Select line cursor mode", "cl",
        lib.cmd_cur_type, enums.cur_type_t("LINE"), shortcut=Keys.ctrl("l"), multicursor=True),
    CCommand("Select block cursor mode", "ck",
        lib.cmd_cur_type, enums.cur_type_t("BLOCK"), shortcut=Keys.ctrl("k"), multicursor=True),
    CCommand("Select character cursor mode", "cc",
        lib.cmd_cur_type, enums.cur_type_t("CHR"), multicursor=True),

    CCommand("Swap primary and secondary cursor positions", "cw",
        lib.cmd_swap,  None, shortcut=Keys.ctrl("w"), multicursor=True),
    CCommand("Enable or disable cursor selection", "ss",
        lib.cmd_stick, None, shortcut=Keys.ctrl("s"), multicursor=True),

    # We run this multicursor command in reverse, so that the order the cursors copy
    # and paste in is the same ...
    CCommand("Copy the cursor contents to a register", "y",
        lib.cmd_copy_to_reg, None, shortcut=Keys.ctrl("y"), multicursor=-1),

    CCommand("Log cursor position", "q",  lib.cmd_tell),

    CCommand("Toggle overwrite mode", "mow",
        lib.cmd_toggle_edit_attrs, enums.edit_attr_t("OW"), shortcut=Keys.ins),

    CCommand("Toggle no-altcurs mode", ":",
        lib.cmd_toggle_edit_attrs, enums.edit_attr_t("NOALTS")),
    CCommand("Split cursor into one cursor per line", ";",
        lib.cmd_split_cur),

    PyCommand("Place altcur here", "#",  "place_cur()", multicursor=1),
    PyCommand("Pop this cursor",   "~",  "pop_cur()"),
    PyCommand("Pop this cursor",   "cx", "pop_cur()"),

    PyCommand("Next cursor",     "cn", "curset.select_next()"),
    PyCommand("Previous cursor", "cp", "curset.select_prev()"),
])

DefaultMode = Mode("default")

@add_global(args=["buf"])
def move(buf, d="RIGHT", n=1):
    """Move the cursor a number of places in a direction.

    Arguments:
        d -- The direction to move.
        n -- The number of places.
        b -- The buffer to operate on.
    """
    buf.move_cur(d, n)

@add_global(args=["buf"])
def home(buf):
    """Go to the start of the line."""
    lib.cmd_home(ChrVec().ptr, buf.ptr, None, 0)

@add_global(args=["buf"])
def end(buf):
    """Go to the end of the line."""
    lib.cmd_end(ChrVec().ptr, buf.ptr, None, 0)

@add_global(args=["buf"])
def swap(buf):
    """Swap the primary and secondary cursors."""
    chrvec = ChrVec()
    lib.cmd_swap(chrvec.ptr, buf.ptr, None, 0)

@add_global(args=["buf"])
def snap(buf):
    """Move the secondary to the primary cursor."""
    chrvec = ChrVec()
    lib.cmd_snap(chrvec.ptr, buf.ptr, None, 0)

@add_global(args=["buf"])
def stick(buf, enable=None):
    """Toggle or set the stickniess of the cursor.

    Arguments:
        enable -- None to toggle the stickiness, or True/False to
                  set it.
        b      -- The buffer to operate on.
    """
    if enable == None:
        chrvec = ChrVec()
        lib.cmd_stick(chrvec.ptr, buf.ptr, None, 0)
    else:
        cur = buf.get_cur()
        cur["sticky"] = bool(enable)
        buf.set_cur(cur)
