__all__ = [
    "enums",
    "structs",
    "util",
    "defs",
    "sysdefs",
    "functs",
    "so"
]

from .so      import lib
from .util import enumspec
from . import *
