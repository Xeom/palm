
Command List
============

This is automatically generated documentation, don't trust it
too much :)

Default commands
----------------

### `!` Run pty command
Python binding to run,
```
single_pyprompt(*make_pty_prompt_cmd(buf))
```

### `#` Place altcur here
Python binding to run,
```
einfo.attrs += ['NOALTS']; buf.altcurs += [cur()]
```

### `'` Start short param
Python binding to run,
```
add_param_short_start(buf)
```

### `,` Move cursor to previous word
C binding to `cmd_move`.
Runs multi-cursor.
Shortcut: `Esc+Left`.

### `.` Move cursor to next word
C binding to `cmd_move`.
Runs multi-cursor.
Shortcut: `Esc+Right`.

### `/` Start text search
Python binding to run,
```
single_pyprompt_param('search_one(r"', '")')
```

### `:` Toggle no-altcurs mode
C binding to `cmd_toggle_edit_attrs`.

### `;` Split cursor into one cursor per line
C binding to `cmd_split_cur`.

### `?` Next search result
Python binding to run,
```
search_continue()
```

### `H` Go to start of line
C binding to `cmd_home`.
Runs multi-cursor.
Shortcut: `Home`.

### `J` Go to end of screen or scroll down
C binding to `cmd_pgdn`.
Shortcut: `PageDown`.

### `K` Go to top of screen or scroll up
C binding to `cmd_pgup`.
Shortcut: `PageUp`.

### `L` Go to end of line
C binding to `cmd_end`.
Runs multi-cursor.
Shortcut: `End`.

### `R` Playback recording
Python binding to run,
```
single_pyprompt('playback(editor, reg.get()', ')')
```

### ``` Start long param
Python binding to run,
```
add_param_long_start(buf)
```

### `bb` New editable buffer
Python binding to run,
```
editor.new_buf(EditBuffer, switch=True)
```

### `bf` New directory list
Python binding to run,
```
editor.new_buf(DirBuffer, switch=True).show_dir('.')
```

### `bl` New buffer list
Python binding to run,
```
editor.get_buf_of_type(BuflistBuffer, switch=True)
```

### `bn` Next buffer
Python binding to run,
```
editor.switch_buf(1)
```
Shortcut: `Ctrl+n`.

### `bp` Previous buffer
Python binding to run,
```
editor.switch_buf(-1)
```
Shortcut: `Ctrl+v`.

### `bx` Kill current buffer
Python binding to run,
```
editor.kill_buf_soon(buf)
```

### `cc` Select character cursor mode
C binding to `cmd_cur_type`.
Runs multi-cursor.

### `ck` Select block cursor mode
C binding to `cmd_cur_type`.
Runs multi-cursor.
Shortcut: `Ctrl+k`.

### `cl` Select line cursor mode
C binding to `cmd_cur_type`.
Runs multi-cursor.
Shortcut: `Ctrl+l`.

### `cn` Next cursor
Python binding to run,
```
next_altcur()
```

### `cp` Previous cursor
Python binding to run,
```
prev_altcur()
```

### `cw` Swap primary and secondary cursor positions
C binding to `cmd_swap`.
Runs multi-cursor.
Shortcut: `Ctrl+w`.

### `cx` Pop this cursor
Python binding to run,
```
pop_cur()
```

### `ex` Exit editor
Python binding to run,
```
editor.soft_exit()
```

### `f!W` Save from current buffer to file
Python binding to run,
```
save_all(force=True)
```

### `f!a` Force-associate current buffer with a file
Python binding to run,
```
single_pyprompt_param('finfo.assoc("','", force=True)')
```

### `f!r` Force-load from file to current buffer
Python binding to run,
```
finfo.read(force=True)
```

### `f!w` Force-save from current buffer to file
Python binding to run,
```
finfo.write(force=True)
```

### `f/` Search files
Python binding to run,
```
single_pyprompt_param('rgrep(r"', '")')
```

### `fW` Save from current buffer to file
Python binding to run,
```
save_all()
```

### `fa` Associate current buffer with a file
Python binding to run,
```
single_pyprompt_param('finfo.assoc("','")')
```

### `fe` Open a new file
Python binding to run,
```
single_pyprompt_param('edit_file("','")')
```
Shortcut: `Ctrl+e`.

### `fi` Show current buffer file info
Python binding to run,
```
editor.popup_buf(PathInfoBuffer).show_path(finfo.path)
```

### `fr` Load from file to current buffer
Python binding to run,
```
finfo.read()
```

### `fw` Save from current buffer to file
Python binding to run,
```
finfo.write()
```

### `gJ` Goto end of file
Python binding to run,
```
goto(buf.last_pos())
```

### `gK` Goto end of file
Python binding to run,
```
goto(0, 0)
```

### `gg` Goto position
Python binding to run,
```
single_pyprompt_param('goto(', ')')
```
Shortcut: `Ctrl+g`.

### `h` Move cursor left
C binding to `cmd_move`.
Runs multi-cursor.
Shortcut: `Left`.

### `j` Move cursor down
C binding to `cmd_move`.
Runs multi-cursor.
Shortcut: `Down`.

### `k` Move cursor up
C binding to `cmd_move`.
Runs multi-cursor.
Shortcut: `Up`.

### `l` Move cursor right
C binding to `cmd_move`.
Runs multi-cursor.
Shortcut: `Right`.

### `md` Select default mode
Python binding to run,
```
buf.bind.set_mode('default')
```

### `me` Select editing mode
Python binding to run,
```
buf.bind.set_mode('edit')
```

### `mow` Toggle overwrite mode
C binding to `cmd_toggle_edit_attrs`.
Shortcut: `Ins`.

### `mr` Record mode
Python binding to run,
```
buf.bind.set_mode('record');reg.push()
```
Shortcut: `Ctrl+r`.

### `q` Log cursor position
C binding to `cmd_tell`.

### `s(` Select parens
Python binding to run,
```
select_brackets(r'(', r')')
```

### `s/*` Select c-comment
Python binding to run,
```
select_brackets(r'/*', r'*/')
```

### `s<` Select angles
Python binding to run,
```
select_brackets(r'<', r'>')
```

### `s[` Select brackets
Python binding to run,
```
select_brackets(r'[', r']')
```

### `s`` Select quotes
Python binding to run,
```
select_brackets(r'`', r''')
```

### `si(` Select in parens
Python binding to run,
```
select_brackets(r'(', r')', incl=False)
```

### `si/*` Select in c-comment
Python binding to run,
```
select_brackets(r'/*', r'*/', incl=False)
```

### `si<` Select in angles
Python binding to run,
```
select_brackets(r'<', r'>', incl=False)
```

### `si[` Select in brackets
Python binding to run,
```
select_brackets(r'[', r']', incl=False)
```

### `si`` Select in quotes
Python binding to run,
```
select_brackets(r'`', r''', incl=False)
```

### `si{` Select in braces
Python binding to run,
```
select_brackets(r'{', r'}', incl=False)
```

### `ss` Enable or disable cursor selection
C binding to `cmd_stick`.
Runs multi-cursor.
Shortcut: `Ctrl+s`.

### `sw` Select word
Python binding to run,
```
select_surrounding(r'\s|^', r'\s|$', 'r', incl=False)
```

### `s{` Select braces
Python binding to run,
```
select_brackets(r'{', r'}')
```

### `whs` Horizontally split current window
Python binding to run,
```
editor.win_split(split='H')
```

### `wn` Next window
Python binding to run,
```
editor.switch_win(1)
```

### `wp` Previous window
Python binding to run,
```
editor.switch_win(-1)
```

### `ws` Split current window
Python binding to run,
```
editor.win_split()
```

### `wvs` Vertically split current window
Python binding to run,
```
editor.win_split(split='V')
```

### `wx` Close current window
Python binding to run,
```
editor.win_close()
```

### `x` New Python prompt
Python binding to run,
```
open_buffer_pyprompt()
```

### `y` Copy the cursor contents to a register
C binding to `cmd_copy_to_reg`.
Runs multi-cursor, in reverse order.
Shortcut: `Ctrl+y`.

### `~` Pop this cursor
Python binding to run,
```
pop_cur()
```


`edit` Mode
---------

### `Y` Cut the cursor to a register
C binding to `cmd_cut_to_reg`.
Runs multi-cursor, in reverse order.
Shortcut: `Ctrl+x`.

### `i` Insert character
Python binding to run,
```
single_pyprompt_param('ins("', '")')
```

### `p` Paste the first register
C binding to `cmd_paste_reg`.
Runs multi-cursor.
Shortcut: `Ctrl+p`.

### `P` Pop and paste the first register
C binding to `cmd_poppaste_reg`.
Runs multi-cursor.
Shortcut: `Ctrl+o`.

### `del` Delete character
C binding to `cmd_del`.
Runs multi-cursor.
Shortcut: `Del`.

### `u` Undo
C binding to `cmd_undo`.

### `U` Redo
C binding to `cmd_redo`.

### `c>` Increase line indent
C binding to `cmd_add_indent`.
Runs multi-cursor.
Shortcut: `Tab`.

### `c<` Decrease line indent
C binding to `cmd_add_indent`.
Runs multi-cursor.
Shortcut: `Shift+Tab`.

### `nl` Insert new line
C binding to `cmd_enter`.
Runs multi-cursor.
Shortcut: `Enter`.

### `nhl` Insert new line before current line
C binding to `cmd_preline_enter`.
Runs multi-cursor.
Shortcut: `Ctrl+[` `Enter`.

### `back` Backspace character
C binding to `cmd_backspace`.
Runs multi-cursor.
Shortcut: `Backsp`.


`prompt` Mode
---------

### `Y` Cut the cursor to a register
C binding to `cmd_cut_to_reg`.
Runs multi-cursor, in reverse order.
Shortcut: `Ctrl+x`.

### `i` Insert character
Python binding to run,
```
single_pyprompt_param('ins("', '")')
```

### `p` Paste the first register
C binding to `cmd_paste_reg`.
Runs multi-cursor.
Shortcut: `Ctrl+p`.

### `P` Pop and paste the first register
C binding to `cmd_poppaste_reg`.
Runs multi-cursor.
Shortcut: `Ctrl+o`.

### `del` Delete character
C binding to `cmd_del`.
Runs multi-cursor.
Shortcut: `Del`.

### `u` Undo
C binding to `cmd_undo`.

### `U` Redo
C binding to `cmd_redo`.

### `run` Run command
Python binding to run,
```
buf.enter()
```
Shortcut: `Enter`.

### `back` Backspace character
Python binding to run,
```
buf.backspace()
```
Shortcut: `Backsp`.


`pyprompt` Mode
---------

### `Y` Cut the cursor to a register
C binding to `cmd_cut_to_reg`.
Runs multi-cursor, in reverse order.
Shortcut: `Ctrl+x`.

### `i` Insert character
Python binding to run,
```
single_pyprompt_param('ins("', '")')
```

### `p` Paste the first register
C binding to `cmd_paste_reg`.
Runs multi-cursor.
Shortcut: `Ctrl+p`.

### `P` Pop and paste the first register
C binding to `cmd_poppaste_reg`.
Runs multi-cursor.
Shortcut: `Ctrl+o`.

### `del` Delete character
C binding to `cmd_del`.
Runs multi-cursor.
Shortcut: `Del`.

### `u` Undo
C binding to `cmd_undo`.

### `U` Redo
C binding to `cmd_redo`.

### `run` Run command
Python binding to run,
```
buf.enter()
```
Shortcut: `Enter`.

### `back` Backspace character
Python binding to run,
```
buf.backspace()
```
Shortcut: `Backsp`.

### `cN` Next scrollback
Python binding to run,
```
buf.next()
```
Shortcut: `Ctrl+n`.

### `cP` Previous scrollback
Python binding to run,
```
buf.prev()
```
Shortcut: `Ctrl+v`.

