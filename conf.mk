FILES=text/font text/chr text/file text/decoder text/encoder text/reg text/indent \
      container/vec container/list container/table container/sortedvec \
      cmdlang/lexer cmdlang/cmd cmdlang/ctx cmdlang/lib cmdlang/obj cmdlang/parser \
      help/help \
      cmd/cmd cmd/cur cmd/nav cmd/edit cmd/py \
      util/pool util/pyfmt util/time util/yaml util/escape util/string \
      io/pyclient io/tty io/input io/io io/playbacker io/pty io/key \
      plat/vt100 \
      bind/bind bind/mode bind/bufbind \
      display/framebuf display/display \
      buf/line buf/buf buf/cur buf/scroll  buf/finfo buf/undo buf/curset buf/sel \
      wm/win wm/wm wm/status wm/update wm/floatwin \
      log/log log/notify \
      hl/token hl/ctx hl/state hl/action hl/worker \
      search/search \
      match/match match/set \
      event/event event/timer event/poll event/loop event/reader event/writer \
      edit/edit  edit/editinfo edit/indent edit/enter edit/ins edit/del \
      editor
HELPFILES=cmdlist cmds prompt syntax edit
HEADERS=

LFLAGS=$(or $(shell pcre2-config --libs8),pcre2-8)
LINKS=yaml util

TESTDIR=test/c/
SRCDIR=src/
INCDIR=inc/
OBJDIR=obj/
BINDIR=bin/
DEPFILE=deps.d
DOCDIR=doc/
HELPDIR=data/help/

INSTDIR=/usr/local/bin/

VERSION=0.2.0

WARNINGS=all extra no-unused-parameter no-switch missing-prototypes no-attributes uninitialized

STD=gnu11

DEFINES=
