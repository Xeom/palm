#if !defined(WM_STATUS_H)
# define WM_STATUS_H
# include "types.h"
# include "container/vec.h"

typedef enum
{
    STATUS_TOK_STR,
    STATUS_TOK_IF,
    STATUS_TOK_ELSE,
    STATUS_TOK_FI,
    STATUS_TOK_ITEM
} status_tok_typ_t;

struct status_tok
{
    vec_s cont;
    struct status_cond *cond;
    struct status_item *item;
    status_tok_typ_t typ;
};

struct status_format
{
    vec_s left;
    vec_s mid;
    vec_s right;
};

void status_format_init(status_format_s *format);

void status_format_kill(status_format_s *format);
int status_format_set(
    status_format_s *format,
    const char *left,
    const char *mid,
    const char *right
);

void status_format(status_format_s *fmt, editor_s *editor, int winind, buf_s *buf, vec_s *status);

void status_generate_part(vec_s *tokens, editor_s *editor, int winind, buf_s *buf, vec_s *part);

int status_tokenize(vec_s *tokens, vec_s *chrs);

void status_token_init(status_tok_s *tok);

void status_token_kill(status_tok_s *tok);

#endif
