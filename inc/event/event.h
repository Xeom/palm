#if !defined(EVENT_EVENT_H)
# define EVENT_EVENT_H
# include <pthread.h>

# include "types.h"

# include "container/vec.h"
# include "container/table.h"

/**
 * @file events.h
 * @brief Event handling system.
 *
 * The event system is designed to replace the myriad of async and
 * event handling code in palm, and unite both the C and Python
 * halves of the codebase in this respect.
 *
 * At the most basic level, the event system allows a user to
 * register an @c event_action_s to happen when an @c event_s
 * matching an @c event_spec_s happens.
 *
 * The types of events can include IO events, timer events,
 * and whatever else code might need to wait for in an async
 * manner.
 *
 */

/** Attributes for events. */
typedef enum
{
    /**
     * This event will go at the front of the queue.
     *
     * If this is not set, then any other queued events will go before
     * this one.
     *
     */
    EVENT_ATTR_NEXT = 0x01,

    /**
     * This event has been cancelled.
     *
     * This can be set on an event that is currently being handled
     * to stop any lower priority handlers from seeing it.
     *
     */
    EVENT_ATTR_CANCELLED = 0x02,

    /**
     * Make this event happen right now.
     *
     */
    EVENT_ATTR_NOW = 0x04

} event_attr_t;

/** Types of event. */
typedef enum
{
    EVENT_RESIZE = 0,    /**< Window resize. */
    EVENT_POLL,      /**< Poll event. */
    EVENT_TIMER,     /**< Timer expiry. */

    EVENT_TEST,      /**< Test event. */

    EVENT_KEY_BYTE,  /**< An input_s has got a byte from the keyboard. */
    EVENT_BINDING,   /**< Key binding. */

    EVENT_IDLE,       /**< There are no more events to handle. */

    EVENT_READER_RECV, /**< A reader_s has read a line. */
    EVENT_READER_EOF,  /**< A reader_s has reached EOF. */

    EVENT_PARSED_KEY, /**< A key_parser_s has parsed a full key_s structure. */
} event_typ_t;

/** A signature for event callbacks.
 *
 * The first argument is a pointer to the current editor.
 * The second argument is a pointer to the event causing the callback.
 * The third argument is a user-defined parameter.
 *
 */
typedef void (*event_cb_t)(editor_s *, event_s *, void *);


/**
 * A specification for matching a set of events.
 *
 * This structure is used to select a specify an event to listen for.
 *
 * Specifications are matched completely and identically, i.e. in order
 * for an event to be matched, i.e.
 *
 * memcmp(&(event.spec), &(event_spec), sizeof(event_spec_s)) == 0
 *
 */
struct event_spec
{
    /**
     * An event-dependent pointer.
     *
     * The meaning of this pointer is event-specific, but its value is matched
     * for any event. If it is not being used, it MUST be set to NULL!
     *
     */
    void *ptr;
    event_typ_t typ; /**< The type of the event being listened for. */
};

typedef enum
{
    KEY_ATTR_SHIFT  = 0x01,
    KEY_ATTR_CTRL   = 0x02,
    KEY_ATTR_MOD    = 0x04
} key_attr_t;

typedef enum
{
    KEY_ESC,
    KEY_UP,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_ENTER,
    KEY_BACKSP,
    KEY_DEL,
    KEY_INS,
    KEY_HOME,
    KEY_END,
    KEY_PGUP,
    KEY_PGDN,
    KEY_TAB,
    KEY_SPASTE,
    KEY_EPASTE,

    KEY_FUNCT,
    KEY_CHAR,
    KEY_NUMPAD
} key_button_t;

struct key
{
    key_button_t button;
    key_attr_t   attrs;
    uint8_t      val;
};

/**
 * An event.
 *
 * This structure contains information about an individual event that has
 * happened.
 *
 */
struct event
{
    /**
     * The specification of the event.
     *
     * Actions matching this specification will be run in response to
     * this event.
     *
     */
    event_spec_s spec;
    double ts;          /**< The event timestamp. */

    /**
     * An event defined pointer.
     *
     * Unlike in an @c event_spec_s, this pointer is specific to the individual
     * event itself, and is not matched against.
     *
     */
    void *ptr;


    event_attr_t attrs; /**< Event attributes. */
    char chr;           /**< For character-related events, a character. */
    key_s key;          /**< For key-related events, a key. */
};

/**
 * An event action.
 *
 * This is a structure defining an action that the event system should take
 * when an event matching a certain event_spec happens.
 *
 */
struct event_action
{
    event_cb_t cb; /**< A callback for the event system to perform. */
    void *param;   /**< A parameter to hand to the callback.        */
    int pri;       /**< A priority (lower get performed first.)     */
};

/**
 * An event context.
 *
 * This represents an entire event system.
 *
 */
struct event_ctx
{
    event_s currevent; /**< The current event being handled. */

    editor_s *editor;  /**< A pointer to the editor. */
    vec_s pending;     /**< A vector of pending events. */

    /**
     * A table of vectors of actions.
     *
     * Each vector is sorted by its priority, in the
     * order that the actions should take place.
     *
     * This table contains @c vec_s vectors containing
     * @c event_action_s structures, and is indexed
     * by @c event_spec_s structures.
     *
     */
    table_s listeners;

    /**
     * A mutex to ensure that the event context does not contain race
     * conditions.
     *
     */
    pthread_mutex_t mtx;
};

/**
 * Initialize an event context.
 *
 * @param ctx    A pointer to the event context to initialise.
 * @param editor A pointer to the editor that this event context is initialized
 *               for.
 *
 * @return 0 on success, -1 on error.
 *
 */
int event_ctx_init(event_ctx_s *ctx, editor_s *editor);

/**
 * Kill an event context.
 *
 * This frees all resources held by an event context, and must be called to
 * release them after event_ctx_init is called.
 *
 * @param ctx A pointer to the event context to kill.
 *
 * @return 0 on success, -1 on error.
 *
 */
void event_ctx_kill(event_ctx_s *ctx);

/**
 * Queue up an event.
 *
 * This informs an event context that an event has happened. It can then
 * handle the event.
 *
 * @param ctx   A pointer to the event context.
 * @param event A pointer to the event. A copy is made of this structure,
 *              so the original can be free'd or discarded.
 *
 * @return 0 on success, -1 on error.
 *
 */
int event_push(event_ctx_s *ctx, const event_s *event);

/**
 * Listen for an event.
 *
 * This causes an event context to take a specific action when an event
 * matching a certain event specification happens.
 *
 * @param ctx  A pointer to the event context.
 * @param spec A pointer to the specification to match events.
 * @param act  A pointer to the action to take in response to an event.
 *
 * @return 0 on success, -1 on error.
 *
 */
int event_listen(event_ctx_s *ctx, const event_spec_s *spec, const event_action_s *act);

/**
 * Remove an action from happening in response to a certain event.
 *
 * @param ctx  A pointer to the event context.
 * @param spec A pointer to the event specification to remove an action from.
 * @param act  A pointer to the action to remove. Any action with the same
 *             callback and parameter will be removed. Note that this does not
 *             need to be the same action, that was passed to @c event_listen,
 *             but just one with the same parameters. If NULL, then ALL actions
 *             awaiting the event are removed.
 *
 * @return 0 on success, -1 on error or if no actions existed.
 *
 */
int event_unlisten(event_ctx_s *ctx, const event_spec_s *spec, const event_action_s *act);

/**
 * Handle an event.
 *
 * This should be called for every event produced by event_get_next().
 *
 * This should not be called directly to make an event happen, instead,
 * event_push() can do this.
 *
 * @param ctx   A pointer to the event context in which the event took place.
 * @param event A pointer to the event.
 *
 */
void event_handle(event_ctx_s *ctx, event_s *event);

/**
 * Get the next event for an event context to handle.
 *
 * This returns a pointer that will remain valid until this function is
 * called again.
 *
 * This should be used in conjunction with event_handle().
 *
 * @param ctx A pointer to the event context to find an event in.
 *
 * @return A pointer to the event, or NULL if no event exists.
 *
 */
event_s *event_get_next(event_ctx_s *ctx);

/**
 * Replace the currently happening event with a different one.
 *
 * This is intended to be used within event callbacks, when they want to
 * consume the current event and send another in place of it.
 *
 * @param ctx A pointer to the current event context.
 * @param new A pointer to the event to replace the current event with.
 *
 */
void event_replace(event_ctx_s *ctx, event_s *new);

#endif
