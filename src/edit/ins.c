#include "text/chr.h"
#include "buf/buf.h"
#include "edit/ins.h"

void edit_ins_chrs_at(edit_info_s *einfo, const chr_s *chrs, size_t nchrs, pos_s *pos)
{
    vec_s line;
    pos_s newpos;

    vec_init(&line, sizeof(chr_s));

    buf_get_line(einfo->buf, pos->row, &line);
    vec_ins(&line, pos->col, nchrs, chrs);
    buf_set_line(einfo->buf, pos->row, &line);

    newpos = (pos_s){ .row = pos->row, .col = pos->col + nchrs };

    buf_shift(einfo->buf, pos, &newpos);

    *pos = newpos;

    vec_kill(&line);
}

void edit_ins_str_at(edit_info_s *einfo, const char *str, size_t len, pos_s *pos)
{
    size_t ind;
    vec_s line;

    vec_init(&line, sizeof(chr_s));

    for (ind = 0; ind < len; ++ind)
    {
        if (decoder_chr(&(einfo->insdecoder), str[ind], &line))
        {
            edit_ins_chrs_at(einfo, vec_get(&line, 0), vec_len(&line), pos);
            edit_break_line_at(einfo, pos);
            vec_clr(&line);
        }
    }

    if (vec_len(&line))
    {
        edit_ins_chrs_at(einfo, vec_get(&line, 0), vec_len(&line), pos);
    }

    vec_kill(&line);
}

void edit_ins_vec(edit_info_s *einfo, vec_s *vec)
{
    edit_ins_str(einfo, vec_get(vec, 0), vec_len(vec));
}

void edit_ins_str(edit_info_s *einfo, const char *str, size_t len)
{
    cur_s cur;

    edit_get_cur(einfo, &cur);

    switch (cur.type)
    {
    case CUR_TYPE_LINE:
    case CUR_TYPE_CHR:
        edit_ins_str_at(einfo, str, len, &(cur.pri));
        break;

    case CUR_TYPE_BLOCK:
        CUR_FOREACH_LINE(&cur, einfo->buf, line)
        {
            edit_ins_str_at(
                einfo, str, len,
                &(pos_s){
                    .row = line.pri.row,
                    .col = cur.pri.col
                }
            );
        }
        break;
    }
}
