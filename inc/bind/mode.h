#if !defined(BIND_MODE_H)
# define BIND_MODE_H
# include "container/vec.h"
# include "bind/bind.h"

struct mode
{
    mode_s *next, *prevmode;

    table_s binds;
    bind_s  defbind;
    char name[MODE_NAME_LEN + 1];
};

mode_s *mode_get(mode_s *mode, const char *name);

void mode_kill(mode_s *mode);

bind_s *mode_key(mode_s *mode, key_s *key);

void mode_bind(mode_s *mode, bind_s *bind);
void mode_unbind(mode_s *mode, key_s *key);

#endif
