from hooks import hooks
from lib       import lib
from core.bind import CmdBindingInfo
from core.bind import Bind
from core.key  import Key
from editor    import log
from lib       import enums
from util.encode import bytify

def setup_all(bbind):
    setup_cmd_mode(bbind)
    setup_single_cmd_mode(bbind)
    setup_edit_mode(bbind)
    setup_prompt_mode(bbind)
    setup_pyprompt_mode(bbind)
    setup_cursormenu_mode(bbind)
    setup_question_mode(bbind)
    setup_default_mode(bbind)

def setup_single_cmd_mode(bbind):
    bbind.set_mode("single-cmd")

    bbind.bind(Bind(
        None,
        cbind=lib.cmd_type_binding,
        carg=CmdBindingInfo(multi=False, greedy=True)
    ))
    bbind.bind(Bind(
        Key("ENTER"),
        cbind=lib.cmd_run_binding,
        carg=CmdBindingInfo(multi=False)
    ))
    bbind.bind(Bind(
        Key("ESC"),
        cbind=lib.cmd_cancel_binding,
        carg=CmdBindingInfo(multi=False)
    ))

def setup_cmd_mode(bbind):
    bbind.set_mode("cmd")

    bbind.bind(Bind(
        None,
        cbind=lib.cmd_type_binding,
        carg=CmdBindingInfo(multi=True, greedy=True)
    ))
    bbind.bind(Bind(
        Key("ENTER"),
        cbind=lib.cmd_run_binding,
        carg=CmdBindingInfo(multi=True)
    ))
    bbind.bind(Bind(
        Key("ESC"),
        cbind=lib.cmd_cancel_binding,
        carg=CmdBindingInfo(multi=False)
    ))

def add_cmd_shortcuts(bbind, shortcuts):
    for key, cmd in shortcuts:
        carg = CmdBindingInfo(
            multi=False,
            greedy=True,
            shortcut=cmd
        )
        bbind.bind(Bind(key, cbind=lib.cmd_shortcut_binding, carg=carg))

def setup_default_mode(bbind, name="default"):

    bbind.set_mode(name)

    bbind.bind(Bind(Key("ESC"), pybind="buf.bind.set_mode('single-cmd')"))
    bbind.bind(Bind(
        Key("CHAR", val=ord("@"), attrs=["CTRL"]),
        pybind="buf.bind.set_mode('cmd')"
    ))
    add_cmd_shortcuts(bbind, [
        (Key("UP"),    b":up"),
        (Key("DOWN"),  b":down"),
        (Key("LEFT"),  b":left"),
        (Key("RIGHT"), b":right"),

        (Key("HOME"),  b":home"),
        (Key("END"),   b":end"),

        (Key("UP",    attrs=["MOD"]), b":pgup"),
        (Key("DOWN",  attrs=["MOD"]), b":pgdn"),
        (Key("LEFT",  attrs=["MOD"]), b"5:move"),
        (Key("RIGHT", attrs=["MOD"]), b"4:move"),

        (Key("UP",    attrs=["SHIFT"]), b"10:up"),
        (Key("DOWN",  attrs=["SHIFT"]), b"10:down"),
        (Key("LEFT",  attrs=["SHIFT"]), b"10:left"),
        (Key("RIGHT", attrs=["SHIFT"]), b"10:right"),

        (Key("PGDN"),  b":pgdn"),
        (Key("PGUP"),  b":pgup"),

        (Key("CHAR", val=ord("N"), attrs=["CTRL"]), b":next_buf"),
        (Key("CHAR", val=ord("V"), attrs=["CTRL"]), b":prev_buf"),

        (Key("CHAR", val=ord("K"), attrs=["CTRL"]), b":cursor_block_mode"),
        (Key("CHAR", val=ord("L"), attrs=["CTRL"]), b":cursor_line_mode"),
        (Key("CHAR", val=ord("W"), attrs=["CTRL"]), b":swap"),
        (Key("CHAR", val=ord("S"), attrs=["CTRL"]), b":stick"),

        (Key("CHAR", val=ord("E"), attrs=["CTRL"]), b":file_edit")
    ])

def setup_edit_mode(bbind):
    setup_default_mode(bbind, "edit")

    toggleow     = bytify(
        f"{enums.edit_attr_t('OW').val} :toggle_edit_attrs"
    )
    togglenoalts = bytify(
        f"{enums.edit_attr_t('ALTS').val} :toggle_edit_attrs"
    )

    add_cmd_shortcuts(bbind, [
        (Key("ENTER"),                b":enter"),
        (Key("ENTER", attrs=["MOD"]), b":preline_enter"),
        (Key("BACKSP"),               b":backsp"),
        (Key("DEL"),                  b":del"),
        (Key("TAB"),                  b":incr_indent"),
        (Key("TAB", attrs=["SHIFT"]), b":decr_indent"),
        (Key("INS"),                  toggleow),
    ])
    bbind.bind(Bind(
        None,
        cbind=lib.insert_binding
    ))

def setup_prompt_mode(bbind, name="prompt"):
    setup_default_mode(bbind, name)

    bbind.bind(Bind(Key("ENTER"),  pybind="buf.enter()"))
    bbind.bind(Bind(Key("BACKSP"), pybind="buf.backspace()"))
    bbind.bind(Bind(None, cbind=lib.insert_binding))

def setup_pyprompt_mode(bbind, name="pyprompt"):
    setup_prompt_mode(bbind, name)

    bbind.bind(Bind(Key("TAB"), pybind="PathAutoCompleter(buf).open_menu()"))
    bbind.bind(Bind(Key.char("N", "CTRL"), pybind="buf.next()"))
    bbind.bind(Bind(Key.char("V", "CTRL"), pybind="buf.prev()"))

def setup_cursormenu_mode(bbind, name="cursor-menu"):
    bbind.set_mode(name)

    bbind.bind(Bind(Key("DOWN"), pybind="sel_next_cursor_menu(1)"))   
    bbind.bind(Bind(Key("TAB"), pybind="sel_next_cursor_menu(1)"))   
    bbind.bind(Bind(Key.char("j"), pybind="sel_next_cursor_menu(1)"))

    bbind.bind(Bind(Key("UP"), pybind="sel_next_cursor_menu(-1)"))   
    bbind.bind(Bind(Key.char("k"), pybind="sel_next_cursor_menu(-1)"))

    bbind.bind(Bind(Key("ENTER"), pybind="enter_cursor_menu()", modeswitch="prev"))
    bbind.bind(Bind(None,    pybind="close_cursor_menu()", modeswitch="prev"))

def setup_question_mode(bbind, name="question"):
    setup_default_mode(bbind, name)

    bbind.bind(Bind(None, pybind="buf.answer(%k)"))
