#include <string.h>
#include <stdint.h>
#include "text/font.h"
#include "util/macro.h"
#include "io/tty.h"
#include "text/chr.h"

chr_s chr_blank = { .text = " ", .font = 0 };
static char *ascii_names[256] = {
    "NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "\\a",
    "\\b", "\\t", "\\n", "\\v", "\\f", "\\r", "SI",  "SO",
    "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB",
    "CAN", "EM",  "SUB", "ESC", "FS",  "GS",  "RS",  "US",
    [0x7f] = "DEL"
};

static const char *whitespace_chrs[] = {
    " ", "\t", "\f", "\r", "\n", "\v", NULL
};

void chrconf_init(chrconf_s *conf)
{
    FONT_DEF(tabfont, "expanded-tab");
    FONT_DEF(brafont, "expanded-hex-bracket");
    FONT_DEF(hexfont, "expanded-hex");
    FONT_DEF(nlfont,  "expanded-nlchr");

    conf->nlstr      = ";";
    conf->tabwidth   = 4;
    memcpy(conf->tabstr, (chr_s[]){
        { .text = ">", .font = tabfont },
        { .text = ".", .font = tabfont },
        { .text = ".", .font = tabfont },
        { .text = ".", .font = tabfont },
        { .text = ".", .font = tabfont },
        { .text = ".", .font = tabfont },
        { .text = ".", .font = tabfont },
        { .text = ".", .font = tabfont }
    }, sizeof(conf->tabstr));

    conf->openbra  = (chr_s){ .text = "<", .font = brafont };
    conf->closebra = (chr_s){ .text = ">", .font = brafont };

    conf->space    = (chr_s){ .text = " ",  .font = 0 };
    conf->tab      = (chr_s){ .text = "\t", .font = 0 };

    conf->hexfont = hexfont;
    conf->nlfont  = nlfont;

}


void chr_print(chr_s *chr, tty_s *tty)
{
    tty_write(tty, chr->text, chr_num_bytes(chr));
}

size_t chr_expanded_width(chr_s *chr, size_t pos, chrconf_s *conf)
{
    uint8_t c;
    c = chr->text[0];

    if (memcmp(chr->text, "\t", chr_num_bytes(chr)) == 0 && conf->tabstr)
    {
        return conf->tabwidth - (pos % conf->tabwidth);
    }
    else if (memcmp(chr->text, "\n", chr_num_bytes(chr)) == 0 && conf->nlstr)
    {
        return strlen(conf->nlstr);
    }
    else if (chr_num_bytes(chr) <= 1 && (c < 0x20 || c > 0x7e))
    {
        if (ascii_names[c])
            return 2 + strlen(ascii_names[c]);
        else
            return 4;
    }
    else
    {
        return 1;
    }
}

void chr_expand(chr_s *chr, size_t pos, chrconf_s *conf, chr_s *dst)
{
    uint8_t c;
    c = chr->text[0];

    if (strcmp(chr->text, "\t") == 0 && conf->tabstr)
    {
        size_t ind, width;
        width = conf->tabwidth - (pos % conf->tabwidth);
        for (ind = 0; ind < width; ++ind)
        {
            memcpy(&dst[ind], &conf->tabstr[MIN(ind, CHR_TABSTR_LEN)], sizeof(chr_s));

            if (chr->font)
                dst[ind].font = chr->font;
        }
    }
    else if (strcmp(chr->text, "\n") == 0 && conf->nlstr)
    {
        dst->font = conf->nlfont;
        strncpy(dst->text, conf->nlstr, sizeof(dst->text) - 1);
    }
    else if (chr_num_bytes(chr) <= 1 && (c < 0x20 || c > 0x7e))
    {
        size_t ind;
        chr_s hexchr;

        memset(&(hexchr.text), 0, sizeof(hexchr.text));
        hexchr.font = conf->hexfont;

        ind = 0;

        memcpy(&dst[ind++], &conf->openbra, sizeof(chr_s));
        if (ascii_names[c])
        {
            char *name;

            for (name = ascii_names[c]; *name; ++name)
            {
                hexchr.text[0] = *name;
                memcpy(&dst[ind++], &hexchr, sizeof(chr_s));
            }
        }
        else
        {
            char *hexchrs = "0123456789ABCDEF";
            hexchr.text[0] = hexchrs[(c & 0xf0) >> 4];
            memcpy(&dst[ind++], &hexchr, sizeof(chr_s));

            hexchr.text[0] = hexchrs[(c & 0x0f)];
            memcpy(&dst[ind++], &hexchr, sizeof(chr_s));
        }
        memcpy(&dst[ind++], &conf->closebra, sizeof(chr_s));
    }
    else
    {
        memcpy(dst, chr, sizeof(chr_s));
    }
}

bool chr_is_whitespace(chr_s *chr)
{
    const char **str;
    if (!chr)
        return false;

    for (str = whitespace_chrs; *str; ++str)
    {
        if (memcmp(chr->text, *str, chr_num_bytes(chr)) == 0)
            return true;
    }

    return false;
}
