from typing import Union, Sequence, Optional, Callable, List
from core.wm     import Wm, AnnotationFloatingWindow, FloatingWindow
from core.editor import Editor
from core.font   import font_by_name
from core.chr    import Chr
from core.chrvec import ChrVec
from core.rect   import Rect

from server.server import add_global

current = None

@add_global(args=["editor"])
def open_cursor_menu(editor: Editor, opts: List[str], cb: Callable[[str], None], rst: Callable[[], None]):
    global current
    close_cursor_menu()

    if len(opts) > 1:
        current = CursorMenu(editor, opts, cb, rst)
    elif len(opts) == 1:
        cb(opts[0])

@add_global()
def close_cursor_menu():
    global current
    if current is not None:
        current.close()
        current = None

@add_global()
def sel_next_cursor_menu(n: int=1):
    global current
    if current is not None:
        current.sel_next(n)

@add_global()
def enter_cursor_menu():
    global current
    if current is not None:
        current.enter()

@add_global()
class CursorMenu(object):
    maxlen = 10
    def __init__(self, editor: Editor, opts: List[str], cb: Callable[[str], None], rst: Callable[[], None]):
        self.editor = editor
        self.win = None
        self.opts = opts
        self.cb   = cb
        self.rst  = rst
        self.entered = False
        self.ind  = 0
        self.scr  = 0

        editor.selbuf.bind.set_mode("cursor-menu")
        self.draw()

    def draw(self):
        self.show_opts(self.opts, self.ind)

    def close(self):
        self.rem_win()

        if not self.entered:
            self.rst()

    def get_sel(self) -> str:
        return self.opts[self.ind]

    def sel_next(self, n: int=1):
        self.ind += n
        self.ind %= len(self.opts)

        if self.ind < self.scr:
            self.scr = self.ind

        if self.ind >= self.scr + self.maxlen:
            self.scr = self.ind - self.maxlen + 1
        self.draw()

    def enter(self):
        self.entered = True
        self.cb(self.get_sel())
        self.close()

    def __del__(self):
        self.rem_win()

    def make_win(self):
        if self.win is None:
            self.win = AnnotationFloatingWindow(self.editor.wm)

    def rem_win(self):
        if self.win is not None:
            self.win.rem()
            self.win = None

    def show_opts(self, opts: Sequence[str], sel: Optional[int]):
        self.make_win()

        fmts = []
        for i, o in enumerate(opts[self.scr:self.scr+self.maxlen], self.scr):
            fmts.append(("{curmenu-sel}" if i == sel else "{curmenu}") + o)

        chrvecs = [ChrVec.from_fmt(f) for f in fmts]
        cur = self.editor.selbuf.get_cur()

        winrect = self.editor.wm.get_dims(self.editor.selbuf.get_win())
        rect = Rect(
            x=winrect.x+cur.pri.col,
            y=winrect.y+cur.pri.row,
            w=max(len(c) for c in chrvecs),
            h=len(chrvecs)
        )

        self.win.resize(rect)
        self.win.fill(
            Rect(x=0, y=0, w=rect.w, h=rect.h),
            Chr.new(" ", font_by_name("curmenu")
        ))

        if sel is not None:
            self.win.fill(
                Rect(x=0, y=sel - self.scr, w=rect.w, h=1),
                Chr.new(" ", font_by_name("curmenu-sel")
            ))

        for i, chrvec in enumerate(chrvecs):
            self.win.set_rect_ptr(
                Rect(x=0, y=i, w=len(chrvec), h=1),
                chrvec[0]
            )
