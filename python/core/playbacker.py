import libtypes
from lib import lib
from .structure import Structure

class Playbacker(Structure):
    typ = libtypes.playbacker_s
    init_funct = lib.playbacker_init
    kill_funct = lib.playbacker_kill

    def init_args(self, inp, pool):
        return [pool.ptr]

    def send(self, chrs):
        if isinstance(chrs, str):
            chrs = chrs.encode("utf-8")

        lib.playbacker_send(self.ptr, chrs, len(chrs))
