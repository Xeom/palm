from .newstruct import SimpleCStructure, CStructure
from lib import lib
from util.encode import bytify, strify
import ctypes
from server.globals import add_global

@add_global()
class Key(SimpleCStructure):
    class_name = "key_s"
    repr_multiline = False

    def __eq__(self, oth):
        a = self.copy()
        b = oth.copy()

        a.canonicalise()
        b.canonicalise()

        return SimpleCStructure.__eq__(a, b)

    @classmethod
    def char(cls, char, attrs=0):
        return cls("CHAR", attrs, ord(char))

    def canonicalise(self):
        lib.key_canonicalise(self.ptr)

    def get_name(self):
        buf = ctypes.c_char * 256
        res = lib.key_get_name(self.ptr, buf, ctypes.sizeof(buf))

        if res != 0:
            raise Exception("Could not get key name.")

        return buf.value

    def to_string(self):
        string = Vec(ctypes.c_char)
        lib.key_to_string(self.ptr, string.ptr)

        return strify(bytes(string))

@add_global()
class KeyParser(CStructure):
    class_name = "key_parser_s"
    init_funct = lib.key_parser_init
    kill_funct = lib.key_parser_kill

    def init_args(args, kwargs):
        def inner(editor):
            return [editor.ptr]

        return inner(*args, **kwargs)


def start():
    lib.key_start()
    add_default_keys()

def end():
    lib.key_end()

def add_mapping(symbol: bytes, key: Key):
    if lib.key_add_mapping(key.ptr, symbol, len(symbol)) == -1:
        raise Exception("Could not add key parser mapping")

def add_duplicate(alias: Key, canonical: Key):
    lib.key_add_duplicate(alias.ptr, canonical.ptr)

def add_default_keys():
    for symbol, keyargs in defaultmappings:
        add_mapping(symbol, Key(*keyargs))

defaultmappings = [
    (b"\x1b",       ["ESC"]),

    (b"\n",         ["ENTER"]),
    (b"\r",         ["ENTER"]),

    (b"\x7f",       ["BACKSP"]),

    (b"\x1b[200~",  ["SPASTE"]),
    (b"\x1b[201~",  ["EPASTE"]),

    (b"\t",         ["TAB"]),

    (b"\x1b[Z",     ["TAB",    "SHIFT"]),
    (b"\x08",       ["BACKSP", "SHIFT"]),

    (b"\x1b\t",     ["TAB",    "MOD"])
]

modcodes = [
    (2, ["SHIFT"]),
    (3, ["MOD"]),
    (3, ["SHIFT", "MOD"]),
    (5, ["CTRL"]),
    (6, ["CTRL", "SHIFT"]),
    (7, ["CTRL", "MOD"]),
    (8, ["SHIFT", "MOD", "CTRL"])
]

keyletters = [
    ("A", ["UP"]),
    ("B", ["DOWN"]),
    ("C", ["RIGHT"]),
    ("D", ["LEFT"]),

    ("H", ["HOME"]),
    ("F", ["END"]),

    ("P", ["FUNCT", 0, 1]),
    ("Q", ["FUNCT", 0, 2]),
    ("R", ["FUNCT", 0, 3]),
    ("S", ["FUNCT", 0, 4]),

    ("M", ["ENTER"]),

    ("j", ["NUMPAD", 0, ord("*")]),
    ("k", ["NUMPAD", 0, ord("+")]),
    ("l", ["NUMPAD", 0, ord(",")]),
    ("m", ["NUMPAD", 0, ord("-")]),
    ("n", ["NUMPAD", 0, ord(".")]),
    ("o", ["NUMPAD", 0, ord("/")]),

    ("p", ["NUMPAD", 0, ord("0")]),
    ("q", ["NUMPAD", 0, ord("1")]),
    ("r", ["NUMPAD", 0, ord("2")]),
    ("s", ["NUMPAD", 0, ord("3")]),
    ("t", ["NUMPAD", 0, ord("4")]),
    ("u", ["NUMPAD", 0, ord("5")]),
    ("v", ["NUMPAD", 0, ord("6")]),
    ("w", ["NUMPAD", 0, ord("7")]),
    ("x", ["NUMPAD", 0, ord("8")]),
    ("y", ["NUMPAD", 0, ord("9")]),
]

keycodes = [
    (1, ["HOME"]),
    (2, ["INS"]),
    (3, ["DEL"]),
    (4, ["END"]),
    (5, ["PGUP"]),
    (6, ["PGDN"]),

    (11, ["FUNCT", 0, 1]),
    (12, ["FUNCT", 0, 2]),
    (13, ["FUNCT", 0, 3]),
    (14, ["FUNCT", 0, 4]),
    (15, ["FUNCT", 0, 5]),
    (17, ["FUNCT", 0, 6]),
    (18, ["FUNCT", 0, 7]),
    (19, ["FUNCT", 0, 8]),
    (20, ["FUNCT", 0, 9]),
    (21, ["FUNCT", 0, 10]),
    (23, ["FUNCT", 0, 11]),
    (24, ["FUNCT", 0, 12])
]

for c in range(0x00, 0x20):
    defaultmappings += [
        (bytify(chr(c)),          ["CHAR", "CTRL", c + ord("@")]),
        (bytify(f"\x1b{chr(c)}"), ["CHAR", ["CTRL", "MOD"], c + ord("@")])
    ]

for c in range(0x20, 0x7f):
    defaultmappings += [
        (bytify(chr(c)), ["CHAR", 0, c]),
    ]

for c in range(0x80, 0x100):
    defaultmappings += [
        (bytify(chr(c)), ["CHAR", 0, c])
    ]

for letter, key in keyletters:
    defaultmappings += [
        (bytify(f"\x1b[{letter}"), key),
        (bytify(f"\x1bO{letter}"), key)
    ]

    for modcode, mods in modcodes:
        if len(key) == 3:
            nkey = [key[0], mods, key[2]]
        else:
            nkey = [key[0], mods]

        defaultmappings += [
            (bytify(f"\x1b[1;{modcode}{letter}"), nkey)
        ]

for keycode, key in keycodes:
    defaultmappings += [
        (bytify(f"\x1b[{keycode}~"), key)
    ]

    if len(key) == 1:
        key = [key[0], 0]

    for modcode, mods in modcodes:
        if len(key) == 3:
            nkey = [key[0], mods, key[2]]
        else:
            nkey = [key[0], mods]

        defaultmappings += [
            (bytify(f"\x1b[{keycode};{modcode}~"), nkey)
        ]


