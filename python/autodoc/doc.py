class Documentation(object):
    def __init__(self):
        self.content: str

class CodeDocumentation(Documentation):
    def __init__(self, typ: str, fname: str, line: int, name: str, docs: str):
        self.fname   = fname
        self.line    = line
        self.name    = name
        self.docs    = docs
        self.typ     = typ
