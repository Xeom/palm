#if !defined(BUF_SEL_H)
# define BUF_SEL_H
# include <pthread.h>
# include "types.h"

struct buf_selector
{
    pthread_mutex_t mtx;
    buf_s        *buf;
    event_ctx_s  *eventctx;
    key_parser_s *keyparser;
};

int buf_selector_init(buf_selector_s *sel, editor_s *editor);

int buf_selector_kill(buf_selector_s *sel);

int buf_select(buf_selector_s *sel, buf_s *buf);

buf_s *buf_get_selected(buf_selector_s *sel);

int buf_get_selected_win(buf_selector_s *sel);

#endif
