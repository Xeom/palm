#if !defined(UTIL_STRING_H)
# define UTIL_STRING_H
# include <stdbool.h>

char *safe_strncpy(char *dst, const char *src, size_t n);

char *zeroed_safe_strncpy(char *dst, const char *src, size_t n);

int str_to_uint(const char *str, unsigned long *dst, size_t *nchrs);

int str_to_int(const char *str, long *dst, size_t *nchrs);

bool str_startswith(const char *str, const char *start);

bool str_endswith(const char *str, const char *end);

#endif
