import sys

from core.buffer   import Buffer
from editor        import log
from core.cur      import Cur
from core.bind     import Bind
from server.server import add_global

from typing import List, Sequence, Optional, Iterator
from core.font   import Font

class MenuItem(object):
    """An item in a MenuBuffer that can be selected with the cursor.

    MenuItems usually are formatted into trees, where parent  items
    can be opened to expose subitems.

    [-] Item
    |-[-] Item
    | |-> Item
    | '-> Item
    '-> Item
    """

    def __init__(self, buf: "MenuBuffer", content: str="", parent=None, prev=None):
        """Initialize an instance of this class.

        Arguments:
            buf     - The buffer where this MenuItem is.
            content - The text to display in the MenuItem.
            parent  - The parent MenuItem, or None if this item has no parent.
            prev    - The item with the same parent preceding this item, or None
                      if it is the first item among its siblings.
        """
        self.siblinglncache: Optional[int] = None
        """
        A cache for remembering how many lines the preceding
        siblings to this tiem take up.

        The line number of this item can be calculated as the
        line number of its parent, plus 1, plus the siblingln.
        """

        self.buf:    MenuBuffer         = buf
        self.parent: Optional[MenuItem] = parent
        self.prev:   Optional[MenuItem] = prev
        self.subs:   List[MenuItem]     = list()

        self.generated: bool = False

        self.expanded:   bool = False
        self.expandable: bool = False

        self.content: str = content

    def clear_sibling_ln_caches(self):
        """Clear the siblinglncaches of the siblings after this one.

        This must be called, if, for example, this item is opened
        or closed and its length changes.
        """
        if self.parent is not None:
            siblings = self.parent.get_subs()
        else:
            siblings = self.buf.items

        myind = siblings.index(self)
        for s in siblings[myind:]:
            s.siblinglncache = None

    def iter_subs(self) -> Iterator["MenuItem"]:
        """Iterate across this item and all of its subitems, recursively.

        The iteration order is the order that they are displayed.
        """
        yield self
        for sub in self.get_subs():
            yield from sub.iter_subs()

    def set_content(self, content: str):
        """Set the content of this item.

        This content is encoded with ["UTF8", "FNT"], and so you can
        put fonts in it e.g. '{title}This is a title font.', while
        '{{' and '}}' provide literal brackets.
        """
        self.content = content

    def add_sub(self, content: str=""):
        """Append a subitem to this item."""
        prev: Optional[MenuItem]
        if len(self.get_subs()):
            prev = self.get_subs()[-1]
        else:
            prev = None

        res = type(self)(self.buf, content, parent=self, prev=prev)

        self.subs.append(res)
        self.clear_sibling_ln_caches()

        return res

    def __len__(self) -> int:
        """Return how many lines are taken up by this item and its children."""
        return 1 + sum(len(s) for s in self.get_subs())

    def get_linenum(self) -> int:
        """Get the line number of this item.

        This is the line number of the buffer where this item appears, if it
        is displayed.
        """
        item = self.prev
        ln   = 0

        if self.siblinglncache is None:
            while item is not None:
                ln += len(item)

                if item.siblinglncache is not None:
                    ln += item.siblinglncache
                    break

                item = item.prev

            self.siblinglncache = ln
        else:
            ln = self.siblinglncache

        if self.parent is not None:
            ln += self.parent.get_linenum() + 1
        else:
            ln += self.buf.head_len()

        return ln

    def set_subs(self, subs: Sequence["MenuItem"]):
        """Set the immediate subitems of this item."""
        self.subs = list(subs)

        prev = None
        for s in subs:
            s.parent = self
            s.buf    = self.buf
            s.prev   = prev
            prev     = s

        self.expandable = True
        self.clear_sibling_ln_caches()

    def gen_subs(self) -> Sequence["MenuItem"]:
        """Generate the subitems of this item if they do not exist.

        This function can be implemented in subclasses, and used as
        an alternative to presetting the items with set_subs. The
        subitems are only generated as needed.
        """
        raise NotImplementedError

    def get_subs(self) -> Sequence["MenuItem"]:
        """Get the immediate subitems of this item."""
        if self.expanded and self.expandable:
            if not self.generated:
                self.set_subs(self.gen_subs())
                self.generated = True

            return self.subs

        else:
            return []

    def draw(self):
        """Draw this item in its buffer."""
        ln = self.get_linenum()
        line = self.get_line()
        self.buf.set(ln, line, attrs=["UTF8", "FNT"])

    def draw_all(self):
        """Draw this item and all its children recursively."""
        self.draw()
        for s in self.get_subs():
            s.draw()

    def enter(self):
        """Handle a keypress of Keys.enter."""
        self.toggle()

    def keypress(self, k: bytes):
        """Handle a keypress.

        Arguments:
            k - The bytes of the keypress.
        """
        pass

    def open(self):
        """Open this item to display its subitems."""
        if self.expanded or not self.expandable:
            return

        self.expanded = True
        self.buf.ins(self.get_linenum() + 1, len(self) - 1)
        self.clear_sibling_ln_caches()

        self.draw_all()

    def close(self):
        """Close this item to hide its subitems."""
        if not self.expanded or not self.expandable:
            return

        self.buf.rem(self.get_linenum() + 1, len(self) - 1)
        self.expanded = False
        self.clear_sibling_ln_caches()

        self.draw_all()

    def toggle(self):
        """Toggle this item between open and closed."""
        if self.expanded:
            self.close()
        else:
            self.open()

    def get_prefix(self) -> str:
        """Get the part of this item that is displayed before the content."""
        rtn = ""

        if self.parent is not None:
            rtn += self.parent.get_sub_prefix()

            if self == self.parent.get_subs()[-1]:
                rtn += "'-"
            else:
                rtn += "|-"

        if self.expandable:
            if self.expanded:
                rtn += "{menu-bra}[{menu-minus}-{menu-bra}]"
            else:
                rtn += "{menu-bra}[{menu-plus}+{menu-bra}]"
        else:
            rtn += ">"

        return rtn

    def get_sub_prefix(self):
        """Get the prefix needed for a subitem of this item."""
        rtn = ""

        if self.parent != None:
            rtn += self.parent.get_sub_prefix()
            if self == self.parent.get_subs()[-1]:
                rtn += "  "
            else:
                rtn += "| "

        return rtn

    def get_line(self):
        """Get the contents of this menu item's line."""
        return "{{menu-tree}}{0}{{menu-item}} {1}".format(self.get_prefix(), self.content)

class MenuBuffer(Buffer):
    """A buffer for containing a menu.

    This buffer contains a sequence of MenuItems, and displays both them,
    and also a head which can contain some fun info.
    """

    def __init__(self, *args, **kwargs):
        """Initialize an instance of this class.

        Arguments are inherited from Buffer.
        """
        super().__init__(*args, **kwargs)

        self.items: List[MenuItem] = []
        self.head:  List[str]      = []

        self.set_cur(Cur.at(0, typ="LINE"))

    def set_head(self, head: Sequence[str]):
        """Set the contents of the head of the buffer.

        The head is a series of lines displayed at the top of the
        buffer.

        Arguments:
            head - A sequence of ["UTF8", "FNT"] encoded strings.
        """
        self.rem(0, len(self.head))
        self.head = list(head)
        self.ins(0, self.head, attrs=["UTF8", "FNT"])
        self.set_cur(Cur.at(self.head_len(), typ="LINE"))

    def set_items(self, items: Sequence[MenuItem]):
        """Set the items in this buffer.

        Arguments:
            items - A sequence of items to be in this buffer.
        """
        self.items = list(items)
        self.rem(self.head_len(), len(self) - self.head_len())

        prev = None
        for item in items:
            item.prev = prev
            prev      = item

        self.draw_all()

    def append_item(self, item: MenuItem):
        """Append an item to the end of this buffer."""
        prev: Optional[MenuItem]
        if len(self.items):
            prev = self.items[-1]
        else:
            prev = None

        item.prev = prev
        self.items.append(item)
        item.draw()

    def head_len(self) -> int:
        """Return the length of the head of this buffer."""
        return len(self.head)

    def draw_all(self):
        """Draw all the items in this buffer."""
        for i in self.items:
            i.draw_all()

    def iter_items(self):
        """Iterate across all the items in this buffer recursively."""
        for i in self.items:
            yield from i.iter_subs()

    def get_item_on_line(self, find: int):
        """Get the item on a certain line number.

        Arguments:
            find - The line number to look up.
        """
        ln = self.head_len()
        for i in self.iter_items():
            if ln == find:
                return i
            ln += 1

        return None

    def get_selected(self) -> MenuItem:
        """Return the currently selected item."""
        return self.get_item_on_line(self.get_cur().pri.row)

    def bind_all(self):
        """Make buffer specific bindings."""
        self.bind.bind(Bind(Keys.enter, pybind="buf.enter()"))
        self.bind.bind(Bind(None,       pybind="buf.keypress(%k)"))

    def enter(self):
        """Handle a Key.enter."""
        sel = self.get_selected()
        if sel is not None:
            sel.enter()

    def keypress(self, key: bytes):
        """Handle a keypress.

        Arguments:
            key - The bytes that were pressed.
        """
        sel = self.get_selected()
        if sel is not None:
            sel.keypress(key)

