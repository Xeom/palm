include conf.mk

ifndef DEBUG
  DEBUG=yes
endif

ifndef VERBOSE
  VERBOSE=no
endif

ifndef NCC
  NCC=no
endif

ifeq ($(DEBUG),yes)
  CFLAGS+=-g
  VERSION+=-debug
else
  WARNINGS+=error
  CFLAGS+=-O2
endif

ifeq ($(VERBOSE),yes)
  AT=
else
  AT=@
endif

ifeq ($(NCC),yes)
  CC=nccgen
  CFLAGS=-ncld -ncfabs -ncgcc
endif

DFLAGS=$(addprefix -D, $(DEFINES)) \
       -DCOMPILETIME='$(shell date +"%Y-%m-%d %H:%M %z")' \
       -DVERSION='$(VERSION)'

WFLAGS=$(addprefix -W, $(WARNINGS))

CFLAGS+=$(WFLAGS) --std=$(STD) -pedantic
CFLAGS+=-pthread -I$(INCDIR) -fPIC -fdiagnostics-color=always
CFLAGS+=$(DFLAGS)

LFLAGS+=$(addprefix -l, $(LINKS))

HFILES=$(addprefix $(INCDIR), $(addsuffix .h, $(FILES) $(HEADERS)))
CFILES=$(addprefix $(SRCDIR), $(addsuffix .c, $(FILES)))
OFILES=$(addprefix $(OBJDIR), $(addsuffix .o, $(FILES)))

TXTFILES=$(addprefix $(HELPDIR), $(addsuffix .txt, $(HELPFILES)))

ifeq ($(DEBUG), yes)
  ERRPIPE=2>&1 | tee -a errs.txt || (less -R errs.txt && /bin/false)
else
  ERRPIPE=
endif

DEPMATCH=^\S+\.o: $(SRCDIR)(\S+)\.c
DEPREPL=$(OBJDIR)\1\.o: $(SRCDIR)\1\.c
DEPCMD=sed -re "s/$(subst /,\\/,$(DEPMATCH))/$(subst /,\\/,$(DEPREPL))/"

$(DEPFILE): $(CFILES) $(HFILES)
	$(AT)$(CC) -MM $(CFLAGS) $^ | $(DEPCMD) > $@
	$(AT)printf "Built dependencies\n"

$(OBJDIR)%.o: $(SRCDIR)%.c conf.mk
	$(AT)mkdir -p $(@D)
	$(AT)$(CC) -fPIC -c $(CFLAGS) $< -o $@ $(ERRPIPE)
	$(AT)printf "Built $@\n"

$(BINDIR)palm.so: $(OFILES)
	$(AT)mkdir -p $(@D)
	$(AT)$(CC) $(CFLAGS) -shared $^ -o $@ $(LFLAGS) $(ERRPIPE)
	$(AT)printf "Built $@\n"

$(BINDIR)test: $(OFILES) $(TESTDIR)*.c
	$(AT)mkdir -p $(@D)
	$(AT)$(CC) $(CFLAGS) -I snow/snow -D SNOW_ENABLED $^ -o $@ $(LFLAGS) $(ERRPIPE)
	$(AT)printf "Built $@\n"

$(DOCDIR)cmdlist.md:
	$(AT)./etc/cmd-doc.py > $@

$(HELPDIR)%.txt: $(DOCDIR)%.md
	$(AT)bash -c './etc/md-conv.py <(markdown $<) > $@'

all: deps $(BINDIR)palm.so $(BINDIR)test
	$(AT)if [ -s errs.txt ]; then cat errs.txt | less -R; rm errs.txt; fi

ctest: $(BINDIR)test
	$(AT)printf "\033[1m ===== RUNNING C TESTS =====\033[0m\n"
	$(AT)$(BINDIR)test -q

pytest: $(BINDIR)palm.so
	$(AT)printf "\033[1m ===== RUNNING PYTHON TESTS =====\033[0m\n"
	$(AT)./test/py/main.py

test: ctest pytest $(BINDIR)test
	$(AT)printf "\033[1m ===== TESTS RUN =====\033[0m\n"

clean_err:
	$(AT)rm -f errs.txt

clean_bin:
	$(AT)rm -rf $(BINDIR)
	$(AT)printf "Removed $(BINDIR)\n"

clean_obj:
	$(AT)rm -rf $(OBJDIR)
	$(AT)printf "Removed $(OBJDIR)\n"

clean_dep:
	$(AT)rm -rf $(DEPDIR)
	$(AT)printf "Removed $(DEPDIR)\n"

helpdocs: $(TXTFILES)

deps: $(DEPFILE)
clean: clean_err clean_bin clean_obj clean_dep

.PHONEY=deps all clean_err clean_bin clean_obj clean_dep clean install uninstall test ctest pytest helpdocs
.DEFAULT_GOAL=all

$(shell rm -f errs.txt)

ifeq (,$(findstring clean,$(MAKECMDGOALS)))
  ifeq (,$(findstring deps,$(MAKECMDGOALS)))
    $(info Including depenendencies ...)
    include $(DEPFILE)
  endif
endif
