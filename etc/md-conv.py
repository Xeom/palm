#!/usr/bin/python3
import random
import math
from html.parser import HTMLParser

class Word(object):
    def __init__(self, word, col="default", space=True):
        self.word = word
        self.col  = col
        self.space = space

    def __len__(self):
        return len(self.word) + int(self.space)

    def print(self, prevcol="default"):
        res = " " if self.space else ""
        if prevcol != self.col:
            res += "{"+self.col+"}"

        res += self.word.replace("{", "{{").replace("}", "}}")

        return res

class Line(object):
    def __init__(self, margin=None, maxlen=60):
        if margin == None:
            margin = list()

        self.maxlen = maxlen
        self.margin = margin
        self.words  = list()

    def __len__(self):
        return sum(len(w) for w in self.words)  + \
               sum(len(w) for w in self.margin)

    def add_word(self, word):
        if len(self.words) == 0:
            word.space = False

        if len(self.words) > 0 and \
           len(word) + len(self) > self.maxlen:
            return False

        else:
            self.words.append(word)
            return True

    def print(self, stretch=False):
        col = "default"
        res = ""

        for word in self.margin:
            res += word.print(prevcol=col)
            col  = word.col

        numspaces = sum(1 for w in self.words if w.space)
        if stretch and numspaces > 0:
            extraspaces = max(0, self.maxlen - len(self))
            extraperword = extraspaces / numspaces
        else:
            extraperword = 0.0
            extraspaces  = 0

        for word in self.words:
            if word.space:
                prev = extraspaces
                extraspaces -= extraperword

                res += " " * (round(prev) - round(extraspaces))

            res += word.print(prevcol=col)
            col  = word.col

        return res

class Block(object):
    def __init__(self, tag, parent=None, margin=None, firstmargin=None, stretchable=True):
        self.tag = tag
        self.stretchable = stretchable

        if margin != None:
            self.margin = margin[:]
        else:
            self.margin = []

        if firstmargin != None:
            self.firstmargin = firstmargin
        else:
            self.firstmargin = self.margin[:]

        self.parent = parent
        self.lines  = []

    def get_margin(self):
        if self.parent != None:
            res = self.parent.get_margin()
        else:
            res = []
        res += self.firstmargin
        self.firstmargin = self.margin
        return res

    def add_word(self, word):
        if len(self.lines) == 0 or not self.lines[-1].add_word(word):
            self.lbreak()
            self.lines[-1].add_word(word)

    def print(self):
        res = []
        for ind, line in enumerate(self.lines):
            final = (ind == len(self.lines) - 1)
            stretch = not final if self.stretchable else False
            res.append(line.print(stretch=stretch))

        return "\n".join(res)

    def lbreak(self):
        l = Line(self.get_margin())
        self.lines.append(l)

class MDParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.res    = []
        self.block  = Block("global")
        self.tags   = []
        self.number = 0

    def get_font(self):
        fonts = {
            "p":    "default",
            "h1":   "title",
            "h2":   "subtitle",
            "h3":   "subsubtitle",
            "h4":   "subsubsubtitle",
            "h5":   "subsubsubsubtitle",
            "h6":   "subsubsubsubtitle",
            "b":    "emph",
            "strong":"emph",
            "i":    "faded",
            "u":    "underline",
            "code": "md-code-blk",
            "li":   "default"
        }

        for tag in self.tags[::-1]:
            if tag in fonts:
                return fonts[tag]

        return "default"

    def descend_block(self, new):
        self.res.append(self.block.print())
        self.block.lines = []
        self.block = new

    def handle_starttag(self, tag, attrs):
        self.tags.append(tag)
        if tag == "p":
            self.descend_block(Block(
                tag,
                self.block,
                margin=[Word(" ")]
            ))
        elif tag in ["h{}".format(n) for n in range(1, 7)]:
            self.descend_block(Block(
                tag,
                self.block
            ))
        elif tag == "li":
            self.descend_block(Block(
                tag,
                self.block,
                firstmargin=[Word(" *", col="md-bullet")],
                margin=[Word("   ")]
            ))

    def handle_endtag(self, tag):
        if self.tags[-1] != tag:
            raise Exception("Unmatched tag: {}, expected {}".format(tag, self.tags[-1]))

        self.tags.pop(-1)

        if self.block.tag == tag:
            self.res.append(self.block.print())
            self.block = self.block.parent

    def handle_data(self, data):
        if len(self.tags) and self.tags[-1] in ("code", "pre"):
            if "\n" in data:
                self.descend_block(Block(
                    self.tags[-1],
                    self.block,
                    margin=[Word(" : ", col="md-section")],
                    stretchable=False
                ))
                for ind, line in enumerate(data.split("\n")[1:-1]):
                    if ind:
                        self.block.lbreak()
                    self.block.add_word(
                        Word(line, self.get_font())
                    )
            else:
                self.block.add_word(
                    Word(data, "md-code-inline")
                )
        else:
            for ind, word in enumerate(data.split()):
                if not word.strip():
                    continue
                if ind == 0 and not (data.startswith(" ") or data.startswith("\n")):
                    space = False
                else:
                    space = True
                self.block.add_word(
                    Word(word, self.get_font(), space=space)
                )

parser = MDParser()

import sys

parser.feed(open(sys.argv[1]).read())
print("\n\n".join(s for s in parser.res if s))
