#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include "log/log.h"
#include "log/notify.h"
#include "io/io.h"
#include "util/time.h"
#include "util/string.h"

log_s default_log;

static void log_str_file(
    log_s *log, log_lvl_t lvl, log_type_t typ, const char *fname, int ln, const char *str, log_file_s *file);

void log_init(log_s *log)
{
    log_typ_conf_s conf[LOG_LVL_CNT][LOG_TYP_CNT] = {
        [LOG_LVL_DEBUG] = {
            [LOG_TYP_SUCC] = { "DBGs", "log-dbg-pri", "log-dbg-sec", "log-succ-bright", 0, '\0', 0 },
            [LOG_TYP_ERR]  = { "DBGe", "log-dbg-pri", "log-dbg-sec", "log-err-bright",  0, '\0', 0 },
            [LOG_TYP_INFO] = { "DBGi",  "log-dbg-pri", "log-dbg-sec", "log-info-bright", 0, '\0', 0 }
        },
        [LOG_LVL_INFO] = {
            [LOG_TYP_SUCC] = { "OK",   "log-succ-dull", "log-succ-dull", "log-succ-bright", 1, 'i', 0 },
            [LOG_TYP_ERR]  = { "WARN", "log-err-dull",  "log-err-dull",  "log-err-bright",  1, 'i', 0 },
            [LOG_TYP_INFO] = { "INFO", "log-info-dull", "log-info-dull", "log-info-bright", 1, 'i', 0 }
        },
        [LOG_LVL_NOTICE] = {
            [LOG_TYP_SUCC] = { "SUCC",   "log-succ", "log-succ-dull", "log-succ-bright", 1, 'N', 0 },
            [LOG_TYP_ERR]  = { "ERR",    "log-err",  "log-err-dull",  "log-err-bright",  1, 'N', 0 },
            [LOG_TYP_INFO] = { "MESG", "log-info", "log-info-dull", "log-info-bright", 1, 'N', 0 }
        },
        [LOG_LVL_ALERT] = {
            [LOG_TYP_SUCC] = { "YAY",   "log-succ-bright", "log-succ", "log-succ-bright", 1, '!', 1 },
            [LOG_TYP_ERR]  = { "CRIT",  "log-err-bright",  "log-err",  "log-err-bright",  1, '!', 1 },
            [LOG_TYP_INFO] = { "ALRT", "log-info-bright", "log-info", "log-info-bright", 1, '!', 1 }
        }
    };

    log->notifyid = 0;
    memcpy(&(log->conf), conf, sizeof(log->conf));
    vec_init(&(log->files), sizeof(log_file_s));
    vec_init(&(log->notifications), sizeof(notification_s));
    pthread_mutex_init(&(log->mtx), NULL);
}

void log_kill(log_s *log)
{
    vec_kill(&(log->files));
    vec_kill(&(log->notifications));
    pthread_mutex_destroy(&(log->mtx));
}

void log_to_file(log_s *log, int fd, bool fonts, const char *ts)
{
    int lvl, typ;

    log_file_s file;

    pthread_mutex_lock(&(log->mtx));

    io_set_nonblk(fd, NULL);
    file.fd    = fd;
    file.fonts = fonts;

    memset(file.tsfmt, 0, sizeof(file.tsfmt));
    if (ts)
    {
        if (!safe_strncpy(file.tsfmt, ts, sizeof(file.tsfmt) - 1))
            return;
    }

    for (lvl = 0; lvl < LOG_LVL_CNT; ++lvl)
    {
        for (typ = 0; typ < LOG_TYP_CNT; ++typ)
        {
            file.enabled[lvl][typ] = log->conf[lvl][typ].logdefault;
        }
    }

    vec_app(&(log->files), &file);

    pthread_mutex_unlock(&(log->mtx));
}

int log_forget_file(log_s *log, int fd)
{
    int rtn;
    pthread_mutex_lock(&(log->mtx));

    rtn = -1;
    VEC_FOREACH(&(log->files), log_file_s *, file)
    {
        if (file->fd == fd)
        {
            vec_del(&(log->files), _ind, 1);
            rtn = 0;
            break;
        }
    }

    pthread_mutex_unlock(&(log->mtx));

    return rtn;
}

void log_str(log_s *log, log_lvl_t lvl, log_type_t typ, const char *fname, int ln, const char *str)
{
    vec_s line;
    vec_init(&line, sizeof(char));
    pthread_mutex_lock(&(log->mtx));
    do
    {
        if (*str == '\0' || *str == '\n')
        {
            vec_app(&line, NULL);
            if (strlen(vec_get(&line, 0)))
            {
                log_notify(log, lvl, typ, vec_get(&line, 0));

                VEC_FOREACH(&(log->files), log_file_s *, file)
                {
                    log_str_file(log, lvl, typ, fname, ln, vec_get(&line, 0), file);
                }
                vec_clr(&line);
            }
        }
        else
        {
            vec_app(&line, str);
        }
    } while (*(str++));
    pthread_mutex_unlock(&(log->mtx));

    vec_kill(&line);
}

static void log_str_file(
    log_s *log, log_lvl_t lvl, log_type_t typ, const char *fname, int ln, const char *str, log_file_s *file)
{
    vec_s           cont;
    log_typ_conf_s *conf;
    int             indent;
    char            c;
    char            fbasename[PATH_MAX];

    io_basename(fbasename, fname, NULL);

    conf = &(log->conf[lvl][typ]);

    if (!file->enabled[lvl][typ])
        return;

    vec_init(&cont, sizeof(char));

    if (strlen(file->tsfmt))
    {
        char ts[64];
        if (util_strfnow(ts, sizeof(ts), file->tsfmt) == 0)
            vec_str(&cont, ts);
    }

    indent = 4 - strlen(conf->name);
    while (indent-- > 0)
        vec_str(&cont, " ");


    if (file->fonts)
    {
        vec_fmt(
            &cont, "{%s}[{%s}%s{%s}]{default}",
            conf->secfont, conf->prifont, conf->name, conf->secfont
        );
    }
    else
    {
        vec_fmt(&cont, "[%s]", conf->name);
    }

    vec_fmt(&cont, "%11s %3d ", fbasename, ln);

    indent = strlen(conf->name) + 3;

    while (*str)
    {
        int  i;
        c = *(str++);
        switch (c)
        {
        case '}':
            if (file->fonts)
                vec_str(&cont, "}}");
            else
                vec_str(&cont, "}");
            break;

        case '{':
            if (file->fonts)
                vec_str(&cont, "{{");
            else
                vec_str(&cont, "{");
            break;

        case '\n':
            if (*str && *str != '\n')
            {
                vec_str(&cont, "\n");
                for (i = 0; i < indent; ++i)
                    vec_str(&cont, " ");
            }
            break;

        default:
            vec_app(&cont, &c);
            break;
        }
    }

    if (vec_len(&cont) == 0 || *(char *)vec_end(&cont) != '\n')
        vec_str(&cont, "\n");

    io_write_all_from_vec(file->fd, &cont, NULL);

    vec_kill(&cont);
}

void log_fmt(log_s *log, log_lvl_t lvl, log_type_t typ, const char *fname, int ln, const char *fmt, ...)
{
    vec_s   str;
    va_list args;
    char    c;
    int     n;

    vec_init(&str, sizeof(char));

    va_start(args, fmt);
    n = vsnprintf(&c, 1, fmt, args);
    vec_add(&str, n + 1, NULL);
    va_end(args);

    va_start(args, fmt);
    vsnprintf(vec_get(&str, 0), n + 1, fmt, args);
    va_end(args);

    log_str(log, lvl, typ, fname, ln, vec_get(&str, 0));

    vec_kill(&str);
}
