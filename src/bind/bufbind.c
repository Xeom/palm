#include <pthread.h>
#include "bind/bufbind.h"
#include "util/macro.h"
#include "log/log.h"

void bufbind_init(bufbind_s *bbind, bind_pycb_t pycb)
{
    pthread_mutex_init(&(bbind->mtx), NULL);
    bbind->mode     = mode_get(NULL, "default");
    vec_init(&(bbind->keys), sizeof(char));
    bbind->pycb = pycb;
}

void bufbind_kill(bufbind_s *bbind)
{
    pthread_mutex_destroy(&(bbind->mtx));
    mode_kill(bbind->mode);
    vec_kill(&(bbind->keys));
}

void bufbind_key(bufbind_s *bbind, key_s *key, buf_s *buf)
{
    bind_s *bind, bindcpy;

    WITH_MTX(&(bbind->mtx))
    {
        bind = mode_key(bbind->mode, key);

        /* Take a copy of the bind. */
        if (bind)
            bind_init_cpy(&bindcpy, bind);
    }

    /* Perform action outside mutex */
    if (bind)
    {
        bind_trigger(&bindcpy, key, buf);
        bind_kill(&bindcpy);
    }
}

void bufbind_bind(bufbind_s *bbind, bind_s *bind)
{
    WITH_MTX(&(bbind->mtx))
    {
        mode_bind(bbind->mode, bind);
    }
}

void bufbind_unbind(bufbind_s *bbind, key_s *key)
{
    WITH_MTX(&(bbind->mtx))
    {
        mode_unbind(bbind->mode, key);
    }
}

void bufbind_set_mode(bufbind_s *bbind, const char *mode)
{
    WITH_MTX(&(bbind->mtx))
    {
        mode_s *prev;

        prev        = bbind->mode;
        bbind->mode = mode_get(bbind->mode, mode);

        bbind->mode->prevmode = prev;
    }
}

void bufbind_get_mode(bufbind_s *bbind, char *name)
{
    WITH_MTX(&(bbind->mtx))
    {
        strcpy(name, bbind->mode->name);
    }
}

void bufbind_prev_mode(bufbind_s *bbind)
{
    WITH_MTX(&(bbind->mtx))
    {
        if (bbind->mode->prevmode)
            bbind->mode = bbind->mode->prevmode;
    }
}
