#!/bin/bash
#
# This script is used to generate the Python C bindings.
#
# When run, it will generate the files functs.py, enums.py, defs.py,
# sysdefs.py, and structs.py, in the python/lib directory. These contain the
# bindings that allow python and C to interface from one another. The bindings
# are generated directly from the C code, so they can remain automatically
# updated!

cd $(dirname $0)

CFLAGS="-I ./../../inc"
HFILES=$(find ../../inc -name '*.h' | sed -e "s/^.\\/..\\/..\\/inc//g")

FLAGS=""
for f in $CFLAGS; do
    FLAGS="$FLAGS --carg '$f'"
done

FLAGS="$FLAGS $HFILES"

tmpfile=$(mktemp)
cat > $tmpfile <<EOF
#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <termios.h>
#include <sys/un.h>

typedef struct sockaddr_un sockaddr_un_s;
typedef struct termios termios_s;
typedef struct sigaction sigaction_s;
typedef FILE FILE_s;
EOF

echo "Generating sysdefs.py ..."
./codegen.py $FLAGS $tmpfile \
    --head 'from .util import *' \
    --head 'from autodoc import CodeDocumentation' \
    --hack \
    --name FILE_s \
    --name pthread_t \
    --name pthread_mutex_t \
    --name time_t \
    --name pthread_cond_t \
    --name termios_s \
    --name sockaddr_un \
    --name sigaction_s \
    --name pid_t > sysdefs.py

rm "$tmpfile"

echo "Generating defs.py ..."
./codegen.py $FLAGS \
    --head 'from .enums      import *' \
    --head 'from .util       import *' \
    --head 'from core.cnames import *' \
    --head 'from autodoc import CodeDocumentation' \
    --name row_t \
    --name col_t \
    --name edit_mode_t \
    --name indent_t \
    --name errmsg_t \
    --name event_cb_t         \
    --name bind_c_t \
    --name bind_pycb_t \
    --name file_wcb_t \
    --name file_rcb_t \
    --name file_cb_t \
    --name io_path_t \
    --name errmsg_t \
    --name sortedvec_cmpcb_t \
    --name sortedvec_startcb_t \
    --name io_errbuf_t > defs.py

echo "Generating structs.py ..."
./codegen.py $FLAGS \
    --head 'from .enums   import *' \
    --head 'from .defs    import *' \
    --head 'from .sysdefs import *' \
    --head 'from .util    import *' \
    --head 'from core.cnames import *' \
    --head 'from autodoc import CodeDocumentation' \
    --name cmd_binding_info_s \
    --name buf_selector_s \
    --name vt100_s \
    --name key_s \
    --name key_parser_s \
    --name cmd_arg_spec_s \
    --name cmd_verb_info_s \
    --name cmd_noun_info_s \
    --name cmd_alias_info_s \
    --name cmd_adverb_info_s \
    --name cmd_s \
    --name help_s \
    --name cmdlang_obj_s   \
    --name cmdlang_parser_s \
    --name cmd_lib_s       \
    --name cmd_ctx_s       \
    --name sortedvec_s     \
    --name cmdlang_lexer_s \
    --name cmdlang_tok_s   \
    --name escape_state_s  \
    --name list_s          \
    --name list_item_s     \
    --name list_range_s    \
    --name curset_s        \
    --name reader_s        \
    --name writer_s        \
    --name poll_ctx_s      \
    --name bind_s          \
    --name buf_s           \
    --name bufbind_s       \
    --name chr_s           \
    --name chrconf_s       \
    --name cur_s           \
    --name decoder_s       \
    --name display_s       \
    --name edit_info_s     \
    --name editor_s        \
    --name encoder_s       \
    --name event_s         \
    --name event_action_s  \
    --name event_ctx_s     \
    --name event_spec_s    \
    --name file_s          \
    --name finfo_s         \
    --name floatwin_s      \
    --name font_s          \
    --name hl_action_s     \
    --name hl_ctx_s        \
    --name hl_state_s      \
    --name hl_tok_s        \
    --name hl_worker_s     \
    --name input_s         \
    --name io_file_info_s  \
    --name io_path_s       \
    --name line_s          \
    --name log_s           \
    --name log_file_s      \
    --name log_typ_conf_s  \
    --name match_s         \
    --name match_result_s  \
    --name match_set_s     \
    --name mode_s          \
    --name notification_s  \
    --name playbacker_s    \
    --name pool_s          \
    --name pool_job_s      \
    --name pool_thread_s   \
    --name pos_s           \
    --name rect_s          \
    --name replace_s       \
    --name search_s        \
    --name search_result_s \
    --name status_format_s \
    --name status_tok_s    \
    --name table_s         \
    --name table_chain_s   \
    --name timer_s         \
    --name timer_ctx_s     \
    --name tty_s           \
    --name tty_conf_s      \
    --name undo_action_s   \
    --name undo_list_s     \
    --name win_s           \
    --name wm_s            \
    --name wm_update_s     \
    --name wm_updater_s    \
    --name framebuf_s      \
    --name mode_s          \
    --name pty_s           \
    --name yaml_obj_s > structs.py

echo "Generating enums.py ..."
./codegen.py $FLAGS \
    --head 'from .util import *' \
    --head 'from autodoc import CodeDocumentation' \
    --name cmdlang_parser_attr_t \
    --name key_parser_attr_t \
    --name vt100_mode_t \
    --name help_typ_t \
    --name cur_dir_t          \
    --name cur_hl_t           \
    --name cmdlang_obj_typ_t  \
    --name cur_type_t         \
    --name decoder_attr_t     \
    --name decoder_chrtype_t  \
    --name edit_attr_t        \
    --name event_attr_t       \
    --name event_typ_t        \
    --name font_attr_t        \
    --name font_colour_t      \
    --name hl_action_type_t   \
    --name hl_tok_type_t      \
    --name io_file_status_t   \
    --name log_lvl_t          \
    --name log_type_t         \
    --name match_attr_t       \
    --name scroll_t           \
    --name status_tok_typ_t   \
    --name timer_attr_t       \
    --name undo_action_type_t \
    --name win_split_t        \
    --name yaml_obj_type_t    \
    --name replace_attr_t     \
    --name escape_state_t     \
    --name cmdlang_lexer_state_t \
    --name cmd_typ_t \
    --name key_button_t \
    --name key_attr_t \
    --name cmdlang_tok_typ_t > enums.py

echo "Generating functs.py ..."
./codegen.py $FLAGS \
    --head 'from .so import lib' \
    --head 'from .util import *' \
    --head 'from .enums import *' \
    --head 'from .defs  import *' \
    --head 'from .sysdefs  import *' \
    --head 'from core.cnames import *' \
    --head 'from autodoc import CodeDocumentation' \
    --functs > functs.py

echo "Done"
