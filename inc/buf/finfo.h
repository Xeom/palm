#if !defined(IO_FINFO_H)
# define IO_FINFO_H
# include <pthread.h>
# include <time.h>
# include "text/file.h"
# include "text/chr.h"
# include "text/decoder.h"
# include "io/io.h"
# include "types.h"

struct finfo
{
    file_s file;
    pthread_mutex_t mtx;
    io_path_s path;
    time_t readtime;
    time_t modtime;
    decoder_attr_t attrs;
    bool associated;
    bool newfile;
};

void finfo_init(finfo_s *finfo, poll_ctx_s *pollctx);

void finfo_kill(finfo_s *finfo);

int finfo_assoc(finfo_s *finfo, const char *path);

void finfo_attrs(finfo_s *finfo, decoder_attr_t *set, decoder_attr_t *get);

int finfo_read(finfo_s *finfo, buf_s *buf, bool force);

int finfo_write(finfo_s *finfo, buf_s *buf, bool force);

void finfo_get_path(finfo_s *finfo, io_path_s *path);

#endif
