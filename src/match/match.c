#include <ctype.h>

#include "match/match.h"

#include "util/escape.h"
#include "log/log.h"

static const uint8_t *match_vec_start(vec_s *v)
{
    static const uint8_t nullchr[] = { 0 };

    if (vec_len(v))
        return vec_get(v, 0);
    else
        return nullchr;
}

int match_init(match_s *match, match_attr_t attrs, const char *text)
{
    memset(match, 0, sizeof(match_s));
    match->attrs = attrs;

    vec_init(&(match->text), sizeof(char));
    if (text)
        vec_str(&(match->text), text);

    if (attrs & MATCH_ESCAPES)
        escape_vec(&(match->text));

    if (attrs & MATCH_REGEX)
    {
        int      errcode;
        size_t   erroff;
        uint32_t regexopts;

        regexopts = PCRE2_UCP | PCRE2_DOTALL | PCRE2_ANCHORED;

        if (attrs & MATCH_VERBOSE)
            regexopts |= PCRE2_EXTENDED;

        match->regex = pcre2_compile(
            match_vec_start(&(match->text)), vec_len(&(match->text)),
            regexopts, &errcode, &erroff, NULL
        );

        if (match->regex == NULL)
        {
            unsigned char errmsg[256];
            pcre2_get_error_message(errcode, errmsg, sizeof(errmsg));

            LOG(ALERT, ERR, "Could not compile regex: %s (%lu)", errmsg, erroff);
            match_kill(match);

            return -1;
        }

        match->matchdata = pcre2_match_data_create_from_pattern(match->regex, NULL);

        if (match->matchdata == NULL)
        {
            LOG(ALERT, ERR, "Could not make match data for regex");
            match_kill(match);

            return -1;
        }
    }

    return 0;
}

void match_kill(match_s *match)
{
    vec_kill(&(match->text));

    if (match->regex)
    {
        pcre2_code_free(match->regex);
        match->regex = NULL;
    }

    if (match->matchdata)
    {
        pcre2_match_data_free(match->matchdata);
        match->matchdata = NULL;
    }
}

int match_check(match_s *match, vec_s *text, size_t off, match_result_s *res)
{
    pcre2_match_data *mdata;

    mdata = match->matchdata;
    if (match->attrs & MATCH_REGEX)
    {
        int nmatch;
        nmatch = pcre2_match(
            match->regex,
            match_vec_start(text), vec_len(text), off,
            0, mdata, NULL
        );

        if (nmatch > 0)
        {
            size_t *offvec;
            offvec = pcre2_get_ovector_pointer(mdata);
            if (match->grp <= 0 || match->grp >= (int)pcre2_get_ovector_count(mdata))
            {
                res->start = offvec[0];
                res->end   = offvec[1];
                res->match = match;
            }
            else
            {
                res->start = offvec[(2 * match->grp)];
                res->end   = offvec[(2 * match->grp) + 1];
                res->match = match;
            }

            return 1;
        }
        else if (nmatch == 0 || nmatch == PCRE2_ERROR_NOMATCH)
        {
            return 0;
        }
        else
        {
            unsigned char errbuf[256];
            pcre2_get_error_message(nmatch, errbuf, sizeof(errbuf));
            LOG(ALERT, ERR, "PCRE Matching error: %s", errbuf);
            return -1;
        }
    }
    else
    {
        VEC_FOREACH(&(match->text), char *, aptr)
        {
            char *bptr;
            bptr = vec_get(text, off + _ind);

            if (!aptr || !bptr)
                return 0;

            if (match->attrs & MATCH_ANYCASE)
            {
                if (toupper(*aptr) != toupper(*bptr))
                    return 0;
            }
            else
            {
                if (*aptr != *bptr)
                    return 0;
            }
        }

        res->start = off;
        res->end   = off + vec_len(&(match->text));
        res->match = match;

        return 1;
    }
}
