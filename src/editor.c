#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "text/font.h"
#include "io/pty.h"
#include "editor.h"
#include "log/log.h"
#include "event/loop.h"
#include "cmd/cmd.h"

#define EDITOR_TERM_TOUT 5

static editor_s *sig_editor = NULL;

static void editor_handle_signal(int signum);
static void editor_resize_cb(editor_s *editor, event_s *event, void *param);

#define DEFAULT_STATUS_LEFT \
    "%(if win-selected)"                            \
        "{win-sel-marker}*{win-horizontal-border} " \
    "%(else)"

#define DEFAULT_STATUS_MID \
    "%(if win-selected)"      \
        "{win-emph}"          \
    "%(else)"                 \
        "{win-faded}"         \
    "%(fi)"                   \
    "%(mode)"                 \
    "{win-horizontal-border}" \
    " %(buf-len)L %(rel) "

#define DEFAULT_STATUS_RIGHT \
    "{win-horizontal-border}"                   \
    "%(if edit-ow)"                             \
        "{win-emph}OW{win-horizontal-border} "  \
    "%(fi)"                                     \
    "%(if edit-noalt)%(else)%(if buf-hasalts)"  \
        "{win-emph}ALTS{win-horizontal-border}" \
    "%(fi)%(fi)"                                \
    "%(if cur-line)"                            \
        "{win-emph}L{win-horizontal-border} "   \
    "%(fi)"                                     \
    "%(if cur-block)"                           \
        "{win-emph}B{win-horizontal-border} "   \
    "%(fi)"                                     \
    "%(if cur-sticky)"                          \
        "%(rel-row)x%(rel-col)"                 \
    "%(else)"                                   \
        "%(pri-row),%(pri-col)"                 \
    "%(fi)"                                     \
    "%(if buf-hasalts)"                         \
        "+%(buf-numalts)"                       \
    "%(fi)"

/* Start up the components of the editor! */
void editor_init(editor_s *editor, int in, int out)
{
    struct sigaction act;

    editor->alive    = true;
    editor->lastterm = 0;
    editor->numterms = 0;
    editor->in       = in;
    editor->out      = out;

    event_ctx_init(&(editor->eventctx), editor);
    poll_ctx_init(&(editor->pollctx), &(editor->eventctx));
    timer_ctx_init(&(editor->timerctx), &(editor->eventctx));

    if (sig_editor == NULL)
        sig_editor = editor;

    pool_init(&(editor->pool), 8);

    tty_init(&(editor->tty), in, out, &(editor->pollctx));
    tty_set_app(&(editor->tty));
    tty_clear_screen(&(editor->tty));

    display_init(&(editor->display), &(editor->tty));
    wm_init(&(editor->wm), &(editor->display));
    hl_ctx_init(&(editor->hlctx));

    buf_selector_init(&(editor->bufselector), editor);

    input_init(&(editor->input), editor);
    playbacker_init(&(editor->playbacker),  &(editor->pool));

    input_add_fd(&(editor->input), in);
    input_add_fd(&(editor->input), editor->playbacker.rdfd);
    input_resume(&(editor->input));

    key_parser_init(&(editor->keyparser), editor);

    cmd_lib_init(&(editor->cmdlib));
    if (cmd_mount_all(&(editor->cmdlib)) == PALM_ERR)
    {
        LOG(ALERT, ERR, "Error while loading command library.");
    }

    status_format_init(&(editor->statusfmt));

    /* The status bar is formatted as a left component, a left-aligned mid *
     * component, and a right component. The left and right are always     *
     * shown, while the mid hidden by notification messages.               */
    status_format_set(
        &(editor->statusfmt),
        DEFAULT_STATUS_LEFT,
        DEFAULT_STATUS_MID,
        DEFAULT_STATUS_RIGHT
    );

    event_listen(
        &(editor->eventctx),
        &(event_spec_s){
            .typ = EVENT_RESIZE, .ptr = NULL
        },
        &(event_action_s){
            .cb    = editor_resize_cb,
            .param = NULL,
            .pri   = -100
        }
    );

    act.sa_handler = editor_handle_signal;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    sigaction(SIGINT,   &act, &(editor->oldintact));
    sigaction(SIGWINCH, &act, &(editor->oldwinact));
}


/* Shut down the components of the editor! */
void editor_kill(editor_s *editor)
{
    LOG(NOTICE, INFO, "Killing editor");

    /* Replace old signal handlers */
    sigaction(SIGINT,   &(editor->oldintact), NULL);
    sigaction(SIGWINCH, &(editor->oldwinact), NULL);
    sig_editor = NULL;

    /* Kill the parts of the editor */
    key_parser_kill(&(editor->keyparser));
    hl_ctx_kill(&(editor->hlctx));
    playbacker_kill(&(editor->playbacker));
    input_kill(&(editor->input));
    wm_kill(&(editor->wm));
    display_kill(&(editor->display));
    tty_kill(&(editor->tty));
    pool_kill(&(editor->pool));
    status_format_kill(&(editor->statusfmt));
    buf_selector_kill(&(editor->bufselector));

    poll_ctx_kill(&(editor->pollctx));
    timer_ctx_kill(&(editor->timerctx));
    event_ctx_kill(&(editor->eventctx));

    LOG(NOTICE, INFO, "Editor killed");
}

void editor_mask_signals(void)
{
    sigset_t sigmask;
    sigemptyset(&sigmask);
    sigaddset(&sigmask, SIGWINCH);
    pthread_sigmask(SIG_BLOCK, &sigmask, NULL);
}

void editor_loop(editor_s *editor)
{
    event_loop(&(editor->eventctx), &(editor->pollctx), &(editor->timerctx));
}

void editor_main(editor_s *editor)
{
    LOG(INFO, INFO, "Entering editor_main()");

    while (editor->alive)
    {
        editor_loop(editor);
    }

    LOG(INFO, INFO, "Exited editor_main()");
}

static void editor_handle_signal(int signum)
{
    LOG(NOTICE, INFO, "Handling signal %d", signum);

    if (!sig_editor)
    {
        LOG(NOTICE, ERR, "Could not handle signal %d - No editor", signum);
        return;
    }

    switch (signum)
    {
    /* We quit in response to these */
    case SIGINT:
    case SIGTERM:
    case SIGABRT:
        editor_term(sig_editor);
        break;
    case SIGWINCH:
        puts("SIG");
        editor_resize(sig_editor);
        break;
    }

    LOG(NOTICE, SUCC, "Handled signal %d", signum);
}

static void editor_resize_cb(editor_s *editor, event_s *event, void *param)
{
    if (editor->alive)
    {
        tty_update_dims(&(editor->tty));
        display_resize(&(editor->display));
        wm_resize(&(editor->wm));
        display_resume(&(editor->display));
    }
    else
    {
        LOG(NOTICE, ERR, "Cannot resize editor as it is not alive");
    }
}


/* Handle a resizing of the editor window */
void editor_resize(editor_s *editor)
{
    event_push(&(editor->eventctx), &(event_s){
        .spec = { .typ = EVENT_RESIZE, .ptr = NULL }
    });
    display_pause(&(editor->display));
    tty_clear_screen(&(editor->tty));
    tty_update_dims(&(editor->tty));
}

void editor_term(editor_s *editor)
{
    time_t now;
    now = time(NULL);

    /* This is the most critical case. Something is very wrong. */
    if (!editor->alive)
    {
        puts("\n\n\nForcefully terminating editor ...");
        exit(0);
    }

    if (editor->lastterm + EDITOR_TERM_TOUT >= now)
        editor->numterms += 1;
    else
        editor->numterms = 1;

    editor->lastterm = now;

    if (editor->numterms == 1)
    {
        LOG(ALERT, INFO, "Send termination signal again to kill editor.");
    }

    if (editor->numterms == 2)
    {
        editor->alive = false;
        LOG(ALERT, INFO, "Killing editor due to termination signal.");
    }
}

void editor_run(editor_s *editor, int inp, int out, char * const cmd[])
{
    pid_t pid;
    pid = fork();

    if (pid != 0)
    {
        LOG(NOTICE, INFO, "Spawned child with PID %d", pid);
        close(inp);
        close(out);
        return;
    }

    dup2(inp, STDIN_FILENO);
    close(inp);

    dup2(out, STDOUT_FILENO);
    dup2(out, STDERR_FILENO);
    close(out);

 //   tty_set_raw(&(editor->tty));

    execvp(cmd[0], cmd);
}

void editor_stdout_pty(editor_s *editor, int inp, int out, char * const cmd[])
{
    pty_s pty;

    /* Stop using STDIN/STDOUT */
    display_pause(&(editor->display));
    input_pause(&(editor->input));
    tty_set_raw(&(editor->tty));
    tty_clear_screen(&(editor->tty));

    LOG(NOTICE, INFO, "Running command, handing over control of stdout/err");

    if (pty_init(&pty, inp, out, cmd) == 0)
    {
        char * const *print;

        /* Close the input and output files in the parent process */
        close(inp);
        close(out);

        printf("Running command ");
        for (print = cmd; *print; ++print)
            printf(" %s", *print);
        printf(" ...\r\n");

        printf("----------------------------\r\n");

        pty_forward_data(&pty, editor->in, editor->out);
        LOG(NOTICE, SUCC, "Ran command sucessfully");
        printf(
            "\r\n"
            "----------------------------\r\n"
            "Command Completed.\r\n"
            "Returned status %d\r\n",
            pty_return_status(&pty)
        );

        usleep(200000);
        puts("Press a key to continue ...\r");
        getchar();
    }
    else
    {
        LOG(ALERT, ERR, "pty_init failed, could not run command");
    }

    tty_set_app(&(editor->tty));
    tty_clear_screen(&(editor->tty));

    pty_kill(&pty);
    input_resume(&(editor->input));
    display_resume(&(editor->display));
    display_refresh_all(&(editor->display));

    if (inp > 0) close(inp);
    if (out > 0) close(out);
}
