#if !defined(TEXT_ENCODER_H)
# define TEXT_ENCODER_H
# include "text/decoder.h"
# include "text/chr.h"

struct encoder
{
    decoder_attr_t attrs;
    short font;
};

void encoder_init(encoder_s *encoder, decoder_attr_t attrs);

void encoder_chr(encoder_s *encoder, chr_s *chr, vec_s *buf);

void encoder_nl(encoder_s *encoder, vec_s *buf);

#endif
