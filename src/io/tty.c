#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "io/tty.h"
#include "io/io.h"
#include "util/err.h"
#include "log/log.h"

/** Get tty configuration from an fd.
 *
 * Copies the terminal attributes of a terminal to a tty_conf_s.
 *
 */
static void tty_conf_from_fd(tty_conf_s *conf, int fd);

/** Configures a tty according to a tty_conf_s.
 *
 * Sets the terminal attributes, and paste and cursor attributes.
 *
 */
static int tty_conf_fd(tty_conf_s *conf, int fd, vt100_s *vt);

static void tty_conf_from_fd(tty_conf_s *conf, int fd)
{
    tcgetattr(fd, &(conf->attrs));

    /* Just guess default values */
    conf->set   = VT100_MODE_DEFAULT_SET;
    conf->reset = VT100_MODE_DEFAULT_RESET;
}

static int tty_conf_fd(tty_conf_s *conf, int fd, vt100_s *vt)
{
    vec_s towrite;

    vec_init(&towrite, sizeof(char));

    /* Set attributes */
    tcsetattr(fd, TCSANOW, &(conf->attrs));

    TRY(vt100_set_modes(vt, conf->set,   true));
    TRY(vt100_set_modes(vt, conf->reset, false));

    while (vec_len(&towrite) > 0)
    {
        char errbuf[IO_ERRBUF_LEN];
        if (io_write_from_vec(fd, &towrite, errbuf) < 0)
        {
            vec_kill(&towrite);
            LOG(ALERT, ERR, "Error configuring tty %d: %s", fd, errbuf);
            return PALM_ERR;
        }
    }

    vec_kill(&towrite);
    return PALM_OK;
}

int tty_init(tty_s *tty, int in, int out, poll_ctx_s *pollctx)
{
    LOG(INFO, INFO, "Initializing tty at %p", (void *)tty);

    tty->in  = in;
    tty->out = out;

    // writer_init(&(tty->writer), pollctx, tty->out);
 
    TRY(vt100_init(&(tty->vt), tty));

    /* Get the original attributes */
    tty_conf_from_fd(&(tty->origconf), tty->out);

    /* Base the other sets of attributes off of the original ones */
    memcpy(&(tty->appconf), &(tty->origconf), sizeof(tty_conf_s));
    memcpy(&(tty->rawconf), &(tty->origconf), sizeof(tty_conf_s));

    /* --- APPLICATION TERMINAL ATTRS --- */
    tty->appconf.attrs.c_lflag &= ~(ICANON | ECHO);
    tty->appconf.attrs.c_iflag &= ~(IXON | IXOFF);
    tty->appconf.attrs.c_iflag |= IXANY;

    tty->appconf.attrs.c_cc[VMIN]  = 1;
    tty->appconf.attrs.c_cc[VTIME] = 0;

    tty->appconf.set = VT100_MODE_PASTE_BRACKET |
                       VT100_MODE_KEYPAD;
    tty->appconf.reset = VT100_MODE_SHOW_CUR;
    /* ---------------------------------- */

    /* --- RAW TERMINAL ATTRS --- */
    tty->rawconf.attrs.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    tty->rawconf.attrs.c_oflag &= ~(OPOST);
    tty->rawconf.attrs.c_cflag &= ~(CSIZE | PARENB);
    tty->rawconf.attrs.c_cflag |= CS8;
    tty->rawconf.attrs.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);

    tty->rawconf.attrs.c_cc[VMIN]  = 1;
    tty->rawconf.attrs.c_cc[VTIME] = 0;

    tty->rawconf.set   = VT100_MODE_DEFAULT_SET;
    tty->rawconf.reset = VT100_MODE_DEFAULT_RESET;
    /* -------------------------- */

    /* Get terminal dims */
    tty_update_dims(tty);

    LOG(INFO, SUCC, "Initialized tty at %p", (void *)tty);

    return PALM_OK;
}

void tty_update_dims(tty_s *tty)
{
    struct winsize w;
    if (ioctl(tty->in, TIOCGWINSZ, &w) == -1)
        LOG(NOTICE, ERR, "ioctl to get terminal dims failed.");

    tty->w = w.ws_col;
    tty->h = w.ws_row;

    LOG(NOTICE, INFO, "Resized TTY to %d cols, %d rows", tty->w, tty->h);
}

void tty_set_raw(tty_s *tty)
{
    tty_conf_fd(&(tty->rawconf), tty->out, &(tty->vt));
}

void tty_set_app(tty_s *tty)
{
    tty_conf_fd(&(tty->appconf), tty->out, &(tty->vt));
}

void tty_set_orig(tty_s *tty)
{
    tty_conf_fd(&(tty->origconf), tty->out, &(tty->vt));
}

int tty_clear_screen(tty_s *tty)
{
    return vt100_clear_display(&(tty->vt));
}

int tty_write(tty_s *tty, const void *data, size_t len)
{
    if (write(tty->out, data, len) < 0)
        return PALM_ERR;
    //writer_write(&(tty->writer), len, data);

    return PALM_OK;
}

void tty_kill(tty_s *tty)
{
    tty_set_orig(tty);
    tty_clear_screen(tty);
    LOG(INFO, INFO, "Killed tty at %p", (void *)tty);

    //if (writer_flush(&(tty->writer)) == PALM_ERR)
    //    LOG(ALERT, INFO, "Error flushing tty writer.");

    //writer_kill(&(tty->writer));
}
