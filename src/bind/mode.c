#include <stdlib.h>
#include "bind/mode.h"
#include "log/log.h"

mode_s *mode_get(mode_s *mode, const char *name)
{
    mode_s *new;

    if (!mode)
    {
        new = malloc(sizeof(mode_s));
        new->next = new;
    }
    else
    {
        mode_s *iter;
        iter = mode;
        do
        {
            if (strncmp(iter->name, name, MODE_NAME_LEN) == 0)
                return iter;

            iter = iter->next;
        } while (iter != mode);

        new = malloc(sizeof(mode_s));
        new->next = mode->next;
        mode->next = new;
    }
    strncpy(new->name, name, MODE_NAME_LEN);
    new->name[MODE_NAME_LEN] = '\0';

    if (new != mode || new->prevmode == NULL)
        new->prevmode = mode;

    table_init(&(new->binds));
    bind_init(&(new->defbind), NULL);

    return new;
}

void mode_kill(mode_s *mode)
{
    mode_s *iter;

    if (!mode)
        return;

    iter = mode;
    do
    {
        mode_s *next;

        TABLE_FOREACH(&(iter->binds), chain)
        {
            bind_s *bind;
            bind = table_chain_val(chain, NULL);
            bind_kill(bind);
        }

        table_kill(&(iter->binds));
        bind_kill(&(iter->defbind));

        next = iter->next;
        free(iter);
        iter = next;
    } while (iter != mode);
}

bind_s *mode_key(mode_s *mode, key_s *key)
{
    bind_s *rtn;

    rtn = table_get(&(mode->binds), key, sizeof(key_s), NULL);

    if (rtn)
    {
        return rtn;
    }
    else
    {
        return &(mode->defbind);
    }
}

static bind_s *mode_new_bind(mode_s *mode, key_s *key)
{
    bind_s *new;

    if (key)
    {
        key_s cannonkey;
        memcpy(&cannonkey, key, sizeof(key_s));
        key_canonicalise(&cannonkey);

        mode_unbind(mode, &cannonkey);
        table_set(
            &(mode->binds), &cannonkey, sizeof(key_s), NULL, sizeof(bind_s)
        );
        new = table_get(
            &(mode->binds), &cannonkey, sizeof(key_s), NULL
        );
    }
    else
    {
        bind_kill(&(mode->defbind));
        new = &(mode->defbind);
    }

    return new;
}

void mode_bind(mode_s *mode, bind_s *bind)
{
    key_s *key;

    if (bind->anykey)
        key = NULL;
    else
        key = &(bind->key);

    bind_init_cpy(mode_new_bind(mode, key), bind);
}

void mode_unbind(mode_s *mode, key_s *key)
{
    bind_s *bind;
    key_s cannonkey;

    if (!key)
    {
        bind_kill(&(mode->defbind));
        bind_init(&(mode->defbind), NULL);
        return;
    }

    memcpy(&cannonkey, key, sizeof(key_s));
    key_canonicalise(&cannonkey);

    bind = table_get(&(mode->binds), &cannonkey, sizeof(key_s), NULL);

    if (bind)
    {
        bind_kill(bind);
        table_del(&(mode->binds), &cannonkey, sizeof(key_s));
    }
}
