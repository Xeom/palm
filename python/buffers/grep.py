import os
import re

from buffers.pyprompt import single_pyprompt
from buffers.loclist  import LocListBuffer, Location
from util.encode      import strify
from core.decoder     import fmt_sanitize
from server.server    import add_global
from typing           import Optional, List
from core.reader      import Reader

@add_global()
class CmdLocListBuffer(LocListBuffer):
    """A Buffer for displaying the results of a command."""

    parse_regex = "(.*):([0-9]+).(.*)"
    """The regex to parse the results.

    Group 1 corresponds to the pathname.
    Group 2 corresponds to the line number.
    Group 3 corresponds to the content.
    """

    include_dummy_content = True
    """Whether to include unparsable lines in the output."""

    def __init__(self, *args, **kwargs):
        """Initialize an instance of this class.

        Arguments are inherited from LocListBuffer.
        """

        super().__init__(*args, **kwargs)
        self.listener = None

        # Openfds contains a list of the fds that are open for talking
        # to the grep command, and that therefore must be closed when
        # the grep command stops talking to us!
        self.openfds  = []

    def parse_line(self, line: str):
        """Parse a line of output from a grep command.

        Arguments:
            line - The line from the grep command. It is expected to be in the
                   format [path]:[row]:[content]. This will cause issues with
                   filenames containing either ':' or '\n', and so we need to
                   upgrade this to use the -z flag sometime!
        """
        match = re.match(self.parse_regex, line)

        if match:
            path    = match.group(1)
            row     = match.group(2)
            content = match.group(3)
            row     = int(row) - 1

            loc = Location(path, row=row)
            self.add_location(loc, content.strip())
        elif self.include_dummy_content:
            self.add_dummy_content(line)

    def stop(self):
        """Stop running the grep command."""
        # Stop listener
        if self.listener is not None:
            self.listener.stop()
            self.listener = None

        # Close open files
        for fd in self.openfds:
            os.close(fd)

        self.openfds = []

    def __del__(self):
        """Kill this class.

        Just ensures that stop is called."""
        super().__del__()
        self.stop()

    def show_results_of_cmd(self, cmd: List[str]):
        """Show the results of a command in this buffer.

        Arguments:
            cmd - The command to run.
        """
        self.set_items([])
        self.rancmd = cmd

        self.stop()

        # We open a pipe to act as stdin for our subprocess
        # and one to act as stdout. This means four fds in
        # total:
        #
        # [ OUR PROCESS (inwend) (outrend) ]
        #                |        ^
        #                v        |
        # [ SUB PROCESS (inrend) (outwend) ]
        #
        # The function run_cmd passes the inrend and outwend
        # file descriptors to the subprocess, and closes them
        # in our parent process, so we need to still close
        # inwend and outrend.

        inrend, inwend = os.pipe()
        outrend, outwend = os.pipe()

        self.editor.run_cmd(cmd, inrend, outwend)

        self.openfds += [inwend, outrend]
        self.listener = Reader(self.editor["pollctx"], outrend)

        # Handler for when we get some output from our command.
        @self.listener.on_recv()
        def cb(line: bytes):
            if not self.killed:
                self.parse_line(strify(line))

        self.cb = cb
        self.listener.start()

        self.set_head(self.make_head())

    def make_head(self) -> List[str]:
        """Return the paragraph at the top of the buffer."""
        return [
            "{title}Command Location Results",
            "",
            "This buffer is showing the results of running the",
            " command:",
            " {emph}" + fmt_sanitize(repr(self.rancmd)),
            "",
            "Press {emph}Enter{default} on any of the lines to go to that location",
            " in the file. You can also press {emph}i{default} to view file info,",
            " or {emph}/{default} to refine your search.",
            ""
        ]

@add_global()
class GrepBuffer(CmdLocListBuffer):
    def __init__(self, *args, **kwargs):
        """Initialize an instance of this class.

        Arguments are inherited from CmdLocListBuffer.
        """

        super().__init__(*args, **kwargs)
        self.regex = ""
        self.directory = None

    """A buffer for displaying the results of a grep command."""
    def keypress(self, key: bytes):
        """Handle a keypress."""
        if key == b"/":
            single_pyprompt(
                self,
                f"buf.search_for({repr(self.regex)[:-1]}",
                f"', {repr(self.directory)})"
            )
        else:
            super().keypress(key)

    def search_for(self, regex: str, directory: Optional[str]=None):
        """Run a grep command!

        Arguments:
            regex     - The PCRE regex to search for.
            directory - The directory to search.

        TODO: Make this accept more flags for configuring the command!
              Maybe it would make sense to add the ability to use the
              find command too :)
        """
        cmd = [
            "grep",
            "--exclude",     ".*",
            "--exclude-dir", ".*",
            "-IRPn",
            "--color=never",
            regex
        ] + ([directory] if directory != None else [])

        self.regex = regex
        self.directory = directory

        self.show_results_of_cmd(cmd)

@add_global(args=["editor"])
def rgrep(editor, string, directory=None):
    """Perform a grep command, and show the results in a GrepBuffer."""
    buf = editor.get_buf_of_type(GrepBuffer)
    editor.win_assoc(buf)
    buf.search_for(string, directory=directory)

@add_global(args=["editor"])
def pep257(editor, directory="."):
    """Perform a pep257 command, and show the results in a GrepBuffer."""
    buf = editor.new_buf(CmdLocListBuffer, switch=True)
    buf.show_results_of_cmd(["pydocstyle", directory])
