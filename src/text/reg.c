#include <pthread.h>
#include "text/reg.h"
#include "util/macro.h"

static pthread_mutex_t reg_mtx = PTHREAD_MUTEX_INITIALIZER;
static vec_s reg_vecs = VEC_INITIALIZER(vec_s);

size_t maxregs = 256;

static vec_s *reg_get_vec(int ind)
{
    if (ind < 0)
        ind = vec_len(&reg_vecs) + ind + 1;

    while (ind < (int)maxregs && (int)vec_len(&reg_vecs) <= ind)
    {
        vec_s *new;

        new = vec_app(&reg_vecs, NULL);
        vec_init(new, sizeof(char));
    }

    return vec_get(&reg_vecs, ind);
}

int reg_push(vec_s *str, int ind)
{
    int rtn;
    vec_s *new;
    WITH_MTX(&reg_mtx)
    {
        if (ind < 0)
            ind = vec_len(&reg_vecs) + ind + 1;

        new = vec_ins(&reg_vecs, ind, 1, NULL);
        if (new)
        {
            vec_init(new, sizeof(char));

            if (str)
                vec_cpy(new, str);

            if (vec_len(&reg_vecs) > maxregs)
                vec_del(&reg_vecs, vec_len(&reg_vecs) - 1, 1);

            rtn = 0;
        }
        else
        {
            rtn = -1;
        }
    }

    return rtn;
}

int reg_pop(vec_s *str, int ind)
{
    int rtn;

    WITH_MTX(&reg_mtx)
    {
        vec_s *reg;
        reg = reg_get_vec(ind);
        if (reg)
        {
            if (str)
                vec_cpy(str, reg);

            vec_del(&reg_vecs, ind, 1);
            rtn = 0;
        }
        else
        {
            rtn = -1;
        }
    }

    return rtn;
}

int reg_set(vec_s *str, int ind)
{
    int rtn;
    WITH_MTX(&reg_mtx)
    {
        vec_s *reg;
        reg = reg_get_vec(ind);

        if (reg)
        {
            vec_clr(reg);
            vec_cpy(reg, str);
            rtn = 0;
        }
        else
        {
            rtn = -1;
        }
    }

    return rtn;
}

int reg_get(vec_s *str, int ind)
{
    int rtn;
    WITH_MTX(&reg_mtx)
    {
        vec_s *reg;
        reg = reg_get_vec(ind);
        vec_clr(str);
        if (reg)
        {
            rtn = 0;
            vec_cpy(str, reg);
        }
        else
        {
            rtn = -1;
        }
    }

    return rtn;
}

int reg_chr(char c, int ind)
{
    int rtn;

    WITH_MTX(&reg_mtx)
    {
        vec_s *reg;
        reg = reg_get_vec(ind);
        if (reg)
        {
            vec_app(reg, &c);
            rtn = 0;
        }
        else
        {
            rtn = -1;
        }
    }

    return rtn;
}

size_t reg_num(void)
{
    size_t len;
    WITH_MTX(&reg_mtx)
    {
        len = vec_len(&reg_vecs);
    }

    return len;
}

