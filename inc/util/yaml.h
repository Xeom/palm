#if !defined(UTIL_YAML_H)
# define UTIL_YAML_H
# include "container/vec.h"
# include "container/table.h"
# include "log/log.h"
# include "types.h"
# include <stdio.h>

typedef enum
{
    YAML_OBJ_NONE,
    YAML_OBJ_VALUE,
    YAML_OBJ_MAPPING,
    YAML_OBJ_SEQUENCE
} yaml_obj_type_t;

struct yaml_obj
{
    yaml_obj_s *parent;
    yaml_obj_type_t type;
    vec_s key, val, seq;
    table_s subs;
};

void yaml_obj_init(yaml_obj_s *obj, const char *name, size_t len);

int yaml_obj_kill(yaml_obj_s *obj);

int yaml_obj_parse(yaml_obj_s *obj, FILE *f);

int yaml_obj_copy_from(yaml_obj_s *obj, yaml_obj_s *oth);

yaml_obj_s *yaml_obj_get(yaml_obj_s *obj, const char *addr[]);
const char *yaml_obj_get_val(yaml_obj_s *obj, const char *addr[], size_t *len);

yaml_obj_s *yaml_obj_add_sub(yaml_obj_s *obj, const char *key, bool *exists);

yaml_obj_s *yaml_obj_add_seq(yaml_obj_s *obj);

#define YAML_MAPPING_FOREACH(obj, key, val) \
    char *key; yaml_obj_s *val; size_t _ind = 0; \
    if ((obj)->type != YAML_OBJ_MAPPING) \
    { \
        LOG(NOTICE, ERR, "Tried to iterate a non-mapping YAML object"); \
    } \
    else for (table_chain_s *_chain = NULL; \
        (_chain = table_next(&((obj)->subs), &_ind, _chain)) && \
        (key = table_chain_key(_chain, NULL)) && \
        (val = table_chain_val(_chain, NULL));)

#define YAML_SEQUENCE_FOREACH(obj, val) \
    yaml_obj_s *val; \
    if ((obj)->type != YAML_OBJ_SEQUENCE) \
    { \
        LOG(NOTICE, ERR, "Tried to iterate a non-sequence YAML object"); \
    } \
    else for (size_t _ind = 0, \
         _len = vec_len(&(obj->seq)); \
         val  = vec_get(&(obj->seq), _ind), \
         _ind < _len; \
         ++_ind)

#endif
