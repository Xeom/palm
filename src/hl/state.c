#include "hl/state.h"
#include "hl/token.h"
#include "text/font.h"
#include <string.h>

void hl_state_init(hl_state_s *state, char *name)
{
    memset(state, 0, sizeof(hl_state_s));
    vec_init(&(state->actions), sizeof(hl_action_s));

    vec_init(&(state->start), sizeof(long));
    vec_init(&(state->end), sizeof(long));

    state->font  = -1;

    vec_init(&(state->name), sizeof(char));
    vec_str(&(state->name), name);
    vec_app(&(state->name), NULL);

    hl_action_init(&(state->startattrs));
    hl_action_init(&(state->endattrs));
}

void hl_state_kill(hl_state_s *state)
{
    vec_kill(&(state->actions));
    vec_kill(&(state->name));
    vec_kill(&(state->start));
    vec_kill(&(state->end));
}

int hl_state_verify(hl_state_s *state)
{
    return 0;
}

int hl_state_add_action(hl_state_s *state, hl_action_s *action)
{
    size_t ind;

    if (hl_action_verify(action) == -1)
    {
        LOG(NOTICE, ERR, "Action failed to verify");
        return -1;
    }

    /* Insert the new action in a sorted location */
    ind = vec_bst(&(state->actions), action, hl_action_cmp);
    vec_ins(&(state->actions), ind, 1, action);

    return 0;
}

hl_action_s *hl_state_match(
    hl_state_s *state,
    hl_ctx_s *ctx,
    vec_s *str,
    size_t *off,
    size_t *start,
    size_t *end
)
{
    VEC_FOREACH(&(state->actions), hl_action_s *, act)
    {
        hl_tok_s *tok;
        tok = hl_ctx_get_token(ctx, act->token);
        if (hl_tok_match(tok, str, off, start, end))
            return act;
    }

    return NULL;
}

void hl_state_apply(hl_state_s *state, hl_ctx_s *ctx, vec_s *str, vec_s *fonts, vec_s *states, size_t *off)
{
    size_t origoff, start, end;
    hl_action_s *act;

    origoff = *off;
    act = hl_state_match(state, ctx, str, off, &start, &end);

    if (act)
    {
        if (state->font != -1)
        {
            for (; origoff < *off; ++origoff)
            {
                unsigned short *fptr;
                fptr = vec_get(fonts, origoff);
                if (fptr && *fptr == 0)
                    *fptr = state->font;
            }
        }

        hl_action_apply(act, ctx, str, fonts, states, start, end);
    }
    else
    {
        unsigned short *fptr;
        if (state->font != -1)
        {
            fptr = vec_get(fonts, *off);
            if (fptr && *fptr == 0)
                *fptr = state->font;
        }
        ++(*off);
    }
}

static int load_token_arr(yaml_obj_s *obj, hl_ctx_s *ctx, vec_s *arr)
{
    if (obj->type != YAML_OBJ_SEQUENCE)
    {
        LOG(NOTICE, ERR, "Expected array");
        return -1;
    }

    YAML_SEQUENCE_FOREACH(obj, val)
    {
        char *name;
        long tok;
        if (val->type != YAML_OBJ_VALUE)
        {
            LOG(NOTICE, ERR, "Expected value as token");
            return -1;
        }

        name = vec_get(&(val->val), 0);
        tok = hl_ctx_lookup_token(ctx, name);

        if (tok == -1)
        {
            LOG(NOTICE, ERR, "Unknown token: %s", name);
            return -1;
        }
        vec_app(arr, &tok);
    }

    return 0;
}

int hl_state_load_info(hl_state_s *state, yaml_obj_s *obj, hl_ctx_s *ctx)
{
    yaml_obj_s *startobj, *endobj; 
    yaml_obj_s *startattrsobj, *endattrsobj;
    const char *font;
    startattrsobj = yaml_obj_get(obj, (const char *[]){ "start-attrs", NULL });
    endattrsobj   = yaml_obj_get(obj, (const char *[]){ "end-attrs",   NULL });
    startobj      = yaml_obj_get(obj, (const char *[]){ "start",       NULL });
    endobj        = yaml_obj_get(obj, (const char *[]){ "end",         NULL });
    font      = yaml_obj_get_val(obj, (const char *[]){ "font",  NULL }, NULL);

    if (startattrsobj)
    {
        if (hl_action_load_info(&(state->startattrs), startattrsobj, ctx) == -1)
            return -1;
    }

    if (endattrsobj)
    {
        if (hl_action_load_info(&(state->endattrs), endattrsobj, ctx) == -1)
            return -1;
    }

    if (startobj)
    {
        if (load_token_arr(startobj, ctx, &(state->start)) == -1)
            return -1;
    }

    if (endobj)
    {
        if (load_token_arr(endobj, ctx, &(state->end)) == -1)
            return -1;

        VEC_FOREACH(&(state->end), long *, toknum)
        {
            hl_action_s act;
            hl_tok_s   *tok;
            hl_action_init(&act);

            tok = hl_ctx_get_token(ctx, *toknum);
            if (tok)
                hl_action_use_defaults(&act, &(tok->defaults));

            hl_action_use_defaults(&act, &(state->endattrs));
            act.token = *toknum;
            act.type  = HL_ACTION_POP;

            hl_state_add_action(state, &act);
        }
    }

    if (font)
        state->font = font_by_name(font);

    return 0;
}

int hl_state_load_actions(hl_state_s *state, yaml_obj_s *obj, hl_ctx_s *ctx)
{
    yaml_obj_s *states, *tokens, *actions;

    tokens  = yaml_obj_get(obj, (const char *[]){ "contains-tokens", NULL });
    states  = yaml_obj_get(obj, (const char *[]){ "contains-states", NULL });
    actions = yaml_obj_get(obj, (const char *[]){ "actions", NULL });

    if (tokens)
    {
        YAML_SEQUENCE_FOREACH(tokens, val)
        {
            hl_tok_s   *tok;
            hl_action_s act;
            long        toknum;
            char        *name;

            if (val->type != YAML_OBJ_VALUE)
            {
                LOG(NOTICE, INFO, "Expect token names to be values");
                return -1;
            }

            hl_action_init(&act);
            name = vec_get(&(val->val), 0);
            toknum = hl_ctx_lookup_token(ctx, name);
            if (toknum == -1)
            {
                LOG(NOTICE, INFO, "Unknown token: %s", name);
                return -1;
            }

            tok = hl_ctx_get_token(ctx, toknum);
            hl_action_use_defaults(&act, &(tok->defaults));
            act.token = toknum;

            if (hl_state_add_action(state, &act) == -1)
                return -1;
        }
    }

    if (states)
    {
        YAML_SEQUENCE_FOREACH(states, val)
        {
            hl_state_s *oth;
            hl_action_s act;
            long        statenum;
            char        *name;

            if (val->type != YAML_OBJ_VALUE)
            {
                LOG(NOTICE, INFO, "Expect state names to be values");
                return -1;
            }

            hl_action_init(&act);
            name = vec_get(&(val->val), 0);
            statenum = hl_ctx_lookup_state(ctx, name);
            if (statenum == -1)
            {
                LOG(NOTICE, INFO, "Unknown state: %s", name);
                return -1;
            }

            oth = hl_ctx_get_state(ctx, statenum);

            VEC_FOREACH(&(oth->start), long *, toknum)
            {
                hl_action_s act;
                hl_tok_s   *tok;
                hl_action_init(&act);

                tok = hl_ctx_get_token(ctx, *toknum);
                if (tok)
                    hl_action_use_defaults(&act, &(tok->defaults));

                hl_action_use_defaults(&act, &(oth->startattrs));
                act.token = *toknum;
                act.state = statenum;
                if (act.type == HL_ACTION_NONE)
                    act.type = HL_ACTION_PUSH;

                if (hl_state_add_action(state, &act) == -1)
                    return -1;
            }
        }
    }

    if (actions)
    {
        YAML_SEQUENCE_FOREACH(actions, val)
        {
            hl_action_s act;
            hl_action_init(&act);
            hl_action_load_info(&act, val, ctx);
            if (hl_state_add_action(state, &act) == -1)
                return -1;
        }
    }

    return 0;
}
