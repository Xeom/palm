from editortest import EditorTest
class NavTest(EditorTest):
    def assertCol(self, col):
        self.awaitReprEqual("buf.get_cur().pri.col", col)
        self.assertReprEqual("buf.get_cur().pri.col", col)

    def assertRow(self, row):
        self.awaitReprEqual("buf.get_cur().pri.row", row)
        self.assertReprEqual("buf.get_cur().pri.row", row)

    def test_goto(self):
        """Test to goto position (gg) command."""
        self.testbed.run(
            f"from util.keys import Keys\n"
            f"buf = editor.new_buf(switch=True)\n"
            f"buf.ins(0, ['LINE']*100)\n"
        )

        self.testbed.run(
            f"editor.playbacker.send(Keys.ctrl('g') + b'30' + Keys.enter)"
        )
        self.assertRow(30)

        self.testbed.run(
            f"editor.playbacker.send(Keys.esc('gg') + b'10' + Keys.enter)"
        )
        self.assertRow(10)

        self.testbed.run(
            f"editor.playbacker.send(Keys.esc('gg') + b'20,3' + Keys.enter)"
        )
        self.assertRow(20)
        self.assertCol(3)

    def test_goto_start_and_end(self):
        """Test the goto buffer end (gJ) and goto buffer start (gK) commands."""
        self.testbed.run(
            f"from util.keys import Keys\n"
            f"buf = editor.new_buf(switch=True)\n"
            f"buf.ins(0, ['LINE']*100)\n"
        )

        self.runCmd("gJ")
        self.assertRow(99)
        self.assertCol(4)

        self.runCmd("gK")
        self.assertRow(0)
        self.assertCol(0)

    def test_home_end(self):
        """Test the Home (H), End (L), PgUp (K) and PgDn (J) commands."""
        self.testbed.run(
            f"from util.keys import Keys\n"
            f"buf = editor.new_buf(switch=True)\n"
            f"buf.ins(0, ['LINE']*100)\n"
        )

        self.runCmd("L")
        self.assertRow(0)
        self.assertCol(4)

        self.runCmd("H")
        self.assertRow(0)
        self.assertCol(0)

        h = int(self.testbed.eval("editor.wm.get_dims(buf.get_win()).h")) - 1

        self.runCmd("J")
        self.assertRow(h - 1)

        self.runCmd("J")
        self.assertRow(h - 1)

        self.runCmd("J")
        self.assertRow(2 * (h - 1))

        self.runCmd("K")
        self.assertRow(h - 1)

        self.runCmd("K")
        self.assertRow(h - 1)

        self.runCmd("K")
        self.assertRow(0)
