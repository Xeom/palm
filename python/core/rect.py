from lib import lib
from .newstruct import SimpleCStructure
from server.globals import add_global

@add_global()
class Rect(SimpleCStructure):
    class_name = "rect_s"
    repr_multiline = False

    @classmethod
    def at(self, x=0, y=0, w=1, h=1):
        """Create a rectangle of the specified position and size."""
        return Rect(x=x, y=y, w=w, h=h)

    def overlap(self, oth: "Rect") -> bool:
        """Test whether this rectangle overlaps with another."""

        return lib.rect_overlap(self.ptr, oth.ptr)
