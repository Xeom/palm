#include "snow.h"
#include "buf/curset.h"

#define FOREACH_CUR(curs) \
    for (cur_s *c = curs; c < curs + (sizeof(curs) / sizeof(cur_s)); ++c)
#define RFOREACH_CUR(curs) \
    for (cur_s *c = curs + (sizeof(curs) / sizeof(cur_s)) - 1; c >= curs; --c)

#define ASSERTEQ_CUR(a, b) \
    do {                                    \
        asserteq((a).pri.row, (b).pri.row); \
        asserteq((a).pri.col, (b).pri.col); \
        asserteq((a).sec.row, (b).sec.row); \
        asserteq((a).sec.col, (b).sec.col); \
    } while (0)

static void add_random_cursors(curset_s *cs, size_t n)
{
    size_t ind;
    for (ind = 0; ind < n; ++ind)
    {
        int row, col;
        row = rand() % 100;
        col = rand() % 100;

        asserteq(curset_push(cs, &CUR_AT(row, col)), 0);
    }
}

describe(curset)
{
    curset_s cs;

    before_each()
    {
        curset_init(&cs, NULL);
    }

    after_each()
    {
        curset_kill(&cs);
    }

    it ("Pushes and pops active cursors")
    {
        cur_s curs[] = {
            CUR_AT(0, 0), CUR_AT(0, 1), CUR_AT(0, 2), CUR_AT(0, 3),
            CUR_AT(1, 0), CUR_AT(1, 1), CUR_AT(1, 2), CUR_AT(1, 3)
        };

        FOREACH_CUR(curs)
        {
            asserteq(curset_push_inactive(&cs, c), 0);
        }

        RFOREACH_CUR(curs)
        {
            cur_s cmp;
            asserteq(curset_pop_inactive(&cs, &cmp), 0);
            ASSERTEQ_CUR(cmp, *c);
        }
    }

    it ("Pushes and pops inactive cursors")
    {
        cur_s curs[] = {
            CUR_AT(0, 0), CUR_AT(0, 1), CUR_AT(0, 2), CUR_AT(0, 3),
            CUR_AT(1, 0), CUR_AT(1, 1), CUR_AT(1, 2), CUR_AT(1, 3)
        };

        FOREACH_CUR(curs)
        {
            asserteq(curset_push_inactive(&cs, c), 0);
        }

        RFOREACH_CUR(curs)
        {
            cur_s cmp;
            asserteq(curset_pop_inactive(&cs, &cmp), 0);
            ASSERTEQ_CUR(cmp, *c);
        }
    }

    it ("Orders cursors")
    {
        size_t ind, ncurs;

        ncurs = 500;
        add_random_cursors(&cs, ncurs);

        cur_s prev = CUR_AT(0, 0);

        curset_select_ind(&cs, 0);
        for (ind = 0; ind < ncurs; ++ind)
        {
            cur_s curr;
            asserteq(curset_get_selected(&cs, &curr), 0);
            assert(!cur_lt(&curr, &prev));
            memcpy(&prev, &curr, sizeof(cur_s));

            curset_select_next(&cs);
        }
    }

    it ("Re-Orders Cursors")
    {
        size_t ind, ncurs;

        ncurs = 100;
        add_random_cursors(&cs, ncurs);

        for (ind = 0; ind < ncurs; ++ind)
        {
            cur_s curr;
            curset_select_ind(&cs, 0);
            asserteq(curset_get_selected(&cs, &curr), 0);
            curr.pri.row = 1000 + rand() % 100;
            curr.pri.col = rand() % 100;
            asserteq(curset_set_selected(&cs, &curr), 0);
        }

        cur_s prev = CUR_AT(0, 0);

        curset_select_ind(&cs, 0);
        for (ind = 0; ind < ncurs; ++ind)
        {
            cur_s curr;
            asserteq(curset_get_selected(&cs, &curr), 0);
            assert(!cur_lt(&curr, &prev));
            assert(curr.pri.row >= 1000);
            memcpy(&prev, &curr, sizeof(cur_s));

            curset_select_next(&cs);
        }
    }
}
