#if !defined(IO_IO_H)
# define IO_IO_H
# include "container/vec.h"
# include <stdbool.h>
# include <poll.h>
# include <limits.h>

# define IO_ERRBUF_LEN 256 /**< The size of an errorbuf */

typedef char io_path_t[PATH_MAX];
typedef char io_errbuf_t[IO_ERRBUF_LEN];

extern int io_default_poll_tout;  /**< The default timeout for polls */
extern int io_default_file_mode;  /**< The default mode flags to create files with */
extern int io_default_dir_mode;   /**< The default mode flags to create directories with*/
extern int io_default_size;       /**< The default size of file read/writes */

extern size_t path_max;           /**< The size of a path buffer. */
extern size_t io_errbuf_len;      /**< The size of an error buffer. */

/**
 * @file  io.h
 * @brief Functions for handling IO.
 *
 * This API is designed to make IO operations easier, make error handling
 * more consistent, and maybe one day to make everything a little more
 * cross platform.
 *
 * Conventions for IO function return values:
 *  * -1 means error.
 *  *  0 means incomplete.
 *  *  1 means complete.
 */

/**
 * Flags for @ref io_file_info_s. These specify the status of a
 * file descriptor.
 *
 * They are set on @ref io_file_info_s structures  by calls to
 * io_poll_files()
 */
typedef enum
{
    IO_FILE_EOF      = 0x01, /**< The fd has reached an EOF condition */
    IO_FILE_READABLE = 0x02, /**< Data can be/was read from the file  */
    IO_FILE_WRITABLE = 0x04, /**< Data can be/was written to the file */
    IO_FILE_CLOSED   = 0x08  /**< The fd is closed or invalid         */
} io_file_status_t;

/**
 * A structure for configuring polling of a file descriptor,
 * and what actions should be taken in response to events.
 *
 * Data and status from the file descriptor are also returned
 * in this structure.
 *
 * Used by the functions io_poll_file() and io_poll_files().
 *
 * Example:
 * @code
 * bool  loop;
 * vec_s recv;
 * vec_init(&recv, sizeof(char));
 * for (loop = true; loop;)
 * {
 *     char errbuf[IO_ERRBUF_LEN];
 *     io_file_info_s finfo = { .fd = STDIN_FILENO, .readvec = &recv };
 *     switch (io_poll_file(&finfo, -1, errbuf))
 *     {
 *     case 1:
 *         if (finfo.status & IO_FILE_READABLE)
 *         {
 *             vec_app(&recv, "\x00");
 *             printf("Got data: '%s'\n", vec_get(&recv, 0));
 *             vec_clr(&recv);
 *         }
 *         else if (finfo.status & (IO_FILE_EOF | IO_FILE_CLOSED)
 *         {
 *             loop = false;
 *         }
 *         break;
 *     case -1:
 *         fprintf(stderr, "Error polling: %s\n", errbuf);
 *         loop = false;
 *         break;
 *     case 0:
 *         puts("TIMEOUT");
 *         break;
 *     }
 * }
 * vec_kill(&recv);
 * @endcode
 *
 * Example configuration to wait for data to be availiable:
 * @code
 * { .fd = x, .awaitin = true }
 * @endcode
 *
 * Example configuration to write data from @ref vec_s @c vec:
 * @code
 * { .fd = x, .writevec = &vec }
 * @endcode
 *
 * Example configuration to read only one character to @ref vec_s @c vec:
 * @code
 * { .fd = x, .ntoread = 1, .readvec = &vec }
 * @endcode
 *
 */
struct io_file_info
{
    /**
     * If not @c NULL, a vector to place characters as they are read from
     * the file descriptor. This implies @c awaitin=true. The characters
     * are automatically read.
     */
    vec_s *readvec;

    /**
     * If not NULL, a vector of character to write to the file when
     * possible. This implies @c awaitout=true if there are characters
     * in the vector. The characters are automatically popped and written.
     */
    vec_s *writevec;

    /**
     * The number of characters to try and read from the
     * file. This value is decremented as characters are read.
     * If this option is zero, then a size of @ref io_default_size
     * is assumed.
     */
    long ntoread;

    /** The status of the file after the last call to io_poll_files() */
    io_file_status_t status;

    int fd; /**< The file descriptor to be polled. */

    bool awaitin;  /**< If true, await an @ref IO_FILE_READABLE condition. */
    bool awaitout; /**< If true, await an @ref IO_FILE_WRITABLE condition. */
};

/**
 * A structure for containing a set of normalized pathnames
 *
 * The io_path_decomp() function uses this to return a set of pathname components.
 *
 */
struct io_path
{
    char real[PATH_MAX]; /**< The full real path.      */
    char base[PATH_MAX]; /**< The base path (filename) */
    char dir[PATH_MAX];  /**< The dir path (directory) */
};

/*** FILE READING AND WRITING ***/

/**
 * Read bytes from a file to a vector.
 *
 * @param fd     The file descriptor to read from.
 * @param buf    A pointer to a char vector where bytes from the file will be
 *               appended.
 * @param n      If not @c NULL, a pointer to the number of characters that should
 *               be read from the file. The value is decremented by the number
 *               that are actually read,
 * @param errbuf A buffer of size @ref IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if file is at EOF, 0 if file was read sucessfully, -1 on error.
 *
 */
int io_read_to_vec(int fd, vec_s *buf, size_t *n, char *errbuf);

/**
 * Write bytes from a vector to a file.
 *
 * @param fd     The file descriptor to write to.
 * @param buf    A pointer to a char vector containing the bytes to write to the
 *               file. The vector is emptied as bytes are written.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if all bytes were written, 0 if some of them were, -1 on error.
 *
 */
int io_write_from_vec(int fd, vec_s *buf, char *errbuf);

/**
 * Write all of the bytes from a vector to a file.
 *
 * This function is the same as io_write_from_vec(), but it retries writing when
 * not all of the bytes could be written.
 *
 * @param fd     The file descriptor to write to.
 * @param buf    A pointer to a char vector containing the bytes to write to the
 *               file. The vector is emptied as bytes are written.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if all bytes were written, -1 on error.
 *
 */
static inline int io_write_all_from_vec(int fd, vec_s *buf, char *errbuf)
{
    int res;
    while (vec_len(buf))
    {
        res = io_write_from_vec(fd, buf, errbuf);

        if (res != 0)
            break;
    }
    return res;
}


/*** FILE POLLING ***/

/**
 * Poll files.
 *
 * This function accepts a set of io_file_info_s configurations, polls them all,
 * awaiting events, and performs whatever actions are needed for each one.
 *
 * A negative timeout will default to using the value of
 * io_poll_default_timeout.
 *
 * @param files  An array of io_file_info_s structures.
 * @param nfiles The number of io_file_info_s structures in the array.
 * @param toutms The maximum number of miliseconds to wait for events before
 *               timing out.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if an event happened, 0 on timeout, -1 on error.
 *
 */
int io_poll_files(io_file_info_s files[], int nfiles, int toutms, char *errbuf);

/**
 * Poll files.
 *
 * This function accepts a set of pointers to io_file_info_s configurations, 
 * polls them all, awaiting events, and performs whatever actions are needed 
 * for each one.
 *
 * A negative timeout will default to using the value of
 * io_poll_default_timeout.
 *
 * This file is identical to io_poll_files(), except that it works on pointers
 * rather than an array of objects.
 *
 * @param files  An array of io_file_info_s structure pointers.
 * @param nfiles The number of io_file_info_s structures in the array.
 * @param toutms The maximum number of miliseconds to wait for events before
 *               timing out.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if an event happened, 0 on timeout, -1 on error.
 *
 */
int io_poll_files_by_ptr(io_file_info_s *files[], int nfiles, int toutms, char *errbuf);

/**
 * Poll a single file.
 *
 * This function accepts a single io_file_info_s configuration, and polls it.
 * It performs the same action as io_poll_files, but for only one file.
 *
 * @param file   A pointer to a io_file_info_s structure.
 * @param toutms The maximum number of miliseconds to wait for events before
 *               timing out.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if an event happened, 0 on timeout, -1 on error.
 *
 */
static inline int io_poll_file(io_file_info_s *file, int toutms, char *errbuf)
{
    return io_poll_files(file, 1, toutms, errbuf);
}

/*** MISC. IO FUNCTIONS ***/

/**
 * Set a file descriptor to non-blocking mode.
 *
 * @param fd      The file descriptor to set non-blocking.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_set_nonblk(int fd, char *errbuf);

/**
 * Check whether a path is a directory.
 *
 * This function uses the stat() call to determine if a path is a directory.
 * ENOENT and ENOTDIR are not considered errors, but are simply indicate that
 * something is not a directory.
 *
 * @param path The path to check.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if the path is a directory, 0 if it is not, -1 on error.
 *
 */
int io_is_dir(const char *path, char *errbuf);

/**
 * Check whether a path is a file.
 *
 * This function uses the stat() call to determine if a path is a file.
 * ENOENT and ENOTDIR are not considered errors, but are simply indicate that
 * something is not a directory.
 *
 * @param path The path to check.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 1 if the path is a file, 0 if it is not, -1 on error.
 *
 */
int io_is_file(const char *path, char *errbuf);

/*** PATH MANIPULATION ***/

/**
 * Get the current user's home directory.
 *
 * @param dst    A buffer of size PATH_MAX, where the home directory is placed.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_gethome(char *dst, char *errbuf);

/**
 * If a path starts with a tilde, expand it into the current user's home dir.
 *
 * @param dst    A buffer of size PATH_MAX, where the resultant path is placed.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_expanduser(char *dst, const char *path, char *errbuf);

/**
 * Get a normalized real path.
 *
 * @param dst   A char buffer of size PATH_MAX, to put the real path.
 * @param path  The path to normalize.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_realpath(char *dst, const char *path, char *errbuf);


/**
 * Get the directory portion of a path.
 *
 * @param dst   A char buffer of size PATH_MAX, to put the directory portion.
 * @param path  The path to get the directory portion of.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_dirname(char *dst, const char *path, char *errbuf);

/**
 * Get the filename portion of a path.
 *
 * @param dst   A char buffer of size PATH_MAX, to put the filename portion.
 * @param path  The path to get the directory portion of.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_basename(char *dst, const char *path, char *errbuf);

/**
 * Get a normalized pathname, relative to another root path.
 *
 * e.g. path="/a/b/c/d", root="/a/b" => dst="c/d"
 *      path="/w/x/y/z", root="/a/b" => dst="/w/x/y/z"
 *      path="c/d",      root=".", cwd="/a/b" => dst="c/d"
 *
 * @param dst    A char buffer of size PATH_MAX to put the final filename.
 * @param path   The path to relativise.
 * @param root   The relative location.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_relpath(char *dst, const char *path, const char *root, char *errbuf);

/**
 * Append a component or series of components to a path.
 *
 * The path components are appended to the path pointer to in dst.
 * e.g. dst="/a/b", component="c/d" => dst="/a/b/c/d"
 *      dst="a/",   component="/b"  => dst="a/b"
 *
 * The result has no trailing slash, with the exception of the
 * root path '/'.
 *
 * @param dst       The path to append components to.
 * @param component The component[s] to append.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_path_join(char *dst, const char *component, char *errbuf);

/**
 * Decompose a path into an io_path_s structure.
 *
 * This extracts the realpath, basename, and dirname of the path.
 * More things may be added in the future!
 *
 * @param decomp An io_path_s structure to decompose the path into.
 * @param path   The path to decompose.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_path_decomp(io_path_s *decomp, const char *path, char *errbuf);

/*** FILE AND DIRECTORY CREATION ***/

/**
 * Recursively create a directory with a given path, if it does not exist.
 *
 * This function is roughly equivalent to:
 * $ mkdir -p $path
 *
 * @param path The path to create.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_create_dir(const char *path, char *errbuf);

/**
 * Recursively create a file with a given path, if it does not exist.
 *
 * This function is roughly equivalent to:
 * $ mkdir -p $(basename $path)
 * $ touch $path
 *
 * @param path The path of the file to create.
 * @param errbuf A buffer of size IO_ERRBUF_LEN, where error messages can go.
 *
 * @return 0 on success, -1 on error.
 *
 */
int io_create_file(const char *path, char *errbuf);

#endif
