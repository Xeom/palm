from typing import Sequence, List, Union

import libtypes
from lib import lib

from .structure import Structure
from .rect      import Rect

from server.server import add_global

@add_global()
class Display(Structure):
    typ = libtypes.display_s
    init_funct = lib.display_init
    kill_funct = lib.display_kill

    def init_args(self, tty):
        return [tty.ptr]

    @property
    def dims(self):
        rtn = Rect()
        lib.display_dims(self.ptr, rtn.ptr)
        return rtn
