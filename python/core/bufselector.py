from .newstruct import CStructure
from .buffer    import Buffer

from lib import lib

class BufSelector(CStructure):
    class_name = "vec_s"

    init_funct = lib.buf_selector_init
    kill_funct = lib.buf_selector_kill

    def init_args(self, args, kwargs):
        def inner(editor):
            return [editor.ptr]

        return inner(*args, **kwargs)


    def select(self, buf: Buffer):
        if buf != None:
            buf = buf.ptr

        if lib.buf_select(self.ptr, buf) == -1:
            raise Exception(f"Could not select buffer {buf}")

    def is_selected(self, buf: Buffer) -> bool:
        sel = lib.buf_get_selected(self.ptr)

        return sel == buf.ptr

    def get_selected_win(self) -> int:
        return lib.buf_get_selected_win(self.ptr)
