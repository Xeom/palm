import os
import paths

from core.buffer   import Buffer
from core.cur      import Cur
from editor        import log
from server.server import add_global

@add_global()
class LogBuffer(Buffer):
    def from_log(self, log=log):
        if hasattr(self, "infd") or hasattr(self, "outfd"):
            raise Exception("Cannot re-associate log-buffer")

        headerfile = open(paths.data + "/log-buffer.txt")
        lines = headerfile.read().split("\n")
        headerfile.close()
        self.ins(0, lines, ["UTF8", "FNT"])

        pipe = self.pipe_to_lines((len(self), None), attrs=["UTF8", "NL", "FNT", "STRIPNL"])

        self.loggingpipe   = pipe
        log.default.to_file(pipe, fonts=True)

    def kill(self):
        log.default.forget_file(self.loggingpipe)
        Buffer.kill(self)
