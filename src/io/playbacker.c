#include "io/playbacker.h"
#include "util/macro.h"
#include "log/log.h"
#include "io/io.h"
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define PLAYBACK_CHNK_SIZE 64

static void playbacker_send_job(void *arg);

int playbacker_init(playbacker_s *pb, pool_s *pool)
{
    int fds[2];
    if (pipe(fds) == -1)
    {
        LOG(ALERT, ERR, "Could not open playbacker pipe: %s", strerror(errno));
        return -1;
    }

    pb->jobrunning = false;

    pb->wrfd = fds[1];
    pb->rdfd = fds[0];

    pthread_mutex_init(&(pb->mtx), NULL);

    pool_job_init(&(pb->job), playbacker_send_job, pb);

    vec_init(&(pb->sendbuf), sizeof(char));

    pb->pool = pool;

    return 0;
}

void playbacker_kill(playbacker_s *pb)
{
    pool_job_await(&(pb->job));
    pthread_mutex_destroy(&(pb->mtx));

    close(pb->wrfd);
    close(pb->rdfd);
}

void playbacker_send(playbacker_s *pb, char *str, size_t len)
{
    WITH_MTX(&(pb->mtx))
    {
        vec_add(&(pb->sendbuf), len, str);
        if (!(pb->jobrunning))
        {
            pool_job_await(&(pb->job));
            pb->jobrunning = true;
            pool_add_job(pb->pool, &(pb->job));
        }
    }
}

static void playbacker_send_job(void *arg)
{
    playbacker_s *pb;
    vec_s  buf;

    vec_init(&buf, sizeof(char));
    pb = arg;

    while (pb->jobrunning)
    {
        WITH_MTX(&(pb->mtx))
        {
            size_t len;
            len = vec_len(&(pb->sendbuf));
            if (len == 0)
            {
                pb->jobrunning = false;
            }
            else
            {
                vec_cpy(&buf, &(pb->sendbuf));
                vec_clr(&(pb->sendbuf));
            }
        }

        char errbuf[IO_ERRBUF_LEN];
        if (io_write_from_vec(pb->wrfd, &buf, errbuf) == -1)
        {
            LOG(ALERT, ERR, "Error writing playback data to fd %d: %s", pb->wrfd, errbuf);
            pb->jobrunning = false;
        }
    }
}
