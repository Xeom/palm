import textwrap
import os
import stat

from datetime import datetime

import util.file

from core.io       import realpath
from core.decoder  import fmt_sanitize

from buffers.info  import InfoBuffer
from server.server import add_global

try:
    import magic
    hasmagic = True
except:
    hasmagic = False

@add_global()
class PathInfoBuffer(InfoBuffer):
    def show_path(self, path):
        self.show_info(self.get_info(path), title="PATH INFO")

    def get_info(self, path):
        if not os.path.exists(path):
            return "No such file"
        s = os.stat(path)
        types = []
        typesmap = {
            stat.S_ISDIR:  "Directory",
            stat.S_ISCHR:  "Character device",
            stat.S_ISREG:  "Regular file",
            stat.S_ISFIFO: "FIFO",
            stat.S_ISLNK:  "Symbolic link",
            stat.S_ISSOCK: "Socket",
        }

        for f, name in typesmap.items():
            if f(s.st_mode):
                types.append(name)

        humansize = s.st_size
        for suffix in " kMGTPEZY":
            if humansize <= 1000 or suffix == "Y":
                humansize = "{0:.3g} {1}B".format(humansize, suffix)
                break
            humansize /= 1024

        if hasmagic and os.path.isfile(path):
            magicinfo = magic.from_file(path)
            magicline = (
                "{{subtitle}} libmagic info {{faded}}(python-magic library)\n"
                "  {0}\n"
            ).format("\n  ".join(textwrap.wrap(magicinfo, 50, break_long_words=False, break_on_hyphens=False)))
        elif not os.path.isfile(path):
            magicline = ""
        else:
            magicline = "{faded} Install the python3-magic library to get more file info\n"

        return (
            "{{subtitle}} General info\n"
            "{{emph}}  Path: {{default}}{path}\n"
            "{{emph}}  Type: {{default}}{types}\n"
            "{{emph}}  Size: {humansize} {{faded}}({size} B)\n"
            "{{subtitle}} Access\n"
            "{{emph}}  Mode: {{default}}{access:04o}\n"
            "{{emph}}  UID: {{default}}{uid},"
            "{{emph}} GID: {{default}}{gid}\n"
            "{{subtitle}} Timestamps\n"
            "{{emph}}  atime: {{default}}{atime}\n"
            "{{emph}}  mtime: {{default}}{mtime}\n"
            "{{emph}}  ctime: {{default}}{ctime}\n"
            "{magic}"
        ).format(
            path=fmt_sanitize(realpath(path)),
            types=", ".join(types),
            access=s.st_mode & 0o7777,
            atime=datetime.fromtimestamp(s.st_atime).isoformat(" "),
            ctime=datetime.fromtimestamp(s.st_ctime).isoformat(" "),
            mtime=datetime.fromtimestamp(s.st_mtime).isoformat(" "),
            humansize=humansize,
            size=s.st_size,
            uid=s.st_uid,
            gid=s.st_gid,
            magic=magicline
        )
