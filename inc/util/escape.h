#if !defined(UTIL_ESCAPE_H)
# define UTIL_ESCAPE_H
# include "types.h"
# include <stdbool.h>

typedef enum
{
    ESCAPE_IDLE,
    ESCAPE_ESCAPED,
    ESCAPE_HEX,
    ESCAPE_OCT,
    ESCAPE_POP
} escape_state_t;

struct escape_state
{
    escape_state_t state;
    int acc;
    int acclen;
};

void escape_init(escape_state_s *state);

int escape_chr(escape_state_s *state, int c, bool *next);

void escape_vec(vec_s *vec);

int escape_sanitize_vec(vec_s *src, vec_s *dst);
#endif
