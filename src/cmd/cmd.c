#include "cmdlang/parser.h"
#include "buf/buf.h"
#include "log/log.h"
#include "cmd/cur.h"
#include "cmd/cmd.h"
#include "cmd/nav.h"
#include "cmd/edit.h"
#include "cmd/py.h"

int cmd_mount_all(cmd_lib_s *lib)
{
    TRY(cmd_cur_mount(lib));
    TRY(cmd_nav_mount(lib));
    TRY(cmd_py_mount(lib));
    TRY(cmd_edit_mount(lib));

    return PALM_OK;
}

static int cmd_cancel(buf_s *buf, errmsg_t errmsg)
{
    TRY(cmdlang_parser_cancel(&(buf->cmdparser), errmsg));

    return PALM_OK;
}

static int cmd_bytes(buf_s *buf, const char *bytes, size_t len, errmsg_t errmsg)
{
    size_t ind;
    for (ind = 0; ind < len; ++ind)
    {
        TRY(cmdlang_parser_type(&(buf->cmdparser), bytes[ind], errmsg));
    }

    return PALM_OK;
}

static int cmd_press(buf_s *buf, key_s *key, errmsg_t errmsg)
{
    if (key->button == KEY_CHAR || key->button == KEY_NUMPAD)
    {
        char c;
        c = key->val;
        TRY(cmdlang_parser_type(&(buf->cmdparser), c, errmsg));

        return PALM_OK;
    }
    else
    {
        char name[256];
        if (key_get_name(key, name, sizeof(name)) != PALM_OK)
            strcpy(name, "<?!>");

        ERRMSG(errmsg, "Key '%s' is not type-able.", name);

        return PALM_ERR;
    }
}


static int cmd_run(buf_s *buf, errmsg_t errmsg)
{
    TRY(cmdlang_parser_run(&(buf->cmdparser), errmsg));

    return PALM_OK;
}

static const cmd_binding_info_s defaultinfo =
{
    .multi       = true,
    .greedy      = false,
    .shortcut    = ""
};

void cmd_shortcut_binding(key_s *key, buf_s *buf, void *param, size_t size)
{
    errmsg_t errmsg;
    const cmd_binding_info_s *info;
    info = (size == sizeof(*info)) ? (cmd_binding_info_s *)param : &defaultinfo;

    SET_BITS(buf->cmdparser.attrs, CMDLANG_PARSER_GREEDY, false);

    if (cmd_cancel(buf, errmsg) != PALM_OK)
    {
        LOG(ALERT, ERR, "Command cancel error: %s", errmsg);
        return;
    }

    if (cmd_bytes(
            buf,
            info->shortcut, strlen(info->shortcut),
            errmsg
        ) != PALM_OK)
    {
        LOG(ALERT, ERR, "Command bytes error: %s", errmsg);
        return;
    }

    if (cmd_run(buf, errmsg) != PALM_OK)
    {
        LOG(ALERT, ERR, "Command run error: %s", errmsg);
        return;
    }
}

void cmd_type_binding(key_s *key, buf_s *buf, void *param, size_t size)
{
    errmsg_t errmsg;
    const cmd_binding_info_s *info;
    info = (size == sizeof(*info)) ? (cmd_binding_info_s *)param : &defaultinfo;

    SET_BITS(buf->cmdparser.attrs, CMDLANG_PARSER_GREEDY, info->greedy);

    if (cmd_press(buf, key, errmsg) == PALM_OK)
    {
        if ((buf->cmdparser.attrs & CMDLANG_PARSER_HAS_RUN) && !info->multi)
            bufbind_prev_mode(&(buf->bind));
    }
    else
    {
        if (!info->multi)
            bufbind_prev_mode(&(buf->bind));

        LOG(ALERT, ERR, "Command error: %s", errmsg);
        cmd_cancel(buf, NULL);
    }
}

void cmd_run_binding(key_s *key, buf_s *buf, void *param, size_t size)
{
    errmsg_t errmsg;
    const cmd_binding_info_s *info;
    info = (size == sizeof(*info)) ? (cmd_binding_info_s *)param : &defaultinfo;
    if (cmd_run(buf, errmsg) == PALM_OK)
    {
        if (!info->multi)
            bufbind_prev_mode(&(buf->bind));
    }
    else
    {
        LOG(ALERT, ERR, "Command error: %s", errmsg);
        cmd_cancel(buf, NULL);
    }
}

void cmd_cancel_binding(key_s *key, buf_s *buf, void *param, size_t size)
{
    errmsg_t errmsg;
    const cmd_binding_info_s *info;
    info = (size == sizeof(*info)) ? (cmd_binding_info_s *)param : &defaultinfo;

    if (cmd_cancel(buf, errmsg) == PALM_OK)
    {
        if (!info->multi)
            bufbind_prev_mode(&(buf->bind));
    }
    else
    {
        LOG(ALERT, ERR, "Command error: %s", errmsg);
    }
}
