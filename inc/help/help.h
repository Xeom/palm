#if !defined(HELP_H)
# define HELP_H
# include "types.h"

# include "util/err.h"

# include "container/vec.h"
# include "container/list.h"

typedef enum
{
    HELP_NONE,
    HELP_CMD,
    HELP_CMDARG
} help_typ_t;

struct help
{
    help_typ_t type;
    vec_s name;
    vec_s help;
    list_s subs;
};

int help_init(help_s *help) DONT_IGNORE;

void help_kill(help_s *help);

int help_set_type(help_s *help, help_typ_t type);

int help_set_info(help_s *help, const char *name, const char *helpmsg) DONT_IGNORE;

help_s *help_add_sub(help_s *help);

#endif
