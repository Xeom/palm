#include "snow.h"
#include "util/pyfmt.h"

describe(pyfmt)
{
    subdesc(from_bytes)
    {
        it ("Formats characters correctly")
        {
            char *expect = "b\"\\x01\\x20\\xff\\xaa\\x55\"";
            vec_s str;
            vec_init(&str, sizeof(char));
            pyfmt_from_bytes(&str, "\x01\x20\xff\xaa\x55", 5);
            asserteq(vec_len(&str), strlen(expect));
            asserteq_buf(vec_get(&str, 0), expect, strlen(expect));
            vec_kill(&str);
        }

        it ("Formats DEL correctly")
        {
            char *expect = "b\"\\x7f\"";
            vec_s str;
            vec_init(&str, sizeof(char));
            pyfmt_from_bytes(&str, "\x7f", 1);
            asserteq(vec_len(&str), strlen(expect));
            asserteq_buf(vec_get(&str, 0), expect, strlen(expect));
            vec_kill(&str);
        }
    }
}
