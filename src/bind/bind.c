#include "util/macro.h"
#include "io/pyclient.h"
#include "util/pyfmt.h"
#include "bind/bind.h"
#include "buf/buf.h"
#include "log/log.h"

static void bind_fmt_py(vec_s *str, key_s *key, char *src, size_t len);

void bind_kill(bind_s *bind)
{
    vec_kill(&(bind->arg));
    vec_kill(&(bind->modeswitch));
}

void bind_init(bind_s *bind, const key_s *key)
{
    vec_init(&(bind->arg),        sizeof(char));
    vec_init(&(bind->modeswitch), sizeof(char));

    if (!key)
    {
        bind->anykey = true;
    }
    else
    {
        memcpy(&(bind->key), key, sizeof(key_s));
        key_canonicalise(&(bind->key));
    }

    bind->cbind       = NULL;
    bind->multicursor = 0;
}

void bind_cbind(bind_s *bind, bind_c_t cbind, vec_s *arg)
{
    vec_clr(&(bind->arg));

    if (arg)
        vec_cpy(&(bind->arg), arg);

    bind->cbind = cbind;
}

void bind_pybind(bind_s *bind, const char *py)
{
    vec_clr(&(bind->arg));

    if (py)
        vec_str(&(bind->arg), py);

    bind->cbind = bind_pycb;
}

void bind_set_multicursor(bind_s *bind, int multicursor)
{
    bind->multicursor = multicursor;
}

void bind_set_modeswitch(bind_s *bind, const char *mode)
{
    vec_clr(&(bind->modeswitch));

    if (mode)
        vec_str(&(bind->modeswitch), mode);

    vec_app(&(bind->modeswitch), NULL);
}

void bind_init_cpy(bind_s *bind, bind_s *oth)
{
    bind_init(bind, &(oth->key));
    bind_cbind(bind, oth->cbind, &(oth->arg));
    bind_set_multicursor(bind, oth->multicursor);
    bind_set_modeswitch(bind, vec_get(&(oth->modeswitch), 0));
}

int bind_cmp(const void *a, const void *b)
{
    const bind_s *binda, *bindb;
    binda = a;
    bindb = b;

    return memcmp(&(binda->key), &(bindb->key), sizeof(key_s));
}

/**
 * A C-bind callback for Py-binds.
 *
 * Pybinds are just C binds with this callback and the python
 * code stored in the binding argument!
 *
 */
void bind_pycb(key_s *key, buf_s *buf, void *arg, size_t len)
{
    vec_s send, recv;
    vec_init(&send, sizeof(char));
    vec_init(&recv, sizeof(char));

    bind_fmt_py(&send, key, arg, len);

    if (buf->bind.pycb)
    {
        (buf->bind.pycb)(&send, &recv);
    }

    vec_kill(&send);
    vec_kill(&recv);
}

void bind_trigger(bind_s *bind, key_s *key, buf_s *buf)
{
    edit_attr_t attrs;
    long selind;
    size_t ncurs;

    if (!buf || !bind->cbind)
        return;

    attrs = buf->einfo.attrs;

    selind = -1;

    if (bind->multicursor != 0 && (attrs & EDIT_ALTS))
    {
        selind = curset_get_selected_ind(&(buf->curset));

        ncurs = curset_num_active(&(buf->curset));

        while (curset_num_active(&(buf->curset)) > 1)
        {
            cur_s cur;
            if (bind->multicursor > 0)
                curset_select_ind(&(buf->curset), -1);
            if (bind->multicursor < 0)
                curset_select_ind(&(buf->curset),  0);

            curset_pop(&(buf->curset),  &cur);
            curset_push_inactive(&(buf->curset), &cur);
        }
    }
    else
    {
        ncurs = 1;
    }

    while (ncurs--)
    {
        (bind->cbind)(key, buf, vec_get(&(bind->arg), 0), vec_len(&(bind->arg)));

        if (ncurs > 0)
        {
            cur_s cur;

            curset_pop_inactive(&(buf->curset), &cur);
            curset_push(&(buf->curset), &cur);
        }
    }

    if (selind != -1)
        curset_select_ind(&(buf->curset), selind);

    if (vec_len(&(bind->modeswitch)) > 1)
    {
        const char *mode;
        mode = vec_get(&(bind->modeswitch), 0);
        if (strcmp(mode, "prev") == 0)
        {
            bufbind_prev_mode(&(buf->bind));
        }
        else
        {
            bufbind_set_mode(&(buf->bind), mode);
        }
    }
}

static void bind_fmt_py(vec_s *str, key_s *key, char *src, size_t len)
{
    size_t ind;
    bool escaped;

    escaped = false;
    for (ind = 0; ind < len; ++ind)
    {
        char c;
        c = src[ind];
        if (escaped)
        {
            vec_s tmp;
            switch (c)
            {
            case '%':
                vec_str(str, "%");
                break;
            case 'k':
                vec_init(&tmp, sizeof(char));
                pyfmt_from_key(&tmp, key);
                vec_cpy(str, &tmp);
                vec_kill(&tmp);
                break;
            }
            escaped = false;
        }
        else if (c == '%')
        {
            escaped = true;
        }
        else
        {
            vec_app(str, &c);
        }
    }
}

