from typing import TYPE_CHECKING
from core.cur import Cur
from core.pos import Pos
from server.server import add_global

if TYPE_CHECKING:
    from core.buffer import Buffer

@add_global(args=["buf"])
def cur(buf):
    """Get the cursor."""
    return buf.get_cur()

@add_global(args=["buf"])
def pri(buf):
    """Return the primary cursor position."""
    return buf.get_cur().pri

@add_global(args=["buf"])
def sec(buf):
    """Return the secondary cursor position."""
    return buf.get_cur().sec

@add_global(args=["buf"])
def row(buf, new=None, sec=None):
    """Return the primary row of the cursor.

    Also optionally give it a new value.

    Arguments:
        new -- The new value for the row, or None.
        sec -- The new value for the secondary row, or None.
        b   -- The buffer to apply the function to.
    """
    if sec == None:
        goto(buf, (new, None))
    else:
        goto(buf, (new, None), (sec, None))

    res = buf.get_cur().pri.row
    return res

class TmpCur(object):
    def __init__(self, buf: "Buffer"):
        self.buf = buf

        self.origcur: Optional[Cur] = None

    def __enter__(self) -> Cur:
        self.origcur = self.buf.get_cur()

        return buf.get_cur()

    def __exit__(self, typ, value, traceback):
        if self.origcur is not None:
            self.buf.set_cur(self.origcur)

@add_global(args=["buf"])
def tmp_cur(buf):
    return TmpCur(buf)

@add_global(args=["buf"])
def col(buf, new=None, sec=None):
    """Return the primary column of the cursor.

    Also optionally give it a new value.

    Examples:
        col(3)       - Set the cursor column to three
        col(0, 3)    - Select the first four characters of the line
        col(None, 3) - Set the secondary column to three

    Arguments:
        new -- The new value for the columns, or None.
        b   -- The buffer to apply the function to.
    """
    if sec == None:
        goto(buf, (None, new))
    else:
        goto(buf, (None, new), (None, sec))

    res = buf.get_cur().pri.col
    return res

@add_global(args=["buf"])
def goto(buf, pos, secarg="None"):
    """Go to a specific position with the cursor.

    You can set the primary and secondary components of the cursor
    seperately, and the column and row components of each individually.

    You can set the components using Pos()es, Cur()s, or integers.

    Examples:
        goto(3) - Set the primary and secondary rows to 3.

        goto(3, 2) or goto([3, 2]) - Set the primary and secondary
            positions to 3:2.

        goto(None, 2) or goto([None, 2]) - Set the primary and
            secondary columns (only) to 2.

        goto(None, (0, 0)) - Set the secondary position (only) to 0:0.

        goto((3, None), None) - Set the primary row to 3.

        goto((0, 0), buf.last_pos()) - Select the entire buffer.

        goto(pri()) - Set both components of the cursor to the primary.

        goto(Cur.at(l, c)) - Copy the position of another cursor.

    Arguments:
        pos    -- The position of the cursor.
        secarg -- If speci
        fied, then pos is set to (pos, secarg).
        b      -- The buffer to operate on.
    """
    if secarg != "None":
        pos = (pos, secarg)

    cur = buf.get_cur()
    cur.goto(pos)
    buf.set_cur(cur)


@add_global(args=["buf"])
def cur_type(buf, typ=None, stick=None):
    """Set the cursor type.

    Arguments:
        typ   -- The type, to pass to cur_type_t(), e.g. "LINE",
               "CHR", "BLK".
        stick -- True/False to set stickiness, otherwise None.
    """
    cur = buf.get_cur()

    if typ != None:
        cur.set_type(typ)

    if stick != None:
        cur["sticky"] = stick

    buf.set_cur(cur)
    return cur.type_name()

@add_global(args=["buf"])
def next_altcur(buf, n=1):
    """Select the n-th next alt cursor.

    Arguments
        n -- The number of alt-cursors to go forwards.
    """
    current = buf.get_cur()

    if n == 0:
        return

    alts = sorted(enumerate(buf.altcurs), key=lambda c:c[1].pri)
    if len(alts) == 0:
        return

    currind = 0
    for alt in alts:
        if alt[1].pri < current.pri:
            currind += 1
        else:
            break

    if n < 0:
        ind = (currind + n) % len(alts)
    else:
        ind = (currind + n - 1) % len(alts)

    i, cur = alts[ind]
    buf.exchange_altcur(i)

@add_global(args=["buf"])
def prev_altcur(buf, n=1):
    """Select the n-th previous alt cursor.

    Arguments:
        n -- The number of alt-cursors to go backwards.
    """
    next_altcur(buf, -n)
