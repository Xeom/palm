from lib       import lib
from util.keys import Keys
from bind.mode import Mode, PyCommand
from core.bind import Bind

from server.server import add_global

@add_global()
def playback(editor, keys):
    editor.playbacker.send(keys)

RecordMode = Mode("record", nocmds=True)

RecordMode.add_bind(Bind(Keys.ctrl("r"), pybind="buf.bind.prev_mode()"))
RecordMode.add_bind(
    Bind(
        Keys.esc("R"),
        pybind="buf.bind.prev_mode();single_pyprompt('playback(editor, reg.get()', ')')"
    )
)
RecordMode.add_bind(Bind(None, cbind=lib.cmd_append_to_reg))

Mode.add_default_cmds([
    PyCommand("Record mode",        "mr", "buf.bind.set_mode('record');reg.push()", shortcut=Keys.ctrl("r")),
    PyCommand("Playback recording", "R", "single_pyprompt('playback(editor, reg.get()', ')')")
])
