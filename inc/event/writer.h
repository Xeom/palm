#if !defined(EVENT_WRITER_H)
# define EVENT_WRITER_H
# include <stdbool.h>
# include <pthread.h>
# include "types.h"
# include "io/io.h"
# include "container/vec.h"

struct writer
{
    io_file_info_s finfo;
    vec_s buf1;
    vec_s buf2;
    pthread_mutex_t mtx;
    vec_s *appbuf;
    poll_ctx_s *pollctx;
    bool alive;
};

void writer_init(writer_s *writer, poll_ctx_s *pollctx, int fd);

void writer_kill(writer_s *writer);

void writer_write(writer_s *writer, size_t n, const char *data);

int writer_flush(writer_s *writer);

#endif
