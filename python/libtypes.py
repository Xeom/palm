import typing

import ctypes
from ctypes import *

class vt100_s(ctypes.Structure): pass
class sortedvec_s(ctypes.Structure): pass
class escape_state_s(ctypes.Structure): pass
class buf_s(ctypes.Structure): pass
class bufbind_s(ctypes.Structure): pass
class chrconf_s(ctypes.Structure): pass
class decoder_s(ctypes.Structure): pass
class display_s(ctypes.Structure): pass
class editor_s(ctypes.Structure): pass
class encoder_s(ctypes.Structure): pass
class finfo_s(ctypes.Structure): pass
class floatwin_s(ctypes.Structure): pass
class hl_action_s(ctypes.Structure): pass
class hl_ctx_s(ctypes.Structure): pass
class hl_state_s(ctypes.Structure): pass
class hl_tok_s(ctypes.Structure): pass
class hl_worker_s(ctypes.Structure): pass
class input_s(ctypes.Structure): pass
class line_s(ctypes.Structure): pass
class log_file_s(ctypes.Structure): pass
class log_typ_conf_s(ctypes.Structure): pass
class match_s(ctypes.Structure): pass
class match_result_s(ctypes.Structure): pass
class match_set_s(ctypes.Structure): pass
class notification_s(ctypes.Structure): pass
class playbacker_s(ctypes.Structure): pass
class pool_s(ctypes.Structure): pass
class pool_job_s(ctypes.Structure): pass
class pool_thread_s(ctypes.Structure): pass
class replace_s(ctypes.Structure): pass
class search_s(ctypes.Structure): pass
class search_result_s(ctypes.Structure): pass
class status_tok_s(ctypes.Structure): pass
class table_s(ctypes.Structure): pass
class table_chain_s(ctypes.Structure): pass
class tty_s(ctypes.Structure): pass
class tty_conf_s(ctypes.Structure): pass
class undo_action_s(ctypes.Structure): pass
class undo_list_s(ctypes.Structure): pass
class win_s(ctypes.Structure): pass
class wm_s(ctypes.Structure): pass
class wm_update_s(ctypes.Structure): pass
class wm_updater_s(ctypes.Structure): pass
class yaml_obj_s(ctypes.Structure): pass
class mode_s(ctypes.Structure): pass
class pty_s(ctypes.Structure): pass
class framebuf_s(ctypes.Structure): pass

