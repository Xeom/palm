from buffers.menu2 import Menu2Buffer, Menu2Item
from hooks         import hooks
from server.server import add_global
from core.key      import Key
import weakref

@add_global()
class BuflistItem(Menu2Item):
    def __init__(self, buf, othbuf):
        self.ref  = weakref.ref(othbuf)
        super().__init__(buf)

    def get_content(self):
        buf = self.ref()
        if buf == None:
            return "-- R.I.P. --"
        else:
            return buf.get_name()

    def kill_buf(self):
        buf = self.ref()

        if buf == None:
            return

        buf.editor.kill_buf_soon(buf)

    def switch_to(self, winid):
        buf = self.ref()
        if buf == None:
            return

        buf.editor.win_assoc(buf, winid)

@add_global()
class BuflistBuffer(Menu2Buffer):
    def populate(self):
        items = []
        for buf in self.editor.bufs:
            items.append(BuflistItem(self, buf))

        items.sort(key=lambda i: i.get_content())

        self.set_items(items)

    def handle_key(self, key):
        if key == Key("enter"):
            self.enter()
        elif key == Key.char("x"):
            self.kill_buf()

    def enter(self):
        item = self.get_selected()
        item.switch_to(self.get_win())

    def kill_buf(self):
        item = self.get_selected()
        item.kill_buf()

    def init(self, *args):
        super().init(*args)
        self.populate()

        @hooks.attach("buf-assoc", "buf-new", "buf-post-kill", weak=True)
        def populate_all(*args):
            if not self.killed:
                self.populate()

        self.populate_all = populate_all
