from core.buffer   import Buffer
from editor        import log
from core.cur      import Cur
from core.bind     import Bind
from server.server import add_global

@add_global()
class Menu2Item(object):
    def __init__(self, buf, content=""):
        self.buf      = buf

        self.parent   = None
        self.next     = None
        self.linenum  = None
        self.subs     = []
        self.expandable = False
        self.expanded   = False
        self.content = content

    def get_linenum(self):
        return self.linenum

    def set_linenum(self, val):
        self.linenum = val
        self.update()

    def set_content(self, content):
        self.content = content
        self.update()

    def get_content(self):
        return self.content

    def is_visible(self):
        item = self.parent
        while item != None:
            if not item.expanded:
                return False

            item = item.parent

        return True

    def is_expandable(self):
        return self.expandable

    def is_expanded(self):
        return self.expanded

    def num_subs(self):
        return len(list(self))

    def num_visible_subs(self):
        return len(s for s in self if s.is_visible())

    def __iter__(self):
        for sub in self.subs:
            yield sub
            yield from sub

    def iter_visible(self):
        if self.is_visible():
            yield self

        for sub in self.subs:
            if sub.is_visible():
                yield from sub

    def open(self):
        if self.expanded:
            return

        self.expanded = True
        self.buf.number_lines()
        self.buf.ins(self.get_linenum() + 1, self.num_visible_subs())
        self.update_all()

    def close(self):
        if not self.expanded:
            return

        self.expanded = False
        self.buf.number_lines()
        self.buf.rem(self.get_linenum() + 1, self.num_visible_subs())
        self.update_all()

    def update_all(self):
        self.update()
        for sub in self.get_subs():
            sub.update()

    def get_prefix(self):
        rtn = ""

        if self.parent != None:
            rtn += self.parent.get_sub_prefix()

            if self.next == None:
                rtn += "'-"
            else:
                rtn += "|-"

        if self.is_expandable():
            if self.is_expanded():
                rtn += "{menu-bra}[{menu-minus}-{menu-bra}]"
            else:
                rtn += "{menu-bra}[{menu-plus}+{menu-bra}]"
        else:
            rtn += ">"

        return rtn

    def get_sub_prefix(self):
        rtn = ""

        if self.parent != None:
            rtn += self.parent.get_sub_prefix()
            if self.next == None:
                rtn += "  "
            else:
                rtn += "| "

        return rtn

    def get_line(self):
        return "{{menu-tree}}{0}{{menu-item}} {1}".format(self.get_prefix(), self.get_content())

    def set_nexts(self, nexts):
        prev = self
        for n in nexts:
            prev.next = n
            n.next = None
            prev = n

    def set_subs(self, subs):
        self.expandable = True
        self.subs = subs

        if len(self.subs) > 0:
            self.subs[0].set_nexts(self.subs[1:])
            for sub in subs:
                sub.parent = self

        self.close()
        self.update()

    def get_subs(self):
        return self.subs if self.is_expanded() else []

    def update(self):
        ln = self.get_linenum()
        if self.is_visible():
            self.buf.set(ln, self.get_line(), attrs=["UTF8", "FNT"])

@add_global()
class Menu2Buffer(Buffer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.items = []
        self.set_cur(Cur.at(self.get_first_ln(), typ="LINE"))

    def update_all(self):
        for item in self.items:
            item.update_all()

    def set_items(self, items):
        self.rem(self.get_first_ln(), len(self) - self.get_first_ln())
        self.items = items
        self.items[0].set_nexts(self.items[1:])
        self.number_lines()
        self.update_all()

    def iter_visible(self):
        for item in self.items:
            yield from item.iter_visible()

    def get_first_ln(self):
        return 0

    def number_lines(self):
        ln = self.get_first_ln()

        i = self.items[0]
        while i != None:
            i.linenum = ln
            ln       += 1

            if len(i.get_subs()) > 0:
                i = i.get_subs()[0]

            else:
                while i.next == None and i.parent != None:
                    i = i.parent

                i = i.next

    def get_selected(self):
        row = self.get_cur().pri.row
        row -= self.get_first_ln()
        items = list(self.iter_visible())
        if 0 <= row < len(items):
            return items[row]
        else:
            return None

    def bind_all(self):
        self.bind.bind(Bind(None, pybind="buf.handle_key(%k)"))

