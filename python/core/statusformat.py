"""A python wrapper of the status_format_s structure."""
from typing import Union, Optional

from lib import lib
from .newstruct import CStructure

from util.encode import bytify

from server.globals import add_global

@add_global()
class StatusFormat(CStructure):
    """A class for containing the configuration of a status format."""
    class_name = "status_format_s"

    init_funct = lib.status_format_init
    kill_funct = lib.status_format_kill

    def set(
            self,
            left:  Optional[Union[str, bytes]]=None,
            mid:   Optional[Union[str, bytes]]=None,
            right: Optional[Union[str, bytes]]=None
        ):
        """Set the status format.

        The status bar has three components, a left-aligned component,
        a right-aligned component, and a middle component.

        The middle component is left-aligned, but the right component
        will be drawn on top of the middle component if the window is
        not wide enough, and notifications will appear on top of the
        middle component.

        The leftmost component is always displayed.

        This function can be used to set any of the three components to
        a new value.

        Arguments:
            left  - If not None, set the left status bar format.
            mid   - If not None, set the mid status bar format.
            right - If not None, set the right status bar format.
        """
        if left is not None:
            left = bytify(left)
        if mid is not None:
            mid = bytify(mid)
        if right is not None:
            right = bytify(right)

        lib.status_format_set(self.ptr, left, mid, right)
