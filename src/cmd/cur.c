#include "buf/buf.h"
#include "buf/cur.h"
#include "cmdlang/helpers.h"
#include "log/log.h"

#include "cmd/cur.h"

NOUN(get_cur, "Get the current cursor position.",
    NO_ARGS)
{
    cur_s cur;
    TRY(buf_get_cur(ctx->buf, &cur));
    TRY(cmdlang_obj_init_cur(res, &cur));

    return PALM_OK;
}

VERB(set_cur,
    "Set the current cursor position.",
    ADD_REQ_ARG(CMDLANG_OBJ_CUR, "cur", "The new position of the cursor."))
{
    TRY(buf_set_cur(ctx->buf, &(args[0].val.cur)));
    return PALM_OK;
}

NOUN(cur_from_pos, "Create a cursor from two positions.",
    ADD_REQ_ARG(CMDLANG_OBJ_POS, "pri", "The primary position of the new cursor"))
{
    cur_s cur;
    cur_init(&cur);

    memcpy(&(cur.pri), &(args[0].val.pos), sizeof(pos_s));
    memcpy(&(cur.sec), &(args[1].val.pos), sizeof(pos_s));

    TRY(cmdlang_obj_init_cur(res, &cur));

    return PALM_OK;
}

NOUN(cur_min, "Get the minimum position in a cursor.",
    ADD_REQ_ARG(CMDLANG_OBJ_CUR, "cur", "The cursor to operate on."))
{
    TRY(cmdlang_obj_init_pos(res, cur_min(&(args[0].val.cur))));
    return PALM_OK;
}

NOUN(cur_max, "Get the maximum position in a cursor.",
    ADD_REQ_ARG(CMDLANG_OBJ_CUR, "cur", "The cursor to operate on."))
{
    TRY(cmdlang_obj_init_pos(res, cur_max(&(args[0].val.cur))));
    return PALM_OK;
}

NOUN(cur_pri, "Get the primary position in a cursor.",
    ADD_REQ_ARG(CMDLANG_OBJ_CUR, "cur", "The cursor to operate on."))
{
    TRY(cmdlang_obj_init_pos(res, &(args[0].val.cur.pri)));
    return PALM_OK;
}

NOUN(cur_sec, "Get the secondary position in a cursor.",
     ADD_REQ_ARG(CMDLANG_OBJ_CUR, "cur", "The cursor to operate on."))
{
    TRY(cmdlang_obj_init_pos(res, &(args[0].val.cur.sec)));
    return PALM_OK;
}

int cmd_cur_mount(cmd_lib_s *lib)
{
    MOUNT(get_cur); MOUNT(set_cur);

    MOUNT(cur_from_pos);

    MOUNT(cur_min); MOUNT(cur_max);
    MOUNT(cur_pri); MOUNT(cur_sec);

    return PALM_OK;
}
