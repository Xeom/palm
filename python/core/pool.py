import libtypes
from lib import lib
from .structure import Structure

class Pool(Structure):
    typ        = libtypes.pool_s
    init_funct = lib.pool_init
    kill_funct = lib.pool_kill
