from ctypes import *
import ctypes
from typing import List, Union

enumspec = Union[
    "Enum",
    int,
    List[
        Union[str, int]
    ], str
]

class Enum(Structure):
    _fields_ = [("val", c_int)]
    _documentation_ = None
    name = None

    def __init__(self, vals: enumspec):
        self.set(vals)

    def set(self, vals: enumspec):
        self.val = self.to_int(vals)

    def __eq__(self, oth):
        if not isinstance(oth, Enum):
            return False
        return self.val == oth.val

    def to_str(self) -> str:
        for attr, val in type(self).__dict__.items():
            if not isinstance(val, int):
                continue

            if val == self.val:
                return attr

        return str(self.val)

    def to_flags(self) -> List[Union[str, int]]:
        res: List[Union[str, int]] = []

        remaining = self.val
        for attr, val in type(self).__dict__.items():
            if not isinstance(val, int):
                continue

            # Do not make _ names!
            if attr.startswith("_"):
                continue

            if val & remaining == val:
                res.append(attr)
                remaining &= ~val

            if val == self.val:
                return [attr]

        if remaining != 0:
            res.append(remaining)

        if len(res) == 0:
            res.append(0)

        return res

    def has_flag(self, bit):
        return bool(self.val & self.to_int(bit))

    def __repr__(self):
        if self.name is not None:
            clsname = self.name
        else:
            clsname = type(self).__name__

        return "<{name}: ".format(name=clsname) + \
               "|".join(str(f) for f in self.to_flags()) + \
               ">"

    @classmethod
    def to_int(cls, vals: enumspec) -> int:
        if isinstance(vals, cls):
            return vals.val
        if vals == None:
            return 0
        if isinstance(vals, list):
            rtn = 0
            for v in vals:
                rtn |= cls.to_int(v)
            return rtn
        elif isinstance(vals, int):
            return vals
        elif isinstance(vals, str):
            return getattr(cls, vals)
        raise Exception("Don't know hot to convert type {} to Enum val".format(type(vals)))

def HACKTYPE(size, align):
    types = [
        c_char, c_short, c_int, c_long,
        c_longlong, c_void_p, c_wchar,
        c_bool
    ]

    class typ(Structure):
        pass

    fields = []

    sizeleft = size
    for t in types:
        if alignment(t) == align and sizeof(t) <= size:
            fields = [("base", t)]
            sizeleft -= sizeof(t)
            break

    if sizeleft > 0:
        fields += [("filler", c_char * sizeleft)]

    typ._fields_ = fields

    assert alignment(typ) == align
    assert sizeof(typ)    == size

    return typ

def TMPSTRUCT(fields):
    class tmp(Structure):
        _fields_ = fields

    return tmp

def TMPUNION(fields):
    class tmp(ctypes.Union):
        _fields_ = fields

    return tmp
