#if !defined(CMDLANG_OBJ_H)
# define CMDLANG_OBJ_H

# include "container/vec.h"

# include "buf/cur.h"

typedef enum
{
    CMDLANG_OBJ_NONE,
    CMDLANG_OBJ_STR,
    CMDLANG_OBJ_NUMBER,
    CMDLANG_OBJ_POS,
    CMDLANG_OBJ_CUR
} cmdlang_obj_typ_t;

struct cmdlang_obj
{
    cmdlang_obj_typ_t typ;
    union
    {
        vec_s str;
        long  number;
        cur_s cur;
        pos_s pos;
    } val;
};

int cmdlang_obj_init(cmdlang_obj_s *obj);

int cmdlang_obj_init_cpy(cmdlang_obj_s *obj, cmdlang_obj_s *src);

int cmdlang_obj_init_str(cmdlang_obj_s *obj, const char *str, size_t n);

int cmdlang_obj_init_number(cmdlang_obj_s *obj, long number);

int cmdlang_obj_init_pos(cmdlang_obj_s *obj, pos_s *pos);

int cmdlang_obj_init_cur(cmdlang_obj_s *obj, cur_s *cur);

void cmdlang_obj_kill(cmdlang_obj_s *obj);

int cmdlang_obj_to_str(cmdlang_obj_s *obj, vec_s *str);

#endif
