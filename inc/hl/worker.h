#if !defined(HL_WORKER_H)
# define HL_WORKER_H
# include "types.h"
# include "container/vec.h"
# include "util/pool.h"
# include <pthread.h>

struct hl_worker
{
    vec_s todo;

    buf_s *buf;
    hl_ctx_s *ctx;
    pthread_mutex_t mtx;

    pool_s *pool;
    pool_job_s job;
    bool jobrunning;
    bool active;
};

void hl_worker_init(hl_worker_s *worker, buf_s *buf, editor_s *editor);

void hl_worker_kill(hl_worker_s *worker);

void hl_worker_push_line(hl_worker_s *worker, size_t ln);

#endif
