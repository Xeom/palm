#include "editor.h"
#include "io/tty.h"
#include "util/macro.h"
#include "display/display.h"

static void *display_thread_main(void *arg);
static void display_switch_bufs(display_s *disp);
static void display_refresh(display_s *disp);

void display_init(display_s *disp, tty_s *tty)
{
    disp->tty = tty;

    framebuf_init(&(disp->framebufs[0]));
    framebuf_init(&(disp->framebufs[1]));
    framebuf_init(&(disp->framebufs[2]));

    disp->frontbuf = &(disp->framebufs[0]);
    disp->midbuf   = &(disp->framebufs[1]);
    disp->backbuf  = &(disp->framebufs[2]);

    disp->needsrefresh = true;
    disp->alive        = false;

    pthread_mutex_init(&(disp->frontbuf_mtx), NULL);
    pthread_cond_init (&(disp->refresh), NULL);

    display_resize(disp);
    display_resume(disp);
}

void display_kill(display_s *disp)
{
    display_pause(disp);

    pthread_mutex_destroy(&(disp->frontbuf_mtx));

    framebuf_kill(&(disp->framebufs[0]));
    framebuf_kill(&(disp->framebufs[1]));
    framebuf_kill(&(disp->framebufs[2]));
}

void display_pause(display_s *disp)
{
    if (!(disp->alive))
        return;

    WITH_MTX(&(disp->frontbuf_mtx))
    {
        disp->alive = 0;
        display_refresh(disp);
    }

    pthread_join(disp->thread, NULL);
}

void display_resume(display_s *disp)
{
    if (disp->alive)
        return;

    disp->alive = true;
    pthread_create(&(disp->thread), NULL, display_thread_main, disp);
}

static void display_switch_bufs(display_s *disp)
{
    framebuf_s *tmp;

    WITH_MTX(&(disp->frontbuf_mtx))
    {
        tmp            = disp->frontbuf;
        disp->frontbuf = disp->backbuf;
        disp->backbuf  = disp->midbuf;
        disp->midbuf   = tmp;

        framebuf_cpy(disp->frontbuf, disp->midbuf);
    }
}

void display_refresh_all(display_s *disp)
{
    WITH_MTX(&(disp->frontbuf_mtx))
    {
        disp->needsrefresh = true;
        disp->refreshall   = true;
        pthread_cond_signal(&(disp->refresh));
    }
}

static void display_refresh(display_s *disp)
{
    disp->needsrefresh = true;
    pthread_cond_signal(&(disp->refresh));
}

void display_resize(display_s *disp)
{
    WITH_MTX(&(disp->frontbuf_mtx))
    {
        framebuf_resize(disp->frontbuf, disp->tty->w, disp->tty->h);
    
        disp->dims.w = disp->tty->w;
        disp->dims.h = disp->tty->h;
        disp->dims.x = 0;
        disp->dims.y = 0;
    }

    display_refresh_all(disp);
}

void display_dims(display_s *disp, rect_s *dims)
{
    WITH_MTX(&(disp->frontbuf_mtx))
    {
        memcpy(dims, &(disp->dims), sizeof(rect_s));
    }
}

void display_stamp(display_s *disp, rect_s *dstrect, framebuf_s *src, rect_s *srcrect)
{
    WITH_MTX(&(disp->frontbuf_mtx))
    {
        framebuf_stamp(disp->frontbuf, dstrect, src, srcrect);
        display_refresh(disp);
    }
}

void display_set(display_s *disp, rect_s *rect, chr_s *mem)
{
    WITH_MTX(&(disp->frontbuf_mtx))
    {
        framebuf_set(disp->frontbuf, rect, mem);
        display_refresh(disp);
    }
}

void display_fill(display_s *disp, rect_s *rect, chr_s *c)
{
    WITH_MTX(&(disp->frontbuf_mtx))
    {
        framebuf_fill(disp->frontbuf, rect, c);
        display_refresh(disp);
    }
}

static void *display_thread_main(void *arg)
{
    display_s *disp;
    disp = arg;

    editor_mask_signals();

    while (disp->alive)
    {
        bool refreshall;
        WITH_MTX(&(disp->frontbuf_mtx))
        {
            if (!disp->needsrefresh)
                pthread_cond_wait(&(disp->refresh), &(disp->frontbuf_mtx));

            disp->needsrefresh = false;
        }

        if (!disp->alive)
            break;

        display_switch_bufs(disp);

        WITH_MTX(&(disp->frontbuf_mtx))
        {
            refreshall = disp->refreshall;
            disp->refreshall = false;
        }

        if (refreshall)
            framebuf_print_diff(disp->midbuf, NULL, disp->tty);
        else
            framebuf_print_diff(disp->midbuf, disp->backbuf, disp->tty);
    }

    return NULL;
}
