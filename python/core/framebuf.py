import libtypes
import ctypes

from lib         import lib
from util.encode import strify, bytify

from .rect import Rect
from .chr  import Chr
from .structure import Structure

class FrameBuf(Structure):
    typ = libtypes.framebuf_s
    init_funct = lib.framebuf_init
    kill_funct = lib.framebuf_kill

    def copy(self) -> "FrameBuf":
        new = FrameBuf()
        lib.framebuf_cpy(new.ptr, self.ptr)
        return new

    def dims_rect(self) -> Rect:
        rect = Rect()
        lib.framebuf_dims_rect(self.ptr, rect.ptr)
        return rect

    def trim_rect(self, rect: Rect):
        lib.framebuf_trim_rect(self.ptr, rect.ptr)

    def get(self, x: int, y: int) -> Chr:
        return Chr.copy_from(lib.framebuf_get(self.ptr, x, y))

    def resize(self, w: int, h: int):
        lib.framebuf_resize(self.ptr, w, h)

    def stamp(self, dstrect: Rect, src: FrameBuf, srcrect: Rect):
        lib.framebuf_stamp(self.ptr, dstrect.ptr, src.ptr, srcrect.ptr)

    def set(self, x: int, y: int, c: Chr):
        self.set_rect(Rect(x=x, y=y), [c])

    def set_rect(self, rect: Rect, chrs: Sequence[Chr]):
        buflen = rect.w * rect.h
        buf = (libtypes.chr_s * buflen)()

        for i, c in enumerate(sequence):
            c.cpy_to(ctypes.pointer(buf[i]))

        if i < buflen:
            raise ValueError("Not enough characters to fill out buffer")

        lib.framebuf_set(self.ptr, rect.ptr, ctypes.byref(buf))

    def fill(self, rect: Rect, c: Chr):
        lib.framebuf_fill(self.ptr, rect.ptr, c.ptr)
