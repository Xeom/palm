#include "text/indent.h"
#include "text/chr.h"
#include "buf/buf.h"
#include "log/log.h"

indent_t indent_from_col(vec_s *chrs, chrconf_s *conf, col_t col)
{
    indent_t rtn;
    col_t ctr;

    rtn = 0;
    for (ctr = 0; ctr < col; ++ctr)
    {
        chr_s *chr;
        chr = vec_get(chrs, ctr);

        if (!chr) break;

        rtn += chr_expanded_width(chr, rtn, conf);
    }

    return rtn;
}

col_t indent_to_col(vec_s *chrs, chrconf_s *conf, indent_t indent)
{
    indent_t ctr;
    col_t rtn;

    rtn = 0;
    for (ctr = 0; ctr < indent; ++rtn)
    {
        chr_s *chr;
        chr = vec_get(chrs, rtn);

        if (!chr) break;

        ctr += chr_expanded_width(chr, ctr, conf);
    }

    return rtn;
}

col_t byte_off_to_col(vec_s *chrs, chrconf_s *conf, size_t byteoff)
{
    size_t ctr;
    col_t rtn;

    rtn = 0;
    for (ctr = 0; ctr < byteoff; ++rtn)
    {
        chr_s *chr;
        chr = vec_get(chrs, rtn);

        if (!chr) break;

        ctr += chr_num_bytes(chr);
    }

    return rtn;
}

size_t byte_off_from_col(vec_s *chrs, chrconf_s *conf, col_t col)
{
    col_t ctr;
    size_t rtn;

    rtn = 0;
    for (ctr = 0; ctr < col; ++col)
    {
        chr_s *chr;
        chr = vec_get(chrs, ctr);

        if (!chr) break;

        rtn += chr_num_bytes(chr);
    }

    return rtn;
}
