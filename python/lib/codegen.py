#!/usr/bin/env python3
import sys
import time
import argparse

import clang.cindex
from clang.cindex import CursorKind, TypeKind, LinkageKind

import shlex

from typing       import Optional
from os.path      import realpath
from dataclasses  import dataclass

forbiddenfields = [
    "ptr",
    "copy",
    "kill",
    "init"
]

def indent(text, depth=1):
    indentstr = "    " * depth

    return "\n".join(
        (indentstr + s) if s else ""
        for s in text.split("\n")
    )

def iter_kind_from_cur(cur, kind):
    if not isinstance(kind, (list, tuple)):
        kind = (kind, )

    for probe in cur.get_children():
        if probe.kind in kind:
            yield probe

ctypenames = {
    TypeKind.VOID:       "c_void",
    TypeKind.BOOL:       "c_bool",
    TypeKind.UCHAR:      "c_ubyte",
    TypeKind.SCHAR:      "c_char",
    TypeKind.CHAR_S:     "c_char",
    TypeKind.WCHAR:      "c_wchar",
    TypeKind.SHORT:      "c_short",
    TypeKind.USHORT:     "c_ushort",
    TypeKind.INT:        "c_int",
    TypeKind.UINT:       "c_uint",
    TypeKind.LONG:       "c_long",
    TypeKind.ULONG:      "c_ulong",
    TypeKind.LONGLONG:   "c_longlong",
    TypeKind.ULONGLONG:  "c_ulonglong",
    TypeKind.FLOAT:      "c_float",
    TypeKind.DOUBLE:     "c_double",
    TypeKind.LONGDOUBLE: "c_longdouble"
}

pytypealias = {
    "uint8_t":          "c_ubyte",
    "POINTER(uint8_t)": "POINTER(c_ubyte)",
    "c_void":           "None",
    "POINTER(c_void)":  "c_void_p",
    "POINTER(POINTER(c_void))":  "POINTER(c_void_p)",
    "POINTER(c_char)":  "c_char_p",
    "POINTER(POINTER(c_char))":  "POINTER(c_char_p)",
    "POINTER(c_wchar)": "c_wchar_p",
    "POINTER(pcre2_code_8)": "c_void_p",
    "POINTER(pcre2_match_data_8)": "c_void_p"
}

ctypealias = {
    "struct sigaction": "sigaction_s",
    "struct termios":   "termios_s",
    "size_t":           "c_size_t",
    "uint64_t":         "c_uint64",
    "FILE":             "FILE_s",
    "atomic_bool":      "c_bool"
}

def type_to_hack(typ):
    return f"HACKTYPE({typ.get_size()}, {typ.get_align()})"

def type_to_python(typ):
    fmt  = "{}"
    name = None

    if args.hack:
        return type_to_hack(typ)

    while typ.kind in (
        TypeKind.POINTER,
        TypeKind.CONSTANTARRAY,
        TypeKind.INCOMPLETEARRAY,
        TypeKind.ELABORATED
    ):
        if typ.kind == TypeKind.POINTER:
            typ  = typ.get_pointee()

            if typ.kind != TypeKind.FUNCTIONPROTO:
                fmt = fmt.format("POINTER({})")

        if typ.kind == TypeKind.ELABORATED:
            typ = typ.get_named_type()

        if typ.kind == TypeKind.INCOMPLETEARRAY:
            typ = typ.get_array_element_type()

            if typ.kind != TypeKind.FUNCTIONPROTO:
                fmt = fmt.format("POINTER({})")

        if typ.kind == TypeKind.CONSTANTARRAY:
            size = typ.get_array_size()
            typ  = typ.get_array_element_type()
            fmt = fmt.format(f"({{}} * {size})")

    if typ.kind == TypeKind.FUNCTIONPROTO:
        name = "CFUNCTYPE(" + \
            ", ".join(
                [type_to_python(typ.get_result())] +
                [type_to_python(t) for t in typ.argument_types()]
            ) + ")"

    elif typ.kind in ctypenames:
        name = ctypenames[typ.kind]

    elif typ.spelling in ctypealias:
        name = ctypealias[typ.spelling]

    elif typ.kind == TypeKind.TYPEDEF:
        name = typ.get_typedef_name()

    elif typ.kind == TypeKind.RECORD:
        name = "TMPUNION([" + \
            ", ".join(
                f"({repr(f.spelling)}, {type_to_python(f.type)})" 
                for f in typ.get_fields()
            ) \
           + "])"

    elif fmt.startswith("POINTER("):
        name = "c_void"

    else:
        print(typ.kind)
        name = type_to_hack(typ)
        print(f"Warning: Could not handle type {typ.spelling}", file=sys.stderr)
        print(f" {typ.kind}", file=sys.stderr)
        print(f" Replaced with {name}", file=sys.stderr)

    name = fmt.format(name)

    if name in pytypealias:
        name = pytypealias[name]

    return name

class CurInfo(object):
    typ = "C-Code"
    def __init__(self, name, cur):
        self.cur   = cur
        self.fname = cur.location.file.name
        self.line  = cur.location.line
        self.name  = name
        self.deps  = []
        self.size  = cur.type.get_size()
        self.align = cur.type.get_align()

    def gen_docs(self):
        raw = self.cur.raw_comment

        if raw:
            lines = []
            for line in raw.split("\n"):
                line = line.strip().strip("/*<").rstrip()

                if line.startswith(" "):
                    line = line[1:]

                lines.append(line + "\n")

            while lines[-1].strip() == "":
                lines.pop(-1)

            yield from lines
            yield "\n"

    def gen_documentation_code(self):
        yield (
               f"CodeDocumentation(\n"
               f"    {repr(self.typ)},\n"
               f"    {repr(realpath(self.fname))}, {repr(self.line)},\n"
               f"    {repr(self.name)},\n"
               f"    (\n"
              )

        for comp in self.gen_docs():
            yield f"        {repr(comp)}\n"

        yield (
               f"    )\n"
               f")\n"
              )

@dataclass
class EnumFieldInfo(object):
    val:  int
    docs: Optional[str] = None

class EnumInfo(CurInfo):
    typ = "Enum"

    def __init__(self, name, cur):
        super().__init__(name, cur)
        self.fields = {}
        self.get_vals()

    def get_enum_items(self):
        yield from iter_kind_from_cur(self.cur, CursorKind.ENUM_CONSTANT_DECL)

    @staticmethod
    def enum_name(name):
        if name[0] in "0123456789":
            return "_" + name

        return name

    def get_vals(self):
        fields = {}

        for enumitem in self.get_enum_items():
            name  = enumitem.spelling
            field = EnumFieldInfo(
                val=enumitem.enum_value,
                docs=enumitem.brief_comment
            )
            fields[name] = field

        prefixlen = 0
        for letters in zip(*list(fields.keys())):
            if len(set(letters)) == 1:
                prefixlen += 1
            else:
                break

        if prefixlen >= min(len(k) for k in fields.keys()):
            prefixlen = 0

        self.fields = {
            self.enum_name(k[prefixlen:]): v for k, v in fields.items()
        }

    def gen_code(self):
        yield f"class {self.name}(Enum):\n"
        yield indent(
            "_documentation_ = " + "".join(self.gen_documentation_code())
        )

        for k, v in self.fields.items():
            yield f"    {k} = {v.val}\n"


    def gen_docs(self):
        yield from super().gen_docs()

        yield "Values:\n"

        maxlen = max(len(k) for k in self.fields.keys())
        for k, v in self.fields.items():
            yield indent(f"{k.ljust(maxlen)} = {v.val}\n")

            if v.docs is not None:
                yield indent(v.docs, 2) + "\n\n"

@dataclass
class StructFieldInfo(object):
    name:   str
    pytype: str
    ctype:  str
    size:   int
    off:    int
    docs:   Optional[str] = None

class StructInfo(CurInfo):
    typ = "Structure"

    def __init__(self, name, cur):
        super().__init__(name, cur)
        self.docs   = {}
        self.fields = []

        self.get_vals()

    def get_fields(self):
        yield from iter_kind_from_cur(self.cur, [
            CursorKind.FIELD_DECL,
            CursorKind.UNION_DECL
        ])

    def get_vals(self):
        self.fields = []
        for field in self.get_fields():
            if field.type.kind in (
                    TypeKind.INCOMPLETEARRAY,
                    TypeKind.RECORD
                ):
                continue

            pytype = type_to_python(field.type)

            name = field.spelling

            if name in forbiddenfields:
                name = "_" + name

            self.fields.append(
                StructFieldInfo(
                    name  = name,
                    ctype = field.type.spelling,
                    pytype = pytype,
                    docs   =  field.brief_comment,
                    size   = field.type.get_size(),
                    off = field.get_field_offsetof()
                )
            )

            self.add_dep(field.type)

    def add_dep(self, deptyp):
        if deptyp.kind == TypeKind.CONSTANTARRAY:
            self.add_dep(deptyp.get_array_element_type())
        elif deptyp.kind == TypeKind.ELABORATED:
            for field in deptyp.get_named_type().get_fields():
                self.add_dep(field.type)
        else:
            self.deps.append(type_to_python(deptyp))

    def gen_docs(self):
        yield from super().gen_docs()

        if len(self.fields) == 0:
            print(f"Warning: {self.name} has no fields", file=sys.stderr)
            print(f" at {self.cur.location}", file=sys.stderr)
            maxlen = 0
        else:
            maxlen = max(len(f.name) for f in self.fields)

        yield "Fields:\n"

        yield indent(f"{'Name'.ljust(maxlen)} => C type / Python Type\n\n")

        for f in self.fields:
            yield indent(f"{f.name.ljust(maxlen)} => {f.ctype} / {f.pytype}\n")

            if f.docs is not None:
                yield indent(f.docs, 2) + "\n\n"


    def gen_code(self):
        off    = 0
        wasted = self.size - sum(f.size for f in  self.fields)


#        print(f"{self.name}", file=sys.stderr)
#        print("  Off  Size Waste", file=sys.stderr)
#        for f in self.fields:
#            print(f"{f.off:5} {f.size:5} {f.off-off:5} {f.name}", file=sys.stderr)
#            off = f.off + f.size * 8
#        print("\n", file=sys.stderr)
        yield f"{self.name}._fields_ = [\n"

        for finfo in self.fields:
            yield f"    ({repr(finfo.name)}, {finfo.pytype}),\n"

        yield "]\n"

        yield f"{self.name}._documentation_ = "
        yield from self.gen_documentation_code()

        yield f"assert sizeof   ({self.name}) == {self.size}\n"
        yield f"assert alignment({self.name}) == {self.align}\n"

class TypedefInfo(CurInfo):
    typ = "Typedef"

    def gen_docs(self):
        yield from super().gen_docs()

        yield "Typedef:\n"
        yield f"{self.name} => {self.cur.type.spelling}\n"

    def gen_code(self):
        yield f"{self.name} = "
        yield type_to_python(self.cur.underlying_typedef_type)
        yield "\n"
        yield from self.gen_documentation_code()

class FunctInfo(CurInfo):
    typ = "CFunction"

    def gen_docs(self):
        yield from super().gen_docs()

        args = list(self.cur.get_arguments())

        yield  "Function:\n"
        yield f"{self.cur.type.get_result().spelling} "
        yield f"{self.cur.spelling}"

        if len(args) == 0:
            yield f"(void)\n"
        else:
            yield "(\n"
            for arg in args:
                yield f"    {arg.type.spelling} {arg.displayname}\n"
            yield ")\n"

    def gen_code(self):
        typ = self.cur.type
        yield f"if hasattr(lib, {repr(self.name)}):\n"
        yield f"    lib.{self.name}.restype  = "
        yield type_to_python(typ.get_result())
        yield "\n"

        yield f"    lib.{self.name}.argtypes = ["
        yield ", ".join(
            type_to_python(a) for a in typ.argument_types()
        )
        yield f"    ]\n"

        yield from self.gen_documentation_code()

def get_curs_of_kind(files, kind):
    index = clang.cindex.Index.create()
    source = "\n".join(f"#include <{f}>" for f in files)
    tu = index.parse(
        "tmp.h",
        args=shlex.split(" ".join(args.carg)),
        unsaved_files=(("tmp.h", source),)
    )

    yield from iter_kind_from_cur(tu.cursor, kind)

def get_typedefs(files):
    yield from get_curs_of_kind(files, CursorKind.TYPEDEF_DECL)

def get_functs(files):
    yield from get_curs_of_kind(files, CursorKind.FUNCTION_DECL)

def get_structs(files):
    yield from get_curs_of_kind(files, CursorKind.STRUCT_DECL)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--carg",
        action="append",
        default=[],
        help="Options to pass to clang"
    )
    parser.add_argument(
        "hfile",
        nargs="+",
        help="Files to parse"
    )
    parser.add_argument(
        "--name",
        action="append",
        help="The names of the things we want to extract",
        default=[]
    )
    parser.add_argument(
        "--head",
        action="append",
        help="Lines of output to prepend to output",
        default=[]
    )
    parser.add_argument(
        "--functs",
        action="store_true",
        help="Document functions!",
        default=[]
    )
    parser.add_argument(
        "--hack",
        action="store_true",
    )

    args = parser.parse_args()

    print(f"# Autogenerated by {__file__} at {time.ctime()}\n")
    print("from ctypes import *")

    for l in args.head:
        print(l, end="" if l.endswith("\n") else "\n")

    if len(args.head):
        print("")

    defs = { n: None for n in args.name }
    hfile = [realpath(f) for f in args.hfile]

    if args.functs:
        for funct in get_functs(args.hfile):
            if funct.linkage == LinkageKind.INTERNAL:
                continue

            if realpath(funct.location.file.name) not in hfile:
                continue

            name = funct.spelling
            defs[name] = FunctInfo(name, funct)

    else:
        for typedef in get_typedefs(args.hfile):
            underlying = typedef.underlying_typedef_type
            decl       = underlying.get_declaration()

            name = typedef.spelling
    
            if name not in defs:
                continue

            if defs[name] is not None:
                print(f"Warning: Double definition of {name}", file=sys.stderr)
                print(f" Original was at {defs[name].cur.location}", file=sys.stderr)
                print(f" New was at {typedef.location}", file=sys.stderr)
                continue

            if decl and decl.kind == CursorKind.STRUCT_DECL and not args.hack:
                defs[name] = StructInfo(name, decl)

            elif decl and decl.kind == CursorKind.ENUM_DECL and not args.hack:
                defs[name] = EnumInfo(name, decl)

            else:
                defs[name] = TypedefInfo(name, typedef)

    def gen_code(k):
        if k in done or k not in defs:
            return

        v = defs[k]

        if v is None:
            print(f"Warning: Could not find {k}", file=sys.stderr)
            return

        for dep in v.deps:
            gen_code(dep)

        print("".join(v.gen_code()))
        done.add(k)


    done = set()
    for k in defs:
        gen_code(k)
