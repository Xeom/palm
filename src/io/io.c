#include "io/io.h"
#include "log/log.h"
#include "util/string.h"
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>

int io_default_poll_tout = 500;
int io_default_size      = 64;

int io_default_file_mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
int io_default_dir_mode  = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;

size_t io_errbuf_len = IO_ERRBUF_LEN;
size_t path_max = PATH_MAX;

#define IO_ERR(buf, ...) \
    do { \
        if (buf) \
            snprintf(buf, IO_ERRBUF_LEN, __VA_ARGS__); \
        else \
            LOG(NOTICE, INFO, "IO Error " __VA_ARGS__); \
    } while (0)

static int io_make_pollfd(io_file_info_s *f, struct pollfd *conf, char *errbuf)
{
    short events;
    conf->fd = f->fd;

    events = 0;

    if (f->awaitin || f->readvec)
        events |= POLLIN;

    if (f->awaitout || (f->writevec && vec_len(f->writevec)))
        events |= POLLOUT;

    if (events == 0 && !f->writevec)
        events |= POLLOUT | POLLIN;

    events |= POLLHUP | POLLERR;

    conf->events = events;

    return 0;
}

static int io_handle_pollfd(io_file_info_s *f, struct pollfd *conf, char *errbuf)
{
    long n;

    f->status = 0;
    if (conf->revents & POLLNVAL)
    {
        f->status |= IO_FILE_EOF | IO_FILE_CLOSED;
        return 0;
    }

    if (conf->revents & POLLERR)
        f->status |= IO_FILE_EOF;

    /* HUP + IN does not constitute an EOF. This is necessary *
     * when reading a pipe from another process!              */
    if ((conf->revents & POLLHUP) && !(conf->revents & POLLIN))
        f->status |= IO_FILE_EOF;
    if (conf->revents & POLLIN)
        f->status |= IO_FILE_READABLE;
    if (conf->revents & POLLOUT)
        f->status |= IO_FILE_WRITABLE;

    if (f->ntoread == 0)
        n = io_default_size;
    else
        n = f->ntoread;

    if ((conf->revents & POLLIN) && f->readvec && n > 0)
    {
        switch (io_read_to_vec(f->fd, f->readvec, (size_t *)&n, errbuf))
        {
        case -1: return -1;
        case  0: break;
        case  1: f->status |= IO_FILE_EOF; break;
        }
        if (f->ntoread > 0)
        {
            if (n == 0)
                f->ntoread = -1;
            else
                f->ntoread = n;
        }
    }

    if ((conf->revents & POLLOUT) && f->writevec)
    {
        switch (io_write_from_vec(f->fd, f->writevec, errbuf))
        {
        case -1: return -1;
        case  0: break;
        case  1: break;
        }
    }

    return 0;
}

int io_read_to_vec(int fd, vec_s *buf, size_t *n, char *errbuf)
{
    ssize_t res;
    void *dst;

    dst = vec_add(buf, *n, NULL);
    res = read(fd, dst, *n);

    /* Error */
    if (res < 0)
    {
        vec_del(buf, vec_len(buf) - *n, *n);
        IO_ERR(errbuf, "Could not read file: %s", strerror(errno));
        return -1;
    }
    /* EOF */
    else if (res == 0)
    {
        vec_del(buf, vec_len(buf) - *n, *n);
        return 1;
    }
    else if (res <= (ssize_t)n)
    {
        *n -= res;
        vec_del(buf, vec_len(buf) - *n, *n);
        return 0;
    }
    else
    {
        vec_del(buf, vec_len(buf) - *n, *n);
        IO_ERR(errbuf, "Got unexpectedly many characters.");
        return -1;
    }
}

int io_write_from_vec(int fd, vec_s *buf, char *errbuf)
{
    ssize_t res;

    if (vec_len(buf) == 0)
        return 1;

    res = write(fd, vec_get(buf, 0), vec_len(buf));

    if (res < 0)
    {
        IO_ERR(errbuf, "Could not write to file: %s", strerror(errno));
        return -1;
    }
    else if (res == 0)
    {
        IO_ERR(errbuf, "File did not accept any bytes");
        return -1;
    }
    else
    {
        vec_del(buf, 0, res);
        if (vec_len(buf) == 0)
            return 1;
        else
            return 0;
    }
}

int io_poll_files(io_file_info_s files[], int nfiles, int toutms, char *errbuf)
{
    int ind;
    io_file_info_s *ptrvec[nfiles];

    for (ind = 0; ind < nfiles; ++ind)
        ptrvec[ind] = &(files[ind]);

    return io_poll_files_by_ptr(ptrvec, nfiles, toutms, errbuf);
}

int io_poll_files_by_ptr(io_file_info_s *files[], int nfiles, int toutms, char *errbuf)
{
    bool   polling;
    int    res, ind;
    struct pollfd confs[nfiles];

    if (toutms < 0)
        toutms = io_default_poll_tout;

    for (ind = 0; ind < nfiles; ++ind)
        if (io_make_pollfd(files[ind], &confs[ind], errbuf) == -1)
            return -1;

    polling = true;
    while (polling)
    {
        res = poll(confs, nfiles, toutms);
        if (res < 0 && errno == EINTR)
            continue;
        else
            polling = false;
    }

    if (res > 0)
    {
        for (ind = 0; ind < nfiles; ++ind)
            if (io_handle_pollfd(files[ind], &confs[ind], errbuf) == -1)
                return -1;

        return 1;
    }
    else if (res == 0)
    {
        for (ind = 0; ind < nfiles; ++ind)
            if (io_handle_pollfd(files[ind], &confs[ind], errbuf) == -1)
                return -1;

        /* Timeout */
        return 0;
    }
    else
    {
        /* Error */
        IO_ERR(errbuf, "Could not poll: %s", strerror(errno));
        return -1;
    }
}

int io_set_nonblk(int fd, char *errbuf)
{
    int flags, rtn;
    flags = fcntl(fd, F_GETFD);
    if (flags == -1)
    {
        IO_ERR(errbuf, "Could not get fd(%d) flags: %s", fd, strerror(errno));
        return -1;
    }

    flags |= O_NONBLOCK;
    rtn = fcntl(fd, F_SETFD, flags);

    if (rtn == -1)
    {
        IO_ERR(errbuf, "Could not set fd(%d) flags: %s", fd, strerror(errno));
        return -1;
    }

    return 0;
}

int io_is_dir(const char *path, char *errbuf)
{
    struct stat statbuf;

    if (stat(path, &statbuf) == -1)
    {
        if (errno != ENOENT && errno != ENOTDIR)
        {
            IO_ERR(errbuf, "Error stat-ing '%s': %s", path, strerror(errno));
            return -1;
        }

        return 0;
    }

    if (statbuf.st_mode & S_IFDIR)
        return 1;
    else
        return 0;
}

int io_is_file(const char *path, char *errbuf)
{
    struct stat statbuf;
    if (stat(path, &statbuf) == -1)
    {
        if (errno != ENOENT && errno != ENOTDIR)
        {
            IO_ERR(errbuf, "Error stat-ing '%s': %s", path, strerror(errno));
            return -1;
        }

        return 0;
    }

    if (statbuf.st_mode & S_IFREG)
        return 1;
    else
        return 0;
}

#define IO_PATH_SEP_CHR '/'
static int io_path_add_sep(char *dst, char **iter, char *end, char *errbuf)
{
    /* At the start of the path, we need no sep. */
    if (*iter == dst)
        return 0;

    if (*iter >= end)
    {
        IO_ERR(errbuf, "Could not add path separator - no room");
        return -1;
    }

    while (*iter > dst && *((*iter) - 1) == IO_PATH_SEP_CHR)
    {
        *iter -= 1;
    }

    *((*iter)++) = IO_PATH_SEP_CHR;
    **iter       = '\0';

    return 0;
}

int io_path_join(char *dst, const char *component, char *errbuf)
{
    char *end;
    char *iter;

    end = dst + PATH_MAX - 1;
    iter = dst + strlen(dst);

    if (io_path_add_sep(dst, &iter, end, errbuf) == -1)
        return -1;

    /* Iterate across the characters in the component */
    for (;iter < end && *component; ++component)
    {
        if (*component == IO_PATH_SEP_CHR)
        {
            if (io_path_add_sep(dst, &iter, end, errbuf) == -1)
                return -1;
        }
        else
        {
            *(iter++) = *component;
        }
    }

    /* Check that we haven't reached the end of the path. */
    if (iter == end)
    {
        IO_ERR(errbuf, "Tried to build a path that was too long.");
        return -1;
    }

    /* Add a final separator, the path now should end in '/' */
    if (io_path_add_sep(dst, &iter, end, errbuf) == -1)
        return -1;

    /* Remove the final separator, unless it is a starting / */
    if (iter > dst + 1)
        *(--iter) = '\0';

    return 0;
}

int io_gethome(char *dst, char *errbuf)
{
    size_t size;
    char *buf;
    uid_t uid;

    struct passwd pwdbuf;
    struct passwd *pwd;

    uid = getuid();

    /* The getpwuid_r function is a bit odd...    *
     *                                            *
     * We need to provide it both a struct passwd *
     * buffer, and a buffer to store all of its   *
     * strings in.                                *
     *                                            *
     * Here, we try allocating bigger buffers     * 
     * until it works.                            */
    buf = NULL;
    for (size = 1024; size < 65536; size *= 2)
    {        
        buf = realloc(buf, size);
        if (!buf)
        {
            IO_ERR(errbuf, "Could not allocate a buffer to contain home dir.");
            return -1;
        }

        int res;
        res = getpwuid_r(uid, &pwdbuf, buf, size, &pwd);

        switch (res)
        {
        case 0:
            /* If the current user is invalid. */
            if (!pwd)
            {
                strcpy(dst, "/");
                return 0;
            }
            else
            {
                strcpy(dst, pwd->pw_dir);
                return 0;
            }
        case ERANGE: /* ERANGE is returned when we have insufficient buffering. */
            break;
        default:
            IO_ERR(errbuf, "Could not get home directory: %s", strerror(errno));
            return -1;
        }
    }

    IO_ERR(errbuf, "Stopped trying to allocate buffer to contian home dir.");
    return -1;
}

int io_expanduser(char *dst, const char *path, char *errbuf)
{
    if (path[0] == '~')
    {
        if (io_gethome(dst, errbuf) == -1)
            return -1;

        if (io_path_join(dst, &path[1], errbuf) == -1)
            return -1;

        return 0;
    }
    else
    {
        if (!safe_strncpy(dst, path, PATH_MAX - 1))
        {
            IO_ERR(errbuf, "Could not copy path to buffer");
            return -1;
        }

        return 0;
    }
}

int io_realpath(char *dst, const char *path, char *errbuf)
{
    char *res;
    char tmp[PATH_MAX];
    vec_s components;

    if (strlen(path) >= PATH_MAX)
    {
        IO_ERR(errbuf, "Path '%s' too long", path);
        return -1;
    }

    if (io_expanduser(tmp, path, errbuf))
        return -1;

    vec_init(&components, PATH_MAX);

    while (!(res = realpath(tmp, dst)))
    {
        if (errno == ENOENT || errno == ENOTDIR)
        {
            char  prev[PATH_MAX];
            char *component;

            component = vec_app(&components, NULL);
            if (io_basename(component, tmp, errbuf) == -1)
                break;

            strcpy(prev, tmp);

            if (io_dirname(tmp, prev, errbuf) == -1)
                break;

            if (strcmp(tmp, prev) == 0)
            {
                IO_ERR(errbuf, "Ran out of path looking for valid root for '%s'", path);
                break;
            }
        }
        else
        {
            IO_ERR(errbuf, "Error getting realpath '%s': %s", path, strerror(errno));
            break;
        }
    }

    if (res)
    {
        VEC_RFOREACH(&components, char *, c)
        {
            if (io_path_join(dst, c, errbuf) == -1)
            {
                res = NULL;
                break;
            }        
        }
    }

    vec_kill(&components);

    if (res)
        return 0;
    else
        return -1;
}

int io_dirname(char *dst, const char *path, char *errbuf)
{
    char *dirpath;

    if (strlen(path) >= PATH_MAX)
    {
        IO_ERR(errbuf, "Path '%s' too long", path);
        return -1;
    }

    if (!safe_strncpy(dst, path, PATH_MAX))
    {
        IO_ERR(errbuf, "Could not copy path to buffer");
        return -1;
    }

    dirpath = dirname(dst);

    if (dirpath != dst)
        memmove(dst, dirpath, strlen(dirpath) + 1);

    return 0;
}

int io_basename(char *dst, const char *path, char *errbuf)
{
    char *basepath;

    if (strlen(path) > PATH_MAX)
    {
        IO_ERR(errbuf, "Path '%s' too long", path);
        return -1;
    }

    if (!safe_strncpy(dst, path, PATH_MAX))
    {
        IO_ERR(errbuf, "Could not copy path to buffer");
        return -1;
    }

    basepath = basename(dst);

    if (basepath != dst)
        memmove(dst, basepath, strlen(basepath) + 1);

    return 0;
}

int io_relpath(char *dst, const char *path, const char *root, char *errbuf)
{
    char realroot[PATH_MAX], realpath[PATH_MAX];
    char *iter;

    if (io_realpath(realroot, root, errbuf) == -1)
        return -1;

    if (io_realpath(realpath, path, errbuf) == -1)
        return -1;

    /* Append a final slash to the realroot, so that it won't *
     * consider apples/bee a prefix of apples/beehive         */
    iter = realroot + strlen(realroot);
    if (io_path_add_sep(realroot, &iter, realroot + PATH_MAX - 1, errbuf) == -1)
    {
        return -1;
    }

    /* If realpath starts with realroot, trim it off! */
    if (strncmp(realpath, realroot, strlen(realroot)) == 0)
    {
        memset(dst, 0, PATH_MAX);
        strcpy(dst, realpath + strlen(realroot));

        return 0;
    }

    strcpy(dst, realpath);
    return 0;
}

int io_path_decomp(io_path_s *decomp, const char *path, char *errbuf)
{
    if (io_dirname(decomp->dir, path, errbuf) == -1)
        return -1;

    if (io_basename(decomp->base, path, errbuf) == -1)
        return -1;

    if (io_realpath(decomp->real, path, errbuf) == -1)
        return -1;

    return 0;
}

int io_create_dir(const char *path, char *errbuf)
{
    char dirpath[PATH_MAX];

    /* A null path already exists */
    if (strcmp(path, "/") == 0 || strlen(path) == 0)
        return 0;

    /* Check if the path already exists */
    if (io_is_dir(path, errbuf))
        return 0;

    /* Get the dirname of the path we are creating */
    if (io_dirname(dirpath, path, errbuf) == -1)
        return -1;

    switch (io_is_dir(dirpath, errbuf))
    {
    case 1:
        break;
    /* If the directory containing the directory we want to make *
     * does not exist, make that one, recursively.               */
    case 0:
        if (io_create_dir(dirpath, errbuf) == -1)
            return -1;
        break;
    case -1:
        return -1;
    }

    LOG(NOTICE, INFO, "Creating directory '%s'", path);
    if (mkdir(path, io_default_dir_mode) == -1)
    {
        IO_ERR(errbuf, "Could not create directory '%s': %s", path, strerror(errno));
        return -1;
    }

    return 0;
}

int io_create_file(const char *path, char *errbuf)
{
    char dirpath[PATH_MAX];

    if (access(path, F_OK) == 0)
        return 0;

    if (io_dirname(dirpath, path, errbuf) == -1)
        return -1;

    if (io_create_dir(dirpath, errbuf) == -1)
        return -1;

    LOG(NOTICE, INFO, "Creating file '%s'", path);
    if (creat(path, io_default_file_mode) == -1)
    {
        IO_ERR(errbuf, "Could not create file '%s': %s", path, strerror(errno));
        return -1;
    }

    return 0;
}
