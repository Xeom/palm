#if !defined(UTIL_ERR)
# define UTIL_ERR
# include <stdio.h>

# include "log/log.h"

# define PALM_ERR         -1
# define PALM_OK           0
# define PALM_COMPLETE     1
# define PALM_INCOMPLETE   2

# define DONT_IGNORE __attribute__ (( warn_unused_result ))

# define TRY(X) do { if ((X) == PALM_ERR) return PALM_ERR; } while (0)

# define ERRMSG_LEN 512

typedef char errmsg_t[ERRMSG_LEN];

#define ERRMSG(buf, ...) \
    do { \
        if (buf) \
            snprintf(buf, ERRMSG_LEN, __VA_ARGS__); \
        else \
            LOG(ALERT, ERR, "Error: " __VA_ARGS__); \
    } while (0)

#define ERRNO_ERRMSG(buf, ...) \
    do { \
        char tmp[ERRMSG_LEN - 10]; \
        snprintf(tmp, ERRMSG_LEN, __VA_ARGS__); \
        if (buf) \
            snprintf(buf, ERRMSG_LEN, "%s: %s", tmp, strerror(errno)); \
        else \
            LOG(ALERT, ERR, "Error: %s: %s", tmp, strerror(errno)); \
    } while (0)

#endif
