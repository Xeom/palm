#include <unistd.h>
#include <pty.h>
#include <errno.h>
#include <sys/wait.h>
#include "log/log.h"
#include "io/io.h"
#include "io/pty.h"

static void pty_run_cmd(pty_s *pty, int inp, int out, char * const cmd[])
{
    if (pty->pid != 0)
        return;

    if (inp > 0)
    {
        dup2(inp, STDIN_FILENO);
        close(inp);
    }

    if (out > 0)
    {
        dup2(out, STDOUT_FILENO);
        close(out);
    }

    execvp(cmd[0], cmd);
}

int pty_init(pty_s *pty, int inp, int out, char * const cmd[])
{
    pid_t pid;
    struct winsize w;

    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    pid = forkpty(&(pty->pfd), NULL, NULL, &w);

    if (pid < 0)
    {
        LOG(ALERT, ERR, "Could not fork process to run tty: %s", strerror(errno));
        return -1;
    }

    pty->pid   = pid;

    pty_run_cmd(pty, inp, out, cmd);

    return 0;
}

void pty_kill(pty_s *pty)
{
    close(pty->pfd);
}

int pty_return_status(pty_s *pty)
{
    int res;
    waitpid(pty->pid, &res, 0);

    return res;
}

void pty_forward_data(pty_s *pty, int inp, int out)
{
    char errbuf[IO_ERRBUF_LEN];

    pty->alive = true;
    while (pty->alive)
    {
        /* Listen to the output of the pty, and the input of *
         * the source of data to the pty.                    */
        io_file_info_s files[] = {
            {
                .fd      = inp,
                .awaitin = true
            },
            {
                .fd      = pty->pfd,
                .awaitin = true
            }
        };

        char tmp;
        int pollres;
        pollres = io_poll_files(files, 2, -1, errbuf);

        /* If no descriptors have data, we carry on */
        if (pollres == 0)
            continue;

        /* An error means it's time to leave! */
        if (pollres == -1)
        {
            LOG(ALERT, ERR, "Could not poll to await pty data: %s", errbuf);
            break;
        }

        /* In case of data to read from input to pty */
        if (files[0].status & IO_FILE_READABLE)
        {
            if (read(inp, &tmp, 1) == 1)
            {
                if (write(pty->pfd, &tmp, 1) != 1)
                {
                    LOG(ALERT, ERR, "Could not copy pty data to fd %d", pty->pfd);
                    pty->alive = false;
                }
            }
        }

        /* In case of output from the pty */
        if (files[1].status & IO_FILE_READABLE)
        {
            if (read(pty->pfd, &tmp, 1) == 1)
            {
                if (write(out, &tmp, 1) != 1)
                {
                    LOG(ALERT, ERR, "Could not copy pty data to fd %d", out);
                    pty->alive = false;
                }
            }
            else
            {
                pty->alive = false;
            }
        }
        else if (files[1].status & IO_FILE_EOF)
        {
            pty->alive = false;
        }
    }
}
