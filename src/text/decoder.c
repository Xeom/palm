#include "text/chr.h"
#include "text/font.h"
#include <sys/types.h>
#include <unistd.h>
#include "buf/buf.h"
#include "text/decoder.h"

static decoder_chrtype_t decoder_get_chrtype(decoder_s *decoder);
static int decoder_cmp_chrbuf(decoder_s *decoder, char *mem, size_t len);
static void decoder_app_chrbuf(decoder_s *decoder, vec_s *line);

static decoder_chrtype_t decoder_get_chrtype(decoder_s *decoder)
{
    vec_s *buf;
    buf = &(decoder->chrbuf);
    unsigned char first, last;
    first = *(unsigned char *)vec_get(buf, 0);
    last  = *(unsigned char *)vec_end(buf);

    if (vec_len(buf) == 0)
        return DECODER_CHR_INCOMPLETE;

    if ((decoder->attrs & DECODER_CR) && (decoder->attrs & DECODER_NL))
    {
        if (decoder_cmp_chrbuf(decoder, "\r", 1) == 0)
            return DECODER_CHR_INCOMPLETE;
        if (decoder_cmp_chrbuf(decoder, "\r\n", 2) == 0)
            return DECODER_CHR_NL;
        if (first == '\r')
            return DECODER_CHR_INVALID;
    }
    else if (decoder->attrs & DECODER_CR)
    {
        if (decoder_cmp_chrbuf(decoder, "\r", 1) == 0)
            return DECODER_CHR_NL;
    }
    else if (decoder->attrs & DECODER_NL)
    {
        if (decoder_cmp_chrbuf(decoder, "\n", 1) == 0)
            return DECODER_CHR_NL;
    }
    else if (decoder->attrs & DECODER_ZRO)
    {
        if (decoder_cmp_chrbuf(decoder, "\0", 1) == 0)
            return DECODER_CHR_NL;
    }

    /* If the decoder is in font mode */
    if (decoder->attrs & DECODER_FNT)
    {
        /* If the chrbuf starts with '{', then it could be a character *
         * of {name} format.                                           */

        if (decoder_cmp_chrbuf(decoder, "{{", 2) == 0)
        {
            /* Delete one of the {s to leave only one */
            vec_del(buf, 0, 1);
            return DECODER_CHR_VALID;
        }
        if (first == '{' && (last == '{' || last == '\0') && vec_len(buf) > 1)
            return DECODER_CHR_INVALID;
        if (first == '{' && last == '}')
            return DECODER_CHR_FONT;
        if (first == '{')
            return DECODER_CHR_INCOMPLETE;

        if (decoder_cmp_chrbuf(decoder, "}}", 2) == 0)
        {
            vec_del(buf, 0, 1);
            return DECODER_CHR_VALID;
        }
        if (first == '}' && vec_len(buf) == 1)
            return DECODER_CHR_INCOMPLETE;
        if (first == '}' && vec_len(buf) > 1)
            return DECODER_CHR_INVALID;
    }

    if (decoder->attrs & DECODER_UTF8)
    {
        int len;
        if ((first & 0xc0) == 0x80)
        {
            return DECODER_CHR_INVALID;
        }

        len = 0;
        if ((first & 0xe0) == 0xc0)
            len = 2;
        else if ((first & 0xf0) == 0xe0)
            len = 3;
        else if ((first & 0xf1) == 0xf0)
            len = 4;

        if (len > 0)
        {
            if (vec_len(buf) < (size_t)len)
                return DECODER_CHR_INCOMPLETE;
            if ((last & 0xc0) != 0x80)
                return DECODER_CHR_INVALID;
            if (vec_len(buf) == (size_t)len)
                return DECODER_CHR_VALID;
        }
    }

    return DECODER_CHR_VALID;
}

static int decoder_cmp_chrbuf(decoder_s *decoder, char *mem, size_t len)
{
    if (vec_len(&(decoder->chrbuf)) == len)
        return memcmp(vec_get(&(decoder->chrbuf), 0), mem, len);

    return -1;
}

static void decoder_app_chrbuf(decoder_s *decoder, vec_s *line)
{
    chr_s *new;
    new = vec_app(line, NULL);
    new->font = decoder->font;
    memset(new->text, 0, sizeof(new->text));
    memcpy(new->text, vec_get(&(decoder->chrbuf), 0), vec_len(&(decoder->chrbuf)));
}

bool decoder_chr(decoder_s *decoder, int chr, vec_s *line)
{
    decoder_chrtype_t chrtype;
    FONT_DEF(invfnt, "invalid-chr");

    if (chr != EOF)
    {
        vec_app(&(decoder->chrbuf), &chr);
        chrtype = decoder_get_chrtype(decoder);
    }
    else
    {
        chrtype = DECODER_CHR_EOF;
    }

    switch (chrtype)
    {
    case DECODER_CHR_NL:
        if (!(decoder->attrs & DECODER_STRIPNL))
            decoder_app_chrbuf(decoder, line);
	// Fall through!
    case DECODER_CHR_EOF:
        vec_clr(&(decoder->chrbuf));
        decoder->font = 0;
        return true;

    case DECODER_CHR_VALID:
        decoder_app_chrbuf(decoder, line);
        vec_clr(&(decoder->chrbuf));
        break;
    case DECODER_CHR_INCOMPLETE:
        break;
    /* The character has a chrbuf of format '{name}', meaning we should *
     * switch fonts to the named font!                                  */
    case DECODER_CHR_FONT:
        *(char *)vec_end(&(decoder->chrbuf)) = '\0';
        decoder->font = font_by_name(vec_get(&(decoder->chrbuf), 1));
        vec_clr(&(decoder->chrbuf));
        break;
    /* For invalid character combinations, each character is added, *
     * one-by-one to the buffer.                                    */
    case DECODER_CHR_INVALID:
        {
            VEC_FOREACH(&(decoder->chrbuf), char *, chr)
            {
                chr_s *new;
                new = vec_app(line, NULL);
                new->font = invfnt;
                memset(new->text, 0, sizeof(new->text));
                new->text[0] = *chr;
            }
            vec_clr(&(decoder->chrbuf));
        }
        break;
    }

    return false;
}

void decoder_str(decoder_s *decoder, const char *str, vec_s *result)
{
    while (*str)
    {
        decoder_chr(decoder, *str, result);
        str++;
    }

    decoder_chr(decoder, EOF, result);
}

void decoder_init(decoder_s *decoder, decoder_attr_t attrs)
{
    decoder->attrs  = attrs;
    decoder->font    = 0;

    vec_init(&(decoder->chrbuf),  sizeof(char));
}

void decoder_kill(decoder_s *decoder)
{
    vec_kill(&(decoder->chrbuf));
}
