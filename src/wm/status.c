#include "wm/status.h"
#include "log/notify.h"
#include "text/font.h"
#include "bind/bufbind.h"
#include "editor.h"

/* An structure representing an item in the statusbar */
struct status_item
{
    /* You reference the item with %(name) */
    char *name;

    /* The function for generating the content. */

    /* Arguments:
     *     generate(
     *         editor, A pointer to the editor.
     *         winind, The id of the window.
     *         buf,    A pointer to the buffer.
     *         part,   A pointer to a char vector to return the content in.
     *     )
     *
     */
    void (*generate)(editor_s *, int, buf_s *, vec_s *);
};

/* A structure representing a conditional in the statusbar */
struct status_cond
{
    char *name;
    bool (*generate)(editor_s *, int, buf_s *);
};

/* A machine to tokenize strings for status formats:
 *
 * SYNTAX:
 * %(item)
 * %(if file-associated) ... %(else) ... %(fi)
 */

typedef enum
{
    STATUS_TOKENIZER_STR,
    STATUS_TOKENIZER_ESC,
    STATUS_TOKENIZER_BRA
} status_tokenizer_state_t;

struct status_tokenizer
{
    vec_s tokens;
    status_tokenizer_state_t state;
};

/* Functions to generate parts of a status! */
static void generate_winind(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    vec_fmt(part, "%d", winind);
}

static void generate_base(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    io_path_s path;
    finfo_get_path(&(buf->finfo), &path);
    vec_str(part, path.base);
}

static void generate_dir(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    io_path_s path;
    finfo_get_path(&(buf->finfo), &path);
    vec_str(part, path.dir);
}

static void generate_rel(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    char relpath[PATH_MAX];
    io_path_s path;

    finfo_get_path(&(buf->finfo), &path);
    io_relpath(relpath, path.real, ".", NULL);
    vec_str(part, relpath);
}

static void generate_real(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    io_path_s path;
    finfo_get_path(&(buf->finfo), &path);
    vec_str(part, path.real);
}

static void generate_pri_row(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    vec_fmt(part, "%ld", cur.pri.row);
}

static void generate_pri_col(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    vec_fmt(part, "%ld", cur.pri.col);
}

static void generate_buf_len(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    vec_fmt(part, "%lu", buf_len(buf));
}

static void generate_sec_row(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    vec_fmt(part, "%ld", cur.sec.row);
}

static void generate_sec_col(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    vec_fmt(part, "%ld", cur.sec.col);
}

static void generate_rel_row(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    vec_fmt(part, "%ld", cur.pri.row - cur.sec.row);
}

static void generate_rel_col(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    vec_fmt(part, "%ld", cur.pri.col - cur.sec.col);
}

static void generate_cur_type(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    if (cur.type == CUR_TYPE_LINE)
        vec_str(part, "LINE");
    else if (cur.type == CUR_TYPE_CHR)
        vec_str(part, "CHR");
    else if (cur.type == CUR_TYPE_BLOCK)
        vec_str(part, "BLOCK");
}

static void generate_mode(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    char mode[MODE_NAME_LEN + 1];
    bufbind_get_mode(&(buf->bind), mode);

    vec_str(part, mode);
}

static void generate_win_cols(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    rect_s dims;
    wm_get_dims(&(editor->wm), winind, &dims);
    vec_fmt(part, "%u", dims.w);
}

static void generate_win_rows(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    rect_s dims;
    wm_get_dims(&(editor->wm), winind, &dims);
    vec_fmt(part, "%u", dims.h);
}

static void generate_buf_numalts(editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    vec_fmt(part, "%lu", curset_num_active(&(buf->curset)) - 1);
}
/* Conditions to apply to the status */

static bool cond_win_selected(editor_s *editor, int winind, buf_s *buf)
{
    return winind == buf_get_selected_win(&(editor->bufselector));
}

static bool cond_cur_sticky(editor_s *editor, int winind, buf_s *buf)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    return cur.sticky;
}

static bool cond_cur_block(editor_s *editor, int winind, buf_s *buf)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    return cur.type == CUR_TYPE_BLOCK;
}

static bool cond_cur_line(editor_s *editor, int winind, buf_s *buf)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    return cur.type == CUR_TYPE_LINE;
}

static bool cond_cur_chr(editor_s *editor, int winind, buf_s *buf)
{
    cur_s cur;
    buf_get_cur(buf, &cur);
    return cur.type == CUR_TYPE_CHR;
}

static bool cond_edit_ow(editor_s *editor, int winind, buf_s *buf)
{
    return buf->einfo.attrs & EDIT_OW;
}

static bool cond_edit_alts(editor_s *editor, int winind, buf_s *buf)
{
    return buf->einfo.attrs & EDIT_ALTS;
}

static bool cond_buf_hasalts(editor_s *editor, int winind, buf_s *buf)
{
    return curset_num_active(&(buf->curset)) > 1;
}

/* Arrays of generators and conditions! */
static struct status_item status_items[] = {
    { "winind",     generate_winind   },
    { "real",       generate_real     },
    { "base",       generate_base     },
    { "dir",        generate_dir      },
    { "rel",        generate_rel      },
    { "pri-row",    generate_pri_row  },
    { "pri-col",    generate_pri_col  },
    { "sec-row",    generate_sec_row  },
    { "sec-col",    generate_sec_col  },
    { "rel-col",    generate_rel_col  },
    { "rel-row",    generate_rel_row  },
    { "cur-type",   generate_cur_type },
    { "mode",       generate_mode     },
    { "buf-len",    generate_buf_len  },
    { "buf-numalts",generate_buf_numalts },
//    { "percent",    generate_percent },
    { "win-cols",   generate_win_cols    },
    { "win-rows",   generate_win_rows    }
};

static struct status_cond status_conds[] = {
    { "win-selected",    cond_win_selected },
//    { "file-associated", cond_file_associated },
    { "cur-sticky",      cond_cur_sticky },
    { "edit-ow",         cond_edit_ow    },
    { "edit-alts",       cond_edit_alts },
    { "buf-hasalts",     cond_buf_hasalts },
    { "cur-block",       cond_cur_block },
    { "cur-line",        cond_cur_line  },
    { "cur-chr",         cond_cur_chr   }
};

/* Indexing the arrays. This need not be fast, as it is done only *
 * during tokenization.                                           */
static struct status_item *status_get_item(vec_s *key)
{
    size_t ind, len;
    len = sizeof(status_items)/sizeof(status_items[0]);

    for (ind = 0; ind < len; ++ind)
    {
        if (vec_strcmp(key, status_items[ind].name) == 0)
            return &status_items[ind];
    }

    return NULL;
}

static struct status_cond *status_get_cond(vec_s *key)
{
    size_t ind, len;
    len = sizeof(status_conds)/sizeof(status_conds[0]);

    for (ind = 0; ind < len; ++ind)
    {
        if (vec_strcmp(key, status_conds[ind].name) == 0)
            return &status_conds[ind];
    }

    return NULL;
}

void status_token_init(status_tok_s *tok)
{
    vec_init(&(tok->cont), sizeof(char));
    tok->typ = STATUS_TOKENIZER_STR;
}

void status_token_kill(status_tok_s *tok)
{
    vec_kill(&(tok->cont));
}

static void strip_string(vec_s *str)
{
    size_t n;

    n = 0;
    {VEC_FOREACH(str, char *, c)
    {
        if (*c == ' ') n += 1;
        else           break;
    }}

    if (n) vec_del(str, 0, n);

    n = 0;
    {VEC_RFOREACH(str, char *, c)
    {
        if (*c == ' ') n += 1;
        else           break;
    }}

    if (n) vec_del(str, vec_len(str) - n, n);
}


/* TOKENIZER */
static void status_tokenizer_init(struct status_tokenizer *machine)
{
    vec_init(&(machine->tokens), sizeof(status_tok_s));
    machine->state = STATUS_TOKENIZER_STR;
}

static void status_tokenizer_kill(struct status_tokenizer *machine)
{
    vec_kill(&(machine->tokens));
}

/* Add a new token */
static void status_tokenizer_push(struct status_tokenizer *machine)
{
    status_tok_s *tok;
    tok = vec_app(&(machine->tokens), NULL);
    status_token_init(tok);
}

/* Push a character to the last token */
static void status_tokenizer_add_char(struct status_tokenizer *machine, char c)
{
    status_tok_s *tok;
    tok = vec_get(&(machine->tokens), vec_len(&(machine->tokens)) - 1);

    vec_app(&(tok->cont), &c);
}

/* Advance state machine with a character */
static int status_tokenizer_consume(struct status_tokenizer *machine, char c)
{
    status_tok_s *tok;

    if (vec_len(&(machine->tokens)) == 0)
    {
        status_tokenizer_push(machine);
    }
    tok = vec_get(&(machine->tokens), vec_len(&(machine->tokens)) - 1);

    switch (machine->state)
    {
    case STATUS_TOKENIZER_STR:
        if (c == '%')
        {
            machine->state = STATUS_TOKENIZER_ESC;
        }
        else
        {
            tok->typ = STATUS_TOK_STR;
            status_tokenizer_add_char(machine, c);
        }
        break;
    case STATUS_TOKENIZER_ESC:
        if (c == '%')
        {
            status_tokenizer_add_char(machine, c);
            machine->state = STATUS_TOKENIZER_STR;
        }
        else if (c == '(')
        {
            status_tokenizer_push(machine);
            machine->state = STATUS_TOKENIZER_BRA;
        }
        else
        {
            LOG(ALERT, ERR, "Invalid character (%c) in status format. Expected '(' after '%%'.", (char)c);
            return -1;
        }
        break;
    case STATUS_TOKENIZER_BRA:
        if (c == ')')
        {
            size_t len;
            void  *mem;

            strip_string(&(tok->cont));

            len = vec_len(&(tok->cont));
            mem = vec_get(&(tok->cont), 0);

            if (len >= 3 && memcmp(mem, "if ", 3) == 0)
            {
                vec_del(&(tok->cont), 0, 2);
                strip_string(&(tok->cont));

                tok->typ = STATUS_TOK_IF;
                tok->cond = status_get_cond(&(tok->cont));

                if (!tok->cond)
                {
                    LOG(ALERT, ERR, "Unknown condition in status format.");
                    return -1;
                }
            }
            else if (len == 2 && memcmp(mem, "fi", 2) == 0)
            {
                tok->typ = STATUS_TOK_FI;
            }
            else if (len == 4 && memcmp(mem, "else", 4) == 0)
            {
                tok->typ = STATUS_TOK_ELSE;
            }
            else
            {
                tok->typ = STATUS_TOK_ITEM;
                tok->item = status_get_item(&(tok->cont));
                if (!tok->item)
                {
                    vec_app(&(tok->cont), NULL);
                    LOG(ALERT, ERR, "'%s'", (char *)vec_get(&(tok->cont), 0));
                    LOG(ALERT, ERR, "Unknown item in status format.");
                    return -1;
                }
            }

            status_tokenizer_push(machine);
            machine->state = STATUS_TOKENIZER_STR;
        }
        else
        {
            status_tokenizer_add_char(machine, c);
        }
        break;
    }

    return 0;
}

/* Split a string of chars into tokens! */
int status_tokenize(vec_s *tokens, vec_s *chrs)
{
    bool err;
    struct status_tokenizer machine;

    VEC_FOREACH(tokens, status_tok_s *, tok)
        status_token_kill(tok);

    vec_clr(tokens);

    status_tokenizer_init(&machine);

    err = false;
    VEC_FOREACH(chrs, char *, c)
    {
        if (status_tokenizer_consume(&machine, *c) == -1)
        {
            err = true;
            break;
        }
    }

    if (!err)
        vec_cpy(tokens, &(machine.tokens));

    status_tokenizer_kill(&machine);

    return err ? -1 : 0;
}

/* Generate the mid, left or right part of a status */
void status_generate_part(vec_s *tokens, editor_s *editor, int winind, buf_s *buf, vec_s *part)
{
    vec_s fmt;
    int enabled;
    decoder_s decoder;

    /* 1  - We are actively showing whatever we find!  *
     * 0  - We are disabled by a false if              *
     * -n - We are disabled by a false if, n layers up */
    enabled = 1;

    vec_init(&fmt, sizeof(char));

    /* No support for nested IFs right now, but *
     * we can add that if we want to later.     */
    VEC_FOREACH(tokens, status_tok_s *, tok)
    {
        if (tok->typ == STATUS_TOK_STR)
        {
            if (enabled > 0)
                vec_cpy(&fmt, &(tok->cont));
        }
        else if (tok->typ == STATUS_TOK_IF)
        {
            if (enabled > 0)
            {
                if ((tok->cond->generate)(editor, winind, buf) == 0)
                    enabled = 0;
            }
            else
            {
                enabled -= 1;
            }
        }
        else if (tok->typ == STATUS_TOK_ELSE)
        {
            if (enabled > 0)
                enabled = 0;
            else if (enabled == 0)
                enabled = 1;
        }
        else if (tok->typ == STATUS_TOK_FI)
        {
            if (enabled <= 0)
                enabled += 1;
        }
        else if (tok->typ == STATUS_TOK_ITEM)
        {
            if (enabled > 0)
                (tok->item->generate)(editor, winind, buf, &fmt);
        }
    }

    decoder_init(&decoder, DECODER_UTF8 | DECODER_FNT);
    VEC_FOREACH(&fmt, char *, c)
        decoder_chr(&decoder, *c, part);
    decoder_chr(&decoder, EOF, part);

    vec_kill(&fmt);
    decoder_kill(&decoder);
}

void status_format(status_format_s *fmt, editor_s *editor, int winind, buf_s *buf, vec_s *status)
{
    vec_s left, mid, right;
    bool selected;
    chr_s blank;
    rect_s dims;

    FONT_DEF(bdrfnt, "win-horizontal-border");

    blank = (chr_s){ .text = " ", .font = bdrfnt };
    selected = (winind == buf_get_selected_win(&(editor->bufselector)));

    wm_get_dims(&(editor->wm), winind, &dims);

    vec_init(&left,  sizeof(chr_s));
    vec_init(&right, sizeof(chr_s));
    vec_init(&mid,   sizeof(chr_s));

    status_generate_part(&(fmt->left), editor, winind, buf, &left);
    status_generate_part(&(fmt->right), editor, winind, buf, &right);
    status_generate_part(&(fmt->mid), editor, winind, buf, &mid);

    if (selected)
    {
        vec_s notification;
        size_t maxlen;

        maxlen = MAX(0, (long)dims.w - 1 - (long)vec_len(&left) - (long)vec_len(&right));
        vec_init(&notification, sizeof(chr_s));
        log_notify_update(&default_log, &notification, maxlen);

        if (vec_len(&notification) > maxlen)
            vec_del(&notification, maxlen, vec_len(&notification) - maxlen);

        if (log_has_printable(&default_log) || vec_len(&notification) > 5)
        {
            vec_cpy(status, &left);
            vec_cpy(status, &notification);
        }
        else
        {
            vec_cpy(status, &left);
            vec_cpy(status, &notification);
            vec_cpy(status, &mid);
            // Append mid
        }

        vec_kill(&notification);
    }
    else
    {
        vec_cpy(status, &left);
        vec_cpy(status, &mid);
    }

    while (vec_len(status) < dims.w - vec_len(&right) - 1 )
    {
        vec_app(status, &blank);
    }

    vec_cpy(status, &right);

    vec_kill(&left);
    vec_kill(&right);
    vec_kill(&mid);
}

void status_format_init(status_format_s *format)
{
    vec_init(&(format->left),  sizeof(status_tok_s));
    vec_init(&(format->mid),   sizeof(status_tok_s));
    vec_init(&(format->right), sizeof(status_tok_s));
}

void status_format_kill(status_format_s *format)
{
    VEC_FOREACH(&(format->left), status_tok_s *, ltok)
        status_token_kill(ltok);

    vec_kill(&(format->left));

    VEC_FOREACH(&(format->right), status_tok_s *, rtok)
        status_token_kill(rtok);

    vec_kill(&(format->right));

    VEC_FOREACH(&(format->mid), status_tok_s *, mtok)
        status_token_kill(mtok);

    vec_kill(&(format->mid));
}

int status_format_set(
    status_format_s *format,
    const char *left,
    const char *mid,
    const char *right
)
{
    vec_s str;
    vec_init(&str, sizeof(char));

    if (left)
    {
        vec_clr(&str);
        vec_str(&str, left);
        if (status_tokenize(&(format->left), &str) == -1)
            return -1;
    }
    if (right)
    {
        vec_clr(&str);
        vec_str(&str, right);
        if (status_tokenize(&(format->right), &str) == -1)
            return -1;
    }
    if (mid)
    {
        vec_clr(&str);
        vec_str(&str, mid);
        if (status_tokenize(&(format->mid), &str) == -1)
            return -1;
    }

    vec_kill(&str);

    LOG(INFO, SUCC, "Set status format.");

    return 0;
}
