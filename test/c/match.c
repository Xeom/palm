#include "snow.h"

#include "match/match.h"

describe(match)
{
    subdesc(init)
    {
        it ("Initializes matches")
        {
            match_s match;
            match_init(&match, 0, "test");

            asserteq(match.attrs, 0);
            asserteq(vec_len(&(match.text)), 4);
            asserteq_buf(vec_get(&(match.text), 0), "test", 4);

            asserteq((void *)match.regex,     NULL);
            asserteq((void *)match.matchdata, NULL);

            asserteq(match.grp, 0);

            match_kill(&match);
        }

        it ("Initializes with escapes")
        {
            match_s match;
            match_init(&match, 0, "\\x31\\x32");

            asserteq(vec_len(&(match.text)), 8);
            asserteq_buf(vec_get(&(match.text), 0), "\\x31\\x32", 8);

            match_kill(&match);

            match_init(&match, MATCH_ESCAPES, "\\x31\\x32");

            asserteq(vec_len(&(match.text)), 2);
            asserteq_buf(vec_get(&(match.text), 0), "12", 2);

            match_kill(&match);
        }
    }

    subdesc(check)
    {
        vec_s str;

        before_each()
        {
            vec_init(&str, sizeof(char));
        }

        after_each()
        {
            vec_kill(&str);
        }

        it ("Matches a string")
        {
            match_s match;
            match_result_s res;
            match_init(&match, 0, "test");
            vec_str(&str, "test");

            asserteq(match_check(&match, &str, 0, &res), 1);
            asserteq(res.start, 0);
            asserteq(res.end,   4);
            asserteq(res.match, &match);

            asserteq(match_check(&match, &str, 1, &res), 0);
            match_kill(&match);
        }

        it ("Matches blank regex lines")
        {
            match_s match;
            match_result_s res;
            match_init(&match, MATCH_REGEX, "^$");
            vec_str(&str, "");

            asserteq(match_check(&match, &str, 0, &res), 1);
            asserteq(res.start, 0);
            asserteq(res.end,   0);
            asserteq(res.match, &match);

            vec_str(&str, "abc");
            asserteq(match_check(&match, &str, 0, &res), 0);
            match_kill(&match);
        }

        /* TODO: 
         * it ("Matches regexes")
         * it ("Processes escapes")
         * it ("Processes escapes with regexes")
         * it ("Accepts verbose regexes")
         * it ("Matches case insensitive")
         * it ("Matches regexes case insenstive")
         */
    }
}
