#include "util/time.h"
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

/* Get seconds as a float */
double util_time_fsec(void)
{
    long long us;

    /* We can derive this from time_us(). */
    us = util_time_us();
    return (double)us / 1.0e6;
}

/* Get seconds since the epoch. */
long long util_time_sec(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    return tv.tv_sec;
}

/* Get time as milliseconds (1e-3) */
long long util_time_ms(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    return 1e3 * tv.tv_sec + tv.tv_usec / 1e3;
}

/* Get time as microseconds (1e-6) */
long long util_time_us(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    return 1e6 * tv.tv_sec + tv.tv_usec;
}

int util_strfnow(char *buf, size_t len, const char *fmt)
{
    time_t now;
    struct tm tm;
    now = time(NULL);
#if defined(_POSIX_C_SOURCE)
    if (!localtime_r(&now, &tm))
        /* Error - could not get localtime */
        return -1;
#else
    struct tm *tmp;
    static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock(&mtx);
    tmp = localtime(&now);
    if (tmp) memcpy(&tm, tmp, sizeof(struct tm));
    pthread_mutex_unlock(&mtx);
    if (!tmp)
        /* Error - could not get localtime */
        return -1;
#endif

    if (strftime(buf, len, fmt, &tm) == 0 && fmt[0])
        /* Format blank or too long */
        return -1;

    return 0;
}
