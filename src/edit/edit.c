#include "buf/buf.h"
#include "buf/cur.h"
#include "edit/edit.h"
#include "edit/indent.h"
#include "text/indent.h"

void edit_get_cur(edit_info_s *einfo, cur_s *rtn)
{
    buf_get_cur(einfo->buf, rtn);

    if (rtn->sec.row < einfo->base)
        rtn->sec = (pos_s){ .row = einfo->base, .col = 0 };

    if (rtn->pri.row < einfo->base)
        rtn->pri = (pos_s){ .row = einfo->base, .col = 0 };

    pos_correct(&(rtn->pri), einfo->buf);
    pos_correct(&(rtn->sec), einfo->buf);
}

void edit_copy_at(edit_info_s *einfo, cur_s *cur, vec_s *chrs)
{
    bool firstline;
    vec_s line;
    row_t row;
    pos_s *min, *max;

    vec_init(&line, sizeof(chr_s));

    firstline = true;

    CUR_FOREACH_LINE(cur, einfo->buf, linecur)
    {
        col_t col;

        row = linecur.pri.row;

        min = cur_min(&linecur);
        max = cur_max(&linecur);

        buf_get_line(einfo->buf, row, &line);

        if (!firstline)
            encoder_nl(&(einfo->cpyencoder), chrs);

        firstline = false;

        for (col = min->col; col <= max->col; ++col)
        {
            chr_s *chr;

            chr = vec_get(&line, col);
            if (chr)
                encoder_chr(&(einfo->cpyencoder), chr, chrs);
        }
    }

    vec_kill(&line);
}

void edit_copy(edit_info_s *einfo, vec_s *chrs)
{
    cur_s cur;

    edit_get_cur(einfo, &cur);
    edit_copy_at(einfo, &cur, chrs);
}
