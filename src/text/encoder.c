#include "text/chr.h"
#include "text/font.h"
#include "text/encoder.h"

void encoder_init(encoder_s *encoder, decoder_attr_t attrs)
{
    encoder->attrs = attrs;
    encoder->font  = 0;
}

void encoder_chr(encoder_s *encoder, chr_s *chr, vec_s *buf)
{
    if (encoder->attrs & DECODER_FNT)
    {
        if (chr->font != encoder->font)
        {
            font_s font;
            font_get_info(chr->font, &font);
            vec_fmt(buf, "{%s}", font.name);
            encoder->font = chr->font;
        }
    }

    vec_str(buf, chr->text);
}

void encoder_nl(encoder_s *encoder, vec_s *buf)
{
    encoder->font = 0;
    if (encoder->attrs & DECODER_STRIPNL)
    {
        if ((encoder->attrs & DECODER_CR) && (encoder->attrs & DECODER_NL))
            vec_str(buf, "\r\n");
        else if (encoder->attrs & DECODER_CR)
            vec_str(buf, "\r");
        else if (encoder->attrs & DECODER_NL)
            vec_str(buf, "\n");
        else if (encoder->attrs & DECODER_ZRO)
            vec_app(buf, NULL);
    }
}

