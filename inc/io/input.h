#if !defined(IO_INPUT_H)
# define IO_INPUT_H
# include <pthread.h>
# include "types.h"
# include "io/io.h"
# include "event/event.h"

# define INPUT_MAX_POLLS 4

struct input
{
    editor_s *editor;

    vec_s     keys;

    io_file_info_s polls[INPUT_MAX_POLLS];
    size_t npolls;
    bool alive;
};

void input_init(input_s *inp, editor_s *editor);

void input_pause(input_s *inp);
void input_resume(input_s *inp);

int input_add_fd(input_s *inp, int fd);
int input_rem_fd(input_s *inp, int fd);

void input_kill(input_s *inp);

static inline void input_event_spec(input_s *inp, event_spec_s *spec)
{
    memset(spec, 0, sizeof(event_spec_s));
    spec->ptr = inp;
    spec->typ = EVENT_KEY_BYTE;
}

#endif
