import paths
import libtypes
from editor import log
from lib import lib
from .structure import Structure
from .yamlobj   import YAMLObj

class HlCtx(Structure):
    typ        = libtypes.hl_ctx_s
    init_funct = lib.hl_ctx_init
    kill_funct = lib.hl_ctx_kill

    def load_yaml(self, obj):
        if lib.hl_ctx_load_yaml(self.ptr, obj.ptr) == -1:
            raise Exception("Could not load highlighting info")

    def load_paths(self, paths):
        obj = YAMLObj()
        for path in paths:
            obj.parse(path)
        self.load_yaml(obj)

    def load_defaults(self):
        obj = YAMLObj()
        for path in paths.hlfiles:
            obj.parse(path)
            try:
                obj.parse(path)
                log.fmt("INFO", "SUCC", "Parsed highlighting info from '%s'", path)
            except:
                log.fmt("ALERT", "ERR", "Could not parse default highlight file '%s'", path)

        try:
            self.load_yaml(obj)
        except:
            log.fmt("ALERT", "ERR", "Could not load syntax information")

    def lookup_state(self, name):
        if not isinstance(name, bytes):
            name = name.encode("utf-8")

        return lib.hl_ctx_lookup_state(self.ptr, name)

class HlWorker(Structure):
    typ        = libtypes.hl_worker_s
    init_funct = lib.hl_worker_init
    kill_funct = lib.hl_worker_kill
