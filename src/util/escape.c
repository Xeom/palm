#include "util/escape.h"
#include "container/vec.h"
#include "util/err.h"
#include <stdbool.h>
#include <ctype.h>

char escape_names[256] = {
    ['a']  = '\a',
    ['b']  = '\b',
    ['t']  = '\t',
    ['n']  = '\n',
    ['v']  = '\v',
    ['f']  = '\f',
    ['r']  = '\r',
    ['e']  = '\x1b',
    ['\\'] = '\\',
    ['"']  = '"',
    ['\''] = '\''
};

char *escape_hexchrs = "0123456789abcdef";


static void escape_push_bits(escape_state_s *state, int n, int bits)
{
    state->acclen  += n;
    state->acc    <<= n;
    state->acc     |= bits;
}

static int escape_pop_bits(escape_state_s *state, int n)
{
    int rtn, mask;

    if (state->acclen < n)
        n = state->acclen;

    mask = ((1 << n) - 1) << (state->acclen - n);
    rtn = (state->acc & mask) >> (state->acclen - n);
    state->acclen  -= n;
    state->acc &= ~mask;

    return rtn;
}

void escape_init(escape_state_s *state)
{
    memset(state, 0, sizeof(escape_state_s));
}

int escape_chr(escape_state_s *state, int c, bool *next)
{
    *next = true;
    switch (state->state)
    {
    case ESCAPE_IDLE:
        if (c == '\\')
        {
            state->state = ESCAPE_ESCAPED;
            return -1;
        }
        else
        {
            return c;
        }
        break;
    case ESCAPE_ESCAPED:
        if (escape_names[c])
        {
            state->state = ESCAPE_IDLE;
            return escape_names[c];
        }
        else if (c == 'x')
        {
            state->state = ESCAPE_HEX;
            return -1;
        }
        else if (c >= '0' && c <= '9')
        {
            state->state = ESCAPE_OCT;
            *next = false;
            return -1;
        }
        else
        {
            escape_push_bits(state, 8, '\\');
            escape_push_bits(state, 8, c);
            state->state = ESCAPE_POP;
            *next = false;
            return -1;
        }
        break;
    case ESCAPE_HEX:
        if (c >= '0' && c <= '9')
        {
            escape_push_bits(state, 4, c - '0');
            return -1;
        }
        else if (c >= 'a' && c <= 'f')
        {
            escape_push_bits(state, 4, c - 'a');
            return -1;
        }
        else
        {
            state->state = ESCAPE_POP;
            *next = false;
            return -1;
        }
        break;
    case ESCAPE_OCT:
        if (c >= '0' && c <= '7')
        {
            escape_push_bits(state, 3, c - '0');

            if (state->acclen % 8 == 1 && state->acclen > 1)
                escape_pop_bits(state, 1);

            return -1;
        }
        else
        {
            state->state = ESCAPE_POP;
            *next = false;
            return -1;
        }
        break;
    case ESCAPE_POP:
        *next = false;
        state->acclen += 7;
        state->acclen -= state->acclen % 8;
        if (state->acclen == 0)
        {
            state->state = ESCAPE_IDLE;
            return -1;
        }
        return escape_pop_bits(state, 8);
    }

    return -1;
}

void escape_vec(vec_s *vec)
{
    escape_state_s state;
    bool   next;
    size_t ind;
    vec_s tmp;

    escape_init(&state);
    vec_init(&tmp, sizeof(char));
    ind = 0;
    /* <= len so that we call with an extra -1 at the end */
    for (ind = 0; ind <= vec_len(vec); ind += next ? 1 : 0)
    {
        unsigned char *c;
        int   res, chr;

        c = vec_get(vec, ind);

        /* handle the -1 at the end */
        chr = c ? *c : -1;

        res = escape_chr(&state, chr, &next);
        if (res != -1)
        {
            char new;
            new = res;
            vec_app(&tmp, &new);
        }
    }
    vec_del(vec, 0, vec_len(vec));
    vec_cpy(vec, &tmp);
    vec_kill(&tmp);
}

int escape_sanitize_vec(vec_s *src, vec_s *dst)
{
    VEC_FOREACH(src, char *, c)
    {
        if (!isprint(*c))
        {
            if (!vec_fmt(dst, "\\x%02x", *c))
                return PALM_ERR;
        }
        else if (*c == '\\' || *c == '"' || *c == '\'')
        {
            if (!vec_fmt(dst, "\\%c", *c))
                return PALM_ERR;
        }
        else
        {
            if (!vec_app(dst, c))
                return PALM_ERR;
        }
    }

    return PALM_OK;
}
