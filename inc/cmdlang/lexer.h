#if !defined(CMDLANG_LEXER_H)
# define CMDLANG_LEXER_H
# include "types.h"
# include "util/err.h"
# include "util/escape.h"
# include "container/vec.h"
# include "cmdlang/obj.h"

# define CMDLANG_TOK_MAXLEN 256

typedef enum
{
    CMDLANG_TOK_NONE,
    CMDLANG_TOK_NUMBER,
    CMDLANG_TOK_STR,
    CMDLANG_TOK_CMD,
    CMDLANG_TOK_SHORT_CMD,
    CMDLANG_TOK_PARAM,
    CMDLANG_TOK_OBJ
} cmdlang_tok_typ_t;

typedef enum
{
    CMDLANG_LEXER_IDLE,
    CMDLANG_LEXER_NUMBER,
    CMDLANG_LEXER_STR,
    CMDLANG_LEXER_SHORT_CMD,
    CMDLANG_LEXER_CMD,
    CMDLANG_LEXER_PARAM
} cmdlang_lexer_state_t;

struct cmdlang_tok
{
    long ival;
    vec_s sval;
    cmd_s *cval;
    cmdlang_obj_s obj;

    cmdlang_tok_typ_t typ;
};

struct cmdlang_lexer
{
    cmd_lib_s *lib;
    cmdlang_lexer_state_t state;
    cmdlang_tok_s tok;
    escape_state_s escstate;
};

void cmdlang_tok_init(cmdlang_tok_s *tok);
void cmdlang_tok_cpy(cmdlang_tok_s *tok, cmdlang_tok_s *src);
void cmdlang_tok_kill(cmdlang_tok_s *tok);

int cmdlang_tok_is_verb(
    cmdlang_tok_s *tok, cmd_lib_s *lib, bool *verb
) DONT_IGNORE;

int cmdlang_lexer_init(
    cmdlang_lexer_s *lexer, cmd_lib_s *lib, errmsg_t errmsg
) DONT_IGNORE;

void cmdlang_lexer_kill(cmdlang_lexer_s *lexer);

int cmdlang_lexer_token(
    cmdlang_lexer_s *lexer, cmdlang_tok_s *tok, errmsg_t errmsg
) DONT_IGNORE;

int cmdlang_lexer_chr(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
) DONT_IGNORE;

int cmdlang_lexer_str_to_toks(
    cmdlang_lexer_s *lexer,
    const char *str,
    vec_s *toks,
    errmsg_t errmsg
) DONT_IGNORE;

#endif
