#if !defined(HL_CTX_H)
# define HL_CTX_H
# if !defined(NO_HL)
#  include "util/yaml.h"
#  include "container/vec.h"
#  include "hl/action.h"
#  include "hl/state.h"
#  include <stdbool.h>

// TODO: Replace with sorted vecs.
struct hl_ctx
{
    vec_s tokens;
    vec_s states;
};

void hl_ctx_init(hl_ctx_s *ctx);

void hl_ctx_kill(hl_ctx_s *ctx);

hl_tok_s *hl_ctx_new_token(hl_ctx_s *ctx);

hl_tok_s *hl_ctx_get_token(hl_ctx_s *ctx, size_t n);

long hl_ctx_lookup_token(hl_ctx_s *ctx, const char *tokname);

hl_state_s *hl_ctx_new_state(hl_ctx_s *ctx);

hl_state_s *hl_ctx_get_state(hl_ctx_s *ctx, size_t n);

long hl_ctx_lookup_state(hl_ctx_s *ctx, const char *statename);

int hl_ctx_load_yaml(hl_ctx_s *ctx, yaml_obj_s *obj);

void hl_ctx_apply(hl_ctx_s *ctx, vec_s *chrs, vec_s *states);

# endif
#endif
