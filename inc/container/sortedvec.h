#if !defined(CONTAINER_SORTEDVEC)
# define CONTAINER_SORTEDVEC
# include "types.h"
# include "util/err.h"
# include "container/vec.h"

/**
 * @brief A vector whose items kept in sorted order.
 *
 * This is useful for finding completions of strings.
 *
 */

typedef int (*sortedvec_cmpcb_t)(const void *, const void *);
typedef int (*sortedvec_startcb_t)(const void *, const void *);

struct sortedvec
{
    vec_s vec;
    sortedvec_cmpcb_t cmpcb;
};

/**
 * Initialize a sorted vector of items.
 *
 * @param svec  A pointer to the sorted vector.
 * @param width The number of bytes to allocate for each item in the vector.
 * @param cb    A callback to compare two items. This works the same as the
 *              callback used by qsort(). @code
 *              cb(a, b) == 0 where a == b
 *              cb(a, b) >  0 where a >  b
 *              cb(a, b) <  0 where a <  b
 *              @endcode
 *
 *
 */
void sortedvec_init(sortedvec_s *svec, size_t width, sortedvec_cmpcb_t cb);

/**
 * Free all resources associated with a sorted vector of items.
 *
 * @param svec A pointer to the sorted vector.
 *
 */
void sortedvec_kill(sortedvec_s *svec);

/**
 * Copy and insert an item into a sorted position in a sorted vector.
 *
 * @param svec A pointer to the sorted vector.
 * @param item A pointer to the item to insert a copy of into the vector.
 *
 * @return 0 on success, -1 on error.
 *
 */
int sortedvec_ins(sortedvec_s *svec, void *item);

/**
 * Find an item in a vector that compares equal to a given item.
 *
 * @param svec  A pointer to the sorted vector.
 * @param item  A pointer to the item to lookup.
 * @param count A pointer to a size_t. If not NULL, the value of the size_t
 *              will be updated to the number of equal items in the vector.
 *
 * @return A pointer to the matching item on success, NULL on error or if
 *         no item was found.
 *
 */
void *sortedvec_find(sortedvec_s *svec, void *item, size_t *count);

/**
 * Remove an item that compares equal to a given item from the vector.
 *
 * It is not an error to try and remove an item that is not present.
 *
 * @param svec  A pointer to the sorted vector.
 * @param item  A pointer to the item to remove.
 *
 * @return 0 on success, -1 on error.
 *
 */
int sortedvec_rem(sortedvec_s *svec, void *item);

/**
 * Check whether an item is contained in a sorted vector.
 *
 * Additionally, a callback may be specified to allow the user to find
 * partial matches. For example, if the string 'cat' was looked up, and an
 * entry for 'catapult' existed.
 *
 * This will work if the partial match would be sorted immediately before the
 * full match, and can be used to find whether valid completions of a string
 * exist.
 *
 * @param svec A pointer to the sorted vector.
 * @param item A pointer to the item to lookup.
 * @param cb   A function pointer where @code
 *             f(a, b) == 1 where b is a partial or complete match of a
 *             f(a, b) == 0 otherwise.
 *             @endcode
 *
 * @return -1 on error, 0 on finding no match, 1 on finding a complete match,
 *         2 on finding a partial match.
 *
 */
int sortedvec_contains(sortedvec_s *svec, void *item, sortedvec_startcb_t cb);

/**
 * Insert an item into a sorted vector if an equal value is not there.
 *
 * @param svec A pointer to the sorted vector.
 * @param item A pointer to the item to try and insert.
 *
 * @return 0 on success, -1 on error.
 *
 */
static inline int sortedvec_ins_if_uniq(sortedvec_s *svec, void *item)
{
    int res;
    res = sortedvec_contains(svec, item, NULL);
    if (res == PALM_ERR)
        return PALM_ERR;

    if (res != PALM_COMPLETE)
        return sortedvec_ins(svec, item);

    return PALM_OK;
}

static inline vec_s *sortedvec_vec(sortedvec_s *svec)
{
    return &(svec->vec);
}

#endif
