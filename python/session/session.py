import os
import subprocess
import libtypes
import ctypes

from editor import log

from core.bufbind   import BufBind
from core.chr       import Chr
from core.cur       import Cur
from core.decoder   import Decoder, fmt_sanitize
from core.editor    import Editor
from core.font      import Font
from core.log       import Log
from core.input     import Input
from core.pos       import Pos
from core.rect      import Rect
from core.structure import Structure
from core.vec       import Vec
from core.wm        import Wm
from core.search    import Search, SearchResult

import core.reg as reg

from hooks import hooks
from lib   import lib
import paths


def print(*vals, sep=" ", end="\n"):
    string = sep.join(str(v) for v in vals) + end
    session.send_reply(string)

def print_expr(val):
    if val != None:
        print(repr(val))

"""Run a system command and display stdout in a popup."""
def sys(cmd, buf=True):
    out, inp = os.pipe()
    buf = editor.popup_buf(InfoBuffer)
    buf.show_info("stdout output for command '{{emph}}{}{{default}}':".format(cmd))

    print("Running shell command {} ...".format(cmd))

    buf.set_fattrs(["UTF8", "NL"])
    subprocess.Popen(cmd, shell=True, stdout=inp, stderr=subprocess.DEVNULL)
    buf.read_file(out)

def print_version():
    import sys
    print("Python " + sys.version)

def helpdocs():
    print(
        "You've run the helpdocs() function!\n"
        "You can now press Esc, x to exit this\n"
        "prompt and browse the help directories."
    )

    new = editor.new_buf(HelpListBuffer)
    new.show_dir(paths.helpdir)
    editor.win_assoc(new, buf.get_win())
