#include "buf/buf.h"
#include "editor.h"

#include "cmdlang/cmd.h"

#include "cmdlang/ctx.h"

static cmdlang_obj_s *cmd_ctx_push_obj(cmd_ctx_s *ctx, errmsg_t errmsg);
static int cmd_ctx_push_first_token(cmd_ctx_s *ctx, cmdlang_tok_s *tok, errmsg_t errmsg);
static int cmd_ctx_run_cmd(cmd_ctx_s *ctx, cmd_s *cmd, errmsg_t errmsg);

int cmd_ctx_init(cmd_ctx_s *ctx, buf_s *buf)
{
    ctx->buf    = buf;
    ctx->editor = buf->editor;
    ctx->lib    = &(buf->editor->cmdlib);

    list_init(&(ctx->tokens),   sizeof(cmdlang_tok_s));
    list_init(&(ctx->objstack), sizeof(cmdlang_obj_s));

    return PALM_OK;
}

static void cmd_ctx_empty_objstack(cmd_ctx_s *ctx)
{
    LIST_FOREACH_DATA(&(ctx->objstack), cmdlang_obj_s *, obj)
    {
        cmdlang_obj_kill(obj);
    }

    list_del(&(ctx->objstack), list_range(&(ctx->objstack)));
}
static void cmd_ctx_empty_tokens(cmd_ctx_s *ctx)
{
    LIST_FOREACH_DATA(&(ctx->tokens), cmdlang_tok_s *, tok)
    {
        cmdlang_tok_kill(tok);
    }

    list_del(&(ctx->tokens), list_range(&(ctx->tokens)));
}

void cmd_ctx_kill(cmd_ctx_s *ctx)
{
    cmd_ctx_empty_objstack(ctx);
    list_kill(&(ctx->objstack));

    cmd_ctx_empty_tokens(ctx);
    list_kill(&(ctx->tokens));
}

int cmd_ctx_cpy(cmd_ctx_s *dst, cmd_ctx_s *src)
{
    dst->buf = src->buf;
    dst->editor = src->editor;
    dst->lib    = src->lib;

    cmd_ctx_empty_objstack(dst);

    LIST_FOREACH_DATA(&(src->objstack), cmdlang_obj_s *, cpyobj)
    {
        cmdlang_obj_s *new;
        new = cmd_ctx_push_obj(dst, NULL);
        if (!new)
            return PALM_ERR;

        TRY(cmdlang_obj_init_cpy(new, cpyobj));
    }

    LIST_FOREACH_DATA(&(src->tokens), cmdlang_tok_s *, cpytok)
    {
        cmdlang_tok_s new;
        cmdlang_tok_init(&new);
        cmdlang_tok_cpy(&new, cpytok);

        TRY(cmd_ctx_push_token(dst, &new, NULL));
    }

    return PALM_OK;
}

static cmdlang_obj_s *cmd_ctx_push_obj(cmd_ctx_s *ctx, errmsg_t errmsg)
{
    list_item_s *item;
    item = list_push_last(&(ctx->objstack), NULL);

    if (!item)
    {
        ERRMSG(errmsg, "Could not push object to command context stack.");
        return NULL;
    }

    return list_item_data(item);
}

static int cmd_ctx_handle_token(
    cmd_ctx_s *ctx, cmdlang_tok_s *tok, errmsg_t errmsg
)
{
    char *sval;
    size_t slen;

    cmdlang_obj_s *obj;

    switch (tok->typ)
    {
    case CMDLANG_TOK_NUMBER:
        obj = cmd_ctx_push_obj(ctx, errmsg);
        if (!obj) return PALM_ERR;

        if (cmdlang_obj_init_number(obj, tok->ival) == PALM_ERR)
        {
            ERRMSG(errmsg, "Could not create cmdlang number object.");
            return PALM_ERR;
        }
        return PALM_OK;

    case CMDLANG_TOK_STR:
        obj = cmd_ctx_push_obj(ctx, errmsg);
        if (!obj) return PALM_ERR;

        sval = vec_get(&(tok->sval), 0);
        slen = vec_len(&(tok->sval));
        if (cmdlang_obj_init_str(obj, sval, slen) == PALM_ERR)
        {
            ERRMSG(errmsg, "Could not create cmdlang string object.");
            return PALM_ERR;
        }
        return PALM_OK;

    case CMDLANG_TOK_CMD:
    case CMDLANG_TOK_SHORT_CMD:
        return cmd_ctx_run_cmd(ctx, tok->cval, errmsg);

    case CMDLANG_TOK_PARAM:
        ERRMSG(errmsg, "Unexpected parameter in command.");
        return PALM_ERR;

    case CMDLANG_TOK_OBJ:
        obj = cmd_ctx_push_obj(ctx, errmsg);
        if (!obj) return PALM_ERR;

        if (cmdlang_obj_init_cpy(obj, &(tok->obj)) == PALM_ERR)
        {
            ERRMSG(errmsg, "Could not create cmdlang object from parameter.");
            return PALM_ERR;
        }

        return PALM_OK;

    default:
        ERRMSG(errmsg, "Unexpected token type: %d.", tok->typ);
        return PALM_ERR;
    }
}

static int cmd_ctx_grab_args(
    cmd_ctx_s *ctx, vec_s *specs, vec_s *objs, errmsg_t errmsg
)
{
    VEC_FOREACH(specs, cmd_arg_spec_s *, spec)
    {
        list_item_s   *item;
        cmdlang_obj_s *obj;

        item = list_last(&(ctx->objstack));
        if (!item)
        {
            if (spec->optional)
            {
                break;
            }
            else
            {
                ERRMSG(errmsg, "Missing argument %lu.", _ind + 1);
                return PALM_ERR;
            }
        }

        obj = list_item_data(item);

        if (!(obj->typ & spec->typemask))
        {
            ERRMSG(
                errmsg,
                "Argument %lu is not of the correct type.", _ind + 1
            );
            return PALM_ERR;
        }

        vec_app(objs, obj);
        list_del(&(ctx->objstack), LIST_RANGE(item, item));
    }

    return PALM_OK;
}

static int cmd_ctx_run_adverb(
    cmd_ctx_s *ctx, cmd_s *cmd, vec_s *args, errmsg_t errmsg
)
{
    int rtn;
    bool cont;
    void *user = NULL;
    cmd_ctx_s saved;

    rtn = PALM_OK;

    if (cmd_ctx_init(&saved, ctx->buf) == PALM_ERR)
    {
        ERRMSG(errmsg, "Could not initialize saved state for adverb.");
        return PALM_ERR;
    }

    if (cmd_ctx_cpy(&saved, ctx) == PALM_ERR)
    {
        ERRMSG(errmsg, "Could not save state for adverb.");
        return PALM_ERR;
    }

    if (cmd->info.adverb.start_cb)
        cmd->info.adverb.start_cb(ctx, vec_get(args, 0), vec_len(args), &user);

    do
    {
        cmd->info.adverb.iter_cb(
            ctx, vec_get(args, 0), vec_len(args), user, &cont
        );

        if (cont)
        {
            if (cmd_ctx_run(ctx, errmsg) == PALM_ERR)
            {
                rtn  = PALM_ERR;
                cont = false;
            }

            if (cmd_ctx_cpy(ctx, &saved) == PALM_ERR)
            {
                ERRMSG(errmsg, "Could not restored saved state for adverb.");
                cont = false;
                rtn  = PALM_ERR;
            }
        }
    } while (cont);

    if (cmd->info.adverb.end_cb)
        cmd->info.adverb.end_cb(ctx, vec_get(args, 0), vec_len(args), user);

    list_del(&(ctx->tokens), list_range(&(ctx->tokens)));

    return rtn;
}

static int cmd_ctx_run_noun(
    cmd_ctx_s *ctx, cmd_s *cmd, vec_s *args, errmsg_t errmsg
)
{
    cmdlang_obj_s *obj;
    obj = cmd_ctx_push_obj(ctx, errmsg);

    if (!obj) return PALM_ERR;

    cmd->info.noun.cb(ctx, vec_get(args, 0), vec_len(args), obj);

    return PALM_OK;
}

static int cmd_ctx_run_alias(
    cmd_ctx_s *ctx, cmd_s *cmd, vec_s *args, errmsg_t errmsg
)
{
    VEC_RFOREACH(&(cmd->info.alias.toks), cmdlang_tok_s *, tok)
    {
        cmdlang_tok_s  subst;
        if (tok->typ == CMDLANG_TOK_PARAM)
        {
            cmdlang_obj_s *obj;
            obj = vec_get(args, tok->ival);

            if (!obj)
            {
                ERRMSG(
                    errmsg,
                    "Invalid param value #%ld - I have %lu arguments.",
                    tok->ival, vec_len(args)
                );
                return PALM_ERR;
            }

            cmdlang_tok_init(&subst);
            subst.typ = CMDLANG_TOK_OBJ;
            cmdlang_obj_init_cpy(&(subst.obj), obj);
        }
        else
        {
            cmdlang_tok_init(&subst);
            cmdlang_tok_cpy(&subst, tok);
        }

        TRY(cmd_ctx_push_first_token(ctx, &subst, errmsg));
    }

    return PALM_OK;
}

static int cmd_ctx_run_verb(
    cmd_ctx_s *ctx, cmd_s *cmd, vec_s *args, errmsg_t errmsg
)
{
    cmd->info.verb.cb(ctx, vec_get(args, 0), vec_len(args));

    return PALM_OK;
}

static int cmd_ctx_run_cmd(cmd_ctx_s *ctx, cmd_s *cmd, errmsg_t errmsg)
{
    int rtn;
    vec_s args;
    vec_init(&args, sizeof(cmdlang_obj_s));

    if (cmd_ctx_grab_args(ctx, &(cmd->argspecs), &args, errmsg) == PALM_ERR)
    {
        vec_kill(&args);
        return PALM_ERR;
    }

    switch (cmd->typ)
    {
    case CMD_ADVERB:
        rtn = cmd_ctx_run_adverb(ctx, cmd, &args, errmsg);
        break;
    case CMD_NOUN:
        rtn = cmd_ctx_run_noun(ctx, cmd, &args, errmsg);
        break;
    case CMD_ALIAS:
        rtn = cmd_ctx_run_alias(ctx, cmd, &args, errmsg);
        break;
    case CMD_VERB:
        rtn = cmd_ctx_run_verb(ctx, cmd, &args, errmsg);
        break;
    }

    VEC_FOREACH(&args, cmdlang_obj_s *, obj)
    {
        cmdlang_obj_kill(obj);
    }

    vec_kill(&args);

    return rtn;
}

int cmd_ctx_run(cmd_ctx_s *ctx, errmsg_t errmsg)
{
    int rtn;

    while (list_len(&(ctx->tokens)))
    {
        list_item_s   *item;
        cmdlang_tok_s *tok;

        item = list_first(&(ctx->tokens));
        tok  = list_item_data(item);

        rtn = cmd_ctx_handle_token(ctx, tok, errmsg);

        cmdlang_tok_kill(tok);
        list_del(&(ctx->tokens), LIST_RANGE(item, item));

        if (rtn == PALM_ERR)
            break;
    }

    return rtn;
}

static int cmd_ctx_push_first_token(cmd_ctx_s *ctx, cmdlang_tok_s *tok, errmsg_t errmsg)
{
    if (!list_push_first(&(ctx->tokens), tok))
    {
        ERRMSG(errmsg, "Could not push token.");
        return PALM_ERR;
    }

    return PALM_OK;
}

int cmd_ctx_push_token(cmd_ctx_s *ctx, cmdlang_tok_s *tok, errmsg_t errmsg)
{
    if (!list_push_last(&(ctx->tokens), tok))
    {
        ERRMSG(errmsg, "Could not push token.");
        return PALM_ERR;
    }

    return PALM_OK;
}

int cmd_ctx_print_tokens(cmd_ctx_s *ctx)
{
    bool first;
    vec_s msg;
    vec_init(&msg, sizeof(char));

    first = true;
    LIST_FOREACH_DATA(&(ctx->objstack), cmdlang_obj_s *, obj)
    {
        if (!first)
            vec_str(&msg, " ");

        first = false;

        cmdlang_obj_to_str(obj, &msg);
    }

    if (vec_len(&msg))
    {
        vec_app(&msg, NULL);
        LOG(ALERT, INFO, "Result: %s", (char *)vec_get(&msg, 0));
    }

    vec_kill(&msg);

    return PALM_OK;
}

int cmd_ctx_cancel(cmd_ctx_s *ctx)
{
    cmd_ctx_empty_objstack(ctx);
    cmd_ctx_empty_tokens(ctx);

    return PALM_OK;
}
