#if !defined(TEXT_FONT_H)
# define TEXT_FONT_H
# include <stdio.h>
# include "types.h"

typedef enum
{
    FONT_BOLD    = 0x0001,
    FONT_FAINT   = 0x0002,
    FONT_ITALIC  = 0x0004,
    FONT_UNDER   = 0x0008,
    FONT_BLINK   = 0x0010,
    FONT_RBLINK  = 0x0020,
    FONT_REV     = 0x0040,
    FONT_HIDE    = 0x0080,
    FONT_STRIKE  = 0x0100
} font_attr_t;

#define FONT_COLOUR_FROM_RGB(r, g, b) (  \
        ((r << 16) & FONT_R_MASK) | \
        ((g << 8)  & FONT_G_MASK) | \
        ((b)       & FONT_B_MASK) \ \
    )

#define FONT_COLOUR_FROM_256(v) ( FONT_256COL | (v & FONT_256_MASK) )
#define FONT_COLOUR_FROM_8(v)   ( FONT_8COL   | (v & FONT_8_MASK) )

#define FONT_COLOUR_R(col) (((col) & FONT_R_MASK) >> 16)
#define FONT_COLOUR_G(col) (((col) & FONT_G_MASK) >> 8)
#define FONT_COLOUR_B(col) (((col) & FONT_B_MASK))

#define FONT_COLOUR_8COL(col)   ((col) & FONT_8_MASK)
#define FONT_COLOUR_256COL(col) ((col) & FONT_256_MASK)

typedef enum
{
    FONT_NOCOLOUR = 0x1000000,
    FONT_8COL     = 0x2000000,
    FONT_256COL   = 0x4000000,
    FONT_BRIGHT   = 0x0000100,

    FONT_R_MASK   = 0x0ff0000,
    FONT_G_MASK   = 0x000ff00,
    FONT_B_MASK   = 0x00000ff,

    FONT_256_MASK = 0x00000ff,
    FONT_8_MASK   = 0x0000007,

    FONT_BLACK    = FONT_COLOUR_FROM_8(0),
    FONT_RED      = FONT_COLOUR_FROM_8(1),
    FONT_GREEN    = FONT_COLOUR_FROM_8(2),
    FONT_YELLOW   = FONT_COLOUR_FROM_8(3),
    FONT_BLUE     = FONT_COLOUR_FROM_8(4),
    FONT_MAGENTA  = FONT_COLOUR_FROM_8(5),
    FONT_CYAN     = FONT_COLOUR_FROM_8(6),
    FONT_WHITE    = FONT_COLOUR_FROM_8(7),

    FONT_BBLACK   = FONT_COLOUR_FROM_8(0) | FONT_BRIGHT,
    FONT_BRED     = FONT_COLOUR_FROM_8(1) | FONT_BRIGHT,
    FONT_BGREEN   = FONT_COLOUR_FROM_8(2) | FONT_BRIGHT,
    FONT_BYELLOW  = FONT_COLOUR_FROM_8(3) | FONT_BRIGHT,
    FONT_BBLUE    = FONT_COLOUR_FROM_8(4) | FONT_BRIGHT,
    FONT_BMAGENTA = FONT_COLOUR_FROM_8(5) | FONT_BRIGHT,
    FONT_BCYAN    = FONT_COLOUR_FROM_8(6) | FONT_BRIGHT,
    FONT_BWHITE   = FONT_COLOUR_FROM_8(7) | FONT_BRIGHT,

    FONT_ORANGE   = FONT_COLOUR_FROM_256(202),
} font_colour_t;


#define FONT_DEF(var, name) \
    static unsigned short var = 0xffff; \
    if (var == 0xffff) var = font_by_name(name);

struct font
{
    char *name;
    font_colour_t fg, bg;
    font_attr_t attrs;
};

void font_start(void);
void font_end(void);

unsigned short font_by_name(const char *name);
void           font_print(unsigned short n, tty_s *tty);
unsigned short font_set(font_s *font);
void           font_get_info(unsigned short n, font_s *info);

#endif
