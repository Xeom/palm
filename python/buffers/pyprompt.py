from buffers.prompt    import PromptBuffer
from server.server     import add_global
from core.chrvec       import ChrVec
from core.cur          import Cur
from core.font         import Font
from core.buffer       import Buffer
from util.instanceiter import InstanceIter

import ast
import os

from hooks       import hooks
from util.encode import bytify
from editor      import log

@add_global()
class PromptHistory(object):
    maxfilelen = 100
    trimmedfilelen = 50
    filename   = os.path.expanduser("~/.palmhistory")

    def __init__(self):
        self.ind  = -1
        self.cont = []
        self.tmp  = []
        self.load_from(self.filename)
        self.save_all(self.filename)

    @staticmethod
    def encode_lines(lines):
        return repr(
            [
                l.decode("utf-8")
                for l in lines
            ]
        )

    @staticmethod
    def decode_lines(lines):
        return [
            l.encode("utf-8")
            for l in ast.literal_eval(lines)
        ]

    def load_from(self, fname):
        if not os.path.isfile(fname):
            return

        res = []
        with open(fname, "r") as f:
            for line in f.readlines():
                try:
                    lines = self.decode_lines(line)
                except:
                    continue

                res.append(lines)

        self.cont = list(reversed(res))

    def save_all(self, fname):
        with open(fname, "w") as f:
            for lines in reversed(self.cont):
                f.write(self.encode_lines(lines))
                f.write("\n")

    def save_lines(self, fname, lines):
        if len(self.cont) > self.maxfilelen:
            self.cont = self.cont[:self.trimmedfilelen]
            self.save_all(fname)

        with open(fname, "a") as f:
            f.write(self.encode_lines(lines))
            f.write("\n")

        self.cont.insert(0, lines)

    def prev(self, typed):
        if len(self.cont) == 0:
            return typed

        if self.ind == -1:
            self.ind = 0
            self.tmp = typed
        else:
            self.ind = min(len(self.cont) - 1, self.ind + 1)

        return self.cont[self.ind]

    def next(self, typed):
        if self.ind == -1:
            return typed

        if self.ind == 0:
            self.ind = -1
            tmp = self.tmp
            self.tmp = []
            return tmp
        else:
            self.ind -= 1

        return self.cont[self.ind]

    def enter(self, typed):
        self.ind = -1
        self.tmp = []

        while len(typed) and len(typed[-1]) == 0:
            typed.pop()

        self.save_lines(self.filename, typed)

pyprompthistory = PromptHistory()

@add_global()
class PyPromptBuffer(PromptBuffer):
    def init(self, *args):
        super().init(*args)
        self.pyclient = None
        self.server.eval_tout *= 2
        self.history = None
        self.prevbuf = None
        self.history = pyprompthistory

    def prev(self):
        if self.history != None:
            lines = self.history.prev(list(self.get_all_typed()))
            while self.pop_prompt() != None:
                pass

            for line in lines:
                self.push_prompt(chrs=ChrVec.from_str(line))

    def next(self):
        if self.history != None:
            lines = self.history.next(list(self.get_all_typed()))
            while self.pop_prompt() != None:
                pass

            for line in lines:
                self.push_prompt(chrs=ChrVec.from_str(line))

    def open_session(self, buf):
        self.prevbuf  = buf
        self.session  = buf.server.add_session()

    def suicide(self):
        if self.editor.selbuf.ptr == self.ptr and self.prevbuf != None:
            prevwin = self.prevbuf.get_win()
        else:
            prevwin = None

        if prevwin == -1:
            prevwin = None

        win = self.get_win()
        if win != -1:
            self.editor.win_close(win)

        self.editor.kill_buf_soon(self)
        if prevwin != None:
            self.editor.sel_win(prevwin)

    def run_code(self, lines):
        self.history.enter(lines)
        data = b"\n".join(lines)

        if data == b"quit\n":
            self.suicide()
            return

        try:
            tree  = ast.parse(data)
            nodes = list(ast.iter_child_nodes(tree))
            if len(nodes) == 1 and isinstance(nodes[0], ast.Expr):
                data = b"print_expr(" + data + b")"
        except:
            pass

        def replycb(data):
            for line in data.strip().split(b"\n"):
                self.log(line)

        self.session.execute(data, replycb=replycb)
        self.done()

    def bind_all(self):
        self.bind.set_mode("pyprompt")

@add_global()
class SinglePyPromptBuffer(PyPromptBuffer):
    multiline = False
    prompt1   = ChrVec.from_fmt("{prompt-1}> ")
    prompt2   = ChrVec.from_fmt("{prompt-2}. ")
    prompt3   = ChrVec.from_fmt("{prompt-3}< ")

    def run_code(self, typed):
        self.suicide()
        super().run_code(typed)

@add_global(args=["buf"])
def open_buffer_pyprompt(buf: Buffer):
    """Open a pyprompt for a buffer and select it.

    If one is already displayed in a window, then that window is
    selected, otherwise, the current window is split to make space
    for a new PyPromptBuffer."""
    if isinstance(buf, PyPromptBuffer):
        buf.suicide()
        return

    editor = buf.editor

    for oth in editor.bufs:
        if isinstance(oth, PyPromptBuffer) and oth.prevbuf == buf:
            win = oth.get_win()
            if win != -1:
                editor.sel_buf(oth)
                return

    editor.popup_buf(PyPromptBuffer).open_session(buf)

@add_global(args=["buf"])
def single_pyprompt(buf: Buffer, precur: str, postcur: str):
    """Open a pyprompt for the current buffer, which executes only one line

    A pyprompt buffer is popped up, which will close after one Enter.
    The text before and after the cursor can be specified."""
    prompt = buf.editor.popup_buf(SinglePyPromptBuffer)
    prompt.open_session(buf)

    if "\n" in postcur:
        postcur, lines = postcur.split("\n", 1)
        lines = lines.split("\n")
    else:
        lines = []

    line = ChrVec.from_fmt(precur)
    post = ChrVec.from_fmt(postcur)
    lines = [ChrVec.from_fmt(l) for l in lines]

    off  = len(post)
    line.cpy_vec(post)

    prompt.clear_prompt()
    for l in [line] + lines:
        prompt.push_prompt(chrs=l)

    prompt.set_cur(Cur.at(0, prompt.line_len(0) - off))
