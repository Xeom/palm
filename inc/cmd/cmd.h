#if !defined(CMD_CMD_H)
# define CMD_CMD_H
# include "types.h"

struct cmd_binding_info
{
    bool multi;
    bool greedy;
    char shortcut[256];
};

int cmd_mount_all(cmd_lib_s *lib);

void cmd_shortcut_binding(key_s *key, buf_s *buf, void *param, size_t size);

void cmd_type_binding(key_s *key, buf_s *buf, void *param, size_t size);

void cmd_run_binding(key_s *key, buf_s *buf, void *param, size_t size);

void cmd_cancel_binding(key_s *key, buf_s *buf, void *param, size_t size);

#endif
