#!/usr/bin/python3
import os
import sys

path = os.path.dirname(os.path.realpath(__file__))
root = os.path.dirname(path)
python = os.path.join(root, "python")
sys.path.append(python)

import bind
from bind.mode import CCommand, PyCommand, Mode
from util.keys import Keys

def print_cmd(cmd):
    out = "### `{keys}` {name}\n".format(keys=cmd.keys.decode("utf-8"), name=cmd.name)
    if isinstance(cmd, CCommand):
        out += "C binding to `{fname}`.\n".format(
            fname = cmd.fptr.__name__, arg = cmd.arg
        )

    elif isinstance(cmd, PyCommand):
        out += "Python binding to run,\n```\n{code}\n```\n".format(**vars(cmd))

    if cmd.multicursor > 0:
        out += "Runs multi-cursor.\n"
    if cmd.multicursor < 0:
        out += "Runs multi-cursor, in reverse order.\n"
    if cmd.shortcut != None:
        out += "Shortcut: {}.\n".format(Keys.reverse_keys(cmd.shortcut))

    print(out)

print("""
Command List
============

This is automatically generated documentation, don't trust it
too much :)

Default commands
----------------
""")

for cmd in sorted(Mode.defaultcmds, key=lambda c:c.keys):
    print_cmd(cmd)

for mode in bind.modes:
    if len(mode.cmds) == 0:
        continue

    print(("\n`{}` Mode\n"
           "---------\n").format(mode.name))

    for cmd in mode.cmds:
        print_cmd(cmd)
