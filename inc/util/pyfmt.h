#if !defined(UTIL_PYFMT_H)
# define UTIL_PYFMT_H
# include "container/vec.h"

void pyfmt_from_bytes(vec_s *str, char *chrs, size_t len);

void pyfmt_from_key(vec_s *str, key_s *key);

#endif
