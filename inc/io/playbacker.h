#if !defined(IO_PLAYBACKER_H)
# define IO_PLAYBACKER_H
# include "types.h"
# include "container/vec.h"
# include "util/pool.h"
# include <stdatomic.h>

/* --- PLAYBACKER --- */

/* This is a structure for sending data between threads. It is *
 * effectively a wrapper around a pipe. The data is sent async *
 * by a worker thread, so that the sending thread need not     *
 * block or worry whether anyone is listening.                 *
 *                                                             *
 * To send data, you can call the playbacker_send function,    *
 * which starts transmitting data into the file                *
 * playbacker->wrfd.                                           *
 *                                                             *
 * The listening process or thread can then read               *
 * playbacker->rdfd to get the data.                           */

struct playbacker
{
    int wrfd;
    int rdfd;

    pthread_mutex_t mtx;
    pool_s         *pool;
    pool_job_s      job;
    atomic_bool     jobrunning;
    vec_s           sendbuf;
};

/**
 * Initialize a playbacker.
 *
 * @param pb   A pointer to the playbacker_s structure.
 * @param pool A pointer to a pool_s structure for running jobs.
 *
 * @return 0 on success, -1 on error.
 *
 */
int playbacker_init(playbacker_s *pb, pool_s *pool);

/**
 * Kill a playbacker.
 *
 * Must be called to release resources associated with a playbacker.
 *
 * @param pb A pointer to the playbacker_s structure.
 *
 */
void playbacker_kill(playbacker_s *pb);

/**
 * Send data across a playbacker.
 *
 * @param pb  A pointer to the playbacker_s structure.
 * @param str A pointer to the data to send.
 * @param len The number of bytes to send.
 *
 */
void playbacker_send(playbacker_s *pb, char *str, size_t len);

#endif
