#include <stddef.h>
#include <string.h>
#include <errno.h>

#include "io/tty.h"
#include "text/font.h"
#include "util/macro.h"
#include "util/err.h"
#include "log/log.h"

#include "plat/vt100.h"

typedef struct vt100_mode_conf vt100_mode_conf_s;

/**
 * An enum containing potential final bytes of terminal CSI sequences.
 *
 */
typedef enum
{
    VT100_CSI_SGR = 'm', /**< Set graphic rendition. */
    VT100_CSI_SM  = 'h', /**< Set mode. */
    VT100_CSI_RM  = 'l', /**< Reset mode. */
    VT100_CSI_CUP = 'H', /**< Goto cursor position. */
    VT100_CSI_EL  = 'K', /**< Erase Line. */
    VT100_CSI_ED  = 'J'  /**< Erase display. */
} vt100_csi_final_t;

/**
 * The parameter values that can be sent an a SGR CSI sequence.
 *
 * These represent different font attributes.
 *
 */
typedef enum
{
    VT100_SGR_RESET  = 0, /**< Reset all attributes. */
    VT100_SGR_BOLD   = 1, /**< Bold font. */
    VT100_SGR_FAINT  = 2, /**< Faint font. */
    VT100_SGR_ITALIC = 3, /**< Italic font. */
    VT100_SGR_UNDER  = 4, /**< Underline. */
    VT100_SGR_BLINK  = 5, /**< Blinking. */
    VT100_SGR_RBLINK = 6, /**< Rapid blinking. */
    VT100_SGR_REV    = 7, /**< Reversed colours. */
    VT100_SGR_HIDE   = 8, /**< Hidden characters. */
    VT100_SGR_STRIKE = 9, /**< Strikethrough. */

    VT100_SGR_FG_8COL_0 = 30, /**< Start of the 8col foreground palette. */
    VT100_SGR_BG_8COL_0 = 40, /**< Start of the 8col background palette. */

    VT100_SGR_FG_8COL_BRIGHT_0 = 90,  /**< The bright 8col fg palette. */
    VT100_SGR_BG_8COL_BRIGHT_0 = 100, /**< The bright 8col bg palette. */

    VT100_SGR_FG = 38, /**< Set a foreground colour. (followed by type) */
    VT100_SGR_BG = 48, /**< Set a background colour. (followed by type) */

    VT100_SGR_256COL = 5, /**< 256 Colour type. (followed by index) */
    VT100_SGR_RGBCOL = 2  /**< RGB Colour type. (followed by r,g,b vals) */
} vt100_sgr_param_t;

/**
 * Optional parameters to pass to CSI erase sequences.
 *
 */
typedef enum
{
    VT100_ERASE_AFTER  = 0, /**< Erase from and including the cursor. */
    VT100_ERASE_BEFORE = 1, /**< Erase up to and including the cursor. */
    VT100_ERASE_ALL    = 2  /**< Erase everything. */
} vt100_erase_t;

/**
 * Types for modes that can be set on the terminal.
 *
 * These can be used to construct the correct sequence to enable and disable
 * the modes.
 *
 */
typedef enum
{
    VT100_MODE_TYPE_PRIVATE, /**< Non-ANSI modes. */
    VT100_MODE_TYPE_ANSI,    /**< ANSI specified modes. */
    VT100_MODE_TYPE_RAW      /**< Unique modes with special sequences. */
} vt100_mode_type_t;

struct vt100_mode_conf
{
    vt100_mode_t mode;     /**< The index of the mode. */
    vt100_mode_type_t typ; /**< The type of the mode.  */
    int  val;              /**< For private/ansi modes, the mode number. */
    const char *set;       /**< For raw modes, the enable sequence. */
    const char *reset;     /**< For raw modes, the disable sequence. */
};
/* MACROS TO HELP DEFINE MODES */

#define PRIVATE_MODE(_mode, _val) {  \
    .mode = VT100_MODE_ ## _mode,    \
    .typ  = VT100_MODE_TYPE_PRIVATE, \
    .val  = _val                     \
}
#define ANSI_MODE(_mode, _val) {     \
    .mode = VT100_MODE_ ## _mode,    \
    .typ  = VT100_MODE_TYPE_ANSI,    \
    .val  = _val                     \
}
#define RAW_MODE(_mode, _set, _reset) { \
    .mode = VT100_MODE_ ## _mode,       \
    .typ  = VT100_MODE_TYPE_RAW,        \
    .set  = _set, .reset = _reset       \
}

static const vt100_mode_conf_s *vt100_get_mode_conf(vt100_mode_t mode);
static int vt100_set_mode_conf(
    vt100_s *vt,
    const vt100_mode_conf_s *conf,
    bool val
);

static int buf_append(char **buf, char *end, char *fmt, ...);
static int param_append(int **params, int *end, ...);

static int vt100_num_csi(
    vt100_s    *vt,
    const char *prefix,
    const int  *vals,
    size_t n,
    vt100_csi_final_t final
);

static int vt100_csi(
    vt100_s *vt,
    const char *param,
    const char *intr,
    char final
);

static int vt100_colour_params(
    int **iter, int *end, font_colour_t col, bool bg
);


/**
 * Get the config for how to set/reset a mode.
 *
 * @param mode The mode to look up.
 *
 * @return A pointer to the mode config, or NULL if it does not exist.
 *
 */
static const vt100_mode_conf_s *vt100_get_mode_conf(vt100_mode_t mode)
{
    size_t i;

    /* Array of all modes. */
    static const vt100_mode_conf_s modeconf[] =
    {
        ANSI_MODE(LINE_FEED, 20),

        PRIVATE_MODE(CURSOR_KEY,    1),
        PRIVATE_MODE(COLUMN,        3),
        PRIVATE_MODE(SCROLLING,     4),
        PRIVATE_MODE(SCREEN,        5),
        PRIVATE_MODE(ORIGIN,        6),
        PRIVATE_MODE(AUTO_WRAP,     7),
        PRIVATE_MODE(AUTO_REP,      8),
        PRIVATE_MODE(INTERLACE,     9),
        PRIVATE_MODE(SHOW_CUR,      25),
        PRIVATE_MODE(ALT_SCREEN,    1049),
        PRIVATE_MODE(PASTE_BRACKET, 2004),

        RAW_MODE(KEYPAD, "\033\075", "\033\076")
    };

    for (i = 0; i < DIM(modeconf); ++i)
    {
        if (modeconf[i].mode == mode)
            return &(modeconf[i]);
    }

    return NULL;
}

/**
 * Set or reset a mode from its config.
 *
 * @param vt   A pointer to the VT100 structure to set the mode on.
 * @param conf A pointer to the mode config.
 * @param val  true to set the mode, false to reset the mode.
 *
 * @return 0 on success, -1 on error.
 *
 */
static int vt100_set_mode_conf(
    vt100_s *vt,
    const vt100_mode_conf_s *conf,
    bool val
)
{
    const char *str;
    switch (conf->typ)
    {
    /* Private modes, e.g. \033[?25h */
    case VT100_MODE_TYPE_PRIVATE:
        TRY(vt100_num_csi(
            vt, "?", &(conf->val), 1, val ? VT100_CSI_SM : VT100_CSI_RM
        ));
        break;
    /* Ansi modes, e.g. \033[20h */
    case VT100_MODE_TYPE_ANSI:
        TRY(vt100_num_csi(
            vt, "",  &(conf->val), 1, val ? VT100_CSI_SM : VT100_CSI_RM
        ));
        break;
    /* Raw modes, e.g. \033< */
    case VT100_MODE_TYPE_RAW:
        str = val ? conf->set : conf->reset;
        if (tty_write(vt->tty, str, strlen(str)) < 0) return PALM_ERR;
        break;
    }

    return PALM_OK;
}

/**
 * Send a CSI sequence to a VT100.
 *
 * @param vt    A pointer to the VT100 structure to send the sequence to.
 * @param param A string containing the param portion of the sequence.
 * @param intr  A string containing the intermediate portion of the sequence.
 * @param final The final character.
 *
 * @return 0 on success, -1 on error.
 *
 */
static int vt100_csi(
    vt100_s *vt, const char *param, const char *intr, char final
)
{
    size_t i;
    vec_s  seq;


    /* Parameter bytes are valid between 0x30-3f */
    for (i = 0; param[i]; ++i)
    {
        if (param[i] > 0x3f || param[i] < 0x30)
        {
            LOG(INFO, ERR, "Invalid CSI param byte 0x%x", param[i]);
            return PALM_ERR;
        }
    }

    /* Intermediate bytes are valid between 0x20-2f */
    for (i = 0; intr[i]; ++i)
    {
        if (intr[i] > 0x2f || param[i] < 0x20)
        {
            LOG(INFO, ERR, "Invalid CSI intermediate byte 0x%x", param[i]);
            return PALM_ERR;
        }
    }

    /* Final bytes are valid between 0x40-7e */
    if (final < 0x40 || final > 0x7e)
    {
        LOG(INFO, ERR, "Invalid CSI final byte 0x%x", final);
        return PALM_ERR;
    }

    /* Send the final sequence */
    vec_init(&seq, sizeof(char));

    if (!vec_fmt(&seq, "\033[%s%s%c", param, intr, final))
    {
        LOG(INFO, ERR, "Could not format CSI sequence.");
        vec_kill(&seq);
        return PALM_ERR;
    }

    if (tty_write(vt->tty, vec_get(&seq, 0), vec_len(&seq)) == PALM_ERR)
    {
        LOG(INFO, ERR, "Could not send CSI sequence.");
        vec_kill(&seq);
        return PALM_ERR;
    }

    vec_kill(&seq);
    return PALM_OK;
}

/**
 * Append a printf-style format string to the end of a buffer of characters.
 *
 * This function checks that there is sufficient space in the buffer for the
 * characters, and returns an error if it runs out.
 *
 * @param buf A pointer to the start of the empty space in the buffer.
 *            This pointer is modified as characters are written.
 * @param end A pointer to the end of the buffer, i.e. &buf[sizeof(buf)]
 * @param fmt A printf-style format string.
 * @param ... printf-style format arguments.
 *
 * @return 0 on success, -1 on error.
 *
 */
static int buf_append(char **buf, char *end, char *fmt, ...)
{
    size_t  len;
    int     res;
    va_list args;

    len = end - *buf;

    if (len == 0) return PALM_ERR;

    va_start(args, fmt);
    res = vsnprintf(*buf, len, fmt, args);
    va_end(args);

    if (res >= (int)len || res < 0)
        return PALM_ERR;

    *buf += res;

    return PALM_OK;
}

/**
 * Append integer parameters to an array of parameters.
 *
 * If there is no more room, an error is returned.
 *
 * @param params A pointer to the start of empty space in the array.
 *               This pointer is modified as values are written.
 * @param end    A pointer to the last space, i.e. &params[DIM(params)]
 * @param ...    A sequence of integer parameters, terminating in -1.
 *
 * @return 0 on success, -1 on error.
 *
 */
static int param_append(int **params, int *end, ...)
{
    va_list args;
    int p;

    va_start(args, end);
    p = va_arg(args, int);
    while (p >= 0)
    {
        if (*params >= end) return PALM_ERR;

        **params  = p;
        *params  += 1;

        p = va_arg(args, int);
    }

    return PALM_OK;
}

/**
 * Append a series of parameters to a list of parameters.
 *
 */
#define PARAMS(iter, end, ...) TRY(param_append(iter, end, __VA_ARGS__, -1))

/**
 * Send a CSI command with numeric parameters.
 *
 * @param vt     A pointer to the VT100 structure.
 * @param prefix A prefix to prepend to the parameters.
 * @param vals   A pointer to an array of integer parameters.
 * @param n      The number of parameters in the array.
 * @param final  The final character to terminate the command.
 *
 * @return 0 on success, -1 on error.
 *
 */
static int vt100_num_csi(
    vt100_s    *vt,
    const char *prefix,
    const int  *vals,
    size_t n,
    vt100_csi_final_t final
)
{
    size_t i;
    char buf[1024];
    char *iter, *end;

    iter = &buf[0];
    end  = &buf[DIM(buf)];

    TRY(buf_append(&iter, end, "%s", prefix));

    for (i = 0; i < n; ++i)
    {
        TRY(buf_append(&iter, end, "%s%d", (i ? ";" : ""), vals[i]));
    }

    TRY(vt100_csi(vt, buf, "", final));

    return PALM_OK;
}

/**
 * Get parameters for a particular colour to send to an SGR command.
 *
 * For the first two parameters, see param_append().
 *
 * @param iter A pointer to empty space in an array of parameters.
 * @param end  A pointer to the end of the array of parameters.
 * @param col  The colour to generate parameters for.
 * @param bg   true if the colour is a background.
 *
 * @return 0 on success, -1 on error.
 *
 */
static int vt100_colour_params(
    int **iter, int *end, font_colour_t col, bool bg
)
{
    /* The NOCOLOUR flag means we use the default colour. */
    if (col & FONT_NOCOLOUR)
        return PALM_OK;

    /* A colour using the 8 colour palette. */
    if (col & FONT_8COL)
    {
        int base;
        /* We select one parameter, 7 bits plus a base */
        if (bg)
        {
            if (col & FONT_BRIGHT) base = VT100_SGR_BG_8COL_BRIGHT_0;
            else                   base = VT100_SGR_BG_8COL_0;
        }
        else
        {
            if (col & FONT_BRIGHT) base = VT100_SGR_FG_8COL_BRIGHT_0;
            else                   base = VT100_SGR_FG_8COL_0;
        }

        /* Append the colour parameter. */
        PARAMS(iter, end, base + FONT_COLOUR_8COL(col));
    }
    /* A 256 colour palette colour. */
    else if (col & FONT_256COL)
    {
        /* We send three bytes for this. */
        PARAMS(
            iter, end,
            bg ? VT100_SGR_BG : VT100_SGR_FG, /* Set fg or bg colour. */
            VT100_SGR_256COL,                 /* Set the type as 256 colour. */
            FONT_COLOUR_256COL(col)           /* Select the colour. */
        );
    }
    /* An RGB colour palette colour. */
    else
    {
        PARAMS(
            iter, end,
            bg ? VT100_SGR_BG : VT100_SGR_FG, /* Set fg or bg colour. */
            VT100_SGR_RGBCOL,   /* Set RGB as the colour type. */
            FONT_COLOUR_R(col), /* Red. */
            FONT_COLOUR_G(col), /* Green. */
            FONT_COLOUR_B(col)  /* Blue. */
        );
    }

    return PALM_OK;
}

/**
 * Initialize a VT100 terminal structure.
 *
 * @param vt  A pointer to the structure to initialize.
 * @param tty A pointer to a tty connected to the VT100.
 *
 * @return 0 on success, -1 on error.
 *
 */
int vt100_init(vt100_s *vt, tty_s *tty)
{
    vt->tty   = tty;
    vt->modes = VT100_MODE_DEFAULT_SET;

    TRY(vt100_reset_font(vt));

    return PALM_OK;
}

int vt100_set_font(vt100_s *vt, font_s *font)
{
    int params[64];
    int *iter, *end;

    iter = &(params[0]);
    end  = &(params[DIM(params)]);

    /* Start by resetting the colours. */
    PARAMS(&iter, end, VT100_SGR_RESET);

    /* Set colours. */
    TRY(vt100_colour_params(&iter, end, font->fg, false));
    TRY(vt100_colour_params(&iter, end, font->bg, true));

    /* Set font attributes. */
    if (font->attrs & FONT_BOLD)   PARAMS(&iter, end, VT100_SGR_BOLD);
    if (font->attrs & FONT_FAINT)  PARAMS(&iter, end, VT100_SGR_FAINT);
    if (font->attrs & FONT_ITALIC) PARAMS(&iter, end, VT100_SGR_ITALIC);
    if (font->attrs & FONT_UNDER)  PARAMS(&iter, end, VT100_SGR_UNDER);
    if (font->attrs & FONT_BLINK)  PARAMS(&iter, end, VT100_SGR_BLINK);
    if (font->attrs & FONT_RBLINK) PARAMS(&iter, end, VT100_SGR_RBLINK);
    if (font->attrs & FONT_REV)    PARAMS(&iter, end, VT100_SGR_REV);
    if (font->attrs & FONT_HIDE)   PARAMS(&iter, end, VT100_SGR_HIDE);
    if (font->attrs & FONT_STRIKE) PARAMS(&iter, end, VT100_SGR_STRIKE);

    /* Send the command. */
    TRY(vt100_num_csi(vt, "", params, iter - params, VT100_CSI_SGR));

    return PALM_OK;
}

int vt100_set_modes(vt100_s *vt, vt100_mode_t modes, bool val)
{
    int i;

    /* Modify the bits recorded in the vt structure. */
    SET_BITS(vt->modes, modes, val);

    for (i = 1; i; i <<= 1)
    {
        const vt100_mode_conf_s *conf;
        conf = vt100_get_mode_conf(i);
        if (!conf)
            continue;

        if (i & modes)
        {
            TRY(vt100_set_mode_conf(vt, conf, val));
        }
    }

    return PALM_OK;
}

int vt100_reset_font(vt100_s *vt)
{
    /* Send \033[0m */
    int params[] = { VT100_SGR_RESET };
    TRY(vt100_num_csi(vt, "", params, DIM(params), VT100_CSI_SGR));

    return PALM_OK;
}

int vt100_set_cursor(vt100_s *vt, int ln, int cn)
{
    /* Send \033[xx;xxH */
    int params[] = { ln, cn };
    TRY(vt100_num_csi(vt, "", params, DIM(params), VT100_CSI_CUP));

    return PALM_OK;
}

int vt100_clear_line(vt100_s *vt)
{
    /* Send \033[2K */
    int params[] = { VT100_ERASE_ALL };
    TRY(vt100_num_csi(vt, "", params, DIM(params), VT100_CSI_EL));

    return PALM_OK;
}

int vt100_clear_display(vt100_s *vt)
{
    /* Send \033[2J */
    int params[] = { VT100_ERASE_ALL };
    TRY(vt100_num_csi(vt, "", params, DIM(params), VT100_CSI_ED));

    return PALM_OK;
}
