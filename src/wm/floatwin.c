#include "wm/floatwin.h"
#include "wm/wm.h"
#include "util/macro.h"

void wm_floatwin_init(floatwin_s *fw)
{
    framebuf_init(&(fw->fbuf));
    fw->rect.x = 0;
    fw->rect.y = 0;
    fw->rect.w = 0;
    fw->rect.h = 0;
}

void wm_floatwin_kill(floatwin_s *fw)
{
    framebuf_kill(&(fw->fbuf));
}

int wm_floatwin_resize(floatwin_s *fw, rect_s *dims)
{
    framebuf_resize(&(fw->fbuf), dims->w, dims->h);
    memcpy(&(fw->rect), dims, sizeof(rect_s));

    return 0;
}

int wm_floatwin_set(floatwin_s *fw, rect_s *rect, chr_s *mem)
{
    framebuf_set(&(fw->fbuf), rect, mem);

    return 0;
}

int wm_floatwin_fill(floatwin_s *fw, rect_s *rect, chr_s *mem)
{
    framebuf_fill(&(fw->fbuf), rect, mem);

    return 0;
}

void wm_floatwin_stamp(floatwin_s *fw, display_s *disp)
{
    rect_s srcrect;
    srcrect.x = 0;
    srcrect.y = 0;
    srcrect.w = fw->rect.w;
    srcrect.h = fw->rect.h;

    display_stamp(disp, &(fw->rect), &(fw->fbuf), &srcrect);
}

int wm_add_floatwin(wm_s *wm)
{
    int rtn;
    floatwin_s *new;

    new = malloc(sizeof(floatwin_s));
    wm_floatwin_init(new);

    WITH_MTX(&(wm->mtx))
    {
        rtn = -1;
        VEC_FOREACH(&(wm->floatwins), floatwin_s **, fwptr)
        {
            if (!(*fwptr))
            {
                rtn = _ind;
                *fwptr = new;
                break;
            }
        }

        if (rtn == -1)
        {
            rtn = vec_len(&(wm->floatwins));
            vec_app(&(wm->floatwins), &new);
        }
    }

    return rtn;
}

int wm_rem_floatwin(wm_s *wm, int ind)
{
    int res;
    floatwin_s **fw;

    WITH_MTX(&(wm->mtx))
    {
        fw = vec_get(&(wm->floatwins), ind);
        if (fw && *fw)
        {
            wm_floatwin_kill(*fw);
            free(*fw);
            *fw = NULL;
            res = 0;
        }
        else
        {
            res = -1;
        }

        while (vec_len(&(wm->floatwins)) && *(win_s **)vec_end(&(wm->floatwins)) == NULL)
        {
            vec_del(&(wm->floatwins), vec_len(&(wm->floatwins)) - 1, 1);
        }
    }

    wm_stamp_disp_all(wm);

    return res;
}

int wm_resize_floatwin(wm_s *wm, int ind, rect_s *dims)
{
    int res;
    floatwin_s **fw;

    WITH_MTX(&(wm->mtx))
    {
        fw = vec_get(&(wm->floatwins), ind);
        if (fw && *fw)
            res = wm_floatwin_resize(*fw, dims);
        else
            res = -1;
    }

    wm_stamp_disp_all(wm);

    return res;
}


int wm_set_floatwin(wm_s *wm, int ind, rect_s *rect, chr_s *mem)
{
    int res;
    floatwin_s **fw;

    WITH_MTX(&(wm->mtx))
    {
        fw = vec_get(&(wm->floatwins), ind);
        if (fw && *fw)
            res = wm_floatwin_set(*fw, rect, mem);
        else
            res = -1;
    }

    wm_stamp_disp_floatwins(wm);

    return res;
}

int wm_fill_floatwin(wm_s *wm, int ind, rect_s *rect, chr_s *mem)
{
    int res;
    floatwin_s **fw;

    WITH_MTX(&(wm->mtx))
    {
        fw = vec_get(&(wm->floatwins), ind);
        if (fw && *fw)
            res = wm_floatwin_fill(*fw, rect, mem);
        else
            res = -1;
    }

    wm_stamp_disp_floatwins(wm);

    return res;
}
