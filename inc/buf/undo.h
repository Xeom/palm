#if !defined(BUF_UNDO_H)
# define BUF_UNDO_H
# include "types.h"
# include "container/vec.h"
# include <stdbool.h>

typedef enum
{
    UNDO_TYPE_NONE,
    UNDO_TYPE_INS,
    UNDO_TYPE_SET,
    UNDO_TYPE_DEL
} undo_action_type_t;

struct undo_list
{
    vec_s actions;
    long off, maxlen;
    bool enabled;
};

struct undo_action
{
    undo_action_type_t typ;
    long ln, n;
    vec_s *prev;
    vec_s *new;
    double ts;

    long parentoff;
    vec_s children;
};

bool buf_undo_get_enable(buf_s *buf);
void buf_undo_set_enable(buf_s *buf, bool enable);

void buf_undo_clear(buf_s *buf);

int buf_undo(buf_s *buf);
int buf_redo(buf_s *buf, long branch);

void undo_list_init(undo_list_s *list);

void undo_list_kill(undo_list_s *list);

int undo_list_compact(undo_list_s *list);

undo_action_s *undo_list_add(undo_list_s *list);

int undo_list_undo(undo_list_s *list, buf_s *buf);

size_t undo_list_num_branches(undo_list_s *list);

int undo_list_redo(undo_list_s *list, buf_s *buf, size_t branch);

void undo_action_init(undo_action_s *act);

void undo_action_kill(undo_action_s *act);

void undo_action_redo(undo_action_s *act, buf_s *buf);

void undo_action_undo(undo_action_s *act, buf_s *buf);

void undo_from_ins(undo_action_s *act, size_t ln, size_t n);

void undo_from_set(undo_action_s *act, size_t ln, vec_s *prev, vec_s *new);

void undo_from_del(undo_action_s *act, size_t ln, vec_s *prev);

#endif
