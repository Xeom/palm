#!/usr/bin/python3
import unittest
import sys
import os

here = os.path.dirname(os.path.realpath(__file__))
os.chdir(here)

sys.path.append("../../python")

from dirbuffer import *
from edit import *
from nav import *

if __name__ == "__main__":
    unittest.main()
