#include "buf/finfo.h"
#include "log/log.h"
#include "buf/buf.h"
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <libgen.h>

char *finfo_bak_path = ".%s.%d.bak";

static int finfo_update_modtime(finfo_s *finfo);

void finfo_init(finfo_s *finfo, poll_ctx_s *pollctx)
{
    pthread_mutexattr_t mtxattr;
    pthread_mutexattr_init(&mtxattr);
    pthread_mutexattr_settype(&mtxattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&(finfo->mtx), &mtxattr);

    file_init(&(finfo->file), pollctx);

    finfo->newfile    = false;
    finfo->attrs      = finfo->file.attrs;
    finfo->associated = false;
    file_set_attrs(&(finfo->file), finfo->attrs);
}

void finfo_kill(finfo_s *finfo)
{
    pthread_mutex_destroy(&(finfo->mtx));
    file_kill(&(finfo->file));
}

int finfo_assoc(finfo_s *finfo, const char *path)
{
    int rtn;
    char errbuf[IO_ERRBUF_LEN];

    switch (io_is_file(path, errbuf))
    {
    case 1:
        break;
    case 0:
        if (io_create_file(path, errbuf) == -1)
        {
            LOG(ALERT, ERR, "Could not create '%s' to associate it: %s", path, errbuf);
            return -1;
        }
        else
        {
            finfo->newfile = true;
            LOG(ALERT, SUCC, "Created new file.");
        }
        break;
    case -1:
        LOG(ALERT, ERR, "Could not tell if '%s' is a file: %s", path, errbuf);
        return -1;
    }
    /* TODO: Make it only make the dir if the file doesn't exist */

    pthread_mutex_lock(&(finfo->mtx));

    if (io_path_decomp(&(finfo->path), path, errbuf) == -1)
    {
        LOG(ALERT, ERR, "Could not decompose path '%s': %s", path, errbuf);
        rtn = -1;
    }
    else
    {
        LOG(ALERT, SUCC, "Associated finfo with path '%s'", finfo->path.real);
        finfo->readtime = time(NULL);
        finfo->associated = true;
        rtn = 0;
    }

    pthread_mutex_unlock(&(finfo->mtx));

    return rtn;
}

void finfo_attrs(finfo_s *finfo, decoder_attr_t *set, decoder_attr_t *get)
{
    pthread_mutex_lock(&(finfo->mtx));
    if (get)
    {
        *get = finfo->attrs;
    }

    if (set)
    {
        finfo->attrs = *set;
        file_set_attrs(&(finfo->file), finfo->attrs);
    }
    pthread_mutex_unlock(&(finfo->mtx));
}

static int finfo_update_modtime(finfo_s *finfo)
{
    struct stat statbuf;

    if (stat(finfo->path.real, &statbuf) == -1)
    {
        LOG(ALERT, ERR, "Could not stat path '%s': '%s'", finfo->path.real, strerror(errno));
        return -1;
    }

    finfo->modtime = statbuf.st_mtime;

    return 0;
}

int finfo_read(finfo_s *finfo, buf_s *buf, bool force)
{
    bool err;
    int fd;

    WITH_MTX(&(finfo->mtx))
    {
        err = false;

        if (!finfo->associated)
        {
            err = true;
            LOG(ALERT, ERR, "Buffer not associated with a file");
        }
        if (!err && buf_len(buf) > 1 && buf_line_len(buf, 0) > 0 && !force)
        {
            err = true;
            LOG(ALERT, ERR, "Reading this file will overwrite the contents of this buffer");
        }
        if (!err && (fd = open(finfo->path.real, O_RDONLY)) == -1)
        {
            err = true;
            LOG(ALERT, ERR, "Could not open '%s': %s", finfo->path.real, strerror(errno));
        }

        if (!err)
        {
            buf_del_line(buf, 0, buf_len(buf));

            finfo->readtime = time(NULL);

            buf_undo_set_enable(buf, false);
            buf_undo_clear(buf);

            file_set_fd(&(finfo->file), fd, finfo->path.base);
            file_read(&(finfo->file), file_read_to_buf_cb, file_enable_undo_cb, buf);
        }
    }

    if (err)
        return -1;
    else
        return 0;
}

int finfo_write(finfo_s *finfo, buf_s *buf, bool force)
{
    bool err;
    int fd;

    WITH_MTX(&(finfo->mtx))
    {
        err  = false;

        if (!finfo->associated)
        {
            LOG(ALERT, ERR, "Buffer not associated with a file");
            err = true;
        }
        if (!err && finfo_update_modtime(finfo) < 0 && !force)
        {
            err = true;
        }
        if (!err && finfo->modtime > finfo->readtime && !force)
        {
            LOG(ALERT, ERR, "File '%s' was modified after buf was loaded", finfo->path.real);
            err = true;
        }
        if (!err && (fd = open(finfo->path.real, O_WRONLY|O_TRUNC)) == -1)
        {
            LOG(ALERT, ERR, "Could not open '%s': %s", finfo->path.real, strerror(errno));
            err = true;
        }

        if (!err)
        {
            finfo->readtime = time(NULL) + 10;

            file_set_fd(&(finfo->file), fd, finfo->path.base);
            file_write(&(finfo->file), file_write_from_buf_cb, NULL, buf);
        }
    }

    if (err)
        return -1;
    else
        return 0;
}

void finfo_get_path(finfo_s *finfo, io_path_s *path)
{
    WITH_MTX(&(finfo->mtx))
    {
        memcpy(path, &(finfo->path), sizeof(io_path_s));
    }
}



