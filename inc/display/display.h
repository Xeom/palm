#if !defined(DISPLAY_DISPLAY_H)
# define DISPLAY_DISPLAY_H
# include <pthread.h>
# include <stdbool.h>
# include "display/framebuf.h"
# include "types.h"

/**
 * A structure representing a terminal display.
 *
 * This structure allows for characters to be written at x,y positions
 * to the display, and then shown to the user, using efficient triple
 * buffer.
 *
 */
struct display
{
    tty_s *tty; /**< A pointer to the tty being drawn on */

    framebuf_s framebufs[3]; /**< A pool of 3 framebufs for triple buffering.  */
    framebuf_s *frontbuf;    /**< The framebuf being edited.                   */
    framebuf_s *midbuf;      /**< The framebuf to be displayed.                */
    framebuf_s *backbuf;     /**< The framebuf containing the current display. */

    pthread_mutex_t frontbuf_mtx; /**< A mutex for editing the front buffer. */

    pthread_cond_t  refresh; /**< A condition for changes to the fron buffer. */
    pthread_t       thread;  /**< The thread printing characters to terminal. */

    rect_s          dims; /**< The current dimensions of the display. */

    bool            needsrefresh; /**< Set when the front buffer has changed. */
    bool            refreshall;   /**< Set when the display needs a redraw.   */

    bool            alive; /**< Set while the display thread should be alive. */

};

/**
 * Initialize a display.
 *
 * @param disp A pointer to the display structure.
 * @param tty  A pointer to the tty where the display will go.
 *
 */
void display_init(display_s *disp, tty_s *tty);

/**
 * Stop and release the resources associated with a display.
 *
 * @param disp A pointer to the display structure.
 *
 */
void display_kill(display_s *disp);

/**
 * Resize a display to match the dimensions of its tty.
 *
 * The tty must therefore be resized before this is called.
 *
 * @param disp A pointer to the display structure.
 *
 */
void display_resize(display_s *disp);

/**
 * Pause the running of the display safely.
 *
 * This allows the terminal to be used for other things.
 *
 * @param disp A pointer to the display structure.
 *
 */
void display_pause(display_s *disp);

/**
 * Resume the running of the display safely.
 *
 * This is the inverse of display_pause().
 *
 * @param disp A pointer to the display structure.
 *
 */
void display_resume(display_s *disp);

/**
 * Set a region of a display to a series of characters.
 *
 * This is cognate to the framebuf_set() method.
 *
 * The array of character starts at the top left of the region,
 * and moves left to right, row by row, to fill the region.
 *
 * The region is allowed to be partially or wholly out of bounds of
 * the display
 *
 * @param disp A pointer to the display structure.
 * @param rect A pointer to the region of the display to set.
 *             The top left corner is 0,0.
 * @param mem  A pointer to the array of characters to place in the
 *             display in the specified region.
 *             This is expected to be of length rect->w * rect->h.
 *
 */
void display_set(display_s *disp, rect_s *rect, chr_s *mem);

/**
 * Re-draw the contents of a display in their entirety.
 *
 * @param disp A pointer to the display structure.
 *
 */
void display_refresh_all(display_s *disp);

/**
 * Paint data from a framebuffer to a display.
 *
 * This is cognate to the framebuf_stamp() method.
 *
 * The region is allowed to be partially or wholly out of bounds of
 * both the framebuffer and the display.
 *
 * @param disp    A pointer to the display structure.
 * @param dstrect A pointer to the region of the display to stamp with
 *                characters from the framebuffer. The width and height
 *                are ignored.
 * @param src     A pointer to the framebuffer containing the characters
 *                to stamp onto the display.
 * @param srcrect A pointer to the region of the framebuffer to stamp to the
 *                display. The width, height, x and y are all respected.
 *
 */
void display_stamp(
    display_s *disp,
    rect_s *dstrect,
    framebuf_s *src,
    rect_s *srcrect
);

/**
 * Fill an area of a display with a repeating character.
 *
 * This is equivalent to display_set(), but the area of the display is filled
 * with only one character, rather than an array of many.
 *
 * @param disp   A pointer to the display structure.
 * @param rect   A pointer to the region of the display to fill.
 * @param c      A pointer to the character to fill the region with.
 *
 */
void display_fill(display_s *disp, rect_s *rect, chr_s *c);

/**
 * Get the current dimensions of a display.
 *
 * @param disp A pointer to the display structure.
 * @param dims A pointer to a rectangle structure where the dimensions
 *             of the display can be placed.
 *
 */
void display_dims(display_s *disp, rect_s *dims);
#endif
