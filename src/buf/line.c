#include <string.h>
#include <stdint.h>
#include "buf/line.h"
#include "text/chr.h"
#include "text/font.h"
#include "util/macro.h"
#include "buf/cur.h"
#include "buf/curset.h"

static void line_expand(line_s *line);

void line_init(line_s *line, buf_s *buf)
{
    line->buf = buf;
    vec_init(&(line->chrs), sizeof(chr_s));
    vec_init(&(line->expanded), sizeof(chr_s));
    vec_init(&(line->hlstate), sizeof(long));
    line_expand(line);
}

void line_kill(line_s *line)
{
    vec_kill(&(line->chrs));
    vec_kill(&(line->expanded));
    vec_kill(&(line->hlstate));
}

void line_get_hlstate(line_s *line, vec_s *state)
{
    vec_clr(state);
    vec_cpy(state, &(line->hlstate));
}

void line_set_hlstate(line_s *line, vec_s *state)
{
    vec_clr(&(line->hlstate));
    vec_cpy(&(line->hlstate), state);
}

size_t line_get_ln(line_s *line)
{
    char *start, *end, *this;
    start = vec_get(&(line->buf->lines), 0);
    end   = vec_end(&(line->buf->lines));
    this  = (char *)line;

    if (this < start || this > end)
        return SIZE_MAX;

    return (this - start) / sizeof(line_s);
}

void line_set_str(line_s *line, char *str, decoder_attr_t attrs)
{
    vec_s chrs;
    decoder_s decoder;

    vec_init(&chrs, sizeof(chr_s));

    decoder_init(&decoder, attrs);
    decoder_str(&decoder, str, &chrs);
    line_set_chrs(line, vec_get(&chrs, 0), vec_len(&chrs));

    decoder_kill(&decoder);
    vec_kill(&chrs);
}

void line_set_chrs(line_s *line, chr_s *mem, size_t len)
{
    vec_del(&(line->chrs), 0, vec_len(&(line->chrs)));
    vec_ins(&(line->chrs), 0, len, mem);
    line_needs_expansion(line);
}

void line_cpy_font(line_s *line, vec_s *oth)
{
    size_t ind, end;
    end = MIN(vec_len(oth), vec_len(&(line->chrs)));
    for (ind = 0; ind < end; ++ind)
    {
        chr_s *src, *dst;
        src = vec_get(oth, ind);
        dst = vec_get(&(line->chrs), ind);

        dst->font = src->font;
    }
    line_needs_expansion(line);
}

void line_set_font(line_s *line, unsigned short font, size_t ind, size_t n)
{
    size_t end;
    for (end = MIN(ind + n, vec_len(&(line->chrs))); ind < end; ++ind)
    {
        chr_s *chr;
        chr = vec_get(&(line->chrs), ind);

        chr->font = font;
    }
    line_needs_expansion(line);
}

size_t line_ind_to_pos(line_s *line, size_t ind)
{
    size_t pos, ctr;
    pos = 0;
    for (ctr = 0; ctr < ind; ++ctr)
    {
        chr_s *chr;
        chr = vec_get(&(line->chrs), ctr);
        if (!chr) break;

        pos += chr_expanded_width(chr, pos, &(line->buf->chrconf));
    }

    return pos;
}

size_t line_ind_to_off(line_s *line, size_t ind)
{
    size_t off, ctr;
    off = 0;
    for (ctr = 0; ctr < ind; ++ctr)
    {
        chr_s *chr;
        chr = vec_get(&(line->chrs), ctr);
        if (!chr) break;

        off += chr_num_bytes(chr);
    }

    return off;
}

size_t line_pos_to_ind(line_s *line, size_t pos)
{
    size_t ind, ctr;

    ind = 0;
    for (ctr = 0; ctr < pos; ++ind)
    {
        chr_s *chr;
        chr = vec_get(&(line->chrs), ind);

        if (!chr) break;

        ctr += chr_expanded_width(chr, pos, &(line->buf->chrconf));
    }

    return ind;
}

size_t line_off_to_ind(line_s *line, size_t off)
{
    size_t ind, ctr;

    ind = 0;
    for (ctr = 0; ctr < off; ++ind)
    {
        chr_s *chr;
        chr = vec_get(&(line->chrs), ind);

        if (!chr) break;

        ctr += chr_num_bytes(chr);
    }

    return ind;
}

void line_get_expanded(line_s *line, vec_s *expanded)
{
    if (line->needsexpand)
    {
        line_expand(line);
    }

    vec_cpy(expanded, &(line->expanded));
}

void line_needs_expansion(line_s *line)
{
    line->needsexpand = true;
}

static void line_expand(line_s *line)
{
    size_t posx;
    pos_s pos;
    cur_hl_t hl;

    FONT_DEF(priffont, "cursor-prifake");
    FONT_DEF(secffont, "cursor-secfake");
    FONT_DEF(prifont,  "cursor-pri");
    FONT_DEF(secfont,  "cursor-sec");
    FONT_DEF(selfont,  "cursor-sel");

    FONT_DEF(altpriffont, "cursor-alt-prifake");
    FONT_DEF(altsecffont, "cursor-alt-secfake");
    FONT_DEF(altprifont,  "cursor-alt-pri");
    FONT_DEF(altsecfont,  "cursor-alt-sec");
    FONT_DEF(altselfont,  "cursor-alt-sel");

    FONT_DEF(deadpriffont, "cursor-dead-prifake");
    FONT_DEF(deadsecffont, "cursor-dead-secfake");
    FONT_DEF(deadprifont,  "cursor-dead-pri");
    FONT_DEF(deadsecfont,  "cursor-dead-sec");
    FONT_DEF(deadselfont,  "cursor-dead-sel");

    pos.row = line_get_ln(line);

    vec_del(&(line->expanded), 0, vec_len(&(line->expanded)));

    posx = 0;
    for (pos.col = 0;
            (hl = curset_get_pos_hl(&(line->buf->curset), &pos, line->buf)) != CUR_HL_END
            || pos.col < (long)vec_len(&(line->chrs));
        ++pos.col)
    {
        chr_s *space, *chr;
        size_t width;

        chr = vec_get(&(line->chrs), pos.col);
        if (!chr)
            chr = &chr_blank;

        width = chr_expanded_width(chr, posx, &(line->buf->chrconf));
        space = vec_add(&(line->expanded), width, NULL);
        chr_expand(chr, posx, &(line->buf->chrconf), space);
        posx += width;

        switch (hl)
        {
        case CUR_HL_PRI:
            space->font = prifont;
            break;
        case CUR_HL_SEC:
            space->font = secfont;
            break;
        case CUR_HL_SEL:
            space->font = selfont;
            break;
        case CUR_HL_PRIFAKE:
            space->font = priffont;
            break;
        case CUR_HL_SECFAKE:
            space->font = secffont;
            break;
        case CUR_HL_ALT_PRI:
            space->font = altprifont;
            break;
        case CUR_HL_ALT_SEC:
            space->font = altsecfont;
            break;
        case CUR_HL_ALT_SEL:
            space->font = altselfont;
            break;
        case CUR_HL_ALT_PRIFAKE:
            space->font = altpriffont;
            break;
        case CUR_HL_ALT_SECFAKE:
            space->font = altsecffont;
            break;
        case CUR_HL_DEAD_PRI:
            space->font = deadprifont;
            break;
        case CUR_HL_DEAD_SEC:
            space->font = deadsecfont;
            break;
        case CUR_HL_DEAD_SEL:
            space->font = deadselfont;
            break;
        case CUR_HL_DEAD_PRIFAKE:
            space->font = deadpriffont;
            break;
        case CUR_HL_DEAD_SECFAKE:
            space->font = deadsecffont;
            break;
        }
    }
    line->needsexpand = false;
}

int line_cmp(line_s *line, vec_s *chrs)
{
    if (vec_len(chrs) != vec_len(&(line->chrs)))
        return -1;

    return memcmp(vec_get(chrs, 0), vec_get(&(line->chrs), 0), vec_len(chrs) * sizeof(chr_s));
}
