#if !defined(UTIL_LIST_H)
# define UTIL_LIST_H
# include <stddef.h>
# include "types.h"

/**
 * An item in a list.
 *
 * Unlike in vectors, this item will not move or be reallocated, and can
 * be transferred between different lists.
 *
 */

struct list_item
{
    list_item_s *next; /**< A pointer to the next item.     */
    list_item_s *prev; /**< A pointer to the previous item. */

    char data[]; /**< Data stored at this item. */
};

/**
 * A structure describing a range of a list, using a first and last element.
 * neither element may be null, and both are included in the range.
 *
 * A valid range is formed when range->first->next->..->next == range->last.
 *
 */
struct list_range
{
    list_item_s *first; /**< The first item in the range. */
    list_item_s *last;  /**< The last item in the range.  */
};

/**
 * Quickly create a list_range from a start and end item.
 *
 */
#define LIST_RANGE(f, l) ((list_range_s){ .first = (f), .last = (l) })

/**
 * A list.
 *
 * This structure is used to store a linked list of @c list_item s.
 *
 */
struct list
{
    size_t width;       /**< The number of bytes stored with each item. */
    size_t len;         /**< The number of items in the list.           */
    list_range_s range; /**< The total range of the list.               */
};

/**
 * Initialize a list.
 *
 * @param list  A pointer to the list.
 * @param width The number of bytes to store with each list item.
 *
 */
void list_init(list_s *list, size_t width);

/**
 * Kill a list.
 *
 * The resources associated with a list and all items contained in its range
 * are freed.
 *
 * @param list  A pointer to the list.
 *
 */
void list_kill(list_s *list);

/**
 * Return the number of items in a list.
 *
 * @param list A pointer to the list.
 *
 * @return The number of items in the list.
 *
 */
size_t list_len(list_s *list);

// TODO: TEST

/**
 * Get the length of a range of a list.
 *
 * This function can also be used to verify that a range is valid.
 *
 * @param range A pointer to the list_range.
 *
 * @return The number of items in the range, or -1 if the range is invalid.
 *
 */
long list_range_len(list_range_s range);

// TODO: TEST

/**
 * Get a midpoint in a range of a list.
 *
 * Returns a point halfway through a range, or if the range contains an
 * odd number of items, then the point closer to the start of the two
 * possible choices is is used.
 *
 * @param range A pointer to the range to split.
 *
 * @return A pointer to the item, or NULL on error. Note that the midpoint
 *         may be the same as the start, if the range contains only one or two
 *         items.
 *
 */
list_item_s *list_range_mid(list_range_s range);

list_item_s *list_first(list_s *list);
list_item_s *list_last(list_s *list);

list_range_s list_range(list_s *list);

// TODO: TEST

list_item_s *list_item_iter(list_item_s *item, long n);

// TODO: TEST

size_t list_item_ind(list_item_s *item);

list_item_s *list_push_before(list_s *list, list_item_s *item, const void *data);
list_item_s *list_push_after(list_s *list, list_item_s *item, const void *data);

static inline list_item_s *list_push_first(list_s *list, const void *data)
{
    return list_push_after(list, NULL, data);
}
static inline list_item_s *list_push_last(list_s *list, const void *data)
{
    return list_push_before(list, NULL, data);
}

// TODO: TEST

int list_del(list_s *list, list_range_s range);

// TODO: TEST

int list_graft_before(list_s *list, list_item_s *item, list_s *src, list_range_s range);

// TODO: TEST

int list_graft_after(list_s *list, list_item_s *item, list_s *src, list_range_s range);

// TODO: TEST

int list_mitosis(list_s *list, list_s *new, list_range_s range);

// TODO: TEST

void list_qsort(list_s *list, int (*f)(const void *, const void *));
void list_msort(list_s *list, int (*f)(const void *, const void *));

static inline void *list_item_data(list_item_s *item)
{
    if (!item) return NULL;

    return (void *)item->data;
}

#define LIST_FOREACH_DATA(list, type, name) \
    type name; \
    for (list_item_s *_item = list_first(list); \
         _item != NULL && (name = list_item_data(_item));                         \
         _item = list_item_iter(_item, 1))


#define LIST_FOREACH(list, name) \
    for (list_item_s *name = list_first(list); \
         name != NULL;                         \
         name = list_item_iter(name, 1))

#define LIST_RFOREACH(list, name) \
    for (list_item_s *name = list_last(list); \
         name != NULL;                         \
         name = list_item_iter(name, -1))
#endif
