#include "snow.h"
#include "text/chr.h"
#include "text/decoder.h"
#include "text/font.h"

static void check_font(vec_s *results, short *fonts)
{
    VEC_FOREACH(results, chr_s *, chr)
    {
        if (chr->font != *fonts)
        {
            fail("Expected chr font %d at index %lu but got %d\n", (int)*fonts, _ind, (int)chr->font);
        }
        fonts += 1;
    }
}

static void check_text(vec_s *results, char **texts)
{
    VEC_FOREACH(results, chr_s *, chr)
    {
        if (strcmp(chr->text, *texts) != 0)
        {
            fail("Expected chr text '%s' at index %lu but got '%s'\n", *texts, _ind, chr->text);
        }
        texts += 1;
    }
}

static void decode_with_eof(decoder_s *decoder, char *test, vec_s *results)
{
    while (*test)
    {
        assert(!decoder_chr(decoder, (int)(*test), results), "Decoder produced a line too soon");
        ++test;
    }

    assert(decoder_chr(decoder, EOF, results), "Decoder didn't produce a line on EOF");
}

describe(decoder)
{
    vec_s results;
    decoder_s decoder;

    before_each()
    {
        vec_init(&results, sizeof(chr_s));
    }

    after_each()
    {
        vec_kill(&results);
    }

    subdesc(fonts)
    {
        it ("Decodes multiple fonts")
        {
            char *test = "one{font-a}two{font-b}three";
            FONT_DEF(a, "font-a");
            FONT_DEF(b, "font-b");

            char *expecttext[] = {"o", "n", "e", "t", "w", "o", "t", "h", "r", "e", "e"};
            short expectfont[] = {  0,   0,   0,   a,   a,   a,   b,   b,   b,   b,   b};

            decoder_init(&decoder, DECODER_FNT);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 11);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Detects and highlights lonely close braces")
        {
            char *test = "x}dead";
            FONT_DEF(inv, "invalid-chr");
            char *expecttext[] = {"x", "}", "d", "e", "a", "d"};
            short expectfont[] = {  0, inv, inv,   0,   0,   0};

            decoder_init(&decoder, DECODER_FNT);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 6);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Decodes escaped braces")
        {
            char *test = "{{}}}}{{text{{wow}}";
            char *expecttext[] = {"{", "}", "}", "{", "t", "e", "x", "t", "{", "w", "o", "w", "}"};
            short expectfont[] = {  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0};

            decoder_init(&decoder, DECODER_FNT);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 13);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Recovers from errors to detect more fonts")
        {
            char *test = "{font-a}x}de{font-b}ad";
            FONT_DEF(inv, "invalid-chr");
            FONT_DEF(a,   "font-a");
            FONT_DEF(b,   "font-b");
            char *expecttext[] = {"x", "}", "d", "e", "a", "d"};
            short expectfont[] = {  a, inv, inv,   a,   b,   b};

            decoder_init(&decoder, DECODER_FNT);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 6);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Decodes fonts at the end of strings")
        {
            char *test = "{font-a}A{font-b}";
            FONT_DEF(inv, "invalid-chr");
            FONT_DEF(a,   "font-a");
            FONT_DEF(b,   "font-b");
            char *expecttext[] = {"A"};
            short expectfont[] = {  a};

            decoder_init(&decoder, DECODER_FNT);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 1);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }
    }

    subdesc(text)
    {
        it ("Handles zero-length strings")
        {
            char *test = "";

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 0);
            decoder_kill(&decoder);
        }
    }

    subdesc(utf8)
    {
        it ("Decodes normal ascii")
        {
       		char *test = "\x01\x02\x03\x04\x05\x06\x07\x08\t\x0b\x0c"
                         "\r\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17"
                         "\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f !\"#$%&'("
                         ")*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQR"
                         "STUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{"
                         "|}~\x7f";
            char *expecttext[] = {
                        "\x01", "\x02", "\x03", "\x04", "\x05", "\x06",
                "\x07", "\x08", "\t",           "\x0b", "\x0c", "\r",
                "\x0e", "\x0f", "\x10", "\x11", "\x12", "\x13", "\x14",
                "\x15", "\x16", "\x17", "\x18", "\x19", "\x1a", "\x1b",
                "\x1c", "\x1d", "\x1e", "\x1f",    " ",    "!",   "\"",
                "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-",
                ".", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8",
                "9", ":", ";", "<", "=", ">", "?", "@", "A", "B", "C",
                "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
                "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
                "Z", "[", "\\", "]", "^", "_", "`", "a", "b", "c",
                "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
                "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y",
                "z", "{", "|", "}", "~", "\x7f"
            };
            short expectfont[128];

            memset(expectfont, 0, sizeof(expectfont));

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 126);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Decodes four-long characters")
        {
            char *test = ":) \xf0\x9f\x98\x84 :)";
            char *expecttext[] = {":", ")", " ", "\xf0\x9f\x98\x84", " ", ":", ")"};
            short expectfont[] = {  0,   0,   0,                  0,   0,   0,   0};

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 7);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Decodes three-long characters")
        {
            char *test = "\x01\x02\xe2\x82\xac\x03\x04";
            char *expecttext[] = {"\x01", "\x02", "\xe2\x82\xac", "\x03", "\x04"};
            short expectfont[] = {     0,      0,              0,      0,      0};

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 5);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Decodes two-long characters")
        {
            char *test = "test\xc2\xa3wow";
            char *expecttext[] = {"t", "e", "s", "t" , "\xc2\xa3", "w", "o", "w"};
            short expectfont[] = {  0,   0,   0,   0,           0,   0,   0,   0};

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 8);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Ignores utf-8 when not enabled")
        {
            char *test = "\xf0\x9f\x98\x84\xe2\x82\xac\xc2\xa3";
            char *expecttext[] = {"\xf0", "\x9f", "\x98", "\x84", "\xe2", "\x82", "\xac", "\xc2", "\xa3"};
            short expectfont[] = {     0,      0,      0,      0,      0,      0,      0,      0,      0};

            decoder_init(&decoder, 0);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 9);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Decodes all characters together")
        {
            char *test = "\xf0\x9f\x98\x84\xe2\x82\xac\xc2\xa3";
            char *expecttext[] = {"\xf0\x9f\x98\x84", "\xe2\x82\xac", "\xc2\xa3"};
            short expectfont[] = {                 0,              0,          0};

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 3);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Recovers from and highlights incomplete characters")
        {
            char *test = "\xf0\x9f\x98\xe2\x82\xac\xc2\xa3";
            FONT_DEF(inv, "invalid-chr");
            char *expecttext[] = {"\xf0", "\x9f", "\x98", "\xe2", "\x82", "\xac", "\xc2\xa3"};
            short expectfont[] = {inv,       inv,    inv,    inv,    inv,    inv,          0};

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 7);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }

        it ("Recovers from and highlights too-long characters")
        {
            char *test = "\xf0\x9f\x98\x84\x82\xac\xc2\xa3";
            FONT_DEF(inv, "invalid-chr");
            char *expecttext[] = {"\xf0\x9f\x98\x84", "\x82", "\xac", "\xc2\xa3"};
            short expectfont[] = {                 0,    inv,    inv,          0};

            decoder_init(&decoder, DECODER_UTF8);
            decode_with_eof(&decoder, test, &results);

            asserteq(vec_len(&results), 4);
            check_text(&results, expecttext);
            check_font(&results, expectfont);

            decoder_kill(&decoder);
        }
    }
}
