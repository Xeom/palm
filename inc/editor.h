#if !defined(EDITOR_H)
# define EDITOR_H
# include <signal.h>
# include "types.h"
# include "wm/wm.h"
# include "io/tty.h"
# include "buf/buf.h"
# include "display/display.h"
# include "io/input.h"
# include "util/pool.h"
# include "hl/ctx.h"
# include "io/playbacker.h"
# include "wm/status.h"
# include "event/event.h"
# include "event/poll.h"
# include "event/timer.h"
# include "cmdlang/lib.h"
# include "io/key.h"
# include "buf/sel.h"

struct editor
{
    bool         alive;

    time_t       lastterm;
    int          numterms;
    struct       sigaction oldwinact;
    struct       sigaction oldintact;

    event_ctx_s  eventctx;
    poll_ctx_s   pollctx;
    timer_ctx_s  timerctx;

    cmd_lib_s    cmdlib;

    wm_s         wm;
    tty_s        tty;
    display_s    display;
    input_s      input;
    pool_s       pool;
    hl_ctx_s     hlctx;
    playbacker_s playbacker;

    buf_selector_s bufselector;

    key_parser_s keyparser;

    status_format_s statusfmt;

    int in, out;
};

void editor_init(editor_s *editor, int in, int out);

void editor_kill(editor_s *editor);

void editor_loop(editor_s *editor);

void editor_main(editor_s *editor);

void editor_resize(editor_s *editor);
void editor_term(editor_s *editor);
void editor_mask_signals(void);

void editor_stdout_pty(editor_s *editor, int inp, int out, char * const cmd[]);

void editor_run(editor_s *editor, int inp, int out, char * const cmd[]);

#endif
