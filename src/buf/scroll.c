#include "util/macro.h"
#include "buf/scroll.h"
#include "display/framebuf.h"
#include "wm/wm.h"

void scroll_update(buf_s *buf, pos_s *show, scroll_t typ)
{
    rect_s dims;
    pos_s  pos;

    if (buf->winind == -1)
        return;

    memcpy(&pos, show, sizeof(pos_s));
    buf_ind_to_pos(buf, &pos);

    wm_get_dims(buf_wm(buf), buf->winind, &dims);

    switch (typ)
    {
    case BUF_SCROLL_SMOOTH:
        if (pos.col - dims.w + 2 <= 0 && pos.col < buf->scrx)
            pos.col = 0;
        buf_set_scr(buf,
            MIN(pos.col, MAX(pos.col - dims.w + 2, buf->scrx)),
            MIN(pos.row, MAX(pos.row - dims.h + 2, buf->scry))
        );
        break;
    case BUF_SCROLL_JUMP:
        break;
    }
}
