from ctypes import *
from lib    import lib
from lib.sysdefs import FILE_s

fptrtype = type(lib.memmove)

class CStdio:
    stdin  = cast(lib.stdin,  POINTER(POINTER(FILE_s))).contents
    stdout = cast(lib.stdout, POINTER(POINTER(FILE_s))).contents
    stderr = cast(lib.stderr, POINTER(POINTER(FILE_s))).contents

lib.fdopen.argtypes = [c_int, c_char_p]
lib.fdopen.restype  = POINTER(FILE_s)
