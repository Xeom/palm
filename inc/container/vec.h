#if !defined(VEC_H)
# define VEC_H
# include <string.h>
# include <stdlib.h>
# include <stdbool.h>
# include "stddef.h"
# include "types.h"
# include "util/macro.h"

/**
 * @file vec.h
 * @brief Functions for creation and manipulation of vectors.
 *
 */

/**
 * A Vector.
 *
 * Vectors are data structures that store a sequence of
 * items in continuous memory.
 *
 * The items are zero-indexed, and the locations of
 * any or all of the items may change when items are
 * added or removed from the vector.
 *
 * Vectors are cheap to append to, but expensive to prepend to.
 *
 */
struct vec
{
    char *mem;        /**< The memory allocated to this vector */
    size_t width;     /**< The number of bytes per item in this vector */
    size_t allocated; /**< The number of bytes allocated to this vector in *mem */
    size_t used;      /**< The number of bytes used by items in this vector */
};

/**
 * Initialize a vector.
 *
 * @param v     A pointer to the vector structure.
 * @param width The size of an item in the vector.
 *
 */
void vec_init(vec_s *v, size_t width);

/**
 * Kill a vector.
 *
 * This function must be called to free resources associated
 * with a vector, after the vector has been used.
 *
 * @param v A pointer to the vector.
 *
 */
void vec_kill(vec_s *v);

/**
 * Insert items into a vector.
 *
 * Insert a series of n items into a vector, such that they start
 * at a certain index, ind.
 *
 * If mem is @c NULL, the newly inserted items will be memset to 0.
 *
 * @param v   A pointer to the vector.
 * @param ind The position in the vector to insert the items.
 * @param n   The number of items to insert.
 * @param mem A pointer to the items to insert.
 *
 * @return A pointer to the start of the newly inserted items,
 *         or @c NULL on error.
 *
 */
void *vec_ins(vec_s *v, size_t ind, size_t n, const void *mem);
void *vec_fill(vec_s *v, size_t ind, size_t n, const void *mem);

/**
 * Delete items from a vector.
 *
 * Delete @c n items from a vector, starting from index ind.
 *
 * @param v   A pointer to the vector.
 * @param ind The index to start deleting from.
 * @param n   The number of items to delete.
 *
 * @return 0 on success, -1 on error.
 *
 */
int vec_del(vec_s *v, size_t ind, size_t n);

/**
 * Get a pointer to an item in a vector.
 *
 * @param v   A pointer to the vector.
 * @param ind The index to get.
 *
 * @return A pointer to the item, or @c NULL if the item does not exist.
 *
 */
void *vec_get(const vec_s *v, size_t ind);

/**
 * Get the number of items in a vector.
 *
 * @param v A pointer to the vector.
 *
 * @return The number of items in the vector.
 *
 */
size_t vec_len(const vec_s *v);

/**
 * Append a printf-style string to a vector.
 *
 * Note that this function does not append a NULL byte.
 *
 * @param v   A pointer to the vector.
 * @param fmt A printf-style format string.
 * @param ... Format arguments, in the same style as printf.
 *
 */
void *vec_fmt(vec_s *v, const char *fmt, ...)
__attribute__((format (printf, 2, 3)));

/**
 * Reverse the contents of a vector.
 *
 * This reverses the order of the items in the vector.
 *
 * @param v A pointer to the vector.
 *
 */
void vec_rev(vec_s *v);

/**
 * Perform a bisection on a vector.
 *
 * This assumes the vector is sorted, and returns the index where an item
 * should be inserted to preserve the sorted order.
 *
 * If the item already exists, the index to that item is returned.
 * Therefore this function can be used for sorted insert, and sorted
 * search!
 *
 * @param v       A pointer to the vector.
 * @param item    The item to find.
 * @param cmpfunc The comparison function for sorting. This should be the same
 *                as a function that could be used with @c qsort().
 *
 * @return The bisected index in the vector.
 *
 */
size_t vec_bst(
    const vec_s *v,
    const void *item,
    int (*cmpfunc)(const void *a, const void *b)
);

/*** MACROS ***/

/**
 * Initialize a vector of a specific type.
 *
 * Usage:
 * @code
 * int x;
 * vec_s vec = VEC_INITIALIZER(int);
 * x = 3;
 * vec_app(&vec, &x);
 * @endcode
 *
 * @param typ The type of the vector.
 *
 */
#define VEC_INITIALIZER(typ) \
    { .mem = NULL, .width = sizeof(typ), .allocated = 0, .used = 0 }

/**
 * Iterate over the contents of a vector.
 *
 * The current index is stored in the variable @c _ind.
 *
 * Usage:
 * @code
 * VEC_FOREACH(&vec, int *, ptr)
 * {
 *     printf("Item %lu has value %d\n", _ind, *ptr);
 * }
 * @endcode
 *
 * @param vec  A pointer to the vector.
 * @param type The type to cast the result of vec_get() to.
 *             i.e. A pointer to the type of the vector.
 * @param name The name of the pointer to items.
 *
 */
#define VEC_FOREACH(vec, type, name) \
    type name;                       \
    for (size_t _ind = 0,            \
         _len = vec_len(vec);        \
         name = vec_get(vec, _ind),  \
         _ind < _len;                \
         ++_ind)

/**
 * Iterate over the contents of a vector in reverse.
 *
 * The same as @ref VEC_FOREACH, but the items are iterated over in
 * a backwards order.
 *
 */
#define VEC_RFOREACH(vec, type, name)    \
    type name;                           \
    for (size_t _ind = vec_len(vec) - 1; \
         (name = vec_get(vec, _ind));    \
         --_ind)

/*** INLINES ***/

/**
 * Get a pointer to the last item in a vector.
 *
 * @param v A pointer to the vector.
 *
 * @return A pointer to the final item, or @c NULL if the vector is empty.
 *
 */
static inline void *vec_end(const vec_s *v)
{
    return vec_get(v, vec_len(v) - 1);
}

/**
 * Append a series of n items to the end of a vector.
 *
 * @param v   A pointer to the vector.
 * @param n   The number of items.
 * @param mem A pointer to the items.
 *
 * @return A pointer to the new items, or @c NULL on error.
 *
 */
static inline void *vec_add(vec_s *v, size_t n, const void *mem)
{
    return vec_ins(v, vec_len(v), n, mem);
}

/**
 * Append a single item to the end of a vector.
 *
 * @param v   A pointer to the vector.
 * @param mem A pointer to the item.
 *
 * @return A pointer to the new items, or @c NULL on error.
 *
 */
static inline void *vec_app(vec_s *v, const void *mem)
{
    return vec_add(v, 1, mem);
}

/**
 * Append the contents of one vector into another.
 *
 * @param v   A pointer to the destination vector.
 * @param oth A pounter to the source vector.
 *
 * @return A pointer to the new items, or @c NULL on error.
 *
 */
static inline void *vec_cpy(vec_s *v, vec_s *oth)
{
    return vec_add(v, vec_len(oth), vec_get(oth, 0));
}

/**
 * Append a string to a vector, sans the terminating @c NULL byte.
 *
 * If you want a null byte at the end of the vector, so it contains
 * a null-terminated string, you can run.
 *
 * @code
 * vec_s str;
 * vec_init(&str, sizeof(char));
 *
 * vec_str(&str, "Hello I am a string!");
 * vec_app(&str, "\x00");
 *
 * printf("%s\n", vec_get(&str, 0));
 *
 * vec_kill(&str);
 * @endcode
 *
 * @param v   A pointer to the vector.
 * @param str A pointer to the null-terminated string.
 *
 * @return A pointer to the start of the inserted string.
 *
 */
static inline void *vec_str(vec_s *v, const char *str)
{
    return vec_add(v, strlen(str), str);
}

/**
 * Clear the contents of a vector.
 *
 * The length of the vector is set to zero, and its contents
 * are deleted.
 *
 * @param v A pointer to the vector.
 *
 * @return 0 on success, -1 on error.
 *
 */
static inline int vec_clr(vec_s *v)
{
    return vec_del(v, 0, vec_len(v));
}

/**
 * Compare a vector with a string.
 *
 * Compare the bytes and length of a vector and a string.
 * This is not alphabetic, the string and vector are sorted by
 * size first, and contents second.
 *
 * @param v   A pointer to the vector.
 * @param str A pointer to the null-terminated string to compare.
 *
 * @return -1 if v < str, 0 if v == str, 1 if v > str.
 *
 */
static inline int vec_strcmp(const vec_s *v, const char *str)
{
    if (vec_len(v) < strlen(str))
        return -1;
    if (vec_len(v) > strlen(str))
        return  1;
    return memcmp(vec_get(v, 0), str, vec_len(v));
}

/**
 * Compare two vectors.
 *
 * Alphabetically compare the bytes and length of two vectors.
 *
 * Like memcmp(), this function returns >0 on a > b, <0 on b > a, and 0
 * on a == b.
 *
 * @param a A pointer to the first vector.
 * @param b A pointer to the second vector.
 *
 */
static inline int vec_cmp(const vec_s *a, const vec_s *b)
{
    int res;
    size_t alen, blen;
    alen = vec_len(a);
    blen = vec_len(b);

    res = memcmp(vec_get(a, 0), vec_get(b, 0), MIN(alen, blen));

    if (res != 0)
        return res;
    
    if (alen > blen)
        return  1;
    if (blen < alen)
        return -1;

    return 0;

}

/**
 * Sort the contents of a vector.
 *
 * @param v The vector to sort.
 * @param f The function pointer of the comparison function.
 *
 */
static inline void vec_qsort(vec_s *v, int (*f)(const void *, const void *))
{
    qsort(vec_get(v, 0), vec_len(v), v->width, f);
}

static inline bool vec_contains(vec_s *v, void *ptr)
{
    VEC_FOREACH(v, void *, cmp)
    {
        if (memcmp(cmp, ptr, v->width) == 0)
            return true;
    }

    return false;
}

#endif
