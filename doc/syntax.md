Syntax Highlighting
===================

Palm supports syntax highlighting buffers, using syntax information stored in
.yaml files, though the format of the information could easily be translated
between any number of formats.

The syntax highlighting is based around a stack of states. In each state, a
set of actions can be associated with a set of tokens. If one of the tokens is
encountered, the appropriate action can be taken. This can be pushing another
state to the stack of states, popping the current state, switching the current
state with another, or simply highlighting the token a specific colour.

A token is specified like so,


    # A dictionary of tokens
    tokens:
        # The name of the token
        tok-name:
            # The regex that the token consists of, either with or without the
            # PCRE2_EXTENDED flag.
            regex: 'token'
            regex-ext: '
                token
            '
    
            # The font to colour this token, unless another is specified where it
            # is associated with an action.
            font:  font-name
    
            # The position to advance the parsing of text to after the token is
            # read. Either 'pre' to not advance the parsing after the token is
            # matched (be sure to avoid infinite loops here!), or 'post', to
            # advance parsing to the end of the token.
            # This can also be overriden where it is associated with an action.
            pos:   pre
    
            # The default action to take in response to this token. Options are
            # 'swap', 'pop' or 'push'.
            type:  pop
    
            # The name of the state to associate with the action if one is required
            # for example, the state to 'swap' to, or 'push' to the stack.
            state: other-state

A state contains a set of actions that can be taken upon encountering specific
tokens. It also contains a font which is the default for text to be coloured
while it is parsed with the state.

    # A dictionary of states
    states:
        # The name of the state.
        example-name:
            font: font-name
            actions:
                - tok: tok-name
                  font: red
                  # The other attributes defined for a token can be set here!
                  # But the defaults are not observed.

States can also contain other attributes, which allow for common patterns to
be expressed simply:

    states:
        example-1:
            start:
                - tok-1
            end:
                - tok-2
        example-2:
            contains-states:
                - example-1

Is equivalent to:

    states:
        example-1:
            actions:
                - tok: tok-2
                  action: pop
    
        example-2:
            actions:
                - tok: tok-1
                  action: push
                  state: example-1


And


    states:
        example-1:
            contains-tokens:
                - tok-1


Is equivalent to

    states:
        example-1:
            actions:
                - tok: tok-1
                  .
                  . The default token attributes
                  .
