import libtypes
import ctypes
from lib import lib, enums
from .structure import Structure
from .chrvec    import ChrVec

def fmt_sanitize(s):
    if isinstance(s, str):
        return s.replace("{", "{{").replace("}", "}}")
    else:
        return s.replace(b"{", b"{{").replace(b"}", b"}}")

class Decoder(Structure):
    typ        = libtypes.decoder_s
    init_funct = lib.decoder_init
    kill_funct = lib.decoder_kill

    def init_args(self, attrs):
        return [enums.decoder_attr_t(attrs)]

    def chr(self, c, chrvec):
        return lib.decoder_chr(self.ptr, c, chrvec.ptr)

    def str(self, s):
        if isinstance(s, str):
            s = s.encode("utf-8")

        result = ChrVec()
        lib.decoder_str(self.ptr, s, result.ptr)

        return result
