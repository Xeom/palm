#if !defined(IO_PYCLIENT_H)
# define IO_PYCLIENT_H
# include "container/vec.h"

/**
 * Send data to a pyserver.
 *
 * This function sends a message to a python server, waits for
 * a response, and returns the reply.
 *
 * @param pyfd The fd of the pyserver socket. 
 * @param send The vector of data to send. This should be
 *             a null-terminated vector of chars. It will be
 *             emptied as the function runs.
 * @param recv The vector of recieved data. This will be
 *             filled with a null-terminated vector of response
 *             from the server. It should be an initialized
 *             vector of chars.
 *
 * @return 0 on success, -1 on error.
 *
 */
int pyclient_send(int pyfd, vec_s *send, vec_s *recv);

#endif
