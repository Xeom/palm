{title}Syntax Highlighting

  Palm  supports syntax highlighting  buffers,  using syntax
  information stored  in .yaml files,  though the  format of
  the  information could  easily be  translated between  any
  number of formats.

  The syntax highlighting is based around a stack of states.
  In each state,  a set of actions can be  associated with a
  set of  tokens. If one of  the tokens is  encountered, the
  appropriate  action  can be  taken.  This can  be  pushing
  another state to the stack  of states, popping the current
  state, switching the current state with another, or simply
  highlighting the token a specific colour.

  A token is specified like so,

 {md-section} : {md-code-blk}tokens:
 {md-section} : {md-code-blk}    # The name of the token
 {md-section} : {md-code-blk}    tok-name:
 {md-section} : {md-code-blk}        # The regex that the token consists of, either with or without the
 {md-section} : {md-code-blk}        # PCRE2_EXTENDED flag.
 {md-section} : {md-code-blk}        regex: 'token'
 {md-section} : {md-code-blk}        regex-ext: '
 {md-section} : {md-code-blk}            token
 {md-section} : {md-code-blk}        '
 {md-section} : {md-code-blk}
 {md-section} : {md-code-blk}        # The font to colour this token, unless another is specified where it
 {md-section} : {md-code-blk}        # is associated with an action.
 {md-section} : {md-code-blk}        font:  font-name
 {md-section} : {md-code-blk}
 {md-section} : {md-code-blk}        # The position to advance the parsing of text to after the token is
 {md-section} : {md-code-blk}        # read. Either 'pre' to not advance the parsing after the token is
 {md-section} : {md-code-blk}        # matched (be sure to avoid infinite loops here!), or 'post', to
 {md-section} : {md-code-blk}        # advance parsing to the end of the token.
 {md-section} : {md-code-blk}        # This can also be overriden where it is associated with an action.
 {md-section} : {md-code-blk}        pos:   pre
 {md-section} : {md-code-blk}
 {md-section} : {md-code-blk}        # The default action to take in response to this token. Options are
 {md-section} : {md-code-blk}        # 'swap', 'pop' or 'push'.
 {md-section} : {md-code-blk}        type:  pop
 {md-section} : {md-code-blk}
 {md-section} : {md-code-blk}        # The name of the state to associate with the action if one is required
 {md-section} : {md-code-blk}        # for example, the state to 'swap' to, or 'push' to the stack.
 {md-section} : {md-code-blk}        state: other-state

  A state contains  a set of actions that can  be taken upon
  encountering  specific tokens.  It  also  contains a  font
  which is the  default for text to be coloured  while it is
  parsed with the state.

 {md-section} : {md-code-blk}states:
 {md-section} : {md-code-blk}    # The name of the state.
 {md-section} : {md-code-blk}    example-name:
 {md-section} : {md-code-blk}        font: font-name
 {md-section} : {md-code-blk}        actions:
 {md-section} : {md-code-blk}            - tok: tok-name
 {md-section} : {md-code-blk}              font: red
 {md-section} : {md-code-blk}              # The other attributes defined for a token can be set here!
 {md-section} : {md-code-blk}              # But the defaults are not observed.

  States can also contain  other attributes, which allow for
  common patterns to be expressed simply:

 {md-section} : {md-code-blk}    example-1:
 {md-section} : {md-code-blk}        start:
 {md-section} : {md-code-blk}            - tok-1
 {md-section} : {md-code-blk}        end:
 {md-section} : {md-code-blk}            - tok-2
 {md-section} : {md-code-blk}    example-2:
 {md-section} : {md-code-blk}        contains-states:
 {md-section} : {md-code-blk}            - example-1

  Is equivalent to:

 {md-section} : {md-code-blk}    example-1:
 {md-section} : {md-code-blk}        actions:
 {md-section} : {md-code-blk}            - tok: tok-2
 {md-section} : {md-code-blk}              action: pop
 {md-section} : {md-code-blk}
 {md-section} : {md-code-blk}    example-2:
 {md-section} : {md-code-blk}        actions:
 {md-section} : {md-code-blk}            - tok: tok-1
 {md-section} : {md-code-blk}              action: push
 {md-section} : {md-code-blk}              state: example-1

  And

 {md-section} : {md-code-blk}    example-1:
 {md-section} : {md-code-blk}        contains-tokens:
 {md-section} : {md-code-blk}            - tok-1

  Is equivalent to

 {md-section} : {md-code-blk}    example-1:
 {md-section} : {md-code-blk}        actions:
 {md-section} : {md-code-blk}            - tok: tok-1
 {md-section} : {md-code-blk}              .
 {md-section} : {md-code-blk}              . The default token attributes
 {md-section} : {md-code-blk}              .
