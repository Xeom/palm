#if !defined(UTIL_TIME_H)
# define UTIL_TIME_H
# include <stddef.h>

/**
 * @brief Utility functions for dealing with time.
 *
 */

/**
 * Return the number of seconds since the epoch as a double.
 *
 * The returned value should be accurate to the nearest microsecond
 * as long as the double can represent that accuracy. (we are okay until
 * the 2100s)
 *
 * @return The number of seconds since the epoch.
 *
 */
double util_time_fsec(void);

/**
 * Return the number of seconds since the epoch as an integer.
 *
 * @return The number of seconds, rounded down.
 *
 */
long long util_time_sec(void);

/**
 * Return the number of milliseconds since the epoch as an integer.
 *
 * @return The number of milliseconds, rounded down.
 *
 */
long long util_time_ms(void);

/**
 * Return the number of microseconds since the epoch as an integer.
 *
 * @return The number of microseconds.
 *
 */
long long util_time_us(void);

/**
 * Format the current time as a string, using the strftime call.
 *
 * The string is formatted in a caller-supplied buffer. This function is also
 * thread safe with itself, but not with other calls to localtime().
 *
 * If the buffer is not large enough, or there is an error, the contents of
 * the buffer are undefined.
 *
 * @param buf A buffer of at least @c len bytes, to contain the formatted
 *            string.
 * @param len The number of bytes in the buffer.
 * @param fmt The format for the time, in a format compatible with strftime().
 *
 * @return -1 if the buffer is not large enough or on error, 0 on success.
 *
 */
int util_strfnow(char *buf, size_t len, const char *fmt);

#endif
