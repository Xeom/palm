import os
import stat

from datetime import datetime
from typing import Type, Optional

from core.buffer   import Buffer
from core.bind     import Bind
from server.server import add_global

from .loclist import fmt_path
from .menu    import MenuBuffer, MenuItem
from .info     import InfoBuffer
from .edit     import EditBuffer
from .pathinfo import PathInfoBuffer

from core.io import IoPath, is_dir
from editor  import log

def gen_diritem_subs(buf: Buffer, parent: Optional[MenuItem], path: str):
    if is_dir(path):
        subpaths = [os.path.join(path, base) for base in os.listdir(path)]
        subpaths.sort(key=lambda i: [os.path.basename(i).startswith("."), is_dir(i), i])
    else:
        return []

    return [DirItem(buf, p, parent) for p in subpaths]

@add_global()
class DirItem(MenuItem):
    def __init__(self, buf: Buffer, path: str, parent=None):
        self.path = IoPath.decompose(path)
        super().__init__(buf, parent)
        self.expandable = self.path.is_dir()
        self.set_content(self.path.base + ("/" if self.path.is_dir() else ""))

    def gen_subs(self):
        subs = gen_diritem_subs(self.buf, self, self.path.real)
        return subs

    def enter(self):
        if self.expandable:
            self.toggle()

        else:
            buf = self.buf.editor.new_buf(self.buf.buftype)
            self.buf.editor.win_assoc(buf, self.buf.get_win())
            buf.edit(self.path.real)

    def keypress(self, key: bytes):
        if key == b"i":
            buf = self.buf.editor.popup_buf(PathInfoBuffer)
            buf.show_path(self.path.real)

@add_global()
class DirBuffer(MenuBuffer):
    buftype: Type[Buffer] = EditBuffer

    def show_dir(self, path):
        path = IoPath.decompose(path)
        self.path = path

        self.set_items(gen_diritem_subs(self, None, path.real))
        self.set_head(self.make_head())

    def make_head(self):
        return [
            "{title}Directory Listing",
            "",
            "This buffer is showing a list of the files at",
            " {{emph}}{0}".format(fmt_path(self.path.real)),
            "",
            "Press {emph}Enter{default} to open and close directories,",
            " or to edit files, and press {emph}i{default} to view file",
            " information.",
            ""
        ]

