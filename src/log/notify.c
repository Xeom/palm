#include <string.h>
#include <stdlib.h>
#include "log/notify.h"
#include "log/log.h"
#include "text/chr.h"
#include "text/font.h"
#include "text/decoder.h"
#include "util/time.h"
#include "util/macro.h"

/** (ms) Maximum age of printed notification. */
#define WAITING_PRINT_TOUT 6000

/** (ms) How long a notification can be printed for. */
#define ACTIVE_PRINT_TOUT 1000

/** 
 * Newer notifications can appear before older ones, when
 * they are this many milliseconds newer.
 */
#define NOTIFICATION_MAX_GAP 500

/** (ms) How long icons are displayed for */
#define ICON_TOUT 6000

static log_typ_conf_s *notification_conf(log_s *log, notification_s *n);
static void log_append_icon(log_s *log, vec_s *chrs, log_lvl_t lvl, log_type_t typ);
static void log_append_all_icons(log_s *log, vec_s *chrs, int icons[LOG_TYP_CNT]);

static log_typ_conf_s *notification_conf(log_s *log, notification_s *n)
{
    return &(log->conf[n->lvl][n->typ]);
}

static void log_append_icon(log_s *log, vec_s *chrs, log_lvl_t lvl, log_type_t typ)
{
    chr_s icon;
    log_typ_conf_s *conf;
    conf = &(log->conf[lvl][typ]);

    memset(&icon, 0, sizeof(icon));
    icon.text[0] = conf->icon;
    icon.font    = font_by_name(conf->iconfont);

    vec_app(chrs, &icon);
}

static void log_append_all_icons(log_s *log, vec_s *chrs, int icons[LOG_TYP_CNT])
{
    bool any;
    int lvl;
    log_type_t typ;

    any = false;
    for (lvl = 0; lvl < LOG_LVL_CNT; ++lvl)
    {
        for (typ = 0; typ < LOG_TYP_CNT; ++typ)
        {
            if (icons[typ] == lvl)
            {
                any = true;
                log_append_icon(log, chrs, lvl, typ);
            }
        }
    }

    if (any)
    {
        vec_app(chrs, &(chr_s){
                .text = " ",
                .font = font_by_name("win-horizontal-border")
            }
        );
    }
}

bool log_has_printable(log_s *log)
{
    bool res;
    long long now;
    now = util_time_ms();

    res = false;
    WITH_MTX(&(log->mtx))
    {
        VEC_FOREACH(&(log->notifications), notification_s *, n)
        {
            log_typ_conf_s *conf;
            conf = notification_conf(log, n);

            if (now <= n->tout && conf->print)
            {
                res = true;
                break;
            }
        }
    }
    return res;
}

void log_notify_update(log_s *log, vec_s *chrs, size_t len)
{
    int icons[LOG_TYP_CNT];
    int numprintable;
    long long now;
    log_type_t typ;

    WITH_MTX(&(log->mtx))
    {
        for (typ = 0; typ < LOG_TYP_CNT; ++typ)
            icons[typ] = -1;

        now = util_time_ms();
        {
            VEC_RFOREACH(&(log->notifications), notification_s *, n)
            {
                if (now > n->tout)
                {
                    vec_kill(&(n->chrs));
                    vec_del(&(log->notifications), _ind, 1);
                }
            }
        }

        numprintable = 0;

        {
            VEC_FOREACH(&(log->notifications), notification_s *, n)
            {
                log_typ_conf_s *conf;
                conf = notification_conf(log, n);
                if (conf->icon && !(numprintable == 0 && conf->print))
                    icons[n->typ] = MAX(icons[n->typ], (int)n->lvl);

                if (conf->print)
                    numprintable += 1;
            }
        }

        log_append_all_icons(log, chrs, icons);

        {
            VEC_FOREACH(&(log->notifications), notification_s *, n)
            {
                log_typ_conf_s *conf;
                conf = notification_conf(log, n);
                if (conf->print)
                {
                    size_t lenstr, off;
                    const int timepause  = 1000;
                    const int scrollrate = 100;

                    if (numprintable > 1)
                        n->tout = MIN(n->tout, now + ACTIVE_PRINT_TOUT);

                    lenstr = vec_len(&(n->chrs));

                    off = 0;
                    if (lenstr > len - vec_len(chrs))
                    {
                        long long phase, scrlen;
                        scrlen = lenstr - (len - vec_len(chrs));
                        phase = (now - n->tstamp) % (scrlen * scrollrate + timepause * 2);
                        if (scrlen <= 0)
                            off = 0;
                        else if (phase > timepause + scrollrate * scrlen)
                            off = scrlen;
                        else if (phase > timepause)
                            off = (phase - timepause) / scrollrate;
                    }

                    vec_ins(
                        chrs,
                        vec_len(chrs),
                        vec_len(&(n->chrs)) - off,
                        vec_get(&(n->chrs), off)
                    );

                    break;
                }
            }
        }
    }
}

void log_notify(log_s *log, log_lvl_t lvl, log_type_t typ, const char *str)
{
    notification_s notification;

    log_typ_conf_s *conf;
    conf = &(log->conf[lvl][typ]);

    /* Skip entries that we can't do anything with */
    if (!(conf->print || conf->icon))
        return;

    vec_init(&(notification.chrs), sizeof(chr_s));

    if (conf->print)
    {
        int deffnt;
        decoder_s decoder;

        /* Convert str to chrs */
        decoder_init(&decoder, DECODER_UTF8 | DECODER_FNT);
        decoder_str(&decoder, str, &(notification.chrs));
        decoder_chr(&decoder, EOF, &(notification.chrs));
        decoder_kill(&decoder);

        deffnt = font_by_name(conf->prifont);
        /* All un-fonted characters get an appropriate colour */
        VEC_FOREACH(&(notification.chrs), chr_s *, chr)
        {
            if (chr->font == 0)
                chr->font = deffnt;
        }
    }

    notification.id  = (log->notifyid)++;
    notification.lvl = lvl;
    notification.typ = typ;
    notification.tstamp = util_time_ms();

    long long tout;
    tout = 0;
    if (conf->print)
        tout = MAX(tout, notification.tstamp + WAITING_PRINT_TOUT);
    else if (conf->icon)
        tout = MAX(tout, notification.tstamp + ICON_TOUT);

    notification.tout = tout;

    size_t ind;
    for (ind = 0; ind < vec_len(&(log->notifications)); ++ind)
    {
        notification_s *n;
        n = vec_get(&(log->notifications), ind);
        if (n->lvl == lvl && n->tstamp + NOTIFICATION_MAX_GAP < notification.tstamp)
            break;
        if (n->lvl < lvl)
            break;
    }
    vec_ins(&(log->notifications), ind, 1, &notification);
}

