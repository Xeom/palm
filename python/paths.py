"""Contains common paths for use by palm."""

import os

python  = os.path.dirname(os.path.realpath(__file__))
root    = os.path.dirname(python)
binary  = os.path.join(root, "bin")
data    = os.path.join(root, "data")
helpdir = os.path.join(data, "help")
etcdir  = os.path.join(root, "etc")

hldir   = os.path.join(data, "hl")
hlfiles = [os.path.join(hldir, f) for f in os.listdir(hldir) if f.endswith(".yaml")]

fontdir   = os.path.join(data, "fonts")
fontfiles = [os.path.join(fontdir, f) for f in os.listdir(fontdir) if f.endswith(".csv")]

sessdir   = os.path.join(python, "session")
sessfiles = [os.path.join(sessdir, f) for f in os.listdir(sessdir) if f.endswith(".py")]

home = os.path.expanduser("~")

rcfiles = [f for f in [
    os.path.expanduser("~/.palmrc.py"),
    os.path.expanduser("~/.config/palm/rc.py")
] if os.path.exists(f)]
