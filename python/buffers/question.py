from core.buffer   import Buffer
from editor        import log
from core.cur      import Cur
from core.bind     import Bind
from core.key      import Key
from server.server import add_global

@add_global()
class Question(object):
    def __init__(self, text):
        self.cbs      = {}
        self.args     = {}
        self.helps    = {}
        self.text     = text

        self.add_answer(b"?", self.show_help, helptext="Display this message")

    def add_answer(self, key, cb, args=None, helptext=None):
        if isinstance(key, str):
            key = key.encode("utf-8")

        self.cbs[key]   = cb
        self.helps[key] = helptext
        self.args[key]  = args

    def bind(self, bind):
        for key in self.cbs:
            bind.bind(Bind(Key.char(key), pybind=f"buf.answer({repr(key)})"))

    def unbind(self, bind):
        for key in self.cbs:
            bind.unbind(Key.char(key))

    def show_prompt(self, buf):
        buf.show_question_info(
            "{} ({}) ...".format(self.text, "/".join(k.decode("utf-8") for k in self.cbs))
        )

    def show_help(self, buf):
        for key, helptext in self.helps.items():
            key = key.decode("utf-8")
            if helptext == None:
                buf.show_question_info(
                    "{k}".format(k=key)
                )
            else:
                buf.show_question_info(
                    "{k}: {text}".format(k=key, text=helptext)
                )

        buf.ask(self, inbuf=buf.askinginbuf)

    def answer(self, keys, buf):
        if keys in self.cbs:
            args = self.args[keys]

            if args == None:
                args = [buf]

            buf.cancel_asking()
            self.cbs[keys](*args)


        else:
            self.show_question_info(
                "Unrecognized answer: {keys}. Type '?' for help.".format(keys)
            )

@add_global()
class QuestionBuffer(Buffer):
    def ask(self, question, inbuf=False):
        self.question = question
        self.bind.set_mode("question")
        self.question.bind(self.bind)
        self.askinginbuf = inbuf
        self.question.show_prompt(self)

    def cancel_asking(self):
        self.question.unbind(self.bind)
        self.bind.prev_mode()
        self.question = None

    def show_question_info(self, prompt):
        if self.askinginbuf:
            self.ins(len(self), prompt)
            self.set_cur(Cur.at(len(self) - 1))
        else:
            log.str("ALERT", "INFO", prompt)

    def answer(self, keys):
        if self.question == None:
            self.bind.set_mode("default")

        self.question.answer(keys, self)
