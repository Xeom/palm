#include "io/key.h"
#include "util/pyfmt.h"

void pyfmt_from_bytes(vec_s *str, char *chrs, size_t len)
{
    size_t ind;

    vec_str(str, "b\"");
    for (ind = 0; ind < len; ++ind)
    {
        unsigned char c;
        c = (unsigned char)chrs[ind];
        vec_fmt(str, "\\x%02x", (int)c);
    }
    vec_str(str, "\"");
}

void pyfmt_from_key(vec_s *str, key_s *key)
{
    vec_fmt(
        str,
        "Key(button=%d, attrs=%d, val=%d)",
        key->button, key->attrs, key->val
    );
}
