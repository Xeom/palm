#include <pthread.h>
#include <stdlib.h>
#include "container/table.h"
#include "text/font.h"
#include "io/tty.h"
#include "log/log.h"

static pthread_mutex_t font_mtx = PTHREAD_MUTEX_INITIALIZER;
static table_s  font_table;
static vec_s    font_vec;

static font_s default_font =
{
    .name = "default",
    .fg = FONT_NOCOLOUR,
    .bg = FONT_NOCOLOUR,
    .attrs = 0
};

static font_s *font_get(unsigned short n);
static unsigned short font_set_with_name(font_s *template, const char *name);

void font_start(void)
{
    table_init(&font_table);
    vec_init(&font_vec, sizeof(font_s));

    font_set(&(font_s){
        .name  = "default",
        .fg    = FONT_NOCOLOUR,
        .bg    = FONT_NOCOLOUR,
        .attrs = 0
    });
}

void font_end(void)
{
    LOG(INFO, INFO, "Stopping font system");

    table_kill(&font_table);

    VEC_FOREACH(&font_vec, font_s *, font)
    {
        free(font->name);
    }

    vec_kill(&font_vec);
}

static font_s *font_get(unsigned short n)
{
    if (n > vec_len(&font_vec))
    {
        LOG(INFO, ERR, "Invalid font (%d) used. Using default instead.", (int)n);
        return &default_font;
    }

    return vec_get(&font_vec, n);
}

void font_print(unsigned short n, tty_s *tty)
{
    WITH_MTX(&font_mtx)
    {
        font_s *font;
        font = font_get(n);
        if (vt100_set_font(&(tty->vt), font) == -1)
            LOG(ALERT, ERR, "Could not set font %u on terminal.", n);
    }
}

static unsigned short font_set_with_name(font_s *template, const char *name)
{
    unsigned short ind, *indptr;
    font_s *font;

    indptr = table_get(&font_table, name, strlen(name), NULL);
    if (indptr)
    {
        ind  = *indptr;
        font = font_get(ind);
    }
    else
    {
        ind  = vec_len(&font_vec);
        font = vec_app(&font_vec, NULL);

 //       LOG(DEBUG, INFO, "Adding a new font (%d): '%s'", (int)ind, name);
    }

    memcpy(font, template, sizeof(font_s));

    font->name = malloc(strlen(name) + 1);
    strcpy(font->name, name);
    table_set(&font_table, name, strlen(name), &ind, sizeof(unsigned short));

    return ind;
}

void font_get_info(unsigned short n, font_s *info)
{
    font_s *font;
    pthread_mutex_lock(&font_mtx);
    font = font_get(n);
    memcpy(info, font, sizeof(font_s));
    pthread_mutex_unlock(&font_mtx);
}

unsigned short font_by_name(const char *name)
{
    unsigned short *indptr, ind;

    pthread_mutex_lock(&font_mtx);

    indptr = table_get(&font_table, name, strlen(name), NULL);
    if (indptr)
        ind = *indptr;
    else
        ind = font_set_with_name(&default_font, name);

    pthread_mutex_unlock(&font_mtx);

    return ind;
}

unsigned short font_set(font_s *font)
{
    unsigned short rtn;

    pthread_mutex_lock(&font_mtx);
    rtn = font_set_with_name(font, font->name);
    pthread_mutex_unlock(&font_mtx);

    return rtn;
}
