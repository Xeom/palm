from core.buffer   import Buffer
from core.bind     import Bind
from buffers.dir   import DirBuffer
from server.server import add_global

from .loclist import fmt_path

# A buffer containing a help file
@add_global()
class HelpBuffer(Buffer):
    def bind_all(self):
        self.bind.bind(Bind("x", pybind="buf.suicide()"))

    def suicide(self):
        self.editor.get_buf_of_type(HelpListBuffer, switch=True)
        self.editor.kill_buf_soon(self)

    def edit(self, path, force=False):
        self.finfo.attrs += ["FNT"]
        self.finfo.assoc(path)
        self.finfo.read()

# A list of availiable help files
@add_global()
class HelpListBuffer(DirBuffer):
    buftype = HelpBuffer

    def make_head(self):
        return [
            "{banner}    __  __ ____ __   _____",
            "{banner}   / /_/ // __// /  / _  /",
            "{banner}  / __  // _/ / /_ / ___/",
            "{banner} /_/ /_//___//___//_/",
            "",
            "This {{emph}}{0}{{default}} is listing the files at".format(type(self).__name__),
            " {{emph}}{0}".format(fmt_path(self.path.real)),
            "",
            "These files should contain some useful",
            " documentation and help to get you started.",
            "",
            "Use the arrow keys to select a file, and press",
            " {emph}Enter{default} to open it. You can press {emph}x{default} to leave any",
            " one of the files.",
            ""
        ]
