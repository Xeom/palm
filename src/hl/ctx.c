#include "hl/ctx.h"
#include "hl/token.h"
#include "log/log.h"
#include "text/font.h"
#include "text/encoder.h"
#include "text/decoder.h"

static int hl_ctx_load_tokens(hl_ctx_s *ctx, yaml_obj_s *obj);
static int hl_ctx_load_state_info(hl_ctx_s *ctx, yaml_obj_s *obj);
static int hl_ctx_load_state_actions(hl_ctx_s *ctx, yaml_obj_s *obj);

void hl_ctx_init(hl_ctx_s *ctx)
{
    memset(ctx, 0, sizeof(hl_ctx_s));
    vec_init(&(ctx->tokens), sizeof(hl_tok_s));
    vec_init(&(ctx->states), sizeof(hl_state_s));
    LOG(INFO, SUCC, "hl_ctx at %p initialized", (void *)ctx);
}

void hl_ctx_kill(hl_ctx_s *ctx)
{
    VEC_FOREACH(&(ctx->states), hl_state_s *, state)
    {
        hl_state_kill(state);
    }

    VEC_FOREACH(&(ctx->tokens), hl_tok_s *, tok)
    {
        hl_tok_kill(tok);
    }

    vec_kill(&(ctx->states));
    vec_kill(&(ctx->tokens));
}

hl_tok_s *hl_ctx_new_token(hl_ctx_s *ctx)
{
    return vec_app(&(ctx->tokens), NULL);
}

hl_tok_s *hl_ctx_get_token(hl_ctx_s *ctx, size_t n)
{
    return vec_get(&(ctx->tokens), n);
}

long hl_ctx_lookup_token(hl_ctx_s *ctx, const char *tokname)
{
    VEC_FOREACH(&(ctx->tokens), hl_tok_s *, tok)
    {
        if (strcmp((char *)vec_get(&(tok->name), 0), tokname) == 0)
            return _ind;
    }

    return -1;
}

hl_state_s *hl_ctx_new_state(hl_ctx_s *ctx)
{
    return vec_app(&(ctx->states), NULL);
}

hl_state_s *hl_ctx_get_state(hl_ctx_s *ctx, size_t n)
{
    return vec_get(&(ctx->states), n);
}

long hl_ctx_lookup_state(hl_ctx_s *ctx, const char *statename)
{
    VEC_FOREACH(&(ctx->states), hl_state_s *, state)
    {
        if (strcmp((char *)vec_get(&(state->name), 0), statename) == 0)
            return _ind;
    }

    return -1;
}

int hl_ctx_load_yaml(hl_ctx_s *ctx, yaml_obj_s *obj)
{
    yaml_obj_s *tokobj, *stateobj;

    tokobj   = yaml_obj_get(obj, (const char *[]){ "tokens", NULL });
    stateobj = yaml_obj_get(obj, (const char *[]){ "states", NULL });

    if (tokobj)
    {
        if (hl_ctx_load_tokens(ctx, tokobj) == -1)
            return -1;
    }

    if (stateobj)
    {
        if (hl_ctx_load_state_info(ctx, stateobj) == -1)
            return -1;
        if (hl_ctx_load_state_actions(ctx, stateobj) == -1)
            return -1;
    }
    return 0;
}

static int hl_ctx_load_tokens(hl_ctx_s *ctx, yaml_obj_s *obj)
{
    YAML_MAPPING_FOREACH(obj, key, val)
    {
        hl_tok_s tok;
        hl_tok_s *new;
        long n;

        hl_tok_init(&tok, key);
        if (hl_tok_load_attrs(&tok, val, ctx) == -1)
        {
            hl_tok_kill(&tok);
            LOG(NOTICE, ERR, "Could not load token '%s'", key);
            return -1;
        }

        n = hl_ctx_lookup_token(ctx, key);
        if (n == -1)
            new = hl_ctx_new_token(ctx);
        else
            new = hl_ctx_get_token(ctx, n);

        memcpy(new, &tok, sizeof(hl_tok_s));
    }

    return 0;
}

static int hl_ctx_load_state_info(hl_ctx_s *ctx, yaml_obj_s *obj)
{
    YAML_MAPPING_FOREACH(obj, key, val)
    {
        hl_state_s state;
        hl_state_s *new;
        long n;

        hl_state_init(&state, key);
        if (hl_state_load_info(&state, val, ctx) == -1)
        {
            hl_state_kill(&state);
            LOG(NOTICE, ERR, "Could not load state info '%s'", key);
            return -1;
        }

        n = hl_ctx_lookup_state(ctx, key);
        if (n == -1)
            new = hl_ctx_new_state(ctx);
        else
            new = hl_ctx_get_state(ctx, n);

        memcpy(new, &state, sizeof(hl_state_s));
    }

    return 0;
}

static int hl_ctx_load_state_actions(hl_ctx_s *ctx, yaml_obj_s *obj)
{
    YAML_MAPPING_FOREACH(obj, key, val)
    {
        hl_state_s *state;
        long n;
        n = hl_ctx_lookup_state(ctx, key);
        state = hl_ctx_get_state(ctx, n);

        if (state)
        {
            if (hl_state_load_actions(state, val, ctx) == -1)
            {
                LOG(NOTICE, ERR, "Could not load state actions '%s'", key);
                return -1;
            }
        }
    }

    return 0;
}

void hl_ctx_apply(hl_ctx_s *ctx, vec_s *chrs, vec_s *states)
{
    vec_s fonts, str, decoded;
    encoder_s encoder;
    decoder_s decoder;
    size_t off;

    if (vec_len(states) == 0)
    {
        return;
    }

    vec_init(&fonts,   sizeof(unsigned short));
    vec_init(&str,     sizeof(char));
    vec_init(&decoded, sizeof(chr_s));

    encoder_init(&encoder, DECODER_UTF8|DECODER_STRIPNL|DECODER_NL);
    VEC_FOREACH(chrs, chr_s *, chr)
        encoder_chr(&encoder, chr, &str);

    vec_ins(&fonts, 0, vec_len(&str), NULL);

    for (off = 0; off <= vec_len(&str); )
    {
        long snum;
        hl_state_s *state;
        snum = *(long *)vec_end(states);

        state = hl_ctx_get_state(ctx, snum);
        if (!state)
            break;
        hl_state_apply(state, ctx, &str, &fonts, states, &off);
    }

    decoder_init(&decoder, DECODER_UTF8);
    VEC_FOREACH(&str, char *, c)
    {
        unsigned short *fnt;
        fnt = vec_get(&fonts, _ind);
        if (!fnt)
            continue;

        chr_s *chr;
        chr = vec_get(chrs, vec_len(&decoded));
        if (!chr)
            continue;

        chr->font = *fnt;

        decoder_chr(&decoder, *c, &decoded);
    }

    vec_kill(&decoded);
    vec_kill(&str);
    vec_kill(&fonts);

    decoder_kill(&decoder);
}
