import libtypes
import ctypes
from typing import TYPE_CHECKING
from lib import lib
from .newstruct import CStructure
from server.globals import add_global

if TYPE_CHECKING:
    from .buffer import Buffer

@add_global()
class Cur(CStructure):
    class_name = "cur_s"

    init_funct = lib.cur_init
    kill_funct = None

    shallow_copyable = True

    @classmethod
    def at(cls, *args, **kwargs):
        # Deprecated: Use Cur(...)
        return cls(*args, **kwargs)

    def __init__(self, *pos, typ=None, sticky=None):

        super().__init__()

        if len(pos) == 1:
            self.goto(pos[0])
        elif len(pos) > 1:
            self.goto(pos)

        if typ != None:
            self.type.set(typ)

        if sticky != None:
            self.sticky = sticky

    def max(self):
        return max(self.pri, self.sec)

    def min(self):
        return min(self.pri, self.sec)

    def type_name(self):
        return lib.cur_type_name(self.type)

    def set_type(self, t):
        # Deprecated: Use self.type.set(...)
        self.type.set(t)

    def set_col(self, cn):
        self.pri.col = cn
        self.sec.col = cn

    def set_row(self, ln):
        self.pri.row = ln
        self.sec.row = ln

    def set_pos(self, pos):
        self.pri.goto(pos)
        self.sec.copy_from(self.pri)

    def goto(self, pos):
        if isinstance(pos, (list, tuple)) and \
             len(pos) == 2 and \
             not any(isinstance(i, (float, int)) for i in pos):
            self.pri.goto(pos[0])
            self.sec.goto(pos[1])

        else:
            self.set_pos(pos)

    def iter_lines(self, buf, rev=False):
        start = self.min().row
        end   = self.max().row

        orig = buf.get_cur()
        lines = range(start, end + 1)

        if rev:
            lines = reversed(lines)

        for row in lines:
            line = buf.get_line(row)
            cur = Cur()
            cur.goto([(row, 0), (row, len(line))])
            buf.set_cur(cur)
            yield row

        buf.set_cur(orig)

    def sort_lines(self, buf, key=None, lnfunct=False):
        if key == None:
            key = lambda x:x

        start = self.min().row
        end   = self.max().row

        tosort = set(range(start, end + 1))

        if not lnfunct:
            funct = lambda ln: key(str(buf.get_line(ln)))
        else:
            funct = key

        order = sorted(tosort, key=funct)
        srctodst = { v: ind + start for ind, v in enumerate(order) }
        dsttosrc = { ind + start: v for ind, v in enumerate(order) }

        while len(tosort) > 0:
            dst = tosort.pop()
            tosort.add(dst)

            vec = buf.get_line(dsttosrc[dst])
            while dst in tosort:
                tmp = buf.get_line(dst)
                buf.set(dst, vec)
                vec = tmp
                tosort.remove(dst)
                dst = srctodst[dst]

    def move(self, direction, n: int, buf: "Buffer"):
        direction = libtypes.cur_dir_t(direction)
        lib.cur_move(self.ptr, direction, n, buf.ptr)

    def expand(self, n: int, buf: "Buffer"):
        self.max().move("RIGHT", n, buf)

    def contract(self, n: int, buf: "Buffer"):
        maxpos = self.max()
        minpos = self.min()

        maxpos.move("LEFT", n, buf)

        if maxpos < minpos:
            maxpos.row = minpos.row
            maxpos.col = minpos.col


class CurSet(CStructure):
    class_name = "curset_s"

    init_funct = lib.curset_init
    kill_funct = lib.curset_kill

    def init_args(self, args, kwargs):
        def inner(buf):
            return [buf.ptr]

        return inner(*args, **kwargs)

    def num_active(self) -> int:
        return lib.curset_num_active(self.ptr)

    def num_inactive(self) -> int:
        return lib.curset_num_inactive(self.ptr)

    def expand_all(self):
        lib.curset_expand_all(self.ptr)

    def get_selected(self) -> Cur:
        rtn = Cur()
        if lib.curset_get_selected(self.ptr, rtn.ptr) == -1:
            raise Exception("Could not get selected cursor position")

        return rtn

    def set_selected(self, cur: Cur):
        if lib.curset_set_selected(self.ptr, cur.ptr) == -1:
            raise Exception("Could not set selected cursor")

    def select_next(self):
        if lib.curset_select_next(self.ptr) == -1:
            raise Exception("Could not select next cursor")

    def select_prev(self):
        if lib.curset_select_prev(self.ptr) == -1:
            raise Exception("Could not select prev cursor")

    def select_ind(self, ind: int):
        if lib.curset_select_ind(self.ptr, ind) == -1:
            raise Exception("Could not select ind")

    def get_selected_ind(self) -> int:
        rtn = lib.curset_get_selected_ind(self.ptr)

        if rtn == -1:
            raise Exception("Could not get selected ind")

        return rtn

    def push(self, cur: Cur):
        if lib.curset_push(self.ptr, cur.ptr) == -1:
            raise Exception("Could not push cursor")

    def pop(self) -> Cur:
        rtn = Cur()

        if lib.curset_pop(self.ptr, rtn.ptr) == -1:
            raise Exception("Could not pop cursor")

        return rtn

    def push_inactive(self, cur: Cur):
        if lib.curset_push_inactive(self.ptr, cur.ptr) == -1:
            raise Exception("Could not push inactive cursor")

    def pop_inactive(self) -> Cur:
        rtn = Cur()

        if lib.curset_pop_inactive(self.ptr, rtn.ptr) == -1:
            raise Exception("Could not pop inactive cursor")


        return rtn

@add_global(args=["buf"])
def pop_cur(buf):
    buf.curset.pop()

@add_global(args=["buf"])
def place_cur(buf, c=None, sec="None", typ=None):
    """Place another cursor at a location.

    Arguments:
        c, sec -- Specify the location of a cursor, in the same format as
                  goto(). If not specified, the location is assumed to be
                  the location of the primary cursor in the buffer.
        typ    -- The type of the new cursor.
    """
    if c == None:
        c = buf.get_cur()
    else:
        c = Cur.at(c, sec, typ=typ)

    buf.einfo.attrs = [a for a in buf.einfo.attrs if a != "ALTS"]
    buf.curset.push(c)

@add_global(args=["buf"])
def clear_altcurs(buf):
    """Delete all cursors other than the current one."""
    while buf.curset.num_active() > 1:
        buf.curset.pop()
