from core.buffer   import Buffer
from core.bind     import Bind
from core.key      import Key
from server.server import add_global

@add_global()
class InfoBuffer(Buffer):
    def init(self, *args):
        Buffer.init(self, *args)
        self.prevwin = self.editor.selbuf.get_win()

    def show_info(self, text, title="INFO"):
        string = (
            "{{title}}{title}{{faded}} (press Enter to exit ...)\n"
            "\n"
            "{text}\n"
            "\n"
        ).format(text=text, title=title)
        lines = string.split("\n")
        self.ins(0, lines, attrs=["UTF8", "FNT"])

    def suicide(self):
        win = self.get_win()
        self.editor.win_close(win)
        self.editor.kill_buf_soon(self)
        self.editor.sel_win(self.prevwin)

    def bind_all(self):
        self.bind.bind(Bind(Key("enter"), pybind="buf.suicide()"))
