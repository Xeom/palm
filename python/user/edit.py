from server.server import add_global
from core.editor   import Editor
from core.buffer   import Buffer
from core.cur      import Cur
from core.pos      import Pos
from core.chrvec   import ChrVec

from lib import lib

@add_global(args=["editor"])
def edit_file(editor: Editor, path: str):
    """Edit a path.

    Opens and selects a new EditBuffer, and selects a file to
    edit in it.
    """
    editor.get_buf_editing_path(path, switch=True)

@add_global(args=["editor"])
def save_all(editor: Editor, force: bool=False):
    """Save every currently open file."""
    for b in editor.bufs:
        if b.finfo.path:
            save(b, force=force)

@add_global(args=["buf"])
def load(buf: Buffer, force: bool=False):
    buf.finfo.read(force=force)

@add_global(args=["buf"])
def reload(buf: Buffer):
    load(buf, force=True)

@add_global(args=["buf"])
def save(buf: Buffer, force: bool=False):
    """Save the current file."""
    buf.finfo.write(force=force)


def obj_to_ln(obj, buf):
    if obj == None:
        obj = buf.get_cur()
    if isinstance(obj, Cur):
        obj = obj.pri
    if isinstance(obj, Pos):
        obj = obj.row

    return obj

@add_global(args=["buf"])
def getl(buf: Buffer, ln=None) -> str:
    """Get the contents of a line."""
    line = buf.get_line(obj_to_ln(ln, buf))

    return str(line)

@add_global(args=["buf"])
def setl(buf: Buffer, val, ln=None) -> str:
    """Set the contents of a line."""
    buf.set(obj_to_ln(ln, buf), val)

@add_global(args=["buf"])
def insl(buf: Buffer, val=1, ln=None):
    """Insert some line(s)."""
    buf.ins(obj_to_ln(ln, buf), val)

@add_global(args=["buf"])
def reml(buf: Buffer, n=1, ln=None):
    """Remove some line(s)."""
    buf.rem(obj_to_ln(ln, buf), n)

# TODO: When the new command system is in place,
#       delete these.
@add_global(args=["buf"])
def backspace(b):
    chrvec = ChrVec()
    lib.cmd_backspace(chrvec.ptr, b.ptr, None, 0)

@add_global(args=["buf"])
def delete(b):
    chrvec = ChrVec()
    lib.cmd_del(chrvec.ptr, b.ptr, None, 0)

@add_global(args=["buf"])
def enter(b):
    chrvec = ChrVec()
    lib.cmd_enter(chrvec.ptr, b.ptr, None, 0)
