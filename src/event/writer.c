#include "event/writer.h"
#include "event/poll.h"
#include "util/macro.h"

static void writer_start(writer_s *writer);
static void writer_stop(writer_s *writer);
static void writer_poll_cb(editor_s *, event_s *, void *param);

static inline void writer_swap_bufs(writer_s *writer)
{
    SWAP(writer->appbuf, writer->finfo.writevec);
}

void writer_init(writer_s *writer, poll_ctx_s *pollctx, int fd)
{
    vec_init(&(writer->buf1), sizeof(char));
    vec_init(&(writer->buf2), sizeof(char));

    writer->finfo = (io_file_info_s){
        .fd = fd,
        .writevec = &(writer->buf1)
    };
    writer->appbuf = &(writer->buf2);

    writer->pollctx = pollctx;
    writer->alive   = false;

    poll_add(pollctx, &(writer->finfo));
}

void writer_write(writer_s *writer, size_t n, const char *data)
{
    WITH_MTX(&(writer->mtx))
    {
        vec_add(writer->appbuf, n, data);

        if (!(writer->alive) && vec_len(writer->appbuf) > 0)
        {
            writer_swap_bufs(writer);
            writer_start(writer);
        }
    }
}

void writer_kill(writer_s *writer)
{
    writer_stop(writer);

    vec_kill(&(writer->buf1));
    vec_kill(&(writer->buf2));

    pthread_mutex_destroy(&(writer->mtx));
}

int writer_flush(writer_s *writer)
{
    TRY(io_write_all_from_vec(writer->finfo.fd, writer->finfo.writevec, NULL));
    TRY(io_write_all_from_vec(writer->finfo.fd, writer->appbuf,         NULL));

    return PALM_OK;
}

static void writer_start(writer_s *writer)
{
    poll_ctx_s  *pollctx;
    event_ctx_s *eventctx;
    event_spec_s spec;

    if (writer->alive)
        return;

    pollctx  = writer->pollctx;
    eventctx = pollctx->eventctx;

    poll_event_spec(&(writer->finfo), &spec);
    event_listen(
        eventctx, &spec,
        &(event_action_s){
            .cb = writer_poll_cb,
            .param = writer
        }
    );

    poll_add(pollctx, &(writer->finfo));

    writer->alive = true;
}

static void writer_stop(writer_s *writer)
{
    if (!writer->alive)
        return;

    poll_rem(writer->pollctx, &(writer->finfo));

    writer->alive = false;
}

static void writer_poll_cb(editor_s *editor, event_s *event, void *param)
{
    writer_s *writer;
    writer = param;

    WITH_MTX(&(writer->mtx))
    {
        if (writer->alive && vec_len(writer->finfo.writevec) == 0)
        {
            if (vec_len(writer->appbuf) > 0)
            {
                writer_swap_bufs(writer);
            }
            else
            {
                writer_stop(writer);
            }
        }
    }
}
