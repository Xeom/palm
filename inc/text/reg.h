#if !defined(TEXT_REG_H)
# define TEXT_REG_H
# include "container/vec.h"

extern size_t maxregs;

int reg_push(vec_s *str, int ind);

int reg_pop(vec_s *str, int ind);

int reg_set(vec_s *str, int ind);

int reg_get(vec_s *str, int ind);

int reg_chr(char c, int ind);

size_t reg_num(void);

#endif
