import libtypes
import ctypes

from hooks  import hooks
from lib    import lib
from editor import log
from lib    import enums

from .file      import File
from .structure import Structure
from .io        import IoPath

class Finfo(Structure):
    typ        = libtypes.finfo_s
    init_funct = lib.finfo_init
    kill_funct = lib.finfo_kill

    def __init__(self, buf, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.buf = buf
        self.file = self["file"]

    def assoc(self, path):
        if isinstance(path, str):
            path = path.encode("utf-8")

        hooks.call("buf-assoc", self.buf)
        lib.finfo_assoc(self.ptr, path)
        hooks.call("buf-post-assoc", self.buf)

    @property
    def attrs(self):
        val = enums.decoder_attr_t(0)
        lib.finfo_attrs(self.ptr, None, ctypes.byref(val))
        return val.to_flags()

    @attrs.setter
    def attrs(self, val):
        val = enums.decoder_attr_t(val)
        lib.finfo_attrs(self.ptr, ctypes.byref(val), None)

    @property
    def paths(self):
        rtn = IoPath()
        lib.finfo_get_path(self.ptr, rtn.ptr)
        return rtn

    @property
    def path(self):
        return self.paths.real

    @property
    def newfile(self):
        return self["newfile"]

    @property
    def associated(self):
        return self["associated"]

    def write(self, buf=None, force=False):
        if buf == None:
            buf = self.buf

        hooks.call("buf-write", buf)
        if lib.finfo_write(self.ptr, buf.ptr, force) == -1:
            raise Exception("Error while writing file")
        hooks.call("buf-post-write", buf)

    def read(self, buf=None, force=False):
        if buf == None:
            buf = self.buf

        hooks.call("buf-read", buf)
        if lib.finfo_read(self.ptr, buf.ptr, force) == -1:
            raise Exception("Error while reading file")
        hooks.call("buf-post-read", buf)

