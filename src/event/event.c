#include "util/macro.h"
#include "util/time.h"
#include "log/log.h"
#include "event/event.h"

static int event_action_cmp(const void *aptr, const void *bptr);

static int event_action_cmp(const void *aptr, const void *bptr)
{
    event_action_s *a, *b;
    a = (event_action_s *)aptr;
    b = (event_action_s *)bptr;

    return a->pri - b->pri;
}

int event_ctx_init(event_ctx_s *ctx, editor_s *editor)
{
    pthread_mutexattr_t attr;

    memset(ctx, 0, sizeof(event_ctx_s));

    vec_init(&(ctx->pending), sizeof(event_s));
    table_init(&(ctx->listeners));

    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP);

    pthread_mutex_init(&(ctx->mtx), &attr);

    pthread_mutexattr_destroy(&attr);

    ctx->editor = editor;

    return 0;
}

void event_ctx_kill(event_ctx_s *ctx)
{
    pthread_mutex_destroy(&(ctx->mtx));

    vec_kill(&(ctx->pending));

    TABLE_FOREACH(&(ctx->listeners), chain)
    {
        vec_s *list;
        list = table_chain_val(chain, NULL);
        vec_kill(list);
    }

    table_kill(&(ctx->listeners));
}


int event_push(event_ctx_s *ctx, const event_s *event)
{
    int res;

    res = 0;

    if (event->attrs & EVENT_ATTR_NOW)
    {
        event_s tmp;
        memcpy(&(tmp), event, sizeof(event_s));
        event_handle(ctx, &tmp);

        return 0;
    }

    WITH_MTX(&(ctx->mtx))
    {
        event_s *pushed;

        if (event->attrs & EVENT_ATTR_NEXT)
            pushed = vec_ins(&(ctx->pending), 0, 1, event);
        else
            pushed = vec_app(&(ctx->pending), event);

        if (pushed)
        {
            pushed->ts = util_time_fsec();
        }
        else
        {
            LOG(ALERT, ERR, "Could not push event to pending stack");
            res = -1;
        }
     }

     return res;
}

int event_listen(
    event_ctx_s *ctx,
    const event_spec_s *spec,
    const event_action_s *act
)
{
    int res;
    size_t ind;
    event_spec_s speccpy;

    memset(&speccpy, 0, sizeof(event_spec_s));
    speccpy.typ = spec->typ;
    speccpy.ptr = spec->ptr;

    res = 0;
    WITH_MTX(&(ctx->mtx))
    {
        vec_s *list;
        list = table_get(
            &(ctx->listeners),
            &speccpy, sizeof(event_spec_s), 
            NULL
        );

        if (!list)
        {
            table_set(
                &(ctx->listeners),
                &speccpy, sizeof(event_spec_s),
                NULL, sizeof(vec_s)
            );
            list = table_get(
                &(ctx->listeners),
                &speccpy, sizeof(event_spec_s),
                NULL
            );

            if (list)
            {
                vec_init(list, sizeof(event_action_s));
            }
            else
            {
                LOG(ALERT, ERR, "Could not insert action into event ctx.");
                res = -1;
            }
        }

        if (res == 0)
        {
            ind = vec_bst(list, act, event_action_cmp);
            if (!vec_ins(list, ind, 1, act))
            {
                LOG(ALERT, ERR, "Could not insert action into event ctx.");
                res = -1;
            }
        }
    }

    return res;
}

int event_unlisten(
    event_ctx_s *ctx,
    const event_spec_s *spec,
    const event_action_s *act
)
{
    int res;
    event_spec_s speccpy;

    memset(&speccpy, 0, sizeof(event_spec_s));
    speccpy.typ = spec->typ;
    speccpy.ptr = spec->ptr;

    res = 0;

    WITH_MTX(&(ctx->mtx))
    {
        vec_s *list;
        list = table_get(&(ctx->listeners), &speccpy, sizeof(event_spec_s), NULL);
        if (list)
        {
            res = -1;
            VEC_RFOREACH(list, event_action_s *, cmp)
            {
                if (!act || (cmp->cb == act->cb &&cmp->param == act->param))

                {
                    vec_del(list, _ind, 1);
                    res = 0;
                }
            }

            if (vec_len(list) == 0)
            {
                vec_kill(list);
                table_del(&(ctx->listeners), &speccpy, sizeof(event_spec_s));
            }
        }
        else
        {
            res = -1;
        }
    }

    return res;
}

void event_handle(event_ctx_s *ctx, event_s *event)
{
    vec_s actionscpy;
    editor_s *editor;

    vec_init(&actionscpy, sizeof(event_action_s));

    WITH_MTX(&(ctx->mtx))
    {
        vec_s *list;
        event_spec_s spec;

        memset(&spec, 0, sizeof(event_spec_s));
        spec.typ = event->spec.typ;
        spec.ptr = event->spec.ptr;

        list = table_get(&(ctx->listeners), &spec, sizeof(event_spec_s), NULL);

        if (list)
        {
            vec_cpy(&actionscpy, list);
        }

        editor = ctx->editor;
    }

    VEC_FOREACH(&actionscpy, event_action_s *, action)
    {
        if (event->attrs & EVENT_ATTR_CANCELLED)
            break;

        (action->cb)(editor, event, action->param);
    }

    vec_kill(&actionscpy);
}

event_s *event_get_next(event_ctx_s *ctx)
{
    event_s *res;

    WITH_MTX(&(ctx->mtx))
    {
        if (vec_len(&(ctx->pending)))
        {
            res = &(ctx->currevent);
            memcpy(res, vec_get(&(ctx->pending), 0), sizeof(event_s));
            vec_del(&(ctx->pending), 0, 1);
        }
        else
        {
            res = NULL;
        }
    }

    return res;
}

void event_replace(event_ctx_s *ctx, event_s *new)
{
    WITH_MTX(&(ctx->mtx))
    {
        ctx->currevent.attrs |= EVENT_ATTR_CANCELLED;
        new->attrs           |= EVENT_ATTR_NEXT;

        event_push(ctx, new);
    }
}
