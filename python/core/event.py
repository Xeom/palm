from lib import lib, defs
from typing import Optional

from .newstruct import CStructure, SimpleCStructure

from server.globals import add_global

@add_global()
class Event(SimpleCStructure):
    class_name = "event_s"

@add_global()
class EventAction(SimpleCStructure):
    @classmethod
    def with_cb_decorator(cls, args=[], pri=0):
        def inner(cb):
            return cls.with_cb(cb, args, pri)

        return inner

    @classmethod
    def with_cb(cls, cb, args=[], pri=0, eventargs=False):
        rtn = cls(pri=pri)
        rtn.set_cb(cb, args, eventargs)
        return rtn

    def remove(self):
        if hasattr(self, "listeners"):
            for ctx, spec in self.listeners:
                ctx.unlisten(spec, self)

        self.listeners = []

    def __del__(self):
        self.remove()

    def set_cb(self, cb, args=[], eventargs=False):
        def inner(editorptr, eventptr, param):
            if eventargs:
                cb(editorptr, eventptr, *args)
            else:
                cb(*args)

        if self.cb:
            raise Exception("Cannot set cb multiple times")

        self.cb = defs.event_cb_t(inner)

    class_name = "event_action_s"

@add_global()
class EventSpec(SimpleCStructure):
    class_name = "event_spec_s"

@add_global()
class EventCtx(CStructure):
    class_name = "event_ctx_s"

    init_funct = lib.event_ctx_init
    kill_funct = lib.event_ctx_kill

    def init_args(self, args, kwargs):
        return [args[0].ptr]

    def push(self, event: Event):
        if lib.event_push(self.ptr, event.ptr) == -1:
            raise Exception("Could not push event")

    def on(self, spec: EventSpec, args=[], eventargs=False):
        def inner(f):
            act = EventAction.with_cb(f, args=args, eventargs=eventargs)
            self.listen(spec, act)

            return act

        return inner

    def listen(self, spec: EventSpec, action: EventAction):
        if lib.event_listen(self.ptr, spec.ptr, action.ptr) == -1:
            raise Exception("Could not listen for event.")

        if not hasattr(action, "listeners"):
            action.listeners = []

        action.listeners.append((self, spec.copy()))

    def unlisten(self, spec: EventSpec, action: Optional[EventAction]):
        if action is not None:
            action = action.ptr

        return lib.event_unlisten(self.ptr, spec.ptr, action)

    def replace(self, new: Event):
        lib.event_replace(self.ptr, event.ptr)
