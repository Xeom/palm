#include "snow.h"
#include "util/string.h"

describe(string)
{
    subdesc(startswith)
    {
        it ("Returns true")
        {
            asserteq(str_startswith("catalogue", "cat"), true);
        }

        it ("Returns false")
        {
            asserteq(str_startswith("catalogue", "dog"), false);
        }

        it ("Handles short strings")
        {
            asserteq(str_startswith("c", "cat"), false);
        }

        it ("Handles empty needles")
        {
            asserteq(str_startswith("text", ""), true);
        }

        it ("Handles empty strings")
        {
            asserteq(str_startswith("", "text"), false);
            asserteq(str_startswith("", ""),     true);
        }
    }

    subdesc(endswith)
    {
        it ("Returns true")
        {
            asserteq(str_endswith("bicycle", "cycle"), true);
        }

        it ("Returns false")
        {
            asserteq(str_endswith("bicycle", "car"), false);
        }

        it ("Handles short strings")
        {
            asserteq(str_endswith("c", "cat"), false);
        }

        it ("Handles empty needles")
        {
            asserteq(str_endswith("text", ""), true);
        }

        it ("Handles empty strings")
        {
            asserteq(str_endswith("", "text"), false);
            asserteq(str_endswith("", ""),     true);
        }
    }
}
