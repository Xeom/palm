import libtypes
import ctypes

from typing import Union, List

from lib import lib, defs
from util.encode import bytify, strify
from .newstruct  import CStructure

from server.globals import add_global

def make_pathbuf():
    """Make a buffer for paths for use with io functions."""
    return defs.io_path_t()

def make_errbuf():
    """Make an errorbuf for use with io functions."""
    return defs.io_errbuf_t()

def realpath(path: Union[str, bytes]) -> str:
    """Get an absolute version of a path, with an expanded home.

    e.g.
    realpath("~/bees.txt") => "/home/user/bees.txt"
    realpath("hello")      => "/my/cwd/hello"

    Arguments:
        path - The path to express as an absolute path.
    """
    path   = bytify(path)
    rtn    = make_pathbuf()
    errbuf = make_errbuf()

    res = lib.io_realpath(rtn, path, errbuf)

    if res == -1:
        raise Exception(f"Could not get real path - {errbuf.value}")

    return rtn.value

def relpath(path: Union[str, bytes], root: Union[str, bytes]=b".") -> str:
    """Express a path as a relative path if possible

    e.g.
    relpath("/a/b/c", "/a") => "b/c",
    relpath("/d/e/f", "/a") => "/d/e/f"

    Arguments:
        path  - The path to express as a relative path.
        root  - The location to make the path relative to.
                By default this is '.', or the current working
                directory.
    """
    path   = bytify(path)
    root   = bytify(root)

    rtn    = make_pathbuf()
    errbuf = make_errbuf()

    res = lib.io_relpath(rtn, path, root, errbuf)

    if res == -1:
        raise Exception(f"Could not get relative path - {errbuf.value}")

    return rtn.value

@add_global()
def is_dir(path: Union[str, bytes]) -> bool:
    """Return whether a path is a directory."""
    path   = bytify(path)
    errbuf = make_errbuf()

    res = lib.io_is_dir(path, errbuf)

    if res == -1:
        raise Exception("Could not tell if path is directory: {}".format(errbuf.value))
    elif res == 0:
        return False
    elif res == 1:
        return True
    else:
        raise Exception(f"io_is_dir returned unexpected value - {res}")

@add_global()
def is_file(path: Union[str, bytes]) -> bool:
    """Return whether a path is a file."""
    path   = bytify(path)
    errbuf = make_errbuf()

    res = lib.io_is_file(path, errbuf)

    if res == -1:
        raise Exception("Could not tell if path is directory: {}".format(errbuf.value))
    elif res == 0:
        return False
    elif res == 1:
        return True
    else:
        raise Exception(f"io_is_file returned unexpected value - {res}")

@add_global()
def create_dir(path: Union[str, bytes]):
    """Create a directory if it doesn't exist.

    If necessary, the parent(s) are also created.
    """
    path   = bytify(path)
    errbuf = make_errbuf()

    if lib.io_create_dir(path, errbuf) == -1:
        raise Exception("Could not create directory: {}".format(errbuf.value))

@add_global()
def create_file(path: Union[str, bytes]):
    """Create a file if it does not exist.

    If necessary, the directories required are also created.
    """
    path   = bytify(path)
    errbuf = make_errbuf()

    if lib.io_create_file(path, errbuf) == -1:
        raise Exception("Could not create file: {}".format(errbuf.value))

@add_global()
class IoPath(CStructure):
    """A class to wrap the io_path_s structure.

    This structure contains the details of decomposed paths.
    Right now, these details are the realpath, base, and dir
    components, though more may be added later!
    """
    class_name = "io_path_s"
    init_funct = None
    kill_funct = None

    @classmethod
    def decompose(cls, path: Union[str, bytes]) -> "IoPath":
        """Decompose a path into an IoPath structure.

        The new structure is returned.

        >>> ip = IoPath.decompose("a/b")
        ... ip.real
        "/cwd/a/b"
        ... ip.base
        "b"
        ... ip.dir
        "a"

        Arguments:
            path - The path to decompose.
        """
        errbuf = make_errbuf()
        path   = bytify(path)

        res    = cls()
        if lib.io_path_decomp(res.ptr, path, errbuf) == -1:
            raise Exception("Could not decompose path: {}".format(strify(errbuf.value)))

        return res
    def get_real(self):
        return strify(self.real)

    def get_dir(self):
        return strify(self.dir)

    def get_base(self):
        return strify(self.base)

    def get_rel(self, root: Union[str, bytes]="."):
        return relpath(self.real, root)

    def is_dir(self) -> bool:
        return is_dir(self.real)

    def is_file(self) -> bool:
        return is_file(self.real)

@add_global()
class IoFileInfo(CStructure):
    """A structure containing info about files to be polled."""
    class_name = "io_file_info_s"

    init_funct = None
    kill_funct = None

    def poll(self, toutms: int=500):
        errbuf = make_errbuf()
        res = lib.io_poll_file(self.ptr, toutms, errbuf)
        if res == -1:
            raise Exception("Could not poll file: {}".format(strify(errbuf.value)))

        return res

    @classmethod
    def poll_many(self, infos: List["IoFileInfo"], toutms: int=500):
        errbuf = make_errbuf()
        vec    = (len(infos) * self.typ)()

        for i, v in enumerate(infos):
            ctypes.memmove(ctypes.byref(vec[i]), v.ptr, ctypes.sizeof(self.typ))

        res = lib.io_poll_files(vec, len(infos), toutms, errbuf)

        for i, v in enumerate(infos):
            ctypes.memmove(v.ptr, ctypes.byref(vec[i]), ctypes.sizeof(self.typ))

        if res == -1:
            raise Exception("Could not poll files: {}".format(strify(errbuf.value)))

        return res
