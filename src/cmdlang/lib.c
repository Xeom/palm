#include "cmdlang/cmd.h"
#include "cmdlang/lib.h"

int cmd_lib_init(cmd_lib_s *lib)
{
    list_init(&(lib->cmds), sizeof(cmd_s));
    table_init(&(lib->byname));
    table_init(&(lib->shortnames));

    return PALM_OK;
}

void cmd_lib_kill(cmd_lib_s *lib)
{
    list_kill(&(lib->cmds));
    table_kill(&(lib->byname));
    table_kill(&(lib->shortnames));
}

cmd_s *cmd_lib_add_cmd(cmd_lib_s *lib, const char *name, const char *help)
{
    cmd_s *rtn;
    list_item_s *item;
    item = list_push_first(&(lib->cmds), NULL);

    if (!item)
        return NULL;

    rtn = list_item_data(item);
    if (cmd_init(rtn, name, help) == PALM_ERR)
    {
        list_del(&(lib->cmds), LIST_RANGE(item, item));
        return NULL;
    }

    table_set(&(lib->byname), name, strlen(name), &rtn, sizeof(cmd_s *));

    return rtn;
}

int cmd_lib_add_shortname(
    cmd_lib_s *lib,
    cmd_s *cmd,
    const char *name,
    size_t len
)
{
    table_set(&(lib->shortnames), name, len, &cmd, sizeof(cmd_s *));

    return PALM_OK;
}

cmd_s *cmd_lib_find_name(cmd_lib_s *lib, const char *name, size_t len)
{
    cmd_s **ptr;
    ptr = table_get(&(lib->byname), name, len, NULL);

    if (!ptr) return NULL;

    return *ptr;
}

cmd_s *cmd_lib_find_shortname(cmd_lib_s *lib, const char *name, size_t len)
{
    cmd_s **ptr;
    ptr = table_get(&(lib->shortnames), name, len, NULL);

    if (!ptr) return NULL;

    return *ptr;
}

