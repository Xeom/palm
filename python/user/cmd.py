from lib import enums
from server.globals import add_global

@add_global()
def cmd_mode_type(buf, key, final=False):
    if key.button == enums.key_button_t("CHAR"):
        buf.cmdparser.type_char(key.val)

#    if final and buf.cmdparser.has_flag("

@add_global()
def cmd_mode_run(buf, final=False):
    pass

@add_global()
def cmd_mode_cancel(buf):
    pass
