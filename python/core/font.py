"""A Python wrapper for text/font.c"""
import csv

from typing import Union
from ast import literal_eval

from lib import lib, enumspec
from .newstruct import CStructure
import libtypes

from util.encode import bytify
from server.globals import add_global


def font_start():
    """Initialize the font system.

    Don't call this.
    """
    lib.font_start()

font_start()

@add_global()
def font_by_name(name: Union[str, bytes]) -> int:
    """Get the index of a font by it's name.

    This looks up the font, returns a unique index for
    the name. If there is no such font, it is assigned
    a new index.

    Arguments:
        name - The name of the font to look for.
    """
    return lib.font_by_name(bytify(name))

@add_global()
class Font(CStructure):
    """A wrapper for the font_s structure.

    This structure is used for specifying font attributes.
    """
    init_funct = None
    kill_funct = None

    class_name = "font_s"

    @classmethod
    def from_name(cls, name: Union[str, bytes]) -> "Font":
        """Get attributes of a font by its name.

        Return a Font instance containing the attributes currently
        configured for the given font.

        Arguments:
            name - The name of the font to look for.
        """
        val = font_by_name(name)
        return cls.from_val(val)

    @classmethod
    def from_val(cls, val: int) -> "Font":
        """Get attributes of a font by its index.

        Return a Font instance containing the attributes currently
        configured for the given font.

        Arguments:
            val - The index of the font.
        """
        rtn = cls()
        lib.font_get_info(val, rtn.ptr)
        return rtn

    def init_action(
            self,
            name: Union[str, bytes],
            fg: enumspec="NOCOLOUR",
            bg: enumspec="NOCOLOUR",
            attrs: enumspec=[]
        ):
        """Generate font from initialization arguments.

        Arguments:
            name  - The name of the font.
            fg    - The foreground colour, as a font_colour_t spec,
                    e.g. 'RED', 0xff0000, libtypes.font_color_t.RED.
            bg    - The background colour, as a font_colour_t spec.
            attrs - Font attributes as a font_attr_t spec,
                    e.g. 'BOLD', ['BRIGHT', 'BRIGHTBG'].
        """
        self.name = bytify(name)
        self.fg.set(fg)
        self.bg.set(bg)
        self.attrs.set(attrs)

    def set(self):
        """Update the global font database with these attributes.

        This must be called before the changes made in this structure
        are applied.
        """
        lib.font_set(self.ptr)

def load_fonts_from_csv(f):
    for line in csv.reader(l for l in f.readlines() if not l.startswith("#")):
        if line:
            Font(*[literal_eval(c.strip()) for c in line]).set()
