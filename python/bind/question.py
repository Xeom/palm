from lib           import lib
from util.keys     import Keys
from bind.mode     import Mode, PyCommand, CCommand
from core.bind     import Bind

QuestionMode = Mode("question")

# If we get stuck and the user panics!
QuestionMode.add_bind(Bind(Keys.esc("") * 5, pybind="buf.cancel_asking()"))
QuestionMode.add_bind(Bind(None,             pybind="buf.answer(%k)"))
