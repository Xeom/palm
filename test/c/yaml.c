#include "snow.h"
#include "util/yaml.h"
#include <stdbool.h>

describe(yaml_obj)
{
    yaml_obj_s obj;

    before_each()
    {
        yaml_obj_init(&obj, NULL, 0);
    }

    after_each()
    {
        yaml_obj_kill(&obj);
    }

    it ("Overrides tokens")
    {
        yaml_obj_s *sub;
        FILE *tmp1, *tmp2;
        tmp1 = tmpfile();
        tmp2 = tmpfile();
        fputs("favourite-colour: yellow\n", tmp1);
        fputs("favourite-colour: red\n", tmp2);
        fseek(tmp1, 0, SEEK_SET);
        fseek(tmp2, 0, SEEK_SET);

        asserteq(yaml_obj_parse(&obj, tmp1), 0);
        asserteq(obj.type, YAML_OBJ_MAPPING);
        asserteq(table_len(&(obj.subs)), 1);

        sub = yaml_obj_get(&obj, (const char *[]){"favourite-colour", NULL});
        assert(sub);
        asserteq(sub->type, YAML_OBJ_VALUE);
        asserteq((char *)vec_get(&(sub->val), 0), "yellow");

        asserteq(yaml_obj_parse(&obj, tmp2), 0);
        sub = yaml_obj_get(&obj, (const char *[]){"favourite-colour", NULL});
        assert(sub);
        asserteq(sub->type, YAML_OBJ_VALUE);
        asserteq((char *)vec_get(&(sub->val), 0), "red");
    
        fclose(tmp1);
        fclose(tmp2);
    }

    it ("Combines duplicated tokens")
    {
        yaml_obj_s *sub;
        FILE *tmp;
        tmp = tmpfile();
        fputs(
            "animals:\n"
            "  cow: moo\n"
            "animals:\n"
            "  cat: meow\n",
            tmp);

        fseek(tmp, 0, SEEK_SET);
        asserteq(yaml_obj_parse(&obj, tmp), 0);
        asserteq(obj.type, YAML_OBJ_MAPPING);
        asserteq(table_len(&(obj.subs)), 1);

        sub = yaml_obj_get(&obj, (const char *[]){"animals", NULL});
        assert(sub);
        asserteq(sub->type, YAML_OBJ_MAPPING);
        asserteq(table_len(&(sub->subs)), 2);

        fclose(tmp);
    }

    it ("Iterates mappings")
    {
        FILE *tmp;
        bool seenyellow, seenred, seengreen;
        tmp = tmpfile();
        fputs(
            "banana: yellow\n"
            "cucumber: green\n"
            "apple: red\n",
            tmp);


        fseek(tmp, 0, SEEK_SET);
        asserteq(yaml_obj_parse(&obj, tmp), 0);
        asserteq(obj.type, YAML_OBJ_MAPPING);
        asserteq(table_len(&(obj.subs)), 3);

        seenyellow = false;
        seenred    = false;
        seengreen  = false;
        YAML_MAPPING_FOREACH(&obj, key, val)
        {
            if (strcmp(key, "banana") == 0)
            {
                asserteq(vec_get(&(val->val), 0), "yellow");
                seenyellow = true;
            }
            if (strcmp(key, "cucumber") == 0)
            {
                asserteq(vec_get(&(val->val), 0), "green");
                seengreen = true;
            }
            if (strcmp(key, "apple") == 0)
            {
                asserteq(vec_get(&(val->val), 0), "red");
                seenred = true;
            }
        }
        assert(seenyellow);
        assert(seengreen);
        assert(seenred);

        fclose(tmp);
    }

    it ("Parses tokens")
    {
        yaml_obj_s *sub;
        FILE *tmp;
        tmp = tmpfile();
        fputs(
            "food:\n"
            "  beef:\n"
            "    juicy: yes\n"
            "    crunchy: no\n"
            "  biscuits:\n"
            "    juicy: no\n"
            "    crunchy: yes\n",
            tmp);
        fseek(tmp, 0, SEEK_SET);
        asserteq(yaml_obj_parse(&obj, tmp), 0);
        asserteq(obj.type, YAML_OBJ_MAPPING);
        asserteq(table_len(&(obj.subs)), 1);

        sub = yaml_obj_get(&obj, (const char *[]){"food", NULL});
        assert(sub);
        asserteq(sub->type, YAML_OBJ_MAPPING);
        asserteq(table_len(&(sub->subs)), 2);

        sub = yaml_obj_get(&obj, (const char *[]){"food", "beef", NULL});
        assert(sub);
        asserteq(sub->type, YAML_OBJ_MAPPING);
        asserteq(table_len(&(sub->subs)), 2);

        const char *answer;
        answer = yaml_obj_get_val(&obj, (const char *[]){"food", "beef", "juicy", NULL}, NULL);
        assert(answer);
        asserteq(answer, "yes");

        fclose(tmp);
    }
}
