#include <stdlib.h>
#include "util/macro.h"
#include "buf/line.h"
#include "text/font.h"
#include "wm/wm.h"
#include "wm/status.h"
#include "wm/update.h"
#include "wm/floatwin.h"
#include "log/log.h"
#include "editor.h"

void wm_init(wm_s *wm, display_s *disp)
{
    win_s *origwin;
    pthread_mutexattr_t mtxattr;

    memset(wm, 0, sizeof(wm_s));
    vec_init(&(wm->windows), sizeof(win_s *));
    vec_init(&(wm->floatwins), sizeof(floatwin_s *));

    pthread_mutexattr_init(&mtxattr);
    pthread_mutexattr_settype(&mtxattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&(wm->mtx), &mtxattr);

    origwin = malloc(sizeof(win_s));

    win_init(origwin, WIN_SPLIT_NONE);
    wm->rootwin = origwin;

    wm->disp = disp;

    vec_app(&(wm->windows), &origwin);

    wm_resize(wm);
    wm_stamp_disp_all(wm);

    wm_updater_init(&(wm->updater), wm);
}

void wm_kill(wm_s *wm)
{
    win_kill(wm->rootwin);
    vec_kill(&(wm->windows));

    VEC_FOREACH(&(wm->floatwins), floatwin_s **, fwptr)
    {
        if (*fwptr)
            wm_floatwin_kill(*fwptr);
    }

    vec_kill(&(wm->floatwins));

    wm_updater_kill(&(wm->updater));
}

void wm_resize(wm_s *wm)
{
    display_dims(wm->disp, &(wm->dims));
    win_resize(wm->rootwin, &(wm->dims));
    wm_stamp_disp_all(wm);
}

void wm_purge_buf(wm_s *wm, buf_s *buf)
{
    wm_updater_purge_buf(&(wm->updater), buf);
}

int wm_split(wm_s *wm, int id, win_split_t dir)
{
    int rtn;
    win_s **win, *new;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), id);
    if (!win || !(*win))
    {
        pthread_mutex_unlock(&(wm->mtx));
        return -1;
    }

    new = win_split(*win, dir);
    if (!new)
    {
        pthread_mutex_unlock(&(wm->mtx));
        return -1;
    }

    wm->rootwin = win_get_root(*win);
    win_resize(wm->rootwin, &(wm->dims));

    VEC_FOREACH(&(wm->windows), win_s **, w)
    {
        if (!(*w))
        {
            *w = new;
            pthread_mutex_unlock(&(wm->mtx));
            wm_stamp_disp_all(wm);
            return _ind;
        }
    }

    vec_app(&(wm->windows), &new);
    rtn = vec_len(&(wm->windows)) - 1;

    pthread_mutex_unlock(&(wm->mtx));

    wm_stamp_disp_all(wm);

    return rtn;
}

int wm_close(wm_s *wm, int ind)
{
    win_s **win;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);
    if (!win || !(*win))
    {
        pthread_mutex_unlock(&(wm->mtx));
        return -1;
    }

    if (win_remove(*win) == -1)
    {
        pthread_mutex_unlock(&(wm->mtx));
        return -1;
    }

    *win = NULL;

    while (vec_len(&(wm->windows)) && *(win_s **)vec_end(&(wm->windows)) == NULL)
    {
        vec_del(&(wm->windows), vec_len(&(wm->windows)) - 1, 1);
    }

    VEC_FOREACH(&(wm->windows), win_s **, w)
    {
        if (*w)
        {
            wm->rootwin = win_get_root(*w);
            break;
        }
    }
    pthread_mutex_unlock(&(wm->mtx));

    win_resize(wm->rootwin, &(wm->dims));
    wm_stamp_disp_all(wm);

    return 0;
}

int wm_get_dims(wm_s *wm, int ind, rect_s *dims)
{
    int rtn;
    win_s **win;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);
    if (!win || !(*win))
    {
        rtn = -1;
    }
    else
    {
        win_get_dims(*win, &(wm->dims), dims);
        rtn = 0;
    }
    pthread_mutex_unlock(&(wm->mtx));

    return rtn;
}

int wm_win_type(wm_s *wm, int ind)
{
    int rtn;
    win_s **win;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);

    if (!win || !(*win))
        rtn = -1;
    else
        rtn = (*win)->type;

    pthread_mutex_unlock(&(wm->mtx));

    return rtn;
}

int wm_next_ind(wm_s *wm, int ind)
{
    int rtn;
    rtn = -1;

    pthread_mutex_lock(&(wm->mtx));

    for (++ind; ; ++ind)
    {
        win_s **win;
        win = vec_get(&(wm->windows), ind);
        if (!win)
            break;

        if (*win)
        {
            rtn = ind;
            break;
        }
    }

    pthread_mutex_unlock(&(wm->mtx));

    return rtn;
}

void wm_update_status(wm_s *wm, int ind, editor_s *editor, buf_s *buf)
{
    vec_s status;
    vec_init(&status, sizeof(chr_s));
    status_format(&(editor->statusfmt), editor, ind, buf, &status);
    wm_set_status(wm, ind, &status);
    vec_kill(&status);
}

int wm_set_status(wm_s *wm, int ind, vec_s *status)
{
    int rtn;
    win_s **win;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);
    if (!win || !(*win) || (*win)->type != WIN_SPLIT_NONE)
    {
        LOG(INFO, ERR, "Tried to set the status bar on an invalid window");
        rtn = -1;
    }
    else
    {
        vec_clr(&((*win)->status));
        vec_cpy(&((*win)->status), status);

        rtn = 0;
    }
    pthread_mutex_unlock(&(wm->mtx));

    if (rtn == 0)
    {
        wm_add_borders(wm, ind);
    }

    return rtn;
}

int wm_add_borders(wm_s *wm, int ind)
{
    rect_s windims;
    rect_s dst;
    chr_s vchr, hchr, cchr;
    win_s **win;
    rect_s *dims;

    FONT_DEF(vfont, "win-vertical-border")
    FONT_DEF(hfont, "win-horizontal-border")
    FONT_DEF(cfont, "win-corner-border")

    pthread_mutex_lock(&(wm->mtx));
    dims = &(wm->dims);

    win = vec_get(&(wm->windows), ind);
    if (!win || !(*win))
    {
        pthread_mutex_unlock(&(wm->mtx));
        return -1;
    }

    if ((*win)->type != WIN_SPLIT_NONE)
        return 0;

    vchr = (chr_s){ .text = " ", .font = vfont };
    hchr = (chr_s){ .text = " ", .font = hfont };
    cchr = (chr_s){ .text = " ", .font = cfont };

    win_get_dims(*win, dims, &windims);

    dst = (rect_s){
        .x = windims.x + windims.w - 1, .y = windims.y, .w = 1, .h = windims.h - 1
    };
    display_fill(wm->disp, &dst, &vchr);

    dst = (rect_s){
        .x = windims.x + windims.w - 1, .y = windims.y + windims.h - 1, .w = 1, .h = 1
    };
    display_fill(wm->disp, &dst, &cchr);

    size_t statlen;

    statlen = MIN(windims.w - 1, vec_len(&((*win)->status)));

    dst = (rect_s){
        .x = windims.x + statlen, .y = windims.y + windims.h - 1, .w = windims.w - 1 - statlen, .h = 1
    };
    display_fill(wm->disp, &dst, &hchr);

    dst = (rect_s){
        .x = windims.x, .y = windims.y + windims.h - 1, .w = statlen, .h = 1
    };
    display_set(wm->disp, &dst, vec_get(&((*win)->status), 0));

    pthread_mutex_unlock(&(wm->mtx));

    return 0;
}

int wm_stamp_disp(wm_s *wm, int ind, rect_s *rect)
{
    win_s **win;
    int rtn;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);

    if (!win || !(*win))
    {
        rtn = -1;
    }
    else
    {
        win_stamp(*win, wm->disp, rect);
        rtn = 0;
    }

    pthread_mutex_unlock(&(wm->mtx));

    return rtn;
}

int wm_stamp_disp_floatwins(wm_s *wm)
{
    WITH_MTX(&(wm->mtx))
    {
        VEC_FOREACH(&(wm->floatwins), floatwin_s **, fwptr)
        {
            if (*fwptr)
                wm_floatwin_stamp(*fwptr, wm->disp);
        }
    }

    return 0;
}

int wm_stamp_disp_all(wm_s *wm)
{
    int ind, len;

    pthread_mutex_lock(&(wm->mtx));
    len = vec_len(&(wm->windows));
    pthread_mutex_unlock(&(wm->mtx));

    for (ind = 0; ind < len; ++ind)
    {
        wm_stamp_disp(wm, ind, NULL);
        wm_add_borders(wm, ind);
    }

    wm_stamp_disp_floatwins(wm);

    return 0;
}

int wm_adj(wm_s *wm, int ind, float adj)
{
    win_s *win, **winptr;
    int rtn;

    pthread_mutex_lock(&(wm->mtx));
    winptr = vec_get(&(wm->windows), ind);

    if (!winptr || !(*winptr))
    {
        rtn = -1;
    }
    else
    {
        win = *winptr;

        if (adj < 0.0) adj = 0.0;
        if (adj > 1.0) adj = 1.0;

        if (win->parent)
        {
            win->parent->adj = adj;
            rtn = 0;
        }
        else
        {
            rtn = -1;
        }
    }

    pthread_mutex_unlock(&(wm->mtx));

    win_resize(wm->rootwin, &(wm->dims));
    wm_stamp_disp_all(wm);

    return rtn;
}

int wm_handle_buf_update(wm_s *wm, int ind, buf_s *buf, long startln, long endln)
{
    win_s **win;
    int rtn;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);

    if (!win || !(*win))
    {
        rtn = -1;
    }
    else
    {
        long ln;
        framebuf_s *fb;
        fb = &((*win)->fbuf);

        for (ln = startln; ln <= endln; ++ln)
        {
            if (ln < buf->scry)
            {
                ln = buf->scry - 1;
            }
            else if (ln > buf->scry + fb->h)
            {
                break;
            }

            wm_update_s update;
            memset(&(update), 0, sizeof(update));
            update.ind = ind;
            update.buf = buf;
            update.ln  = ln;

            wm_updater_queue(&(wm->updater), &update);
        }

        rtn = 0;
    }

    pthread_mutex_unlock(&(wm->mtx));


    return rtn;
}

int wm_set_win(wm_s *wm, int ind, rect_s *rect, chr_s *mem)
{
    win_s **win;
    int rtn;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);

    if (!win || !(*win))
    {
        rtn = -1;
    }
    else
    {
        framebuf_s *fb;
        fb = &((*win)->fbuf);
        framebuf_set(fb, rect, mem);
        rtn = 0;
    }

    wm_stamp_disp(wm, ind, rect);
    wm_stamp_disp_floatwins(wm);
    pthread_mutex_unlock(&(wm->mtx));

    return rtn;
}

int wm_fill_win(wm_s *wm, int ind, rect_s *rect, chr_s *mem)
{
    win_s **win;
    int rtn;

    pthread_mutex_lock(&(wm->mtx));
    win = vec_get(&(wm->windows), ind);

    if (!win || !(*win))
    {
        rtn = -1;
    }
    else
    {
        framebuf_s *fb;
        fb = &((*win)->fbuf);
        framebuf_fill(fb, rect, mem);
        rtn = 0;
    }

    wm_stamp_disp(wm, ind, rect);
    wm_stamp_disp_floatwins(wm);
    pthread_mutex_unlock(&(wm->mtx));

    return rtn;
}
