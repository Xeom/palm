"""Commands and bindings for editing text."""

import ctypes
from lib       import lib
from bind.mode import Mode, CCommand, PyCommand
from core.bind import Bind
from util.keys import Keys

PyPromptMode = Mode("pyprompt")
PromptMode   = Mode("prompt")
EditMode     = Mode("edit")

indentincr = ctypes.c_int( 4)
indentdecr = ctypes.c_int(-4)

line_edit_cmds = [
    CCommand("Cut the cursor to a register", "Y",
        lib.cmd_cut_to_reg, shortcut=Keys.ctrl("x"), multicursor=-1),

    PyCommand("Insert character", "i", "single_pyprompt_param('ins(\"', '\")')"),

    CCommand("Paste the first register", "p",
        lib.cmd_paste_reg, shortcut=Keys.ctrl("p"), multicursor=True),
    CCommand("Pop and paste the first register", "P",
        lib.cmd_poppaste_reg, shortcut=Keys.ctrl("o"), multicursor=True),

    CCommand("Delete character",    "del",
        lib.cmd_del, shortcut=Keys.delete, multicursor=True),

    CCommand("Undo", "u", lib.cmd_undo),
    CCommand("Redo", "U", lib.cmd_redo),

    PyCommand("Autocomplete Path", "ap", "PathAutoCompleter(buf).open_menu()")
]

PromptMode.add_cmds(line_edit_cmds)
PyPromptMode.add_cmds(line_edit_cmds)
EditMode.add_cmds(line_edit_cmds)

prompt_cmds = [
    PyCommand("Run command",         "run",
        "buf.enter()", shortcut=Keys.enter),
    PyCommand("Backspace character", "back",
        "buf.backspace()", shortcut=Keys.backspace),
]

PromptMode.add_cmds(prompt_cmds)
PyPromptMode.add_cmds(prompt_cmds)

PyPromptMode.add_cmds([
    PyCommand("Next scrollback",     "cN", "buf.next()", shortcut=Keys.ctrl("n")),
    PyCommand("Previous scrollback", "cP", "buf.prev()", shortcut=Keys.ctrl("v")),
])

EditMode.add_cmds([
    CCommand("Increase line indent", "c>",
        lib.cmd_add_indent, indentincr, shortcut="\t", multicursor=True),
    CCommand("Decrease line indent", "c<",
        lib.cmd_add_indent, indentdecr, shortcut=Keys.shift("\t"), multicursor=True),

    CCommand("Insert new line", "nl",
        lib.cmd_enter, shortcut=Keys.enter, multicursor=True),
    CCommand("Insert new line before current line", "nhl",
        lib.cmd_preline_enter, shortcut=Keys.esc(Keys.enter), multicursor=True),

    CCommand("Backspace character", "back",
        lib.cmd_backspace, shortcut=Keys.backspace, multicursor=True),
])

PasteMode = Mode("paste", nocmds=True)

PasteMode.add_bind(Bind(None, cbind=lib.cmd_ins, multicursor=True))
PasteMode.add_bind(Bind(Keys.pasteend, pybind="buf.bind.prev_mode()"))

EditMode.add_bind(Bind(None, cbind=lib.cmd_ins, multicursor=True))
EditMode.add_bind(Bind(Keys.pastestart, pybind="buf.bind.set_mode('paste')"))

PromptMode.add_bind(Bind(None, cbind=lib.cmd_ins, multicursor=True))
PromptMode.add_bind(Bind(Keys.pastestart, pybind="buf.bind.set_mode('paste')"))

PyPromptMode.add_bind(Bind(None, cbind=lib.cmd_ins, multicursor=True))
PyPromptMode.add_bind(Bind(Keys.pastestart, pybind="buf.bind.set_mode('paste')"))
PyPromptMode.add_bind(Bind("\t", pybind="PathAutoCompleter(buf).open_menu()"))

Mode.add_default_cmds([
    PyCommand("Select editing mode", "me", "buf.bind.set_mode('edit')")
])


