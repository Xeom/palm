#if !defined(EDIT_INDENT_H)
# define EDIT_INDENT_H
# include "edit/edit.h"

void edit_rstrip(edit_info_s *einfo, vec_s *chrs);

col_t edit_skip_whitespace(edit_info_s *einfo, vec_s *chrs);

indent_t edit_get_indent(edit_info_s *einfo, vec_s *chrs);

col_t edit_set_indent(edit_info_s *einfo, vec_s *chrs, indent_t indent);

col_t edit_pad_to_indent(edit_info_s *einfo, vec_s *chrs, indent_t indent);

void edit_shift_indent(edit_info_s *einfo, int lvl);

#endif
