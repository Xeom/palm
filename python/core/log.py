import libtypes
import inspect
import ctypes
from lib import lib, enums
from .newstruct import CStructure
from util.encode import bytify

from server.globals import add_global

@add_global()
class Log(CStructure):
    class_name = "log_s"

    init_funct = lib.log_init
    kill_funct = lib.log_kill

    def to_file(self, f, fonts=False, ts=None):
        if hasattr(f, "fileno"):
            f = f.fileno()

        if ts is not None:
            ts = bytify(ts)

        lib.log_to_file(self.ptr, f, fonts, ts)

    def forget_file(self, f):
        if hasattr(f, "fileno"):
            f = f.fileno()

        if lib.log_forget_file(self.ptr, f) == -1:
            raise Exception("File was not in log")

    def lvl_to_int(self, lvl):
        if isinstance(lvl, int):
            return lvl

        return getattr(libtypes.log_lvl_t, lvl)

    def typ_to_int(self, typ):
        if isinstance(typ, int):
            return typ

        return getattr(libtypes.log_type_t, typ)

    def fmt(self, lvl, typ, fmt, *args, caller=None):
        lvl = enums.log_lvl_t(lvl)
        typ = enums.log_type_t(typ)

        try:
            if caller is None:
                caller = inspect.getframeinfo(inspect.stack()[1][0])
            filename = caller.filename.encode("utf-8")
            lineno   = caller.lineno
        except:
            filename = b"???"
            lineno   = -1

        if isinstance(fmt, str):
            fmt = fmt.encode("utf-8")

        if not isinstance(fmt, bytes):
            raise ValueError("Format for logging must be a string or bytes")

        args = list(args)
        for ind in range(len(args)):
            if isinstance(args[ind], str):
                args[ind] = args[ind].encode("utf-8")

        if len(args) != fmt.count(b"%") - fmt.count(b"%%"):
            raise ValueError("Not enough arguments to satisfy format")

        lib.log_fmt(self.ptr, lvl, typ, filename, lineno, fmt, *args)

    def str(self, lvl, typ, string, caller=None):
        lvl = enums.log_lvl_t(lvl)
        typ = enums.log_type_t(typ)

        try:
            if caller is None:
                caller = inspect.getframeinfo(inspect.stack()[1][0])
            filename = caller.filename.encode("utf-8")
            lineno   = caller.lineno
        except:
            filename = b"???"
            lineno   = -1

        if isinstance(string, str):
            string = string.encode("utf-8")

        if not isinstance(string, bytes):
            raise ValueError("String for logging must be a string or bytes")

        lib.log_str(self.ptr, lvl, typ, filename, lineno, string)

