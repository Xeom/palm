#if !defined(IO_PTY_H)
# define IO_PTY_H
# include <sys/types.h>
# include <stdbool.h>
# include "container/vec.h"
# include "types.h"

struct pty
{
    int   pfd;
    pid_t pid;
    bool  alive;
};

int pty_init(pty_s *pty, int inp, int out, char * const cmd[]);

void pty_kill(pty_s *pty);

void pty_forward_data(pty_s *pty, int inp, int out);

int pty_return_status(pty_s *pty);

#endif
