import libtypes
import os
from lib import lib
from .structure import Structure
from typing import Union, IO, AnyStr, Optional

class Input(Structure):
    typ        = libtypes.input_s
    init_funct = lib.input_init
    kill_funct = lib.input_kill

    def add_file(self, f: Union[int, IO[AnyStr]]):
        if isinstance(f, int):
            fd = f
        else:
            fd = f.fileno()

        lib.input_add_fd(self.ptr, fd)

    def init(self, *args):
        super().init(*args)

        self.pipe = os.pipe()

from .buffer import Buffer
