#if !defined(WM_FLOATWIN_H)
# define WM_FLOATWIN_H
# include "types.h"
# include "display/framebuf.h"
# include "display/display.h"
/**
 * A file containing functions to implement floating windows
 * in the window manager.
 *
 * Like normal windows, the floating windows are managed by index.
 * The function wm_add_floatwin() can be used to obtain an index
 * for a new floating window, which can then be used to manipulate
 * the floating window, rather than accessing it directly.
 *
 */

/** A structure representing a floating window */
struct floatwin
{
    framebuf_s fbuf; /**< A framebuf of window contents.     */
    rect_s rect;     /**< A rect specifying window placement */
};

/**
 * Initialize a floating window.
 *
 * Should not be used with floating windows managed by a window manager!
 *
 * @param fw A pointer to the floating window.
 *
 */
void wm_floatwin_init(floatwin_s *fw);

/**
 * Free resources associated with a floating window.
 *
 * Should not be used with floating windows managed by a window manager!
 *
 * @param fw A pointer to the floating window.
 *
 */
void wm_floatwin_kill(floatwin_s *fw);

/**
 * Resize a floating window.
 *
 * Should not be used with floating windows managed by a window manager!
 *
 * @param fw   A pointer to the floating window.
 * @param dims A pointer to a rect_s structure containing the new width,
 *             height, and position of the window.
 *
 */
int wm_floatwin_resize(floatwin_s *fw, rect_s *dims);

int wm_floatwin_set(floatwin_s *fw, rect_s *rect, chr_s *mem);

int wm_floatwin_fill(floatwin_s *fw, rect_s *rect, chr_s *mem);

void wm_floatwin_stamp(floatwin_s *fw, display_s *disp);

int wm_add_floatwin(wm_s *wm);

int wm_rem_floatwin(wm_s *wm, int ind);

int wm_resize_floatwin(wm_s *wm, int ind, rect_s *dims);

int wm_set_floatwin(wm_s *wm, int ind, rect_s *rect, chr_s *mem);

int wm_fill_floatwin(wm_s *wm, int ind, rect_s *rect, chr_s *mem);

#endif
