from buffers.edit import EditBuffer
from server.server import add_global

@add_global()
class NotesBuffer(EditBuffer):
    notesnumber = 1
    def init(self, *args, **kwargs):
        super().init(*args, **kwargs)
        lines = (
            "{{title}}NOTES BUFFER {0}\n\n"
            "  Hello,\n\n"
            "    I am {{emph}}NotesBuffer{{default}} number {{emph}}{0}{{default}}. You can use\n"
            "    me to take notes!\n\n"
            "  Thank you for your time. {{emph}}:)\n"
        ).format(NotesBuffer.notesnumber).split("\n")
        self.ins(0, lines, ["UTF8", "FNT", "NL"])
        NotesBuffer.notesnumber += 1
