#if !defined(CMDLANG_CMD_H)
# define CMDLANG_CMD_H
# include "types.h"

# include <stddef.h>
# include <stdbool.h>

# include "container/vec.h"
# include "help/help.h"

# include "cmdlang/lib.h"

typedef enum
{
    CMD_NONE    = 0,
    CMD_ADVERB  = 1,
    CMD_VERB    = 2,
    CMD_NOUN    = 4,
    CMD_ALIAS   = 8
} cmd_typ_t;

struct cmd_adverb_info
{
    int (*start_cb)(cmd_ctx_s *ctx, cmdlang_obj_s *args, size_t nargs, void **user);
    int (*iter_cb)(cmd_ctx_s *ctx, cmdlang_obj_s *args, size_t nargs, void *user, bool *cont);
    int (*end_cb)(cmd_ctx_s *ctx, cmdlang_obj_s *args, size_t nargs, void *user);
};

struct cmd_verb_info
{
    int (*cb)(cmd_ctx_s *ctx, cmdlang_obj_s *args, size_t nargs);
};

struct cmd_noun_info
{
    int (*cb)(cmd_ctx_s *ctx, cmdlang_obj_s *args, size_t nargs, cmdlang_obj_s *res);
};

struct cmd_alias_info
{
    vec_s toks;
    cmd_lib_s *lib;
};

struct cmd_arg_spec
{
    bool optional;
    cmd_typ_t typemask;
};

struct cmd
{
    help_s help;
    cmd_typ_t typ;

    vec_s argspecs;

    union
    {
        cmd_adverb_info_s adverb;
        cmd_noun_info_s   noun;
        cmd_verb_info_s   verb;
        cmd_alias_info_s  alias;
    } info;
};

int cmd_init(cmd_s *cmd, const char *name, const char *help) DONT_IGNORE;
void cmd_kill(cmd_s *cmd);

int cmd_add_argument(cmd_s *cmd, const char *name, const char *help, cmd_arg_spec_s *spec) DONT_IGNORE;

int cmd_set_verb(cmd_s *cmd, cmd_verb_info_s *info) DONT_IGNORE;
int cmd_set_adverb(cmd_s *cmd, cmd_adverb_info_s *info) DONT_IGNORE;
int cmd_set_noun(cmd_s *cmd, cmd_noun_info_s *info) DONT_IGNORE;
int cmd_set_alias(cmd_s *cmd, cmd_alias_info_s *info) DONT_IGNORE;

int cmd_set_alias_from_str(
    cmd_s *cmd, const char *str, errmsg_t errmsg
) DONT_IGNORE;
int cmd_alias_info_init(
    cmd_alias_info_s *info, cmd_lib_s *lib
);
int cmd_alias_info_from_str(
    cmd_alias_info_s *info, const char *str, errmsg_t errmsg
);
void cmd_alias_info_kill(cmd_alias_info_s *info);

int cmd_is_verb(cmd_s *cmd, cmd_lib_s *lib, bool *verb) DONT_IGNORE;

#endif
