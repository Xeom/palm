#include "hl/token.h"
#include "match/match.h"
#include "log/log.h"

void hl_tok_init(hl_tok_s *tok, char *name)
{
    memset(tok, 0, sizeof(hl_tok_s));
    tok->type = HL_TOK_NOTYPE;
    tok->group = 0;

    vec_init(&(tok->name), sizeof(char));
    vec_str(&(tok->name), name);
    vec_app(&(tok->name), NULL);
}

void hl_tok_kill(hl_tok_s *tok)
{
    vec_kill(&(tok->name));

    switch (tok->type)
    {
    case HL_TOK_REGEX:
        match_kill(&(tok->match));
        break;
    }
}

int hl_tok_set_regex(hl_tok_s *tok, const char *regex, bool ext)
{
    match_attr_t attrs;

    if (tok->type != HL_TOK_NOTYPE)
    {
        LOG(INFO, ERR, "hl_tok cannot be of two types");
        return -1;
    }

    attrs = MATCH_REGEX;

    if (ext)
        attrs |= MATCH_VERBOSE;

    if (match_init(&(tok->match), attrs, regex) == -1)
        return -1;

    tok->type = HL_TOK_REGEX;
    tok->match.grp = tok->group;

    return 0;
}

int hl_tok_verify(hl_tok_s *tok)
{
    if (tok->type == HL_TOK_NOTYPE)
    {
        LOG(INFO, ERR, "hl_tok failed to verify.");
        return -1;
    }
    else
    {
        return 0;
    }
}

bool hl_tok_match(hl_tok_s *tok, vec_s *str, size_t *off, size_t *start, size_t *end)
{
    bool rtn;

    rtn = false;
    if (tok->type == HL_TOK_REGEX)
    {
        match_result_s res;
        switch (match_check(&(tok->match), str, *off, &res))
        {
        case 0: break;
        case 1:
            if (*off >= res.end)
                *off = *off + 1;
            else
                *off = res.end;
            rtn = true;

            *start = res.start;
            *end   = res.end;
            break;
        case -1:
            LOG(ALERT, ERR, "Error while trying to match hl_tok");
            break;
        }
    }

    return rtn;
}

int hl_tok_load_attrs(hl_tok_s *tok, yaml_obj_s *obj, hl_ctx_s *ctx)
{
    const char *regstr, *extstr, *grpstr;

    regstr = yaml_obj_get_val(obj, (const char *[]){ "regex",       NULL }, NULL);
    extstr = yaml_obj_get_val(obj, (const char *[]){ "regex-ext",   NULL }, NULL);
    grpstr = yaml_obj_get_val(obj, (const char *[]){ "group",       NULL }, NULL);

    hl_action_load_info(&(tok->defaults), obj, ctx);

    if (regstr)
        hl_tok_set_regex(tok, regstr, false);
    if (extstr)
        hl_tok_set_regex(tok, extstr, true);
    if (grpstr)
    {
        tok->group = atoi(grpstr);
        tok->match.grp = tok->group;
    }

    if (hl_tok_verify(tok) == -1)
    {
        LOG(NOTICE, ERR, "Incomplete/invalid token");
        return -1;
    }

    return 0;
}
