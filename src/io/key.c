#include <ctype.h>

#include "log/log.h"
#include "editor.h"

#include "io/key.h"

#define KEY_MAX_MAPPING_LEN 32
#define KEY_PARSER_TIMEOUT 0.05

typedef struct key_mapping key_mapping_s;

struct key_mapping
{
    vec_s keys;
    bool complete;
};

static const char *key_button_names[] =
{
    [KEY_ESC]    = "Esc",
    [KEY_UP]     = "Up",
    [KEY_DOWN]   = "Down",
    [KEY_LEFT]   = "Left",
    [KEY_RIGHT]  = "Right",
    [KEY_ENTER]  = "Enter",
    [KEY_BACKSP] = "Backsp",
    [KEY_DEL]    = "Del",
    [KEY_INS]    = "Ins",
    [KEY_HOME]   = "Home",
    [KEY_END]    = "End",
    [KEY_PGUP]   = "PgUp",
    [KEY_PGDN]   = "PgDn",
    [KEY_TAB]    = "Tab",
    [KEY_SPASTE] = "SPaste",
    [KEY_EPASTE] = "EPaste",
    [KEY_FUNCT]  = "Funct",
    [KEY_CHAR]   = "Char",
    [KEY_NUMPAD] = "Numpad"
};

static table_s mappings;

static int key_parser_emit(key_parser_s *parser, key_s *key) DONT_IGNORE;
static int key_parser_handle_byte(key_parser_s *parser, uint8_t b) DONT_IGNORE;
static int key_parser_handle_timeout(key_parser_s *parser) DONT_IGNORE;
static int key_parser_handle_invalid_symbol(key_parser_s *parser) DONT_IGNORE;
static int key_parser_handle_symbol(key_parser_s *parser) DONT_IGNORE;

static int key_parser_launch_timer(key_parser_s *parser) DONT_IGNORE;
static int key_parser_cancel_timer(key_parser_s *parser) DONT_IGNORE;

static void key_parser_timeout_cb(editor_s *editor, event_s *event, void *prm);
static void key_parser_keybyte_cb(editor_s *edtior, event_s *event, void *prm);

/*** KEY MAPPINGS ***/

/* Uncomment to enable logging, useful for low-level key debug. */
// #define LOG_KEYS

static int key_mapping_init(key_mapping_s *mapping)
{
    vec_init(&(mapping->keys),  sizeof(key_s));

    mapping->complete = true;

    return PALM_OK;
}

static void key_mapping_kill(key_mapping_s *mapping)
{
    vec_kill(&(mapping->keys));
}

static key_mapping_s *key_mapping_create(const char *bytes, size_t len)
{
    key_mapping_s *rtn;
    rtn = table_get(&mappings, bytes, len, NULL);

    if (!rtn)
    {
        table_set(&mappings, bytes, len, NULL, sizeof(key_mapping_s));
        rtn = table_get(&mappings, bytes, len, NULL);

        if (!rtn) return NULL;

        if (key_mapping_init(rtn) == PALM_ERR)
            return NULL;
    }

    return rtn;
}

static key_mapping_s *key_mapping_find(const uint8_t *bytes, size_t len)
{
    return table_get(&mappings, bytes, len, NULL);
}

/*** KEYS ***/
static int key_cmpcb(const void *a, const void *b)
{
    return memcmp(a, b, sizeof(key_s));
}

void key_canonicalise(key_s *key)
{
    vec_s synonyms;

    /* TODO: CACHE */

    vec_init(&(synonyms), sizeof(key_s));

    TABLE_FOREACH(&mappings, chain)
    {
        key_mapping_s *map;
        map = table_chain_val(chain, NULL);

        if (vec_contains(&(map->keys), key))
            vec_cpy(&synonyms, &(map->keys));
    }

    vec_qsort(&synonyms, key_cmpcb);

    if (vec_len(&(synonyms)))
    {
        memcpy(key, vec_get(&(synonyms), 0), sizeof(key_s));
    }

    vec_kill(&(synonyms));
}

int key_get_name(key_s *key, char *buf, size_t buflen)
{
    const char *root;
    char suffix[64];
    char prefix[64];
    int res;

    if (key->button < DIM(key_button_names) &&
        key->button >= 0 &&
        key_button_names[key->button])
    {
        root = key_button_names[key->button];
    }
    else
    {
        root = "Unknown";
    }

    if (key->button == KEY_CHAR)
    {
        if (isprint(key->val))
            snprintf(suffix, sizeof(suffix), "(%c)", key->val);
        else
            snprintf(suffix, sizeof(suffix), "(0x%02x)", key->val);
    }
    else if (key->button == KEY_FUNCT)
    {
        snprintf(suffix, sizeof(suffix), "(%d)", key->val);
    }
    else
    {
        suffix[0] = '\0';
    }

    snprintf(prefix, sizeof(prefix), "%s%s%s",
        (key->attrs & KEY_ATTR_SHIFT)  ? "Shift+"  : "",
        (key->attrs & KEY_ATTR_CTRL)   ? "Ctrl+"   : "",
        (key->attrs & KEY_ATTR_MOD)    ? "Mod+"    : ""
    );

    res = snprintf(buf, buflen, "%s%s%s", prefix, root, suffix);

    if (res >= (int)buflen || res < 0)
        return PALM_ERR;
    else
        return PALM_OK;
}

void key_to_string(key_s *key, vec_s *string)
{
    if ((key->button == KEY_CHAR || key->button == KEY_NUMPAD) && key->attrs == 0)
    {
        char c;
        c = key->val;
        vec_app(&string, &c);
    }
    else
    {
        char buf[256];
        key_get_name(key, buf, sizeof(buf));

        vec_fmt(&string, "<%s>", buf);
    }
}

/*** KEY SYSTEM ***/

void key_start(void)
{
    table_init(&mappings);
    LOG(INFO, SUCC, "Key system started.");
}

void key_end(void)
{
    TABLE_FOREACH(&mappings, chain)
    {
        key_mapping_s *mapping;
        mapping = table_chain_val(chain, NULL);

        key_mapping_kill(mapping);
    }

    table_kill(&mappings);
    LOG(INFO, SUCC, "Key system ended.");
}

int key_add_mapping(key_s *key, const char *bytes, size_t len)
{
    size_t part;
    key_mapping_s *map;
    for (part = 0; part < len; ++part)
    {
        map = key_mapping_create(bytes, part);

        if (!map) return PALM_ERR;

        map->complete = false;
    }

    map = key_mapping_create(bytes, len);

    if (!map) return PALM_ERR;
    if (!vec_app(&(map->keys), key)) return PALM_ERR;

    return PALM_OK;
}

/*** KEY PARSER ***/

int key_parser_init(key_parser_s *parser, editor_s *editor)
{
    event_spec_s spec;

    memset(parser, 0, sizeof(key_parser_s));

    parser->editor = editor;
    vec_init(&(parser->symbol), sizeof(char));

    input_event_spec(&(editor->input), &spec);

    event_listen(
        &(editor->eventctx), &spec,
        &(event_action_s){
            .cb = key_parser_keybyte_cb,
            .param = parser
        }
    );

    return PALM_OK;
}

void key_parser_kill(key_parser_s *parser)
{
    event_spec_s spec;
    vec_kill(&(parser->symbol));

    input_event_spec(&(parser->editor->input), &spec);
    event_unlisten(
        &(parser->editor->eventctx), &spec,
        &(event_action_s){
            .cb = key_parser_keybyte_cb,
            .param = parser,
        }
    );
}

static int key_parser_emit(key_parser_s *parser, key_s *key)
{
    event_s event;

    memset(&event, 0, sizeof(event));

    key_parser_event_spec(parser, &(event.spec));

#if defined(LOG_KEYS)
    char keyname[512];
    key_get_name(key, keyname, sizeof(keyname));
    LOG(ALERT, INFO, "KEY: %s(%d)", keyname, key->button);
#endif

    memcpy(&(event.key), key, sizeof(key_s));
    key_canonicalise(&(event.key));

    TRY(event_push(&(parser->editor->eventctx), &event));

    return PALM_OK;
}

static int key_parser_handle_symbol(key_parser_s *parser)
{
    key_mapping_s *resmap; /* A complete symbol mapping. */

    resmap = key_mapping_find(
        vec_get(&(parser->symbol), 0), vec_len(&(parser->symbol))
    );

    if (!resmap)
    {
        if (parser->attrs & KEY_PARSER_IMPATIENT)
        {
            vec_clr(&(parser->symbol));
            return PALM_OK;
        }
        else
        {
            return key_parser_handle_invalid_symbol(parser);
        }
    }

    if (vec_len(&(resmap->keys)) == 0)
    {
        return PALM_OK;
    }
    else if (resmap->complete || (parser->attrs & KEY_PARSER_IMPATIENT))
    {
        TRY(key_parser_emit(parser, vec_get(&(resmap->keys), 0)));
        vec_clr(&(parser->symbol));
    }

    return PALM_OK;
}

static int key_parser_handle_timeout(key_parser_s *parser)
{
    int rtn;

    SET_BITS(parser->attrs, KEY_PARSER_IMPATIENT, true);
    rtn = key_parser_handle_symbol(parser);
    SET_BITS(parser->attrs, KEY_PARSER_IMPATIENT, false);

#if defined(LOG_KEYS)
    LOG(ALERT, INFO, "TIMEOUT");
#endif

    return rtn;
}

static int key_parser_handle_invalid_symbol(key_parser_s *parser)
{
    int rtn;
    vec_s tmp;

    vec_init(&tmp, sizeof(uint8_t));

    vec_cpy(&tmp, &(parser->symbol));
    vec_clr(&(parser->symbol));

    rtn = PALM_OK;

#if defined(LOG_KEYS)
    LOG(ALERT, INFO, "INVALID SYMBOL");
#endif

    SET_BITS(parser->attrs, KEY_PARSER_IMPATIENT, true);
    VEC_FOREACH(&tmp, uint8_t *, iter)
    {
        if (key_parser_handle_byte(parser, *iter) == PALM_ERR)
        {
            rtn = PALM_ERR;
            break;
        }
    }
    SET_BITS(parser->attrs, KEY_PARSER_IMPATIENT, false);

    vec_kill(&tmp);
    return rtn;
}

/*** Handling timeout ***/

/* Timeout is when an incomplete symbol has been typed, but a certain time *
 * has passed since the last character was typed. The keys that have been  *
 * typed are therefore assumed to be individual.                           */

static int key_parser_launch_timer(key_parser_s *parser)
{
    event_spec_s eventspec;

    if (parser->attrs & KEY_PARSER_TIMER_RUNNING)
        TRY(key_parser_cancel_timer(parser));

    memset(&(parser->timer), 0, sizeof(timer_s));
    parser->timer.delay = KEY_PARSER_TIMEOUT;

    timer_event_spec(&(parser->timer), &eventspec);

    TRY(event_listen(
        &(parser->editor->eventctx), &eventspec,
        &(event_action_s) {
            .cb = key_parser_timeout_cb,
            .param = parser,
        }
    ));

    timer_add(&(parser->editor->timerctx), &(parser->timer));
    SET_BITS(parser->attrs, KEY_PARSER_TIMER_RUNNING, true);

    return PALM_OK;
}

static int key_parser_cancel_timer(key_parser_s *parser)
{
    if (parser->attrs & KEY_PARSER_TIMER_RUNNING)
    {
        timer_rem(&(parser->editor->timerctx), &(parser->timer));
        SET_BITS(parser->attrs, KEY_PARSER_TIMER_RUNNING, false);
    }

    return PALM_OK;
}

static void key_parser_timeout_cb(editor_s *editor, event_s *event, void *prm)
{
    key_parser_s *parser;
    parser = (key_parser_s *)prm;
    SET_BITS(parser->attrs, KEY_PARSER_TIMER_RUNNING, false);

    if (key_parser_handle_timeout(parser) == PALM_ERR)
        LOG(ALERT, INFO, "Error handling parser timeout.");
}

static int key_parser_handle_byte(key_parser_s *parser, uint8_t c)
{
#if defined(LOG_KEYS)
    LOG(ALERT, INFO, "BYTE %d", c);
#endif

    if (vec_app(&(parser->symbol), &c) == NULL)
        return PALM_ERR;

    TRY(key_parser_handle_symbol(parser));

    return PALM_OK;
}

static void key_parser_keybyte_cb(editor_s *edtior, event_s *event, void *param)
{
    key_parser_s *parser;
    parser = (key_parser_s *)param;

    if (key_parser_handle_byte(parser, event->chr) == PALM_ERR)
    {
        LOG(ALERT, ERR, "Error handling character in key parser.");
        return;
    }

    if (vec_len(&(parser->symbol)))
    {
        if (key_parser_launch_timer(parser) == PALM_ERR)
        {
            LOG(ALERT, ERR, "Error launching timeout timer in key parser.");
            return;
        }
    }
    else
    {
        if (key_parser_cancel_timer(parser) == PALM_ERR)
        {
            LOG(ALERT, ERR, "Error cancelling timeout timer in key parser.");
            return;
        }
    }
}

void key_parser_event_spec(key_parser_s *parser, event_spec_s *spec)
{
    memset(spec, 0, sizeof(event_spec_s));
    spec->typ = EVENT_PARSED_KEY;
    spec->ptr = parser;
}
