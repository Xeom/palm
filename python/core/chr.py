import libtypes
import ctypes

from typing import Union, TYPE_CHECKING

from lib            import lib
from util.encode    import strify, bytify
from server.globals import add_global

from .newstruct import SimpleCStructure
from .vec       import Vec

if TYPE_CHECKING:
    from .buffer  import Buffer

@add_global()
class Chr(SimpleCStructure):
    class_name = "chr_s"

    init_funct = None
    kill_funct = None

    text_len   = 5

    def expanded_width(self, pos: int, buf: "Buffer") -> int:
        return lib.chr_expanded_width(self.ptr, pos, buf["chrconf"].byref())

    def set_font(self, font: Union[int, str, bytes]):
        if isinstance(font, (str, bytes)):
            font = font_by_name(font)

        self.font = font

    def set_text(self, text: Union[str, bytes]):
        text = bytify(text)
        text = text.ljust(self.text_len, b"\0")[:self.text_len]

        self.ptr.contents.text = text

    def get_text(self) -> bytes:
        return self.ptr.contents.text

    @classmethod
    def new(cls, text, font=0):
        rtn = cls()
        rtn.set_font(font)
        rtn.set_text(text)

        return rtn

    def __str__(self):
        return self.ptr.contents.text.decode("utf-8")

    def __bytes__(self):
        return self.ptr.contents.text

