#include "text/chr.h"
#include "container/vec.h"
#include "text/font.h"
#include <sys/types.h>
#include <unistd.h>
#include "buf/buf.h"
#include "text/file.h"
#include "edit/ins.h"
#include "edit/enter.h"
#include "log/log.h"
#include "io/io.h"
#include "util/string.h"
#include "editor.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>

int file_name_len = FILE_NAME_LEN;

static void file_decode_byte(file_s *file, int byte);
static void file_decode_bytes(file_s *file);
static void file_encode_line(file_s *file);

static void file_read_cb(editor_s *editor, event_s *event, void *param);
static void file_write_cb(editor_s *editor, event_s *event, void *param);

int file_init(file_s *file, poll_ctx_s *pollctx)
{
    file->pollctx = pollctx;

    if (!safe_strncpy(file->name, "<Unnamed>", FILE_NAME_LEN))
        return -1;

    file->writing = false;
    file->reading = false;

    file->attrs = DECODER_UTF8 | DECODER_STRIPNL | DECODER_NL;
    decoder_init(&(file->decoder), file->attrs);
    encoder_init(&(file->encoder), file->attrs);

    file->startln    = 0;
    file->endln      = LONG_MAX;
    file->currln     = 0;

    vec_init(&(file->line),  sizeof(chr_s));
    vec_init(&(file->bytes), sizeof(char));

    return 0;
}

static void file_stop(file_s *file)
{
    if (file->writing)
    {
        file->writing = false;
        poll_rem(file->pollctx, &(file->finfo));

        if (close(file->fd) == -1)
            LOG(ALERT, ERR, "Error closing '%s': %s", file->name, strerror(errno));
        else if (file->err)
            LOG(ALERT, ERR, "Error writing '%s'", file->name);
        else if (!file->done)
            LOG(ALERT, INFO, "Interrupted writing '%s'", file->name);
        else
            LOG(ALERT, SUCC, "Wrote '%s': %ldL, %ldC", file->name, file->lcount, file->ccount);

        file->done = true;

        if (file->cb)
            (file->cb)(file, file->param);
    }

    if (file->reading)
    {
        file->reading = false;
        poll_rem(file->pollctx, &(file->finfo));

        if (close(file->fd) == -1)
            LOG(ALERT, ERR, "Error closing '%s': %s", file->name, strerror(errno));
        else if (file->err)
            LOG(ALERT, ERR, "Error reading '%s'", file->name);
        else if (!file->done)
            LOG(ALERT, INFO, "Interrupted reading '%s'", file->name);
        else
            LOG(ALERT, SUCC, "Read '%s': %ldL, %ldC", file->name, file->lcount, file->ccount);

        file->done = true;

        if (file->cb)
            (file->cb)(file, file->param);
    }
}

void file_kill(file_s *file)
{
    file_stop(file);

    decoder_kill(&(file->decoder));

    vec_kill(&(file->line));
    vec_kill(&(file->bytes));
}

int file_set_fd(file_s *file, int fd, const char *name)
{
    file_stop(file);

    file->fd = fd;

    if (!safe_strncpy(file->name, name, FILE_NAME_LEN))
        return -1;

    return 0;
}

void file_set_attrs(file_s *file, decoder_attr_t attrs)
{
    file_stop(file);

    file->attrs = attrs;

    decoder_kill(&(file->decoder));

    decoder_init(&(file->decoder), file->attrs);
    encoder_init(&(file->encoder), file->attrs);
}

void file_set_line_range(file_s *file, long start, long end)
{
    file_stop(file);

    file->startln = start;
    file->endln   = end;
}

static int file_listen_for_reads(file_s *file)
{
    event_spec_s spec;

    file->finfo = (io_file_info_s){
        .fd = file->fd,
        .readvec = &(file->bytes)
    };

    poll_event_spec(&(file->finfo), &spec);

    TRY(event_listen(
        file->pollctx->eventctx, &spec,
        &(event_action_s){
            .cb = file_read_cb,
            .param = file
        })
    );

    poll_add(file->pollctx, &(file->finfo));

    file->reading = true;

    return PALM_OK;
}

static int file_listen_for_writes(file_s *file)
{
    event_spec_s spec;

    file->finfo = (io_file_info_s){
        .fd = file->fd,
        .writevec = &(file->bytes),
        .awaitout = true /* We start with 0 bytes, so we explicitly wait. */
    };

    poll_event_spec(&(file->finfo), &spec);

    TRY(event_listen(
        file->pollctx->eventctx, &spec,
        &(event_action_s){
            .cb = file_write_cb,
            .param = file
        })
    );

    poll_add(file->pollctx, &(file->finfo));

    file->writing = true;

    return PALM_OK;
}

int file_read(file_s *file, file_rcb_t rcb, file_cb_t cb, void *cbparam)
{
    file_stop(file);

    file->param = cbparam;
    file->rcb   = rcb;
    file->cb    = cb;

    file->done    = false;
    file->err     = false;

    file->ccount  = 0;
    file->lcount  = 0;

    vec_clr(&(file->bytes));
    vec_clr(&(file->line));

    file->currln = file->startln;

    TRY(file_listen_for_reads(file));

    return PALM_OK;
}

int file_write(file_s *file, file_wcb_t wcb, file_cb_t cb, void *cbparam)
{
    file_stop(file);

    file->param = cbparam;
    file->wcb   = wcb;
    file->cb    = cb;

    file->done    = false;
    file->err     = false;

    file->ccount  = 0;
    file->lcount  = 0;

    vec_clr(&(file->bytes));
    vec_clr(&(file->line));

    file->currln = file->startln;

    TRY(file_listen_for_writes(file));

    return PALM_OK;
}

static void file_decode_byte(file_s *file, int byte)
{
    if (file->currln > file->endln)
        return;

    if (decoder_chr(&(file->decoder), byte, &(file->line)))
    {
        (file->rcb)(file, &(file->line), file->param, file->currln);
        vec_clr(&(file->line));
        file->currln += 1;
        file->lcount += 1;
    }
}

static void file_decode_bytes(file_s *file)
{
    VEC_FOREACH(&(file->bytes), char *, c)
    {
        if (file->currln > file->endln)
            break;

        file->ccount++;
        file_decode_byte(file, (int)*c);
    }
    vec_clr(&(file->bytes));
}

static void file_encode_line(file_s *file)
{
    bool ended;

    if (file->currln > file->endln)
    {
        file->done = true;
        return;
    }

    ended = false;

    (file->wcb)(file, &(file->line), file->param, file->currln, &ended);
    file->currln += 1;
    file->lcount += 1;

    VEC_FOREACH(&(file->line), chr_s *, chr)
    {
        encoder_chr(&(file->encoder), chr, &(file->bytes));
    }

    if (ended)
    {
        encoder_nl(&(file->encoder), &(file->bytes));
        file->done = true;
    }

    vec_clr(&(file->line));
}

static void file_read_cb(editor_s *editor, event_s *event, void *param)
{
    file_s *file;
    file = param;

    if (file->reading && !file->done && file->currln != file->endln)
    {
        if (file->finfo.status & IO_FILE_READABLE)
        {
            file_decode_bytes(file);
        }
    }

    if (file->finfo.status & (IO_FILE_EOF | IO_FILE_CLOSED))
    {
        file_decode_byte(file, -1);
        file->done = true;
    }

    if (file->done)
    {
        file_stop(file);
    }

}

static void file_write_cb(editor_s *editor, event_s *event, void *param)
{
    file_s *file;
    file = param;

    LOG(ALERT, INFO, "WCB");

    if (file->writing && !file->done && !file->err)
    {
        if (file->finfo.status & IO_FILE_WRITABLE)
        {
            while (vec_len(&(file->bytes)) == 0 && !file->done && !file->err)
            {
                file_encode_line(file);
                file->ccount += vec_len(&(file->bytes));
            }
        }
    }

    if (file->finfo.status & (IO_FILE_EOF | IO_FILE_CLOSED))
    {
        file->err = true;
        file->done = true;
    }

    if (file->done)
    {
        file_stop(file);
    }
}

void file_enable_undo_cb(file_s *file, void *arg)
{
    buf_s *buf;
    buf = arg;

    buf_undo_clear(buf);
    buf_undo_set_enable(buf, true);
}

void file_read_to_buf_cb(file_s *file, vec_s *line, void *arg, row_t ln)
{
    buf_s *buf;
    cur_s cur;
    buf = arg;

    buf_get_cur(buf, &cur);

    buf_ins_line(buf, ln, 1);
    buf_set_line(buf, ln, line);

 //   if (ln > 1)
   //     buf_shift(buf, &(pos_s){ .row = ln - 1 }, &(pos_s){ .row = ln });
}

void file_write_from_buf_cb(file_s *file, vec_s *line, void *arg, row_t ln, bool *ended)
{
    buf_s *buf;
    buf = arg;

    if (ln >= buf_len(buf))
    {
        *ended = true;
        return;
    }

    buf_get_line(buf, ln, line);
}

void file_read_to_cur_cb(file_s *file, vec_s *line, void *arg, row_t ln)
{
    buf_s *buf;
    buf = arg;

    if ((long)ln > file->startln)
        edit_enter(&(buf->einfo));

    edit_ins_vec(&(buf->einfo), line);
}

void file_read_to_buf_repl_cb(file_s *file, vec_s *line, void *arg, row_t ln)
{
    buf_s *buf;
    buf = arg;

    LOG(ALERT, INFO, "SET LINE: ln %lu, len %lu", ln, vec_len(line));
    buf_set_line(buf, ln, line);
}
