{title}Using the Python Prompt

{subtitle}Activating the prompt

  The 'x' command will open  an interactive python prompt in
  a new  window. You can press {md-code-inline}Esc{default},  {md-code-inline}x {default}in order  to run this
  command from  a normal  mode. Within  the prompt,  you can
  press {md-code-inline}Esc{default}, {md-code-inline}x {default}again, in order to close it.

  This opening  of the pyprompt  corresponds to  calling the
  function     {md-code-inline}open_buffer_pyprompt(){default}.      The     function
  {md-code-inline}single_pyprompt()  {default}is also  sometimes  called by  shortcut
  commands.  This opens  a single-use  prompt, which  closes
  after one command is executed,  and has text already typed
  both before and after the cursor. As an example, you could
  bind a  key to {md-code-inline}single_pyprompt("goto(", ")"){default}, in  order to
  make goto commands quicker.

{subtitle}Using the Prompt

  The prompt  behaves  mostly like a  normal  python prompt,
  with the  exception that  the text  that has  already been
  typed is  entirely editable, and that,  in order to  run a
  command,  you  must press  {md-code-inline}Enter  {default}twice. This  allows  for
  multiple commands  to be run in  one go. You can  also use
  the {md-code-inline}Ctrl-V {default}and {md-code-inline}Ctrl-N {default}keys to view command history.

{subtitle}Session Files

  For  each  session  of  Python  console,  certain  default
  variables are  defined.  These include -  {md-code-inline}buf{default},  the buffer
  that the session is associated with, as well as {md-code-inline}editor{default}, {md-code-inline}wm
  , and  {md-code-inline}finfo{default}, which  are all  associated with  the buffer.
  {md-code-inline}session  {default}is  also  added.  This  variable  refers  to  the
  ShellSession instance that the console is running.

  After these variables are added  to the global scope, some
  code  can be  executed  in this  scope,  to define  useful
  helper functions. This code  is stored in 'session files',
  which are  stored in the  {md-code-inline}python/session {default}directory,  or in
  the home  directory. The  files should  be short,  as they
  will be executed whenever a  python prompt is started, and
  should simply define helper functions.

{subtitle}Helper functions

  Here  are some  examples  of helper  functions defined  in
  Python prompt sessions.

{subsubtitle}Editing Helpers - {md-code-inline}session/edit.py

 {md-bullet} * {default} {md-code-inline}edit_file() {default}- Open a file for editing.

 {md-bullet} * {default} {md-code-inline}getl(){default}, {md-code-inline}setl() {default}- Set or get the current line's contents
      as a string.

 {md-bullet} * {default} {md-code-inline}insl() {default}- Insert some lines.

 {md-bullet} * {default} {md-code-inline}ins() {default}- Insert  some characters as if  they were pasted
      or typed.

 {md-bullet} * {default} {md-code-inline}cut(){default}, {md-code-inline}cpy() {default}- Cut or copy the selection to a string.

 {md-bullet} * {default} {md-code-inline}backspace(){default},  {md-code-inline}delete(){default},  {md-code-inline}enter()   {default}-  Run  the  actions
      corresponding to these keys.

{subsubtitle}Iterating Helpers - {md-code-inline}session/iter.py

 {md-bullet} * {default} {md-code-inline}iter_sel(){default}, {md-code-inline}iter_buf()  {default}- Iterate  across the  lines in
      the buffer or  selection, selecting each  one with the
      cursor before yielding the line number.

 {md-bullet} * {default} {md-code-inline}sort_sel(){default}, {md-code-inline}sort_buf() {default}-  Sort the lines  in the buffer
      or selection by some key.

 {md-bullet} * {default} {md-code-inline}apply_sel(){default},  {md-code-inline}apply_buf() {default}-  Apply  a  function to  the
      contents of all the lines in the buffer or selection.

{subsubtitle}Navigation Helpers - {md-code-inline}session/nav.py

 {md-bullet} * {default} {md-code-inline}cur(){default},  {md-code-inline}pri(){default},  {md-code-inline}sec()   {default}-  Get  the   cursor  and  it's
      components,

 {md-bullet} * {default} {md-code-inline}row(){default}, {md-code-inline}col() {default}- Get or set the cursor row and column.

 {md-bullet} * {default} {md-code-inline}goto() {default}- Move the cursor to a location.

 {md-bullet} * {default} {md-code-inline}move()  {default}- Move  the  cursor  a number  of  places in  a
      direction.

 {md-bullet} * {default} {md-code-inline}home(){default}, {md-code-inline}end() {default}- Go to the start or end of the line.

 {md-bullet} * {default} {md-code-inline}swap() {default}- Swap the primary and secondary cursors.

 {md-bullet} * {default} {md-code-inline}snap() {default}- Snap the secondary cursor to the primary.

 {md-bullet} * {default} {md-code-inline}stick(){default},  {md-code-inline}cur_type()  {default}-  Change  the  cursor  type  and
      stickiness.

{subsubtitle}Session Helpers - {md-code-inline}session/session.py

 {md-bullet} * {default} {md-code-inline}print() print_expr() {default}- Print a value to the prompt.

 {md-bullet} * {default} {md-code-inline}sys()  {default}- Run  a command  and  display the  result in  a
      popup.

 {md-bullet} * {default} {md-code-inline}print_version() {default}- Print the Python version.

{subtitle}Examples

{subsubtitle}Capitalize Selection

   {md-section} : {md-code-blk}ins(cut().upper())

{subsubtitle}Indent selection

   {md-section} : {md-code-blk}apply_sel(lambda x:"    "+x)

  or, a faster approach, using  the functions for doing this
  specific job,  implemented in C.  As a bonus,  this method
  will respect  your  indentation settings -  using  tabs or
  spaces as appropriate.

   {md-section} : {md-code-blk}for ln in iter_sel():
   {md-section} : {md-code-blk}    buf.indent_set(ln, buf.indent_get(ln) + 4)

  or, the fastest  is to simply use the C  function bound to
  the tab key!  This will do everything in C  and save a lot
  of time!  To invoke the C  action associated with  the tab
  key  manually from  the  python console,  you  can do  the
  following, though it is clearly a hacky method.

   {md-section} : {md-code-blk}lib.default_add_indent(
   {md-section} : {md-code-blk}    Vec(ctypes.c_char).ptr, 
   {md-section} : {md-code-blk}    buf.ptr, 
   {md-section} : {md-code-blk}    ctypes.byref(ctypes.c_int(4)), 
   {md-section} : {md-code-blk}    ctypes.sizeof(ctypes.c_int)
   {md-section} : {md-code-blk})

{subsubtitle}Strip trailing whitespace

   {md-section} : {md-code-blk}apply_buf(str.rstrip)

{subsubtitle}Sort by length

   {md-section} : {md-code-blk}sort_sel(len)

{subsubtitle}Select current line.

   {md-section} : {md-code-blk}home()
   {md-section} : {md-code-blk}stick(True)
   {md-section} : {md-code-blk}end()

  or, using {md-code-inline}goto() {default}to move the cursor,

   {md-section} : {md-code-blk}stick(True)
   {md-section} : {md-code-blk}goto([row(), 0], [row(), buf.line_len(row())])
