#if !defined(MATCH_MATCH_H)
# define MATCH_MATCH_H

/* PCRE time! */
# define PCRE2_CODE_UNIT_WIDTH 8
# include <pcre2.h>

# include <stddef.h>
# include "types.h"
# include "container/vec.h"
# include <stdbool.h>

/**
 * @file  match.h
 * @brief Functions implementing the string matching system.
 *
 * The match system is used to match strings of bytes.
 *
 * It attempts to generalize regexes, normal text searches,
 * etc. with room to add things like searching for incorrectly
 * spelled words.
 *
 */

/**
 * Enum containing attributes for text matching patterns.
 */
typedef enum
{
    /**
     * When searching backwards, a valid match can end after the
     * inital offset, as long as it starts before the offset.
     *
     * e.g. if we start at the indicated matches, and search
     * backwards for cat, the matches would be as indicated.
     *
     * @code
     * WITH MATCH_BEYONDOFF SET       WITHOUT MATCH_BEYONDOFF SET
     * 'cat cat ' => 'cat cat '       'cat cat ' => 'cat cat '
     *       ^            ^^^               ^        ^^^
     * 'cat cat ' => 'cat cat '       'cat cat ' => 'cat cat '
     *      ^         ^^^                  ^         ^^^
     * 'cat cat ' => 'cat cat '       'cat cat ' => 'cat cat '
     *         ^          ^^^                 ^          ^^^
     * @endcode
     *
     */
    MATCH_BEYONDOFF  = 0x01,
     /**
      * The match is a PCRE 
      *
      */
    MATCH_REGEX      = 0x02,
    /**
     * If this is a PCRE, it is verbose.
     * Whitespace is ignored and it can contain comments.
     *
     */
    MATCH_VERBOSE    = 0x04,
    /**
     * This search pattern contains escape sequences, which are
     * processed before everything else. Note that when combined
     * with MATCH_REGEX, you will have to deal with double escape
     * sequences.
     *
     */
    MATCH_ESCAPES    = 0x08,
    /**
     * The pattern is case insensitive.
     *
     */
    MATCH_ANYCASE    = 0x10
} match_attr_t;

/**
 * Structure used to return match locations.
 *
 * Note that offsets are in *bytes*, so if you need to search in a buffer, you
 * need to use the buf_off_to_ind() and buf_ind_to_off() functions to get
 * the correct offsets (byte offsets) and indexes (chr_s indexes), otherwise
 * your code will not handle unicode correctly.
 *
 */
struct match_result
{
    size_t start;   /**< The offset of the first character of the match. */
    size_t end;     /**< The offset after the end of the match. */
    match_s *match; /**< A pointer to the matched @c match_s structure. */
};

/**
 * A structure used to contain a pattern for matching bytes.
 *
 */
struct match
{
    vec_s text;                  /**< The text of the matching pattern. */

    match_attr_t attrs;          /**< The match attributes. */
    int   grp;                   /**< For regexes, the group of the regex *
                                  *   to match. */

    pcre2_code       *regex;     /**< For regex matches, a compiled PCRE. */
    pcre2_match_data *matchdata; /**< For regex matches, a place to store *
                                  *   matchdata.                          */

    void *usrptr; /**< A slot to store a pointer to identify this pattern. */
    char  usrchr; /**< A slot to store a char to identify this pattern. */
};

/**
 * Initialize a @c match_s structure.
 *
 * @param match A pointer to the @c match_s structure.
 * @param attrs The attributes of the pattern.
 * @param text  A NULL-terminated string containing the pattern to match.
 *
 * e.g.
 * @code
 * match_init(m, MATCH_REGEX | MATCH_ANYCASE, "cat$")
 * @endcode
 * Match every line ending with 'cat', regardless of case.
 *
 * @code
 * match_init(m, MATCH_ESCAPE, "\\0");
 * @endcode
 * Match a null byte.
 *
 * @return -1 on error, 0 on success.
 *
 */
int match_init(match_s *match, match_attr_t attrs, const char *text);

/**
 * Release resources allocated by a @c match_s structure.
 *
 * This must be called on any @c match_s structure that has had match_init()
 * called on it.
 *
 * @param match A pointer to the @c match_s structure.
 *
 */
void match_kill(match_s *match);

/**
 * Check whether a match exists at a certain position in a string.
 *
 * @param match A pointer to the @c match_s structure to check for.
 * @param text  A vector of bytes containing the text to check.
 * @param off   The offset that the hypothetical match would start at.
 * @param res   If a match exists, information about it will be returned here.
 *
 * @return -1 on error, 0 on no match, 1 on match found.
 *
 */
int match_check(match_s *match, vec_s *text, size_t off, match_result_s *res);

#endif
