from lib           import lib
from util.keys     import Keys
from bind.mode     import Mode, PyCommand
from core          import log
from server.server import add_global

pty_lastcmd = "wc"

@add_global()
def make_pty_prompt_cmd(buf):
    srcarg = buf.get_param(0)
    dstarg = buf.get_param(1)
    cmdarg = buf.get_param(2)

    if srcarg == b"l":
        src = "buf.pipe_from_lines()"
    elif srcarg == b"b":
        src = "buf.pipe_from_buf()"
    elif srcarg == b"s":
        src = "None"
    elif srcarg:
        log.str("ALERT", "ERR", "Unrecognized pty src: {}".format(srcarg))
        return
    else:
        src = "None"

    if dstarg == b"l":
        dst = "buf.pipe_to_lines()"
    elif dstarg == b"b":
        dst = "buf.pipe_to_buf()"
    elif dstarg == b"r":
        dst = "buf.pipe_to_lines_repl()"
    elif dstarg == b"c":
        dst = "buf.pipe_to_cur()"
    elif dstarg == b"s":
        dst = "None"
    elif dstarg:
        log.str("ALERT", "ERR", "Unrecognized pty dst: {}".format(dstarg))
        return
    else:
        dst = "None"

    if cmdarg:
        cmd = cmdarg.decode("utf-8")
    else:
        cmd = ""

    return (
        "editor.pty('{}".format(cmd),
        "',\n    {},\n    {}\n)".format(src, dst)
    )

Mode.add_default_cmds([
    PyCommand("Run pty command", "!", "single_pyprompt(*make_pty_prompt_cmd(buf))")
])
