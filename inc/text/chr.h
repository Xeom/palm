#if !defined(TEXT_CHR_H)
# define TEXT_CHR_H
# include <stdio.h>
# include <stdbool.h>
# include "types.h"
# define CHR_TABSTR_LEN 8

struct chr
{
    char text[6];
    unsigned short font;
};

struct chrconf
{
    char *nlstr;
    short tabwidth;
    chr_s tabstr[CHR_TABSTR_LEN];
    chr_s openbra, closebra;
    short hexfont, nlfont;
    chr_s space;
    chr_s tab;
};

extern chr_s chr_blank;

void chrconf_init(chrconf_s *conf);

void chr_print(chr_s *chr, tty_s *tty);

size_t chr_expanded_width(chr_s *chr, size_t pos, chrconf_s *conf);

void chr_expand(chr_s *chr, size_t pos, chrconf_s *conf, chr_s *dst);

bool chr_is_whitespace(chr_s *chr);

static inline size_t chr_num_bytes(chr_s *chr)
{
    size_t rtn;

    for (rtn = 0; rtn < sizeof(chr->text); ++rtn)
    {
        if (chr->text[rtn] == '\0')
            break;
    }

    if (rtn == 0)
        rtn = 1;

    return rtn;
}

#endif
