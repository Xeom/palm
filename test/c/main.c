#include "log/log.h"
#include "text/font.h"
#define SNOW_DEFAULT_ARGS "--no-maybes"
#include "snow.h"
#include <unistd.h>

snow_main_decls;

int main(int argc, char **argv)
{
    int rtn;
    log_init(&(default_log));
    log_to_file(&default_log, STDERR_FILENO, false, NULL);
    font_start();
    rtn = snow_main_function(argc, argv);
    font_end();
    log_kill(&(default_log));
    return rtn;
}
