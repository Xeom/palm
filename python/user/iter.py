from server.server import add_global
from core.cur import Cur

@add_global(args=["buf"])
def iter_sel(buf, rev=False):
    """Iterate across the lines in a cursor selection.

    The cursor is moved to select each line, one by one, and
    each line number is yielded. After this, the cursor is
    reset.
    """
    yield from buf.get_cur().iter_lines(buf, rev=rev)

@add_global(args=["buf"])
def iter_buf(buf, rev=False):
    """Iterate across the lines in a buffer.

    The cursor is moved to select each line, one by one, and
    each line number is yielded. After this, the cursor is
    reset
    """
    cur = Cur()
    cur.goto([(0, 0), b.last_pos()])
    yield from cur.iter_lines(buf, rev=rev)

@add_global(args=["curset"])
def iter_curs(curset, rev=False):
    """Iterate across all cursors.

    Each cursor is, in turn, selected and yielded.
    """
    num = curset.num_active()

    curset.select_ind(0)
    for i in range(num):
        if rev:
            curset.select_prev()
    
        yield curset.get_selected()

        if not rev:
            curset.select_next()

@add_global(args=["buf"])
def sort_sel(buf, k=None, lnfunct=False):
    """Sort the lines in a selection by a key.

    The lines within the current buffer selection are sorted.

    Arguments:
        buf     - The buffer to apply this function to.
        k       - A key function to sort the lines by.
        lnfunct - If True, the key function is passed a line number,
                  and if False, it is passed a string of line contents.

    Examples:

        Sort the current selection by line length:

        >>> sort_sel(len)

        Sort the current selection by numerical value of the first word:

        >>> sort_sel(lambda l: int(l.split()[0]))
    """
    buf.get_cur().sort_lines(buf, key=k, lnfunct=lnfunct)

@add_global(args=["buf"])
def sort_buf(buf, k=None, lnfunct=False):
    """Sort the lines in a buffer by a key.

    The entire buffer is sorted by a key.

    Arguments:
        buf     - The buffer to apply this function to.
        k       - A key function to sort the lines by.
        lnfunct - If True, the key function is passed a line number,
                  and if False, it is passed a string of line contents.
    """
    cur = Cur.at([(0, 0), buf.last_pos()])
    cur.sort_lines(buf, key=k, lnfunct=lnfunct)

@add_global(args=["buf"])
def apply_sel(buf, f):
    """Apply a function to all the lines in the selection.

    The contents of each line in the selection are set to the result of a
    function of the current contents of each line.

    Arguments:
        buf - The buffer to apply this function to.
        f   - A function that accepts a string of line contents, and returns
              a string to replace that line with.
    """
    for ln in iter_sel(buf):
        buf.set(ln, f(str(buf.get_line(ln))))

@add_global(args=["buf"])
def apply_buf(buf, f):
    """Apply a function to all the lines in the buffer.

    The contents of each line in the selection are set to the result of a
    function of the current contents of each line.

    Arguments:
        buf - The buffer to apply this function to.
        f   - A function that accepts a string of line contents, and returns
              a string to replace that line with.
    """
    for ln in iter_buf(buf):
        buf.set(ln, f(str(buf.get_line(ln))))
