#if !defined(WM_WM_H)
# define WM_WM_H
# include "buf/buf.h"
# include "wm/win.h"
# include "wm/update.h"

struct wm
{
    pthread_mutex_t mtx;
    vec_s           windows;
    vec_s           floatwins;
    win_s          *rootwin;
    display_s      *disp;
    rect_s          dims;
    int             winsel;
    wm_updater_s    updater;
};

void wm_init(wm_s *wm, display_s *disp);

void wm_resize(wm_s *wm);

void wm_kill(wm_s *wm);

void wm_purge_buf(wm_s *wm, buf_s *buf);

int wm_split(wm_s *wm, int id, win_split_t dir);

int wm_close(wm_s *wm, int ind);

int wm_get_dims(wm_s *wm, int ind, rect_s *dims);

int wm_win_type(wm_s *wm, int ind);

int wm_next_ind(wm_s *wm, int ind);

int wm_add_borders(wm_s *wm, int ind);

int wm_stamp_disp(wm_s *wm, int ind, rect_s *rect);

int wm_stamp_disp_all(wm_s *wm);

int wm_set_win(wm_s *wm, int ind, rect_s *rect, chr_s *mem);

int wm_set_status(wm_s *wm, int ind, vec_s *status);

void wm_update_status(wm_s *wm, int ind, editor_s *editor, buf_s *buf);

int wm_fill_win(wm_s *wm, int ind, rect_s *rect, chr_s *mem);

int wm_handle_buf_update(wm_s *wm, int ind, buf_s *buf, long startln, long endln);

int wm_adj(wm_s *wm, int ind, float change);

int wm_stamp_disp_floatwins(wm_s *wm);

#endif
