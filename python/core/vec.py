import libtypes
import ctypes
from lib import lib
from .newstruct import CStructure

class Vec(CStructure):
    class_name = "vec_s"

    init_funct = lib.vec_init
    kill_funct = lib.vec_kill

    _fields_ = [
        ('mem', ctypes.c_char_p),
        ('width', ctypes.c_size_t),
        ('allocated', ctypes.c_size_t),
        ('used', ctypes.c_size_t),
    ]

    @property
    def typ(self):
        if hasattr(self, "_typ"):
            return self._typ
        else:
            return None

    @classmethod
    def from_other(cls, other):
        if other.typ is None:
            rtn = cls(other.width)
        else:
            rtn = cls(other.typ)

        rtn.cpy_vec(other)

        return rtn

    def init_args(self, args, kwargs):
        def inner(typ):
            if isinstance(typ, int):
                return [typ]
            else:
                self._typ = typ
                return [ctypes.sizeof(typ)]

        return inner(*args, **kwargs)

    def ptrtype(self):
        if not hasattr(self, "typ") or self.typ == None:
            return ctypes.c_void_p
        else:
            return ctypes.POINTER(self.typ)

    def __len__(self) -> int:
        return lib.vec_len(self.ptr)

    def __getitem__(self, ind: int):
        ptr = lib.vec_get(self.ptr, ind)

        ptr = ctypes.cast(ptr, self.ptrtype())

        return ptr

    def __iter__(self):
        ind = 0
        while ind < len(self):
            yield self[ind]
            ind += 1

    def ins(self, ind: int, n: int, data=None):
        lib.vec_ins(self.ptr, ind, n, data)

    def add(self, n: int, data=None):
        self.ins(len(self), n, data)

    def ins_bytes(self, ind: int, data: bytes):
        self.ins(ind, len(data) // self.width, data)

    def add_bytes(self, data: bytes):
        self.ins(len(self), len(data) // self.width, data)

    def rem(self, ind, n):
        lib.vec_del(self.ptr, ind, n)

    def cpy_vec(self, oth):
        if len(oth) != 0:
            self.add(len(oth), oth[0])

    def __bytes__(self):
        return ctypes.string_at(self[0], size=len(self))

