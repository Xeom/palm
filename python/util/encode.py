from typing import Union

defaultencoding = "utf-8"

def bytify(obj: Union[bytes, str]) -> bytes:
    if isinstance(obj, bytes):
        return obj
    elif isinstance(obj, str):
        return obj.encode(defaultencoding)
    else:
        raise Exception(f"Expected string or bytes object, got {obj}")

def strify(obj: Union[bytes, str]) -> str:
    if isinstance(obj, bytes):
        return obj.decode(defaultencoding)
    elif isinstance(obj, str):
        return obj
    else:
        raise Exception(f"Expected string or bytes object, got {obj}")

