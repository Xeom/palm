#include <string.h>
#include <pthread.h>

#include "container/vec.h"
#include "util/macro.h"
#include "buf/buf.h"
#include "buf/scroll.h"
#include "log/log.h"
#include "buf/curset.h"

#define WITH_CUR_EXPAND(buf, cur, prev)                       \
    BEFORE_AFTER(                                             \
        memcpy(prev, cur, sizeof(cur_s)),                     \
        (buf_expand_cur(buf, prev), buf_expand_cur(buf, cur)) \
    )

void curset_init(curset_s *curset, buf_s *buf)
{
    pthread_mutexattr_t mtxattr;

    memset(curset, 0, sizeof(curset_s));

    list_init(&(curset->active),   sizeof(cur_s));
    list_init(&(curset->inactive), sizeof(cur_s));

    curset->buf    = buf;
    curset->sorted = true;

    pthread_mutexattr_init(&mtxattr);
    pthread_mutexattr_settype(&mtxattr, PTHREAD_MUTEX_RECURSIVE_NP);
    pthread_mutex_init(&(curset->mtx), &mtxattr);
    pthread_mutexattr_destroy(&mtxattr);
}

void curset_kill(curset_s *curset)
{
    list_kill(&(curset->active));
    list_kill(&(curset->inactive));
}

void curset_expand_all(curset_s *curset)
{
    WITH_MTX(&(curset->mtx))
    {
        LIST_FOREACH(&(curset->active), item)
        {
            cur_s *cur;
            cur = list_item_data(item);
            buf_expand_cur(curset->buf, cur);
        }
    }
}

void curset_shift(curset_s *curset, pos_s *from, pos_s *to)
{
    cur_s  prev;
    cur_s *cur;

    WITH_MTX(&(curset->mtx))
    {
        LIST_FOREACH(&(curset->active), item)
        {
            cur = list_item_data(item);
            WITH_CUR_EXPAND(curset->buf, cur, &prev)
            {
                cur_shift(cur, from, to);
                cur_correct(cur, curset->buf);
            }
        }

        LIST_FOREACH(&(curset->inactive), item)
        {
            cur = list_item_data(item);
            WITH_CUR_EXPAND(curset->buf, cur, &prev)
            {
                cur_shift(cur, from, to);
                cur_correct(cur, curset->buf);
            }
        }
    }
}

int curset_get_selected(curset_s *curset, cur_s *cur)
{
    int rtn;

    rtn = 0;
    WITH_MTX(&(curset->mtx))
    {
        if (!curset->selected)
            rtn = -1;
        else
            memcpy(cur, list_item_data(curset->selected), sizeof(cur_s));
    }

    return rtn;
}

int curset_set_selected(curset_s *curset, cur_s *cur)
{
    int rtn;
    cur_s prev;

    rtn = 0;

    WITH_MTX(&(curset->mtx))
    {
        if (!curset->selected)
        {
            rtn = -1;
            continue;
        }

        WITH_CUR_EXPAND(curset->buf, list_item_data(curset->selected), &prev)
        {
            memcpy(list_item_data(curset->selected), cur, sizeof(cur_s));
        }

        curset->sorted = false;
    }

    return rtn;
}

static int cur_cmp_cb(const void *aptr, const void *bptr)
{
    const cur_s *a, *b;
    a = aptr;
    b = bptr;

    return cur_cmp(a, b);
}

static int curset_ensure_sorted(curset_s *curset)
{
    if (!curset->sorted)
        list_msort(&(curset->active), cur_cmp_cb);

    curset->sorted = true;

    return 0;
}

int curset_select_prev(curset_s *curset)
{
    cur_s prev;

    WITH_MTX(&(curset->mtx))
    {
        curset_ensure_sorted(curset);

        WITH_CUR_EXPAND(curset->buf, list_item_data(curset->selected), &prev)
        {
            if (curset->selected)
                curset->selected = curset->selected->prev;

            if (!curset->selected)
                curset->selected = list_last(&(curset->active));
        }
    }

    return 0;
}

int curset_select_next(curset_s *curset)
{
    cur_s prev;

    WITH_MTX(&(curset->mtx))
    {
        curset_ensure_sorted(curset);

        WITH_CUR_EXPAND(curset->buf, list_item_data(curset->selected), &prev)
        {
            if (curset->selected)
                curset->selected = curset->selected->next;

            if (!curset->selected)
                curset->selected = list_first(&(curset->active));
        }
    }

    return 0;
}

long curset_get_selected_ind(curset_s *curset)
{
    long rtn;

    rtn = 0;
    WITH_MTX(&(curset->mtx))
    {
        curset_ensure_sorted(curset);

        if (!curset->selected)
            rtn = -1;
        else
            rtn = list_item_ind(curset->selected);
    }

    return rtn;
}

int curset_select_ind(curset_s *curset, long n)
{
    int rtn;
    cur_s prev;

    rtn = 0;

    WITH_MTX(&(curset->mtx))
    {
        curset_ensure_sorted(curset);

        WITH_CUR_EXPAND(curset->buf, list_item_data(curset->selected), &prev)
        {
            if (n >= 0)
            {
                list_item_s *first;
                first = list_first(&(curset->active));
                curset->selected = list_item_iter(first, n);
            }
            if (n < 0)
            {
                list_item_s *last;
                last = list_last(&(curset->active));
                curset->selected = list_item_iter(last, n + 1);
            }

            if (!curset->selected)
            {
                rtn = -1;
                curset->selected = list_first(&(curset->active));
            }
        }
    }

    return rtn;
}

int curset_push(curset_s *curset, cur_s *cur)
{
    int rtn;
    list_item_s *item;

    rtn = 0;

    WITH_MTX(&(curset->mtx))
    {
        item = list_push_after(&(curset->active), NULL, cur);
        if (!item)
        {
            rtn = -1;
            continue;
        }

        curset->sorted = false;

        curset->selected = item;
        buf_expand_cur(curset->buf, (cur_s *)list_item_data(item));
    }

    return rtn;
}

int curset_pop(curset_s *curset, cur_s *cur)
{
    int rtn;
    cur_s prev;
    list_item_s *item;

    rtn = 0;

    WITH_MTX(&(curset->mtx))
    {
        item = curset->selected;
        if (!item)
        {
            rtn = -1;
            continue;
        }

        curset_select_next(curset);
        if (curset->selected == item)
        {
            rtn = -1;
            continue;
        }

        if (!cur) cur = &prev;
        memcpy(cur, list_item_data(item), sizeof(cur_s));

        list_del(&(curset->active), LIST_RANGE(item, item));

        buf_expand_cur(curset->buf, cur);
    }

    return rtn;
}

int curset_push_inactive(curset_s *curset, cur_s *cur)
{
    int rtn;
    rtn = 0;

    WITH_MTX(&(curset->mtx))
    {
        if (!list_push_after(&(curset->inactive), NULL, cur))
            rtn = -1;
    }

    return rtn;
}

int curset_pop_inactive(curset_s *curset, cur_s *cur)
{
    int rtn;
    list_item_s *item;

    rtn = 0;

    WITH_MTX(&(curset->mtx))
    {
        item = list_first(&(curset->inactive));
        if (!item)
        {
            rtn = -1;
            continue;
        }


        memcpy(cur, list_item_data(item), sizeof(cur_s));

        list_del(&(curset->inactive), LIST_RANGE(item, item));
    }

    return rtn;
}

size_t curset_num_active(curset_s *curset)
{
    return list_len(&(curset->active));
}

size_t curset_num_inactive(curset_s *curset)
{
    return list_len(&(curset->inactive));
}

int buf_get_cur(buf_s *buf, cur_s *cur)
{
    int res;

    res = 0;
    WITH_MTX(&(buf->mtx))
    {
        if (curset_get_selected(&(buf->curset), cur) == -1)
        {
            LOG(ALERT, ERR, "Could not get cursor");
            res = -1;
        }
    }

    return res;
}

int buf_set_cur(buf_s *buf, cur_s *new)
{
    int    res;

    res = 0;
    WITH_MTX(&(buf->mtx))
    {
        do {
            if (curset_set_selected(&(buf->curset), new) == -1)
            {
                LOG(ALERT, ERR, "Could not set new cursor");
                res = -1;
                break;
            }

            scroll_update(buf, &(new->pri), BUF_SCROLL_SMOOTH);
        } while (0);
    }

    return res;
}

void buf_shift(buf_s *buf, pos_s *from, pos_s *to)
{
    WITH_MTX(&(buf->mtx))
    {
        cur_s cur;
        curset_shift(&(buf->curset), from, to);

        buf_get_cur(buf, &cur);
        scroll_update(buf, &(cur.pri), BUF_SCROLL_SMOOTH);
    }
}

cur_hl_t curset_get_pos_hl(curset_s *curset, pos_s *pos, buf_s *buf)
{
    bool alts;
    cur_hl_t max, hl;

    max = CUR_HL_END;
    alts = buf->einfo.attrs & EDIT_ALTS;

    WITH_MTX(&(buf->mtx)) WITH_MTX(&(curset->mtx))
    {
        LIST_FOREACH(&(buf->curset.active), item)
        {
            cur_s *cur;
            cur = list_item_data(item);
            hl = cur_get_pos_hl(cur, *pos, buf);

            if (!alts && item != curset->selected)
            {
                switch (hl)
                {
                case CUR_HL_PRI:     hl = CUR_HL_DEAD_PRI;     break;
                case CUR_HL_SEC:     hl = CUR_HL_DEAD_SEC;     break;
                case CUR_HL_SEL:     hl = CUR_HL_DEAD_SEL;     break;
                case CUR_HL_PRIFAKE: hl = CUR_HL_DEAD_PRIFAKE; break;
                case CUR_HL_SECFAKE: hl = CUR_HL_DEAD_SECFAKE; break;
                }
            }
            else if (item != curset->selected)
            {
                switch (hl)
                {
                case CUR_HL_PRI:     hl = CUR_HL_ALT_PRI;     break;
                case CUR_HL_SEC:     hl = CUR_HL_ALT_SEC;     break;
                case CUR_HL_SEL:     hl = CUR_HL_ALT_SEL;     break;
                case CUR_HL_PRIFAKE: hl = CUR_HL_ALT_PRIFAKE; break;
                case CUR_HL_SECFAKE: hl = CUR_HL_ALT_SECFAKE; break;
                }
            }
            max = cur_max_hl(hl, max);
        }
    }

    return max;
}
