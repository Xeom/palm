import unittest
import tempfile
import os
import socket
import paths
import time
import subprocess

class Testbed(object):
    def __init__(self):
        self.sockname = tempfile.mktemp("-palm-test")
        self.proc = subprocess.Popen([
               os.path.join(paths.python, "__init__.py"),
                "--sock", self.sockname,
            ],

            stdout=subprocess.DEVNULL
        )

        tout = time.time() + 10
        while not os.path.exists(self.sockname):
            if time.time() > tout:
                raise Exception("palm did not create a socket file.")

        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.connect(self.sockname)
        sock.settimeout(10)

        self.sock = sock

    def __del__(self):
        self.proc.wait(1)
        self.proc.terminate()
        self.sock.close()
        if os.path.exists(self.sockname):
            os.unlink(self.sockname)

    def run(self, data, reply=False):
        if isinstance(data, str):
            data = data.encode("utf-8")

        self.sock.send(data + b"\x00")
        recv = b""
        while not recv.endswith(b"\x00"):
            recv += self.sock.recv(1)

        if not reply and len(recv) > 1:
            raise Exception(recv[:-1])

        return recv[:-1]

    def eval(self, code):
        return self.run(f"session.listener.send(repr({code}))", reply=True).decode("utf-8")

class EditorTest(unittest.TestCase):
    def setUp(self):
        self.testbed = Testbed()

    def tearDown(self):
        self.testbed.sock.send(b"editor['alive'] = False\x00")

    def runCmd(self, cmd):
        self.testbed.run(
            f"editor.playbacker.send(Keys.esc('{cmd}'))"
        )
        

    def assertReprEqual(self, code, obj):
        self.assertEqual(
            self.testbed.eval(code),
            repr(obj)
        )

    def assertBufferContents(self, buf, content):
        for n, line in enumerate(content):
            self.assertReprEqual(
                f"{buf}.get_str({n})", line
            )

    def awaitReprCond(self, code, cond):
        timeout = time.time() + 10.0
        while time.time() < timeout:
            if cond(self.testbed.eval(code)):
                return

    def awaitReprEqual(self, code, obj):
        self.awaitReprCond(code, lambda c: c == repr(obj))
    def awaitReprNotEqual(self, code, obj):
        self.awaitReprCond(code, lambda c: c != repr(obj))


def make_dir_structure(structure):
    oldcwd = os.getcwd()
    for name, cont in structure.items():
        if isinstance(cont, str):
            with open(name, "w") as f:
                f.write(cont)
        elif isinstance(cont, dict):
            os.mkdir(name)
            os.chdir(name)
            make_dir_structure(cont)
            os.chdir(oldcwd)

def make_tmp_dir_structure(structure):
    tmpdir = tempfile.TemporaryDirectory()
    os.chdir(tmpdir.name)
    make_dir_structure(structure)

    return tmpdir
