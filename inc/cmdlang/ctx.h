#if !defined(CMDLANG_CTX_H)
# define CMDLANG_CTX_H
# include "types.h"
# include "container/list.h"
# include "util/err.h"

struct cmd_ctx
{
    cmd_lib_s *lib;
    editor_s  *editor;
    buf_s     *buf;
    list_s objstack;
    list_s tokens;
};

int cmd_ctx_init(cmd_ctx_s *ctx, buf_s *buf) DONT_IGNORE;

void cmd_ctx_kill(cmd_ctx_s *ctx);

int cmd_ctx_cpy(cmd_ctx_s *dst, cmd_ctx_s *src) DONT_IGNORE;

int cmd_ctx_run(cmd_ctx_s *cmd, errmsg_t errmsg) DONT_IGNORE;

int cmd_ctx_cancel(cmd_ctx_s *cmd) DONT_IGNORE;

int cmd_ctx_push_token(cmd_ctx_s *ctx, cmdlang_tok_s *tok, errmsg_t errmsg) DONT_IGNORE;

int cmd_ctx_print_tokens(cmd_ctx_s *ctx);

#endif
