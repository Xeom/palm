#if !defined(BUF_SCROLL_H)
# define BUF_SCROLL_H
# include "buf/buf.h"
# include "buf/cur.h"

typedef enum
{
    BUF_SCROLL_JUMP,
    BUF_SCROLL_SMOOTH
} scroll_t;

void scroll_update(buf_s *buf, pos_s *show, scroll_t typ);

#endif
