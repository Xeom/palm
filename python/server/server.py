import os
import ctypes
import sys
import traceback
import paths
import time
import socket

from typing import Optional, List

from editor import log

from util.encode       import strify, bytify
from core.log          import Log
from core.vec          import Vec

from lib import defs

from .globals import add_default_globals_to_namespace, add_global

class ShellServer(object):
    session_tout = 0.1
    eval_tout    = 5.0

    def __init__(self, editor, globaldef={}):
        self.editor = editor
        self.globaldef = globaldef.copy()
        add_default_globals_to_namespace(self.globaldef)
        self.sessions = []

    def add_session(self):
        session = ShellSession(self)
        self.sessions.append(session)
        return session

    def rem_session(self, session):
        self.sessions.remove(session)

class ShellSession(object):
    def __init__(self, server, sessionfiles=True):
        self.server = server
        self.sessionfiles = sessionfiles
        self.globalvars = server.globaldef.copy()
        self.globalvars["session"] = self
        self.ccb = None
        self.reply = None

        self.load_session()

    def load_session(self):
        if not self.sessionfiles:
            return

        for fname in os.listdir(paths.python + "/session"):
            l = {}
            if not fname.endswith(".py"):
                continue

            with open(paths.python + "/session/" + fname) as sessionfile:
                exec(sessionfile.read(), self.globalvars, l)

            self.globalvars.update(l)

    def send_reply(self, string):
        if self.replycb is not None:
            self.replycb(bytify(string))
        else:
            log.str("ALERT", "INFO", f"(Shell) {string}")

    def execute(self, string, replycb=None):
        self.replycb = replycb
        res = []
        log.str("DEBUG", "INFO", "ShellSession executing '{}'\n".format(string))
        try:
            exec(string, self.globalvars, self.globalvars)
            log.str("DEBUG", "INFO", "ShellSession executed\n")
        except Exception as e:
            exc = traceback.format_exc()
            self.send_reply(exc)
            log.str(
                "NOTICE", "ERR",
                "Exception encountered in '{}':\n{}".format(
                    strify(string),
                    exc
                )
            )
        finally:
            self.replycb = None

    def gen_c_cb(self):
        if self.ccb is not None:
            return self.ccb

        def cb(sent, reply):
            sent:  Vec = sent.contents
            reply: Vec = reply.contents
            self.execute(strify(bytes(sent)), replycb=reply.add_bytes)

        self.ccb = defs.bind_pycb_t(cb)
        return self.ccb

"""
class ShellServer(object):
    session_tout = 0.1
    eval_tout    = 5.0

    def __init__(self, editor, globaldef={}):
        super().__init__()

        self.editor    = editor
        self.globaldef = globaldef.copy()
        add_default_globals_to_namespace(self.globaldef)
        self.sessions  = []

    def stop(self):
        for sess in self.sessions:
            sess.stop()

    def add_session(self):
        conn1, conn2 = socket.socketpair()
        session = ShellSession(self, conn1, conn2)
        self.sessions.append(session)
        log.fmt("INFO", "INFO", "Created session with fd %d", session.conn.fileno())
        return conn2

    def rem_session(self, session):
        self.sessions.remove(session)
        session.stop()
        log.fmt("INFO", "INFO", "Removed session with fd %d", session.conn.fileno())

    def bind_and_wait_for_connection(self, name: str):
        " ""Bind a socket to the file system, and wait for a single connection." ""
        log.fmt("ALERT", "INFO", "Waiting for connection on socket %s", name)
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.bind(name)
        sock.listen(1)
        conn, client = sock.accept()
        session = ShellSession(self, conn, None, sessionfiles=False)
        self.sessions.append(session)
        log.fmt("ALERT", "SUCC", "Got connection on %s", name)

class ShellSession(object):
    sessionfiles = paths.sessfiles[:]

    def __init__(self, server, conn, remote, sessionfiles=True):
        self.server = server
        self.conn   = conn
        self.remote = remote

        self.sessionfiles = sessionfiles

        pollctx = self.server.editor["pollctx"]
        self.reader = Reader(pollctx, conn.fileno(), linesep="\\0", linesepattrs=["ESCAPES"])
        self.writer = Writer(pollctx, conn.fileno())

        self.globalvars = server.globaldef.copy()
        self.globalvars["session"] = self

        self.load_session()

        @self.reader.on_recv()
        def recvcb(line: bytes):
            self.execute(strify(line).strip("\x00"))

        @self.reader.on_eof()
        def eofcb():
            self.stop()
            self.server.rem_session(self)

        self.reader.start()

        self.recvcb = recvcb
        self.eofcb  = eofcb

    def __del__(self):
        self.stop()

    def stop(self):
        self.reader.stop()
        self.conn.close()

    def load_session(self):
        if not self.sessionfiles:
            return

        for fname in os.listdir(paths.python + "/session"):
            l = {}
            if not fname.endswith(".py"):
                continue

            with open(paths.python + "/session/" + fname) as sessionfile:
                exec(sessionfile.read(), self.globalvars, l)

            self.globalvars.update(l)

    def execute(self, string):
        log.str("DEBUG", "INFO", "ShellSession executing '{}'\n".format(string))
        try:
            exec(string, self.globalvars, self.globalvars)
            log.str("DEBUG", "INFO", "ShellSession executed\n")
        except Exception as e:
            exc = traceback.format_exc()
            self.writer.write(exc.encode("utf-8"))
            log.str("NOTICE", "ERR", "Exception encountered in '{}':\n{}".format(strify(string), exc))

        self.writer.write(b"\x00")
"""
