#include "hl/worker.h"
#include "editor.h"

static void hl_worker_launch_job(hl_worker_s *worker);
static long hl_worker_pop_line(hl_worker_s *worker);
static void hl_worker_remove_line(hl_worker_s *worker, size_t ln);
static void hl_worker_job(void *arg);

void hl_worker_init(hl_worker_s *worker, buf_s *buf, editor_s *editor)
{
    pthread_mutexattr_t mtxattrs;

    pthread_mutexattr_init(&mtxattrs);
    pthread_mutexattr_settype(&mtxattrs, PTHREAD_MUTEX_RECURSIVE_NP);
    pthread_mutex_init(&(worker->mtx), &mtxattrs);
    pthread_mutexattr_destroy(&mtxattrs);

    worker->buf = buf;
    worker->ctx = &(editor->hlctx);

    worker->active = true;

    worker->pool = &(editor->pool);
    worker->jobrunning = false;
    pool_job_init(&(worker->job), hl_worker_job, worker);

    vec_init(&(worker->todo), sizeof(long));
}

void hl_worker_kill(hl_worker_s *worker)
{
    pthread_mutex_lock(&(worker->mtx));
    vec_clr(&(worker->todo));
    worker->active = false;
    pthread_mutex_unlock(&(worker->mtx));

    pool_job_await(&(worker->job));
    pool_job_kill(&(worker->job));

    vec_kill(&(worker->todo));
}

static void hl_worker_launch_job(hl_worker_s *worker)
{
    pthread_mutex_lock(&(worker->mtx));
    if (!(worker->jobrunning))
    {
        pool_job_await(&(worker->job));
        worker->jobrunning = true;
        pool_add_job(worker->pool, &(worker->job));
    }
    pthread_mutex_unlock(&(worker->mtx));
}

static long hl_worker_pop_line(hl_worker_s *worker)
{
    long rtn;
    pthread_mutex_lock(&(worker->mtx));
    if (vec_len(&(worker->todo)) == 0)
        rtn = -1;
    else
    {
        size_t *val;
        val = vec_get(&(worker->todo), 0);
        rtn = *val;
        vec_del(&(worker->todo), 0, 1);
    }
    pthread_mutex_unlock(&(worker->mtx));

    return rtn;
}

void hl_worker_push_line(hl_worker_s *worker, size_t ln)
{
    bool tolaunch;

    pthread_mutex_lock(&(worker->mtx));
    if (worker->active)
    {
        vec_app(&(worker->todo), &ln);
        tolaunch = true;
    }
    else
    {
        tolaunch = false;
    }
    pthread_mutex_unlock(&(worker->mtx));

    if (tolaunch)
        hl_worker_launch_job(worker);
}

static void hl_worker_remove_line(hl_worker_s *worker, size_t ln)
{
    pthread_mutex_lock(&(worker->mtx));
    if (worker->active)
    {
        VEC_RFOREACH(&(worker->todo), size_t *, lptr)
        {
            if (ln == *lptr)
               vec_del(&(worker->todo), _ind, 1);
        }
    }
    pthread_mutex_unlock(&(worker->mtx));
}

static void hl_worker_job(void *arg)
{
    hl_worker_s *worker;
    worker = arg;

    while (worker->jobrunning)
    {
        long ln;

        pthread_mutex_lock(&(worker->mtx));
        ln = hl_worker_pop_line(worker);
        if (ln == -1)
            worker->jobrunning = false;
        else
            hl_worker_remove_line(worker, ln);
        pthread_mutex_unlock(&(worker->mtx));

        if (ln != -1)
        {
            vec_s chrs;
            vec_s states;
            vec_init(&chrs, sizeof(chr_s));
            vec_init(&states, sizeof(long));

            buf_hlstate(worker->buf, ln, &states, NULL);
            if (vec_len(&states) != 0)
            {
                buf_get_line(worker->buf, ln, &chrs);
                hl_ctx_apply(worker->ctx, &chrs, &states);
                buf_cpy_font(worker->buf, ln, &chrs);
                buf_hlstate(worker->buf, ln + 1, NULL, &states);
            }

            vec_kill(&chrs);
            vec_kill(&states);
        }
    }
}
