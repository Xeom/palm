"""A module for managing callbacks.

This module should enable hooks on callbacks, events,
or file poll attributes. It is a bit messy and hacky,
but this kind of thing ususally is. It allows us to
have a nice single-threaded implementation of the
python layer with a good deal of flexibility and never
any worries about race conditions.
"""

import weakref
import time
import bisect
import sys
import os
import traceback

from core.io  import IoFileInfo
from typing import List, Optional, Callable, Sequence, Union, Iterator
from util.encode import bytify, strify

class Hook(object):
    """A callback for an event.

    A series of functions can be hooked onto an instance of this class,
    and they can all be run when some event happens, whether this is
    a timer, file or other kind of event.
    """

    def __init__(self):
        """Initialize an instance of this class."""
        self.functs = []

    def add(self, funct: Callable, weak: bool=False):
        """Add a function to this hook.

        Arguments:
            funct - The function to add.
            weak  - If True, the function is weakly referenced by this
                    hook, and the hook will forget the function once it
                    is garbaged collected. If False, the function is
                    strongly referenced by this hook, and the hook will
                    not forget the function until hook.detach(...) is
                    called.
        """
        if weak:
            self.functs.append(weakref.ref(funct))
        else:
            def strongref():
                return funct
            self.functs.append(strongref)

    def detach(self, funct: Callable):
        """Detach a function from this hook.

        If multiple instances of the same function are attached,
        then only one will be detached.

        Arguments:
            funct - The function to detach, as was passed to .add()
        """
        for ref in self.functs:
            if ref() == funct:
                self.functs.remove(ref)
                return True
        return False

    def get_functs(self) -> Iterator[Callable]:
        """Iterate through the functions attached here."""
        toforget = []
        for ref in self.functs:
            funct = ref()
            if funct != None:
                yield funct
            else:
                toforget.append(ref)

        for ref in toforget:
            self.functs.remove(ref)

    def run(self, *args):
        """Fun all the functions attached to this hook."""
        for f in self.get_functs():
            f(*args)

    def is_empty(self) -> bool:
        """Return true if nothing is attached to this hook."""
        if len(self.functs) == 0:
            return True

        if not any(ref() is not None for ref in self.functs):
            return True

        return False

class HookManager(object):
    """A class to contain a set of named hooks.

    This class contains and manages a set of hooks,
    each of which is named and called when a corresponding
    event happens.

    >>> hooks = HookManager()
    >>> @hooks.attach("beef")
    ... def beefhandler():
    ...    print("moo")
    ...
    >>> hooks.call("beef")
    moo
    """

    def __init__(self):
        """Initialize an instance of this class."""
        self.attachments = {}

    def attach(self, *names, weak: bool=False):
        """Return a decorator to attach a function to a hook in this class.

        Usage:
        >>> hooks = HookManager()
        ... @hooks.attach("event1", "event2")
        ... def cb(*args):
        ...     ...

        Arguments:
            - *names - The names of the hooks to listen for.
            - weak   - If True, keep only a weak reference to the
                       function, and forget about the reference when
                       it is deleted. If False, keep a strong reference
                       to the function, so that .detach must be manually
                       called.
        """
        # Function to decorate
        def glue(funct: Callable):
            for name in names:
                hook = self.attachments.get(name)
                if hook == None:
                    hook = Hook()
                    self.attachments[name] = hook

                hook.add(funct, weak)

            # Replace function with itself
            return funct
        return glue

    def detach(self, funct: Callable, event: Optional[str]=None):
        """Detach a function from listening to an event."""
        if event == None:
            for hook in self.attachments.values():
                hook.detach(funct)
        else:
            self.attachments[event].detach(funct)

    def call(self, name, *args):
        """Call a particular hook by name.

        Arguments:
            name  - The name of the hook to call.
            *args - The arguments to pass to the callbacks on the hook.
        """
        hook = self.attachments.get(name)
        if hook != None:
            hook.run(*args)

# Initialize instances of Hook, Timer and Poll managers
# for use in the editor!
hooks  = HookManager()
