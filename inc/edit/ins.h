#if !defined(EDIT_INS_H)
# define EDIT_INS_H
# include "edit/edit.h"

void edit_ins_chrs_at(edit_info_s *einfo, const chr_s *chrs, size_t nchrs, pos_s *pos);

void edit_ins_str_at(edit_info_s *einfo, const char *str, size_t len, pos_s *pos);

void edit_ins_vec(edit_info_s *einfo, vec_s *vec);

void edit_ins_str(edit_info_s *einfo, const char *str, size_t len);

#endif
