#include <limits.h>
#include "buf/buf.h"
#include "buf/line.h"
#include "buf/scroll.h"
#include "bind/bufbind.h"
#include "text/chr.h"
#include "text/font.h"
#include "util/macro.h"
#include "text/decoder.h"
#include "log/log.h"
#include "wm/wm.h"
#include "editor.h"

void buf_init(buf_s *buf, editor_s *editor)
{
    pthread_mutexattr_t mtxattr;

    LOG(DEBUG, INFO, "Initializing a new buffer at %p", (void *)buf);

    buf->editor = editor;

    pthread_mutexattr_init(&mtxattr);
    pthread_mutexattr_settype(&mtxattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&(buf->mtx), &mtxattr);

    vec_init(&(buf->lines), sizeof(line_s));

    curset_init(&(buf->curset), buf);

    line_init(&(buf->blankline), buf);
    line_set_str(&(buf->blankline), "{blank-line}<", DECODER_FNT);

    buf->winind = -1;
    buf->scrx   =  0;
    buf->scry   =  0;

    chrconf_init(&(buf->chrconf));
    undo_list_init(&(buf->undolist));
    edit_info_init(&(buf->einfo), buf);
    finfo_init(&(buf->finfo), &(editor->pollctx));

    vec_init(&(buf->defhl), sizeof(long));

    hl_worker_init(&(buf->hlworker), buf, editor);

    vec_init(&(buf->cmdparams), sizeof(vec_s));

    curset_push(&(buf->curset), &CUR_AT(0, 0));

    if (cmd_ctx_init(&(buf->cmdctx), buf) == PALM_ERR)
        LOG(ALERT, ERR, "Could not initialize command context.");

    if (cmdlang_parser_init(&(buf->cmdparser), &(buf->cmdctx), &(editor->cmdlib), NULL) == PALM_ERR)
        LOG(ALERT, ERR, "Could not initialize command parser.");

    LOG(INFO, SUCC, "Buffer at %p initialized", (void *)buf);
}

void buf_kill(buf_s *buf)
{
    LOG(DEBUG, INFO, "Killing the buffer at %p", (void *)buf);

    buf->hlworker.active = false;
    hl_worker_kill(&(buf->hlworker));
    finfo_kill(&(buf->finfo));

    buf_del_line(buf, 0, buf_len(buf));

    cmdlang_parser_kill(&(buf->cmdparser));
    cmd_ctx_kill(&(buf->cmdctx));

    wm_purge_buf(&(buf->editor->wm), buf);

    edit_info_kill(&(buf->einfo));

    undo_list_kill(&(buf->undolist));
    vec_kill(&(buf->lines));
    line_kill(&(buf->blankline));
    pthread_mutex_destroy(&(buf->mtx));

    vec_kill(&(buf->defhl));

    curset_kill(&(buf->curset));

    LOG(INFO, SUCC, "Buffer at %p killed", (void *)buf);
}

row_t buf_len(buf_s *buf)
{
    size_t rtn;

    pthread_mutex_lock(&(buf->mtx));
    rtn = vec_len(&(buf->lines));
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

size_t buf_line_len(buf_s *buf, row_t ln)
{
    line_s *line;
    size_t rtn;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);

    if (line)
        rtn = vec_len(&(line->chrs));
    else
        rtn = 0;

    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

int buf_win_assoc(buf_s *buf, int winind)
{
//    wm_handle_buf_update(buf_wm(buf), buf->winind, NULL, 0, LONG_MAX);
    pthread_mutex_lock(&(buf->mtx));
    buf->winind = winind;
    wm_handle_buf_update(buf_wm(buf), buf->winind, buf, 0, LONG_MAX);
    pthread_mutex_unlock(&(buf->mtx));

    return 0;
}

int buf_get_win(buf_s *buf)
{
    int rtn;

    pthread_mutex_lock(&(buf->mtx));
    rtn = buf->winind;
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

void buf_set_defhl(buf_s *buf, vec_s *defhl)
{
    pthread_mutex_lock(&(buf->mtx));
    vec_clr(&(buf->defhl));
    vec_cpy(&(buf->defhl), defhl);
    if (vec_len(&(buf->lines)))
    {
        buf_hlstate(buf, 0, NULL, defhl);
        hl_worker_push_line(&(buf->hlworker), 0);
    }
    pthread_mutex_unlock(&(buf->mtx));
}

int buf_hlstate(buf_s *buf, row_t ln, vec_s *get, vec_s *set)
{
    int rtn;
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);
    if (!line)
    {
        rtn = -1;
    }
    else
    {
        if (get)
            line_get_hlstate(line, get);
        if (set)
        {
            vec_s prev;
            vec_init(&prev, sizeof(long));
            line_get_hlstate(line, &prev);
            line_set_hlstate(line, set);
            if (vec_len(&prev) != vec_len(set) ||
                memcmp(vec_get(&prev, 0), vec_get(set, 0), vec_len(set) * sizeof(long)))
                hl_worker_push_line(&(buf->hlworker), ln);
        }
        rtn = 0;
    }
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

int buf_get_line(buf_s *buf, row_t ln, vec_s *chrs)
{
    int rtn;
    line_s *line;
    vec_del(chrs, 0, vec_len(chrs));

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);
    if (!line)
    {
        rtn = -1;
    }
    else
    {
        vec_cpy(chrs, &(line->chrs));
        rtn = 0;
    }
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

int buf_get_expanded(buf_s *buf, row_t ln, vec_s *chrs)
{
    int rtn;
    line_s *line;
    vec_del(chrs, 0, vec_len(chrs));

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);
    if (!line)
    {
        line = &(buf->blankline);
    }
    line_get_expanded(line, chrs);
    rtn = 0;
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

int buf_get_str(buf_s *buf, row_t ln, vec_s *chrs)
{
    int rtn;
    line_s *line;
    vec_del(chrs, 0, vec_len(chrs));

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);
    if (!line)
    {
        rtn = -1;
    }
    else
    {
        VEC_FOREACH(&(line->chrs), chr_s *, chr)
        {
            vec_str(chrs, chr->text);
        }
        rtn = 0;
    }
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

int buf_set_line(buf_s *buf, row_t ln, vec_s *chrs)
{
    int rtn;
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);
    if (!line)
    {
        rtn = -1;
    }
    else if (line_cmp(line, chrs) == 0)
    {
        rtn = 0;
    }
    else
    {
        undo_from_set(undo_list_add(&(buf->undolist)), ln, &(line->chrs), chrs);
        undo_list_compact(&(buf->undolist));

        line_set_chrs(line, vec_get(chrs, 0), vec_len(chrs));
        rtn = 0;

        hl_worker_push_line(&(buf->hlworker), ln);
        wm_handle_buf_update(buf_wm(buf), buf->winind, buf, ln, ln);
    }

    pthread_mutex_unlock(&(buf->mtx));
    return rtn;
}

int buf_cpy_font(buf_s *buf, row_t ln, vec_s *chrs)
{
    int rtn;
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);
    if (!line)
    {
        rtn = -1;
    }
    else
    {
        line_cpy_font(line, chrs);
        rtn = 0;
    }

    wm_handle_buf_update(buf_wm(buf), buf->winind, buf, ln, ln);
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

int buf_set_font(buf_s *buf, row_t ln, unsigned short font, size_t ind, size_t n)
{
    int rtn;
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), ln);
    if (!line)
    {
        rtn = -1;
    }
    else
    {
        line_set_font(line, font, ind, n);
        rtn = 0;
    }

    wm_handle_buf_update(buf_wm(buf), buf->winind, buf, ln, ln);
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

int buf_ins_line(buf_s *buf, row_t ln, size_t n)
{
    row_t len, ctr;

    pthread_mutex_lock(&(buf->mtx));
    len = buf_len(buf);
    if (ln > len)
    {
        n += ln - len;
        ln = len;
    }

    vec_ins(&(buf->lines), ln, n, NULL);

    for (ctr = ln; ctr < ln + (row_t)n; ++ctr)
    {
        line_init(vec_get(&(buf->lines), ctr), buf);
    }

    if (vec_len(&(buf->lines)) == n || ln == 0)
    {
        buf_hlstate(buf, 0, NULL, &(buf->defhl));
    }

    hl_worker_push_line(&(buf->hlworker), MAX(0, (long)ln - 1));
    wm_handle_buf_update(buf_wm(buf), buf->winind, buf, ln, len + n - 1);
    undo_from_ins(undo_list_add(&(buf->undolist)), ln, n);
    undo_list_compact(&(buf->undolist));
    pthread_mutex_unlock(&(buf->mtx));

    return 0;
}

int buf_del_line(buf_s *buf, row_t ln, size_t n)
{
    int rtn;
    row_t ind, len;
    pthread_mutex_lock(&(buf->mtx));

    len = buf_len(buf);;
    for (ind = ln; ind < ln + (row_t)n; ++ind)
    {
        line_s *line;
        line = vec_get(&(buf->lines), (size_t)ind);

        if (!line) break;

        undo_from_del(undo_list_add(&(buf->undolist)), ln, &(line->chrs));
        undo_list_compact(&(buf->undolist));

        line_kill(line);
    }

    if (ln >= len)
    {
        rtn = -1;
    }
    else
    {
        if (ln + (row_t)n > len)
            n = len - ln;

        vec_del(&(buf->lines), ln, n);

        rtn = 0;
    }

    hl_worker_push_line(&(buf->hlworker), MAX(0, (long)ln - 1));
    wm_handle_buf_update(buf_wm(buf), buf->winind, buf, ln, LONG_MAX);
    pthread_mutex_unlock(&(buf->mtx));

    return rtn;
}

void buf_refresh(buf_s *buf, row_t ln, size_t n)
{
    size_t ctr;

    pthread_mutex_lock(&(buf->mtx));

    for (ctr = ln; ctr < ln + n; ++ctr)
    {
        if (ctr >= vec_len(&(buf->lines)))
            break;

        line_needs_expansion(vec_get(&(buf->lines), ctr));
    }

    wm_handle_buf_update(buf_wm(buf), buf->winind, buf, ln, n + ln);
    pthread_mutex_unlock(&(buf->mtx));
}

void buf_get_scr(buf_s *buf, long *x, long *y)
{
    pthread_mutex_lock(&(buf->mtx));
    if (x)
        *x = buf->scrx;
    if (y)
       *y = buf->scry;
    pthread_mutex_unlock(&(buf->mtx));
}

void buf_set_scr(buf_s *buf, long x, long y)
{
    long prevx, prevy;

    pthread_mutex_lock(&(buf->mtx));

    prevx = buf->scrx;
    prevy = buf->scry;

    buf->scrx = x;
    buf->scry = y;

    if (prevx != x || prevy != y)
    {
        wm_handle_buf_update(buf_wm(buf), buf->winind, buf, 0, LONG_MAX);
    }
    pthread_mutex_unlock(&(buf->mtx));
}

void buf_move_cur(buf_s *buf, cur_dir_t dir, int n)
{
    cur_s cur;

    pthread_mutex_lock(&(buf->mtx));

    buf_get_cur(buf, &cur);
    cur_move(&cur, dir, n, buf);
    buf_set_cur(buf, &cur);

    pthread_mutex_unlock(&(buf->mtx));
}

wm_s *buf_wm(buf_s *buf)
{
    return &(buf->editor->wm);
}

void buf_chrconf(buf_s *buf, chrconf_s *get, chrconf_s *set)
{
    pthread_mutex_lock(&(buf->mtx));
    if (get)
        memcpy(get, &(buf->chrconf), sizeof(chrconf_s));

    if (set)
        memcpy(&(buf->chrconf), set, sizeof(chrconf_s));

    pthread_mutex_unlock(&(buf->mtx));
}

void buf_ind_to_pos(buf_s *buf, pos_s *pos)
{
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), pos->row);

    if (line)
    {
        pos->col = line_ind_to_pos(line, pos->col);
    }
    pthread_mutex_unlock(&(buf->mtx));
}

void buf_pos_to_ind(buf_s *buf, pos_s *pos)
{
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), pos->row);

    if (line)
    {
        pos->col = line_pos_to_ind(line, pos->col);
    }
    pthread_mutex_unlock(&(buf->mtx));
}

void buf_off_to_ind(buf_s *buf, pos_s *pos)
{
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), pos->row);

    if (line)
    {
        pos->col = line_off_to_ind(line, pos->col);
    }
    pthread_mutex_unlock(&(buf->mtx));
}

void buf_ind_to_off(buf_s *buf, pos_s *pos)
{
    line_s *line;

    pthread_mutex_lock(&(buf->mtx));
    line = vec_get(&(buf->lines), pos->row);

    if (line)
    {
        pos->col = line_ind_to_off(line, pos->col);
    }
    pthread_mutex_unlock(&(buf->mtx));
}

