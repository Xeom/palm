#include "util/err.h"
#include "util/escape.h"

#include "cmdlang/obj.h"

int cmdlang_obj_init(cmdlang_obj_s *obj)
{
    obj->typ = CMDLANG_OBJ_NONE;
    return PALM_OK;
}

int cmdlang_obj_init_cpy(cmdlang_obj_s *obj, cmdlang_obj_s *src)
{
    switch (src->typ)
    {
    case CMDLANG_OBJ_STR:
        return cmdlang_obj_init_str(
            obj, vec_get(&(src->val.str), 0), vec_len(&(src->val.str))
        );
    case CMDLANG_OBJ_NUMBER:
        return cmdlang_obj_init_number(obj, src->val.number);
    case CMDLANG_OBJ_POS:
        return cmdlang_obj_init_pos(obj, &(src->val.pos));
    case CMDLANG_OBJ_CUR:
        return cmdlang_obj_init_cur(obj, &(src->val.cur));
    }
    obj->typ = src->typ;

    return PALM_ERR;
}

int cmdlang_obj_init_str(cmdlang_obj_s *obj, const char *str, size_t n)
{
    obj->typ = CMDLANG_OBJ_STR;
    vec_init(&(obj->val.str), sizeof(char));

    if (vec_add(&(obj->val.str), n, str) == NULL)
        return PALM_ERR;

    return PALM_OK;
}

int cmdlang_obj_init_number(cmdlang_obj_s *obj, long number)
{
    obj->typ = CMDLANG_OBJ_NUMBER;
    obj->val.number = number;

    return PALM_OK;
}

int cmdlang_obj_init_pos(cmdlang_obj_s *obj, pos_s *pos)
{
    obj->typ = CMDLANG_OBJ_POS;
    memcpy(&(obj->val.pos), pos, sizeof(pos_s));

    return PALM_OK;
}

int cmdlang_obj_init_cur(cmdlang_obj_s *obj, cur_s *cur)
{
    obj->typ = CMDLANG_OBJ_CUR;
    memcpy(&(obj->val.cur), cur, sizeof(cur_s));

    return PALM_OK;
}

void cmdlang_obj_kill(cmdlang_obj_s *obj)
{
    if (obj->typ == CMDLANG_OBJ_STR)
    {
        vec_kill(&(obj->val.str));
    }
}

int cmdlang_obj_to_str(cmdlang_obj_s *obj, vec_s *str)
{
    switch (obj->typ)
    {
    case CMDLANG_OBJ_NONE:
        if (vec_str(str, "<None>") == NULL)
            return PALM_ERR;

        break;

    case CMDLANG_OBJ_STR:
        if (vec_str(str, "\"") == NULL)
            return PALM_ERR;

        if (escape_sanitize_vec(&(obj->val.str), str) == PALM_ERR)
            return PALM_ERR;

        if (vec_str(str, "\"") == NULL)
            return PALM_ERR;

        break;

    case CMDLANG_OBJ_NUMBER:
        if (vec_fmt(str, "%ld", obj->val.number) == NULL)
            return PALM_ERR;

        break;

    case CMDLANG_OBJ_POS:
        if (vec_fmt(str, "Pos(%ld,%ld)", obj->val.pos.row, obj->val.pos.col) == NULL)
            return PALM_ERR;

        break;

    case CMDLANG_OBJ_CUR:
        if (vec_fmt(
                str, "Cur((%ld,%ld), (%ld,%ld), typ='%s', sticky=%s)",
                obj->val.cur.pri.row, obj->val.cur.pri.col,
                obj->val.cur.sec.row, obj->val.cur.sec.col,
                cur_type_name(obj->val.cur.type),
                obj->val.cur.sticky ? "True" : "False"
            ) == NULL)
            return PALM_ERR;

        break;
    }

    return PALM_OK;
}
