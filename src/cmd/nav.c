#include "cmd/nav.h"
#include "container/vec.h"
#include "buf/buf.h"
#include "wm/wm.h"
#include "cmdlang/helpers.h"
#include "editor.h"

/* --- COMMANDS FOR NAVIGATING AND USING CURSORS --- *
 *                                                   *
 * In this file, are commands for moving around and  *
 * navigating buffers.                               *
 *
 * ------------------------------------------------- */

/* --- Cursor movement --- */

VERB(move, "Move the cursor n places in a specific direction.",
    ADD_REQ_ARG(CMDLANG_OBJ_NUMBER, "dir",
        "The direction to move, as specified by the cur_dir_t enum.");
    ADD_OPT_ARG(CMDLANG_OBJ_NUMBER, "n",
        "The number of places to move. 1 by default."))
{
    cur_dir_t dir;
    long n;

    dir = args[0].val.number;
    n   = (nargs == 2) ? args[1].val.number : 1;

    buf_move_cur(ctx->buf, dir, n);

    return PALM_OK;
}

VERB(home,
    "Move the cursor to the start of the current line."
    " In line-edit mode, the cursor moves instead to the start of the"
    " line-editing region if it is inside the region.",
    NO_ARGS)
{
    cur_s cur;

    buf_get_cur(ctx->buf, &cur);
    cur.pri.col = 0;
    cur.sec.col = 0;

    TRY(buf_set_cur(ctx->buf, &cur));

    return PALM_OK;
}

VERB(end, "Move the cursor to the end of the current line.",
    NO_ARGS)
{
    cur_s cur;
    size_t llen;

    TRY(buf_get_cur(ctx->buf, &cur));
    llen = buf_line_len(ctx->buf, cur.pri.row);
    buf_move_cur(ctx->buf, CUR_DIR_RIGHT, llen - cur.pri.col);

    return PALM_OK;
}

VERB(pgup,
    "Move the cursor to the top of the visible area of the buffer,"
    " or, if it is already there, scroll the buffer upwards, such that"
    " the cursor is on the last visible line, or as far as the buffer can"
    " scroll.",
    NO_ARGS)
{
    long scrx, scry;
    rect_s dims;
    cur_s cur;
    buf_get_scr(ctx->buf, &scrx, &scry);
    buf_get_cur(ctx->buf, &cur);

    if (cur.pri.row > scry)
    {
        buf_move_cur(ctx->buf, CUR_DIR_UP, cur.pri.row - scry);
    }
    else
    {
        wm_get_dims(&(ctx->buf->editor->wm), buf_get_win(ctx->buf), &dims);
        buf_set_scr(ctx->buf, scrx, MAX(0, cur.pri.row - dims.h + 2));
    }

    return PALM_OK;
}

VERB(pgdn,
    "Move the cursor to the bottom of the visible area of the buffer,"
    " of, if it is already there, scroll the buffer downwards, such that"
    " the cursor is on the first visible line.",
    NO_ARGS)
{
    long scrx, scry;
    rect_s dims;
    cur_s cur;
    buf_s *buf;

    buf = ctx->buf;

    buf_get_scr(buf, &scrx, &scry);
    buf_get_cur(buf, &cur);
    wm_get_dims(&(buf->editor->wm), buf_get_win(buf), &dims);

    if (cur.pri.row < scry + dims.h - 2)
    {
        buf_move_cur(buf, CUR_DIR_DOWN, scry + dims.h - 2 - cur.pri.row);
    }
    else
    {
        buf_set_scr(buf, scrx, cur.pri.row);
    }

    return PALM_OK;
}

/* --- Commands for selecting text --- */

/* TODO: REPLACE WITH ALIAS */
VERB(swap, "Swap the primary and secondary positions of the current cursor.",
    NO_ARGS)
{
    cur_s cur;

    TRY(buf_get_cur(ctx->buf, &cur));
    SWAP(cur.pri, cur.sec);
    TRY(buf_set_cur(ctx->buf, &cur));

    return PALM_OK;
}

/* TODO: REPLACE WITH ALIAS */
VERB(snap, "Snap the secondary position of the current cursor to the primary.",
    NO_ARGS)
{
    cur_s cur;
    TRY(buf_get_cur(ctx->buf, &cur));
    cur.sec = cur.pri;
    TRY(buf_set_cur(ctx->buf, &cur));

    return PALM_OK;
}

VERB(stick,
    "Toggle the cursor's stickiness. If any area is currently selected,"
    " then toggling off stickiness will snap the area.",
    NO_ARGS)
{
    cur_s cur;

    TRY(buf_get_cur(ctx->buf, &cur));

    if (cur.sticky)
    {
        cur.sticky = false;
        cur.sec = cur.pri;
    }
    else
    {
        cur.sticky = true;
    }

    TRY(buf_set_cur(ctx->buf, &cur));

    return PALM_OK;
}

/* --- Misc. Cursor Commands --- */

VERB(cur_type,
    "Toggle the cursor's type. If the cursor is already of the specified"
    " type, it is set to the CHR type.",
    ADD_REQ_ARG(CMDLANG_OBJ_NUMBER, "type",
        "The type to toggle the cursor between, as specified by"
        " cur_type_t."))
{
    cur_s cur;
    cur_type_t prev, new;

    TRY(buf_get_cur(ctx->buf, &cur));

    prev = cur.type;
    switch (args[0].val.number)
    {
    case CUR_TYPE_CHR:   new = CUR_TYPE_LINE;  break;
    case CUR_TYPE_LINE:  new = CUR_TYPE_BLOCK; break;
    case CUR_TYPE_BLOCK: new = CUR_TYPE_CHR;   break;
    default:             new = CUR_TYPE_CHR;   break;
    }

    if (new == prev)
    {
        new = CUR_TYPE_CHR;
    }
    cur.type = new;
    LOG(
        ALERT, INFO,
        "Cursor type switched from %s to %s",
        cur_type_name(prev), cur_type_name(new)
    );

    cur.sec = cur.pri;
    if (cur.type == CUR_TYPE_BLOCK || cur.type == CUR_TYPE_LINE)
        cur.sticky = true;
    else
        cur.sticky = false;

    TRY(buf_set_cur(ctx->buf, &cur));

    return PALM_OK;
}

/** Split Cursor Command.
 *
 * If there are no alt-cursors, split the current cursor into
 * alt-cursors - one on each line in the selection of the cursor.
 * Otherwise, remove all altcursors.
 */
VERB(split_cur,
    "If there are no alternate cursors, split the current cursor into"
    " alternate cursors, one for each line selected by the cursor."
    " Otherwise, remove all alternate cursors.",
    NO_ARGS)
{
    size_t ncurs;
    cur_s cur;
    buf_s    *buf;
    curset_s *curset;

    buf    = ctx->buf;
    curset = &(buf->curset);

    buf_get_cur(buf, &cur);
    ncurs = curset_num_active(curset);
    if (ncurs > 1 && cur.pri.row == cur.sec.row)
    {
        curset_select_ind(curset, 1);
        while (curset_num_active(curset) > 1)
            curset_pop(curset, NULL);
    }
    else
    {
        vec_s alts;

        curset_select_ind(curset, 0);
        while (curset_num_active(curset) > 1)
        {
            curset_pop(curset, &cur);
            curset_push_inactive(curset, &cur);
        }

        vec_init(&alts, sizeof(cur_s));
        while (ncurs--)
        {
            vec_clr(&alts);
            buf_get_cur(buf, &cur);
            cur_split_by_line(&cur, &alts);
            buf_set_cur(buf, &cur);

            VEC_FOREACH(&alts, cur_s *, c)
                curset_push(curset, c);

            if (ncurs > 0)
            {
                curset_pop_inactive(curset, &cur);
                curset_push(curset, &cur);
            }
        }

        curset_select_ind(curset, 0);

        SET_BITS(buf->einfo.attrs, EDIT_ALTS, true);

        vec_kill(&alts);
    }

    return PALM_OK;
}

int cmd_nav_mount(cmd_lib_s *lib)
{
    MOUNT(move);
    MOUNT(home); MOUNT(end);

    MOUNT(pgup); MOUNT(pgdn);

    MOUNT(snap); MOUNT(stick); MOUNT(swap);

    MOUNT(cur_type); MOUNT(split_cur);

    return PALM_OK;
}
