#if !defined(CMDLANG_HELPERS_H)
# define CMDLANG_HELPERS_H
# include "cmdlang/ctx.h"
# include "cmdlang/cmd.h"
# include "cmdlang/obj.h"

#define CMD_REQ_ARG(types) \
    (cmd_arg_spec_s){                         \
        .optional = false,                    \
        .typemask = types \
    }

#define CMD_OPT_ARG(types) \
    (cmd_arg_spec_s){                         \
        .optional = true,                     \
        .typemask = types \
    }

#define ADD_REQ_ARG(types, name, desc) \
    TRY(cmd_add_argument(cmd, name, desc, &CMD_REQ_ARG(types)));

#define ADD_OPT_ARG(types, name, desc) \
    TRY(cmd_add_argument(cmd, name, desc, &CMD_OPT_ARG(types)));

#define MOUNT(name) TRY( \
        mount_ ## name ( \
            cmd_lib_add_cmd( \
                lib, #name, desc_ ## name \
            ) \
        ) \
    )

#define NO_ARGS

#define VERB(_name, _desc, _args) \
    static const char *desc_ ## _name = _desc; \
    static int cmd_ ## _name(cmd_ctx_s *, cmdlang_obj_s *, size_t); \
    static int mount_ ## _name(cmd_s *cmd) \
    { \
        TRY(cmd_set_verb(cmd, &(cmd_verb_info_s){ .cb = cmd_ ## _name })); \
        _args; \
        return PALM_OK; \
    } \
    static int cmd_ ## _name( \
        cmd_ctx_s *ctx, cmdlang_obj_s *args, size_t nargs \
    )

#define NOUN(_name, _desc, _args) \
    static const char *desc_ ## _name = _desc; \
    static int cmd_ ## _name(cmd_ctx_s *, cmdlang_obj_s *, size_t, cmdlang_obj_s *); \
    static int mount_ ## _name(cmd_s *cmd) \
    { \
        TRY(cmd_set_noun(cmd, &(cmd_noun_info_s){ .cb = cmd_ ## _name })); \
        _args; \
        return PALM_OK; \
    } \
    static int cmd_ ## _name( \
        cmd_ctx_s *ctx, cmdlang_obj_s *args, size_t nargs, cmdlang_obj_s *res \
    )

#endif
