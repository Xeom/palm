#include "snow.h"
#include "util/pool.h"

#define REPS 100
#define JOBS 16

struct test_job_spec
{
    int *addr;
    int  val;
    int  sleep;
};

static void test_job(void *arg)
{
    struct test_job_spec *spec;
    spec = arg;

    if (spec->sleep > 0)
    {
        usleep(spec->sleep);
    }

    *(spec->addr) = spec->val;
}

describe(pool)
{
    pool_s pool;

    after_each()
    {
        pool_kill(&pool);
    }

    it ("Handles one job")
    {
        pool_job_s job;
        int i, target;

        pool_init(&pool, 1);
        for (i = 0; i < REPS; ++i)
        {
            struct test_job_spec spec = { &target, i, 0 };
            pool_job_init(&job, test_job, &spec);
            pool_add_job(&pool, &job);
            pool_job_await(&job);
            asserteq(target, i);
            pool_job_kill(&job);
        }
    }

    it ("Handles many jobs in parallel")
    {
        pool_job_s jobs[JOBS];
        int i, j, targets[JOBS];

        pool_init(&pool, JOBS);
        for (i = 0; i < REPS; ++i)
        {
            struct test_job_spec specs[JOBS];
            for (j = 0; j < JOBS; ++j)
            {
                specs[j].addr  = &targets[j];
                specs[j].val   = i + j;
                specs[j].sleep = 0;
                pool_job_init(&jobs[j], test_job, &specs[j]);
                pool_add_job(&pool, &jobs[j]);
            }

            for (j = 0; j < JOBS; ++j)
            {
                pool_job_await(&jobs[j]);
                pool_job_kill(&jobs[j]);
            }

            for (j = 0; j < JOBS; ++j)
            {
                asserteq(targets[j], i + j);
            }
        }
    }

    it ("Handles many jobs")
    {
        pool_job_s jobs[JOBS];
        int i, j, targets[JOBS];

        pool_init(&pool, 1);
        for (i = 0; i < REPS; ++i)
        {
            struct test_job_spec specs[JOBS];
            for (j = 0; j < JOBS; ++j)
            {
                specs[j].addr  = &targets[j];
                specs[j].val   = i + j;
                specs[j].sleep = 0;
                pool_job_init(&jobs[j], test_job, &specs[j]);
                pool_add_job(&pool, &jobs[j]);
            }

            for (j = 0; j < JOBS; ++j)
            {
                pool_job_await(&jobs[j]);
                pool_job_kill(&jobs[j]);
            }

            for (j = 0; j < JOBS; ++j)
            {
                asserteq(targets[j], i + j);
            }
        }
    }

    it ("Orders jobs correctly with one thread")
    {
        pool_job_s jobs[JOBS];
        struct test_job_spec specs[JOBS];
        int j, target;

        pool_init(&pool, 1);
        for (j = 0; j < JOBS; ++j)
        {
            specs[j].addr  = &target;
            specs[j].val   = j;
            specs[j].sleep = 0;
            pool_job_init(&jobs[j], test_job, &specs[j]);
            pool_add_job(&pool, &jobs[j]);
        }

        for (j = 0; j < JOBS; ++j)
        {
            pool_job_await(&jobs[j]);
            pool_job_kill(&jobs[j]);
        }

        asserteq(target, JOBS - 1);
    }

    it ("Does not wait for jobs that haven't been started")
    {
        pool_job_s job;

        pool_init(&pool, JOBS);
        pool_job_init(&job, test_job, NULL);
        pool_job_await(&job);
    }

    it ("Definitely runs jobs all at once (race condition)")
    {
        pool_job_s jobs[JOBS];
        struct test_job_spec specs[JOBS];
        int j, target;

        pool_init(&pool, JOBS);
        for (j = 0; j < JOBS; ++j)
        {
            specs[j].addr  = &target;
            specs[j].val   = j;
            specs[j].sleep = (JOBS - j) * 1000;
            pool_job_init(&jobs[j], test_job, &specs[j]);
            pool_add_job(&pool, &jobs[j]);
        }

        for (j = 0; j < JOBS; ++j)
        {
            pool_job_await(&jobs[j]);
            pool_job_kill(&jobs[j]);
        }

        asserteq(target, 0);
    }
}
// Reuse job

// Ordering with one thread

// Ordering with many threads

