#if !defined(TEXT_DECODER_H)
# define TEXT_DECODER_H
# include "container/vec.h"
# include "types.h"
# include <stdio.h>
# include <stdbool.h>

typedef enum
{
    DECODER_CR       = 0x01,
    DECODER_NL       = 0x02,
    DECODER_ZRO      = 0x04,
    DECODER_UTF8     = 0x08,
    DECODER_STRIPNL  = 0x20,
    DECODER_FNT      = 0x40,
    DECODER_ALWAYSNL = 0x80,
    DECODER_DEFAULT  = DECODER_NL | DECODER_UTF8 | DECODER_STRIPNL
} decoder_attr_t;

typedef enum
{
    DECODER_CHR_NL,
    DECODER_CHR_VALID,
    DECODER_CHR_INCOMPLETE,
    DECODER_CHR_INVALID,
    DECODER_CHR_FONT,
    DECODER_CHR_EOF
} decoder_chrtype_t;

struct decoder
{
    vec_s chrbuf;
    decoder_attr_t attrs;
    short font;
};

void decoder_init(decoder_s *decoder, decoder_attr_t attrs);

void decoder_kill(decoder_s *decoder);

bool decoder_chr(decoder_s *decoder, int chr, vec_s *line);

void decoder_str(decoder_s *decoder, const char *str, vec_s *result);

#endif
