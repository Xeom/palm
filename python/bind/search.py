from lib           import lib
from util.keys     import Keys
from bind.mode     import Mode, PyCommand
from server.server import add_global
from buffers.grep  import GrepBuffer

from core.search import Search
from core.pos    import Pos
from core.log    import log
from core.cur    import Cur

import libtypes

@add_global(args=["buf"])
def search_one(buf, string, attrs=0, b=False, off=None, loop=True):
    search = Search()
    search.add_needle(string, attrs)

    if len(string) == 0:
        return search_continue(buf, loop)

    if off == None:
        off = buf.get_cur().pri

    search.start(buf, off, backward=b)

    res = search.get_result()

    if res == None and loop:
        if b:
            log.str("ALERT", "INFO", "Searching from end of buffer")
            search.start(buf, buf.last_pos(), backward=True)
        else:
            log.str("ALERT", "INFO", "Searching from start of buffer")
            search.start(buf, Pos.at(0, 0), backward=False)

        res = search.get_result()

    if res == None:
        log.str("ALERT", "INFO", "No such string found")
    else:
        log.str("ALERT", "SUCC", "Found '{}'".format(string))
        buf.set_cur(res.get_cur())

    buf.search = search

    return res

@add_global(args=["buf"])
def search_continue(buf, loop=True):
    if not hasattr(buf, "search") or buf.search == None:
        raise Exception("No search was started")

    search = buf.search

    search.cont()
    res = search.get_result()
    if res == None and loop:
        if search.backward:
            log.str("ALERT", "INFO", "Searching from end of buffer")
            search.start(buf, buf.last_pos(), backward=True)
        else:
            log.str("ALERT", "INFO", "Searching from start of buffer")
            search.start(buf, Pos.at(0, 0))

        res = search.get_result()

    if res == None:
        log.str("ALERT", "INFO", "No such string found")
    else:
        log.str("ALERT", "SUCC", "Found next occurrence")
        buf.set_cur(res.get_cur())

    buf.search = search

    return res

@add_global(args=["buf"])
def select_surrounding(buf, left, right, attrs=0, incl=True):
    forwardsearch = Search()
    backsearch    = Search()

    forwardsearch.add_needle(right, attrs=attrs)
    backsearch.add_needle(left, attrs=attrs)

    off = buf.get_cur().pri

    forwardsearch.start(buf, off)
    backsearch.start(buf, off, backward=True)

    forwardres = forwardsearch.get_result()
    backres    = backsearch.get_result()

    if forwardres is None or backres is None:
        log.str("ALERT", "ERR", "Could not find matching text")
    else:
        if incl:
            cur = Cur.at(backres.start, forwardres.end)
        else:
            cur = Cur.at(backres.end, forwardres.start)

        if cur.sec.col > cur.pri.col:
            cur.sec.col -= 1

        cur.sticky = True

        buf.set_cur(cur)

@add_global(args=["buf"])
def select_brackets(buf, left, right, attrs=0, incl=True):
    """A hacky but effective implementation of a bracket selector."""
    forwardsearch = Search()
    backsearch    = Search()

    forwardsearch.add_needle(right, flag="+", attrs=attrs)
    forwardsearch.add_needle(left,  flag="-", attrs=attrs)

    backsearch.add_needle(right, flag="-", attrs=attrs)
    backsearch.add_needle(left,  flag="+", attrs=attrs)

    off = buf.get_cur().pri
    off.col += 1

    forwardsearch.start(buf, off)
    backsearch.start(buf, off, backward=True)

    cnt = 0

    forwardres = forwardsearch.get_result()
    while forwardres != None:
        cnt += {b"+": 1, b"-": -1}[forwardres.flag]
        if cnt == 1:
            break
        forwardsearch.cont()
        forwardres = forwardsearch.get_result()

    cnt = 0

    backres = backsearch.get_result()
    while backres != None:
        cnt += {b"+": 1, b"-": -1}[backres.flag]
        if cnt == 1:
            break

        backsearch.cont()
        backres = backsearch.get_result()

    if backres == None or forwardres == None:
        log.str("ALERT", "ERR", "Could not find matching text")
        return None

    if incl:
        cur = Cur.at(backres.start, forwardres.end)
    else:
        cur = Cur.at(backres.end, forwardres.start)

    cur.contract(1, buf)
    cur.sticky = True

    buf.set_cur(cur)


Mode.add_default_cmds([
    PyCommand("Start text search",  "/",  "single_pyprompt_param('search_one(r\"', '\")')"),
    PyCommand("Next search result", "?",  "search_continue()"),
    PyCommand("Search files",       "f/", "single_pyprompt_param('rgrep(r\"', '\")')"),
    PyCommand("Goto position",      "gg",  "single_pyprompt_param('goto(', ')')", shortcut=Keys.ctrl("g")),
    PyCommand("Goto end of file",   "gJ",  "goto(buf.last_pos())"),
    PyCommand("Goto end of file",   "gK",  "goto(0, 0)"),
    PyCommand("Select word",        "sw",  "select_surrounding(r'\s|^', r'\s|$', 'r', incl=False)")
])

for name,             cmd,    lchr,  rchr,  attr in (
    ("single-quotes", ["'"],  "'",   "'",   0),
    ("double-quotes", ["\""], "\"",  "\"",  0),
    ("quotes",        ["`"],  "`",   "'",   0),
    ("c-comment",     ["/*"], "/*",   "*/", 0),
    ("star",          ["*"],  "*",   "*",   0),
    ("underscores",   ["_"],  "_",   "_",   0),
    ("sentence",      ["."],  "[A-Z][a-z]+|^$", "\\.|^$", ["REGEX", "BEYONDOFF"])
):
    for c in cmd:
        Mode.add_default_cmds([
            PyCommand(
                f"Select {name}", f"s{c}",
                f"select_surrounding({repr(lchr)}, {repr(rchr)}, attrs={repr(attr)})",
                multicursor=True
            ),
            PyCommand(
                f"Select in {name}", f"si{c}",
                f"select_surrounding({repr(lchr)}, {repr(rchr)}, attrs={repr(attr)}), incl=False)",
                multicursor=True
            )
        ])

for name,            cmd,        lchr, rchr, attr in (
    ("brackets",     ["[", "]"], "[",  "]",  0),
    ("parens",       ["(", ")"], "(",  ")",  0),
    ("braces",       ["{", "}"], "{",  "}",  0),
    ("angles",       ["<", ">"], "<",  ">",  0),
    ("tex-begin",    ["beg"],    r"\\begin\{\w+\}", r"\\end\{\w+\}", ["REGEX", "BEYONDOFF"])
):
    for c in cmd:
        Mode.add_default_cmds([
            PyCommand(
                f"Select {name}", f"s{c}",
                f"select_brackets({repr(lchr)}, {repr(rchr)}, attrs={repr(attr)})",
                multicursor=True
            ),
            PyCommand(
                f"Select in {name}", f"si{c}",
                f"select_brackets({repr(lchr)}, {repr(rchr)}, attrs={repr(attr)}, incl=False)",
                multicursor=True
            )
        ])
