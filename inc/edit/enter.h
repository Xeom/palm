#if !defined(EDIT_ENTER_H)
# define EDIT_ENTER_H
# include "edit/edit.h"

void edit_break_line_at(edit_info_s *einfo, pos_s *pos);

void edit_break_column_at(edit_info_s *einfo, pos_s *pos);

void edit_enter(edit_info_s *einfo);

void edit_preline_enter(edit_info_s *einfo);

#endif
