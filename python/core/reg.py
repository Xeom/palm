from lib import lib
import ctypes
from .vec import Vec

def push(s="", ind=0):
    if isinstance(s, str):
        s = s.encode("utf-8")

    vec = Vec(ctypes.c_char)
    vec.ins(0, len(s), s)

    if lib.reg_push(vec.ptr, ind) != 0:
        raise Exception("reg_push failed")

def pop(ind=0):
    vec = Vec(ctypes.c_char)
    if lib.reg_pop(vec.ptr, ind) != 0:
        raise Exception("reg_pop failed")

    return bytes(vec)

def set(s="", ind=0):
    if isinstance(s, str):
        s = s.encode("utf-8")

    vec = Vec(ctypes.c_char)
    vec.ins(0, len(s), s)

    if lib.reg_set(vec.ptr, ind) != 0:
        raise Exception("reg_set failed")


def get(ind=0):
    vec = Vec(ctypes.c_char)
    if lib.reg_get(vec.ptr, ind) != 0:
        raise Exception("reg_get failed")

    return bytes(vec)

def chr(c, ind=0):
    if lib.reg_chr(c, ind) != 0:
        raise Exception("reg_chr failed")

def num():
    return lib.reg_num()
