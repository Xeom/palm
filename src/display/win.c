typedef enum win_split_t
{
    WIN_SPLIT_NONE = 0,
    WIN_SPLIT_H,
    WIN_SPLIT_V
};

struct win
{
    win_s *parent;
    win_s *child1, *child2;
    framebuf_s fbuf;
    win_split_t type;
    float adj;
    int bufid;
}

struct wm
{
    pthread_mutex_t mtx;
    vec_s windows;
    vec_s buffers;
    win_s root;
    int winsel;
}

void wm_init(wm_s *wm)
{
    memset(&(wm->root), 0, sizeof(win_s));
}

void wm_split(wm_s *wm, int id, win_split_t dir)
{
    
}

void wm_kill(wm_s *wm)

void win_split(win_s *w

void win_get_dims(
