#include <ctype.h>
#include <errno.h>
#include <string.h>

#include "util/string.h"
#include "log/log.h"

#include "cmdlang/lib.h"
#include "cmdlang/cmd.h"
#include "cmdlang/obj.h"

#include "cmdlang/lexer.h"

static int emit(
    cmdlang_lexer_s *lexer, cmdlang_tok_typ_t typ, errmsg_t errmsg
) DONT_IGNORE;
static int clear_sval(
    cmdlang_tok_s *tok, errmsg_t errmsg
) DONT_IGNORE;
static int append_char(
    cmdlang_tok_s *tok, char c, errmsg_t errmsg
) DONT_IGNORE;

static int cmdlang_lex_idle(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
) DONT_IGNORE;
static int cmdlang_lex_number(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
) DONT_IGNORE;
static int cmdlang_lex_str(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
) DONT_IGNORE;
static int cmdlang_lex_short_cmd(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
) DONT_IGNORE;
static int cmdlang_lex_cmd(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
) DONT_IGNORE;
static int cmdlang_lex_param(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
) DONT_IGNORE;

void cmdlang_tok_init(cmdlang_tok_s *tok)
{
    memset(tok, 0, sizeof(cmdlang_tok_s));
    vec_init(&(tok->sval), sizeof(char));
}

void cmdlang_tok_kill(cmdlang_tok_s *tok)
{
    vec_kill(&(tok->sval));

    if (tok->typ == CMDLANG_TOK_OBJ)
    {
        cmdlang_obj_kill(&(tok->obj));
    }
}

void cmdlang_tok_cpy(cmdlang_tok_s *tok, cmdlang_tok_s *src)
{
    tok->typ = src->typ;

    switch (tok->typ)
    {
    case CMDLANG_TOK_NUMBER:
    case CMDLANG_TOK_PARAM:
        tok->ival = src->ival;
        break;

    case CMDLANG_TOK_STR:
        vec_clr(&(tok->sval));
        vec_cpy(&(tok->sval), &(src->sval));
        break;

    case CMDLANG_TOK_CMD:
    case CMDLANG_TOK_SHORT_CMD:
       tok->cval = src->cval;
       break;

    case CMDLANG_TOK_OBJ:
        cmdlang_obj_init_cpy(&(tok->obj), &(src->obj));
        break;
    }
}

int cmdlang_tok_is_verb(cmdlang_tok_s *tok, cmd_lib_s *lib, bool *verb)
{
    if (tok->typ != CMDLANG_TOK_SHORT_CMD &&
        tok->typ != CMDLANG_TOK_CMD)
    {
        *verb = false;
        return PALM_OK;
    }

    return cmd_is_verb(tok->cval, lib, verb);
}

static int emit(cmdlang_lexer_s *lexer, cmdlang_tok_typ_t typ, errmsg_t errmsg)
{
    cmdlang_tok_s *tok;
    tok = &(lexer->tok);

    tok->typ = typ;

    if (typ == CMDLANG_TOK_NUMBER || typ == CMDLANG_TOK_PARAM)
    {
        long res;
        char *str;
        vec_app(&(tok->sval), NULL);
        str = vec_get(&(tok->sval), 0);

        if (str_to_int(str, &res, NULL) == -1)
        {
            ERRNO_ERRMSG(
                errmsg,
                "Could not understand number '%s'", str
            );
            return PALM_ERR;
        }

        tok->ival = res;
    }
    else if (typ == CMDLANG_TOK_SHORT_CMD)
    {
        tok->cval = cmd_lib_find_shortname(
            lexer->lib,
            vec_get(&(tok->sval), 0),
            vec_len(&(tok->sval))
        );

        if (!tok->cval)
        {
            ERRMSG(
                errmsg,
                "Could not find short command '%.*s'",
                (int)vec_len(&(tok->sval)), (char *)vec_get(&(tok->sval), 0)
            );
            return PALM_ERR;
        }
    }
    else if (typ == CMDLANG_TOK_CMD)
    {
        tok->cval = cmd_lib_find_name(
            lexer->lib,
            vec_get(&(tok->sval), 0),
            vec_len(&(tok->sval))
        );

        if (!tok->cval)
        {
            printf("%lu", vec_len(&(tok->sval)));
            ERRMSG(
                errmsg,
                "Could not find command '%.*s'",
                (int)vec_len(&(tok->sval)), (char *)vec_get(&(tok->sval), 0)
            );
            return PALM_ERR;
        }
    }

    return PALM_OK;
}

static int clear_sval(cmdlang_tok_s *tok, errmsg_t errmsg)
{
    vec_clr(&(tok->sval));
    return PALM_OK;
}

static int append_char(cmdlang_tok_s *tok, char c, errmsg_t errmsg)
{
    if (!vec_app(&(tok->sval), &c))
    {
        ERRMSG(errmsg, "Could not append character to token");
        return PALM_ERR;
    }

    return PALM_OK;
}

static int cmdlang_lex_idle(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
)
{
    if (isspace(c) || c == '\0')
    {
        *next = true;
    }
    else if (isdigit(c) || c == '-' || c == '+')
    {
        TRY(clear_sval(&(lexer->tok), errmsg));
        lexer->state = CMDLANG_LEXER_NUMBER;
        *next = false;
    }
    else if (c == '"')
    {
        escape_init(&(lexer->escstate));

        TRY(clear_sval(&(lexer->tok), errmsg));

        lexer->state = CMDLANG_LEXER_STR;
        *next = true;
    }
    else if (c == ':')
    {
        TRY(clear_sval(&(lexer->tok), errmsg));

        lexer->state = CMDLANG_LEXER_CMD;
        *next = true;
    }
    else if (c == '#')
    {
        TRY(clear_sval(&(lexer->tok), errmsg));

        lexer->state = CMDLANG_LEXER_PARAM;
        *next = true;
    }
    else
    {
        TRY(clear_sval(&(lexer->tok), errmsg));

        lexer->state = CMDLANG_LEXER_SHORT_CMD;
        *next = false;
    }

    return PALM_OK;
}

static int cmdlang_lex_number(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
)
{
    if (isdigit(c) || ((c == '-' || c == '+') && vec_len(&(lexer->tok.sval)) == 0))
    {
        *next = true;

        TRY(append_char(&(lexer->tok), c, errmsg));
    }
    else
    {
        lexer->state = CMDLANG_LEXER_IDLE;
        *next = false;

        TRY(emit(lexer, CMDLANG_TOK_NUMBER, errmsg));
    }

    return PALM_OK;
}

static int cmdlang_lex_str(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
)
{
    if (c == '\0')
    {
        ERRMSG(errmsg, "String terminated without closing \"");
        return PALM_ERR;
    }
    else if (c == '"' && lexer->escstate.state != ESCAPE_ESCAPED)
    {
        int res;

        do {
            res = escape_chr(&(lexer->escstate), -1, next);

            if (res != -1)
            {
                TRY(append_char(&(lexer->tok), (char)res, errmsg));
            }
        } while (!(*next));

        lexer->state = CMDLANG_LEXER_IDLE;
        return emit(lexer, CMDLANG_TOK_STR, errmsg);
    }
    else
    {
        int res;
        res = escape_chr(&(lexer->escstate), c, next);

        if (res != -1)
            return append_char(&(lexer->tok), (char)res, errmsg);
    }

    return PALM_OK;
}

static int cmdlang_lex_short_cmd(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
)
{
    if (isspace(c) || isdigit(c) || c == ':' || c == '"' || c == '\0' || c == '-' || c == '+')
    {
        lexer->state = CMDLANG_LEXER_IDLE;
        *next = false;
        TRY(emit(lexer, CMDLANG_TOK_SHORT_CMD, errmsg));
    }
    else
    {
        *next = true;
        TRY(append_char(&(lexer->tok), c, errmsg));

        if(cmd_lib_find_shortname(
                lexer->lib,
                vec_get(&(lexer->tok.sval), 0),
                vec_len(&(lexer->tok.sval))
            ))
        {
            lexer->state = CMDLANG_LEXER_IDLE;
            TRY(emit(lexer, CMDLANG_TOK_SHORT_CMD, errmsg));
        }
    }

    return PALM_OK;
}

static int cmdlang_lex_cmd(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
)
{
    if (isspace(c) || c == ':')
    {
        lexer->state = CMDLANG_LEXER_IDLE;
        *next = false;
        TRY(emit(lexer, CMDLANG_TOK_CMD, errmsg));
    }
    else
    {
        *next = true;
        TRY(append_char(&(lexer->tok), c, errmsg));
    }

    return PALM_OK;
}

static int cmdlang_lex_param(
    cmdlang_lexer_s *lexer, char c, bool *next, errmsg_t errmsg
)
{
    if (isdigit(c))
    {
        *next = true;
        TRY(append_char(&(lexer->tok), c, errmsg));
    }
    else
    {
        lexer->state = CMDLANG_LEXER_IDLE;
        *next = false;

        TRY(emit(lexer, CMDLANG_TOK_PARAM, errmsg));
    }

    return PALM_OK;
}

int cmdlang_lexer_init(cmdlang_lexer_s *lexer, cmd_lib_s *lib, errmsg_t errmsg)
{
    lexer->state = CMDLANG_LEXER_IDLE;
    lexer->tok.typ = CMDLANG_TOK_NONE;
    lexer->lib     = lib;
    cmdlang_tok_init(&(lexer->tok));

    return PALM_OK;
}

void cmdlang_lexer_kill(cmdlang_lexer_s *lexer)
{
    cmdlang_tok_kill(&(lexer->tok));
}

int cmdlang_lexer_token(
    cmdlang_lexer_s *lexer, cmdlang_tok_s *tok, errmsg_t errmsg
)
{
    if (lexer->tok.typ == CMDLANG_TOK_NONE)
        return PALM_OK;

    tok->typ  = lexer->tok.typ;
    tok->ival = lexer->tok.ival;
    tok->cval = lexer->tok.cval;

    vec_clr(&(tok->sval));
    vec_cpy(&(tok->sval), &(lexer->tok.sval));

    return PALM_COMPLETE;
}

int cmdlang_lexer_str_to_toks(
    cmdlang_lexer_s *lexer,
    const char *str,
    vec_s *toks,
    errmsg_t errmsg
)
{
    while (1)
    {
        bool next;
        cmdlang_tok_s tok;

        cmdlang_tok_init(&tok);

        TRY(cmdlang_lexer_chr(lexer, *str, &next, errmsg));

        switch (cmdlang_lexer_token(lexer, &tok, errmsg))
        {
        case PALM_COMPLETE:
            if (!vec_app(toks, &tok))
            {
                cmdlang_tok_kill(&tok);
                ERRMSG(errmsg, "Could not allocate space for token.");
                return PALM_ERR;
            }
            break;

        case PALM_OK:
            cmdlang_tok_kill(&tok);
            break;

        case PALM_ERR:
            cmdlang_tok_kill(&tok);
            return PALM_ERR;
        }

        if (next)
        {
            if (*str) ++str;
            else      break;
        }

    }

    return PALM_OK;
}

int cmdlang_lexer_chr(
    cmdlang_lexer_s *lexer,
    char c,
    bool *next,
    errmsg_t errmsg
)
{
    *next = true;
    lexer->tok.typ = CMDLANG_TOK_NONE;

    switch (lexer->state)
    {
    case CMDLANG_LEXER_IDLE:
        TRY(cmdlang_lex_idle(lexer, c, next, errmsg));
        break;
    case CMDLANG_LEXER_NUMBER:
        TRY(cmdlang_lex_number(lexer, c, next, errmsg));
        break;
    case CMDLANG_LEXER_STR:
        TRY(cmdlang_lex_str(lexer, c, next, errmsg));
        break;
    case CMDLANG_LEXER_SHORT_CMD:
        TRY(cmdlang_lex_short_cmd(lexer, c, next, errmsg));
        break;
    case CMDLANG_LEXER_CMD:
        TRY(cmdlang_lex_cmd(lexer, c, next, errmsg));
        break;
    case CMDLANG_LEXER_PARAM:
        TRY(cmdlang_lex_param(lexer, c, next, errmsg));
        break;
    default:
        ERRMSG(errmsg, "cmdlang parser in unknown state.");
        return PALM_ERR;
    }

    if (c == '\0' && lexer->state != CMDLANG_LEXER_IDLE)
    {
        ERRMSG(
            errmsg,
            "Lexer should be in idle state after '\\0', not %d",
            lexer->state
        );
        return PALM_ERR;
    }

    return PALM_OK;
}
