#include "edit/editinfo.h"

void edit_info_init(edit_info_s *einfo, buf_s *buf)
{
    memset(einfo, '\0', sizeof(*einfo));

    einfo->buf  = buf;
    einfo->mode = EDIT_MODE_NORMAL;

    einfo->shiftwidth = +4;

    decoder_init(&(einfo->insdecoder), DECODER_DEFAULT);
    encoder_init(&(einfo->cpyencoder), DECODER_DEFAULT);
}

void edit_info_kill(edit_info_s *einfo)
{
    decoder_kill(&(einfo->insdecoder));
}

void edit_set_decoder_attrs(edit_info_s *einfo, decoder_attr_t attrs)
{
    decoder_kill(&(einfo->insdecoder));

    decoder_init(&(einfo->insdecoder), attrs);
    encoder_init(&(einfo->cpyencoder), attrs);
}
