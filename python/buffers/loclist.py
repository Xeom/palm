from typing import Optional

from buffers.menu     import MenuItem, MenuBuffer
from buffers.pathinfo import PathInfoBuffer

from core.buffer import Buffer
from core.cur    import Cur

from server.server import add_global
from core.decoder  import fmt_sanitize
from core.font     import Font

def fmt_path(path: str):
    return "{path}" + fmt_sanitize(path).replace("/", "{path-slash}/{path}") + "{default}"

@add_global()
class Location(object):
    """A class represening a location.

    The usage of this class will likely be expanded later to include
    search, and may get a C implementaion.
    """
    maxpathlen = 25
    numwidth   = 6

    def __init__(self, path: str, row: Optional[int]=None, col: Optional[int]=None):
        """Initialize this Location.

        Arguments:
            path - The path of the location.
            row  - If not None, the row of the location.
            col  - If not None, the column of the location.
        """
        self.path = path
        self.row  = row
        self.col  = col

    def print_path(self) -> str:
        """Pretty print the path of the location.

        The output is formatted in the ["UTF8", "FNT"]
        encoding.
        """
        if len(self.path) > self.maxpathlen:
            return "{faded}.." + fmt_path(self.path[-(self.maxpathlen - 2):])
        else:
            return fmt_path(self.path.rjust(self.maxpathlen))

    def __str__(self) -> str:
        """Pretty print this location.

        The output is formatted in the ["UTF8", "FNT"]
        encoding.
        """
        return (
            f"{self.print_path()}" +
            "{linenum}" +
            f" L{self.row}".rjust(self.numwidth) if self.row is not None else "" +
            f" C{self.col}".rjust(self.numwidth) if self.col is not None else "" +
            "{default}"
        )

    def goto(self, editor):
        """Go to this location."""
        buf = editor.get_buf_editing_path(self.path, switch=True)
        cur = Cur.at(self.row, self.col)
        buf.set_cur(cur)

    def show_info(self, editor):
        """Show file info about this location."""
        buf = editor.popup_buf(PathInfoBuffer)
        buf.show_path(self.path)

@add_global()
class LocListItem(MenuItem):
    """An item in a LocListBuffer."""

    def __init__(self, buf: MenuBuffer, loc: Location, cont: str, parent=None):
        """Initialize an instance of this class.

        Arguments:
            buf    - The buffer this is an item in.
            loc    - The location we want to display.
            cont   - Whatever content we want to display in the line
                     alongside the location.
            parent - If not None, the parent item of this item.
        """
        super().__init__(buf, parent=parent)
        self.loc = loc
        self.linecont = cont
        self.set_content(self.gen_content())

    def gen_content(self) -> str:
        """Generate the content of the line."""
        cont = (
            str(self.loc) +
            "{faded}: {default}" + fmt_sanitize(self.linecont)
        )

        return cont

    def enter(self):
        """Handle enter being pressed on this item.

        Enter will cause us to go to the location of this item.
        """
        editor = self.buf.editor
        self.loc.goto(editor)

    def keypress(self, key: bytes):
        """Handle keypresses."""

        if key == b"i":
            self.loc.show_info(self.buf.editor)

@add_global()
class LocListBuffer(MenuBuffer):
    """A buffer for containing a list of locations."""

    def add_location(self, loc: Location, cont: str):
        """Add a location."""
        self.append_item(LocListItem(self, loc, cont))

    def add_dummy_content(self, cont: str):
        """Add content without a location."""
        self.append_item(MenuItem(self, cont))

