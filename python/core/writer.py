from typing import Union

from util.encode    import bytify
from server.globals import add_global

from lib import lib

from .newstruct import CStructure

@add_global()
class Writer(CStructure):
    init_funct = lib.writer_init
    kill_funct = lib.writer_kill

    class_name = "writer_s"

    def init_args(self, args, kwargs):
        return [args[0].ptr, args[1]]

    def write(self, data: Union[str, bytes]):
        data = bytify(data)
        lib.writer_write(self.ptr, len(data), data)
