# .------------------------------.
# | An example ~/.palmrc.py file |
# '------------------------------'

import os
import re

from server.server import add_global
from hooks import hooks
from editor import log

# Highlight .c and .h files
@hooks.attach("buf-post-assoc")
def handle_assoc(buf):
    base = buf.finfo.paths.get_base()

    syntax_associations = {
        ".*\\.py":                  ["py"],
        ".*\\.([ch](pp)?|cc|hh)":   ["c"],
        ".*\\.json":                ["json"],
        ".*\\.(tex|cls)":           ["tex"],
#       ".*\\.mk|Makefile(\\..*)?": ["make"],
#       ".*\\.yaml":                ["yaml"],
        ".*\\.md":                  ["md"],
    }

    for regex, hl in syntax_associations.items():
        match = re.match(regex, base)
        if match and match.end() == len(base):
            buf.set_defhl(hl)
            break


# FILE TEMPLATES
# --------------

def get_c_file_name_components(buf):
    terminators = [
        "inc", "src",
        "include", "source",
        "h", "c",
        "hpp", "cpp"
    ]

    path = buf.finfo.path
    path, fname = os.path.split(path)

    rtn = [fname.rsplit(".")[0]]
    while os.path.basename(path) not in terminators and path:
        path, comp = os.path.split(path)
        if not comp:
            break
        rtn.insert(0, comp)

    if not path:
        return [rtn[-1]]

    return rtn

def apply_template(buf, template, force=False):
    if not force and not buf.is_empty():
        log.str("ALERT", "ERR", "Buffer is not empty. Set force=True to override.")
        return

    name = get_c_file_name_components(buf)

    fmtargs = {
        "name/": "/".join(name),
        "name": "_".join(name),
        "NAME": "_".join(c.upper() for c in name),
        "path": buf.finfo.path,
        "base": buf.finfo.paths.base,
        "BASE": buf.finfo.paths.base.upper(),
        "dir":  buf.finfo.paths.dir
    }

    lines = [l.format(**fmtargs) for l in template]

    buf.set(0, lines)

@add_global(args=["buf"])
def gen_cfile(buf, force=False):
    apply_template(buf,
        [
            "#include \"{name/}.h\"",
            ""
        ]
    )

@add_global(args=["buf"])
def gen_hfile(buf, force=False):
    apply_template(buf,
        [
            "#if !defined({NAME}_H)",
            "# define {NAME}_H",
            "",
            "#endif",
            ""
        ],
        force=force
    )

@add_global(args=["buf"])
def gen_texfile(buf, force=False):
    apply_template(buf,
        [
            "% __ {base} __",
            "\\documentclass{{}}",
            "",
            "\\title{{}}",
            "",
            "\\begin{{document}}",
            "\\maketitle",
            "",
            "\\section*{{}}",
            ""
            "\\end{{document}}",
            ""
        ],
        force=force
    )

@hooks.attach("buf-post-read")
def handle_read(buf):
    path = buf.finfo.paths

    if not buf.finfo.newfile:
        return

    template_associations = {
        ".*\\.h":   gen_hfile,
        ".*\\.c":   gen_cfile,
        ".*\\.tex": gen_texfile
    }

    for regex, funct in template_associations.items():
        match = re.match(regex, path.get_base())
        if match and match.end() == len(path.get_base()):
            funct(buf)
            break
