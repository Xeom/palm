#include <signal.h>
#include <math.h>
#include "event/loop.h"
#include "event/event.h"
#include "util/time.h"
#include "util/macro.h"
#include "event/timer.h"
#include "event/poll.h"
#include "log/log.h"

double event_max_delay = 0.1;

static const event_s event_idle = {
    .spec = { .typ = EVENT_IDLE }
};

void event_loop(event_ctx_s *eventctx, poll_ctx_s *pollctx, timer_ctx_s *timerctx)
{
    double now, expiry, delay;
    event_s *event;

    event = event_get_next(eventctx);

    if (!event)
    {
        event_push(eventctx, &event_idle);
        event = event_get_next(eventctx);
    }

    while (event)
    {
        event_handle(eventctx, event);
        event = event_get_next(eventctx);
    }

    expiry = timer_ctx_get_expiry(timerctx);
    now    = util_time_fsec();

    if (isnan(expiry))
    {
        delay = event_max_delay;
    }
    else
    {
        delay  = expiry - now;
        delay  = MAX(MIN(delay, event_max_delay), 0.0);
    }

    switch (poll_wait(pollctx, (int)(delay * 1000)))
    {
    case 1:
        poll_ctx_handle(pollctx);
        timer_ctx_handle(timerctx);
        break;
    case 0:
        timer_ctx_handle(timerctx);
        poll_ctx_handle(pollctx);
        break;
    case -1:
        timer_ctx_handle(timerctx);
        break;
    }
}
