#include "snow.h"
#include "bind/bind.h"
#include "bind/mode.h"

static vec_s results;
/*
static void bind_f(vec_s *keys, buf_s *buf, void *arg, size_t len)
{
    vec_add(&results, len, arg);
}*/

describe(bind)
{
    before_each()
    {
        vec_init(&results, sizeof(char));
    }

    after_each()
    {
        vec_kill(&results);
    }

#if 0
    it ("Runs C-bindings")
    {
        vec_s avec, bvec, cvec;
        bind_s abind, bbind, cbind;

        vec_init(&avec, sizeof(char));
        vec_init(&bvec, sizeof(char));
        vec_init(&cvec, sizeof(char));

        vec_str(&avec, "A");
        vec_str(&bvec, "B");
        vec_str(&cvec, "C");

        bind_init(&abind, NULL, bind_f, &avec, false);
        bind_init(&bbind, NULL, bind_f, &bvec, false);
        bind_init(&cbind, NULL, bind_f, &cvec, false);

        vec_kill(&avec);
        vec_kill(&bvec);
        vec_kill(&cvec);

        bind_trigger(&cbind, NULL, NULL);
        bind_trigger(&bbind, NULL, NULL);
        bind_trigger(&abind, NULL, NULL);
        bind_trigger(&bbind, NULL, NULL);
        bind_trigger(&cbind, NULL, NULL);

        asserteq(vec_len(&results), 5);
        asserteq_buf(vec_get(&results, 0), "CBABC", 5);

        bind_kill(&abind);
        bind_kill(&bbind);
        bind_kill(&cbind);
    }


    it ("Doesn't run when not bound")
    {
        bind_s bind;
        bind_init(&bind, NULL, NULL, NULL, false);
        bind_trigger(&bind, NULL, NULL);

        asserteq(vec_len(&results), 0);

        bind_kill(&bind);
    }
#endif
}

describe(mode)
{
    mode_s *mode;

    it ("Initializes correctly")
    {
        mode = NULL;
        mode = mode_get(NULL, "Meow");

        asserteq(mode->name, "Meow");
        asserteq(mode->next, mode);

        mode_kill(mode);
    }

    before_each()
    {
        mode = mode_get(NULL, "mode1");
        vec_init(&results, sizeof(char));
    }

    after_each()
    {
        vec_kill(&results);
        mode_kill(mode);
    }

    it ("Creates new modes")
    {
        mode_s *mode2;
        mode_s *mode3;

        mode2 = mode_get(mode, "mode2");
        mode3 = mode_get(mode, "mode3");

        assertneq(mode, mode2);
        assertneq(mode, mode3);
        assertneq(mode2, NULL);
        assertneq(mode3, NULL);
    }

    it ("Can get any mode from any other")
    {
        mode_s *mode2;
        mode_s *mode3;

        mode2 = mode_get(mode, "mode2");
        mode3 = mode_get(mode, "mode3");

        asserteq(mode,  mode_get(mode, "mode1"));
        asserteq(mode2, mode_get(mode, "mode2"));
        asserteq(mode3, mode_get(mode, "mode3"));

        asserteq(mode,  mode_get(mode2, "mode1"));
        asserteq(mode2, mode_get(mode2, "mode2"));
        asserteq(mode3, mode_get(mode2, "mode3"));

        asserteq(mode,  mode_get(mode3, "mode1"));
        asserteq(mode2, mode_get(mode3, "mode2"));
        asserteq(mode3, mode_get(mode3, "mode3"));
    }

#if 0
/* THESE TESTS USE AN OLDER API. The code is rewritten. */
    it ("Can make and trigger bindings")
    {
        vec_s keys;
        vec_s karg, aarg;
        vec_init(&keys, sizeof(char));
        vec_init(&karg, sizeof(char));
        vec_init(&aarg, sizeof(char));

        vec_str(&karg, "1"); vec_str(&aarg, "A");

        mode_bind(mode, &karg, bind_f, &aarg, false);

        vec_clr(&karg); vec_clr(&aarg);
        vec_str(&karg, "2"); vec_str(&aarg, "B");

        mode_bind(mode, &karg, bind_f, &aarg, false);

        vec_kill(&karg);
        vec_kill(&aarg);

        vec_str(&keys, "1");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "2");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "1");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "2");
        mode_keys(mode, &keys, NULL);

        asserteq(vec_len(&results), 4);
        asserteq_buf(vec_get(&results, 0), "ABAB", 4);

        vec_kill(&keys);
    }

    it ("Can make and trigger long bindings")
    {
        vec_s keys;
        vec_s karg,aarg;
        vec_init(&keys, sizeof(char));
        vec_init(&karg, sizeof(char));
        vec_init(&aarg, sizeof(char));

        vec_str(&karg, "cow"); vec_str(&aarg, "A");

        mode_bind(mode, &karg, bind_f, &aarg, false);

        vec_clr(&karg); vec_clr(&aarg);
        vec_str(&karg, "cat"); vec_str(&aarg, "B");

        mode_bind(mode, &karg, bind_f, &aarg, false);

        vec_kill(&karg);
        vec_kill(&aarg);

        vec_str(&keys, "c");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "o");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "w");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "c");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "a");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "t");
        mode_keys(mode, &keys, NULL);

        asserteq(vec_len(&keys), 0);
        asserteq(vec_len(&results), 2);
        asserteq_buf(vec_get(&results, 0), "AB", 2);
        vec_kill(&keys);
    }

    it ("Can trigger None bindings")
    {
        vec_s keys;
        vec_s karg, aarg;
        vec_init(&keys, sizeof(char));
        vec_init(&karg, sizeof(char));
        vec_init(&aarg, sizeof(char));

        vec_str(&aarg, "A");

        mode_bind(mode, NULL, bind_f, &aarg, false);

        vec_clr(&aarg);
        vec_str(&aarg, "B");
        vec_str(&karg, "2");

        mode_bind(mode, &karg, bind_f, &aarg, false);
        vec_kill(&karg);
        vec_kill(&aarg);

        vec_str(&keys, "1");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "2");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "3");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "2");
        mode_keys(mode, &keys, NULL);

        asserteq(vec_len(&results), 4);
        asserteq_buf(vec_get(&results, 0), "ABAB", 4);

        vec_kill(&keys);
    }

    it ("Can unbind")
    {
        vec_s keys;
        vec_s karg, aarg;
        vec_init(&keys, sizeof(char));
        vec_init(&karg, sizeof(char));vec_init(&aarg, sizeof(char));

        vec_str(&karg, "1"); vec_str(&aarg, "A");

        mode_bind(mode, &karg, bind_f, &aarg, false);

        vec_clr(&karg); vec_clr(&aarg);
        vec_str(&karg, "2"); vec_str(&aarg, "B");

        mode_bind(mode, &karg, bind_f, &aarg, false);

        vec_kill(&aarg);
        vec_str(&keys, "1");
        mode_keys(mode, &keys, NULL);

        vec_clr(&karg); vec_str(&karg, "1");
        mode_unbind(mode, &karg);


        vec_str(&keys, "2");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "1");
        mode_keys(mode, &keys, NULL);
        vec_str(&keys, "2");
        mode_keys(mode, &keys, NULL);

        asserteq(vec_len(&results), 3);
        asserteq_buf(vec_get(&results, 0), "ABB", 3);

        vec_kill(&keys);
        vec_kill(&karg);
    }
#endif
}

describe(bufbind)
{
}


